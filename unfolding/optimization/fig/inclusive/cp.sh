#/bin/bash

for folder in `ls -d */`
do
    echo ${folder%%/}

    destination=${folder}
    if   [[ ${folder} =~ "mT_ZZ" ]]; then destination="mt_zz";
    elif [[ ${folder} =~ "Z_pT"  ]]; then destination="ptz";   fi

    bias_plot="/lustre/collider/zhuyifan/VBSZZ/unfolding/iteration_opt/run/inclusive/${destination}/plot/Bias_graph"
    cp ${bias_plot}/*.png ${folder}/
done
