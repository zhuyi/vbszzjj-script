#include "inc/MC.h"
#include "inc/Setting.h"
#include "inc/Util.h"

// inclusive

TCut pre_selection = "passPresel==1&&"
                     "met_tst>70&&(event_type==0||event_type==1)&&event_3CR==0&&M2Lep>80&&M2Lep<100&&leading_pT_lepton>30&&subleading_pT_lepton>20";
TCut truth_preselection = "truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20&&"
                          "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5&&"
//                          "truth_leading_jet_pt>30&&"
                          "truth_M2Lep>76&&truth_M2Lep<106&&"
                          "truth_event_type==1||truth_event_type==0";
TCut SR = "met_tst>110&&MetOHT>0.65&&abs(dLepR)<1.8&&abs(dMetZPhi)>2.2&&n_bjets==0";
TCut FR = "truth_met_tst>95&&truth_MetOHT_neu>0.65&&abs(truth_dLepR)<1.8&&abs(truth_dMetZPhi)>2.2";

double x_z_pt[]     = {50, 155, 230, 300, 400, 1500};
double x_mt_zz[]    = {200., 350., 450., 600., 3000.};
double x_n_jets[]   = {0, 1, 2, 3, 11};
double x_leading_pT_lepton[] = {30, 90, 110, 130, 160, 200, 900};
double x_dphill[]   = {0., 0.3, 0.6, 1., 1.4, 1.8};
double x_met_tst[]  = {130, 200, 250, 300, 375, 500, 2000};
double x_mjj[]      = {0, 200, 400, 8000};
double x_pt_zz[]    = {0, 30, 60, 100, 1500};
double x_z_rapidity[] = {0, 0.4, 1.2, 1.7, 2.2, 2.5};
double x_dmetzphi[] = {2.2, 2.4, 2.6, 2.8, 3.0, 3.2};
double x_cp_zz_1[]  = {-1., -0.6, -0.3, -0.1, 0.1, 0.3, 0.6, 1.};
double x_cp_zz_2[]  = {-1., -0.75, -0.5, -0.3, -0.1, 0.1, 0.3, 0.5, 0.75, 1.};
double x_leading_jet_pt[] = {};

/*
TCut pre_selection = "passPresel==1&&"
                     "met_tst>70&&(event_type==0||event_type==1)&&event_3CR==0&&M2Lep>80&&M2Lep<100&&leading_pT_lepton>30&&subleading_pT_lepton>20&&"
                     "leading_jet_pt>30&&second_jet_pt>30";
TCut truth_preselection = "truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20&&"
                          "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5&&"
                          "truth_leading_jet_pt>30&&truth_second_jet_pt>30&&"
                          "abs(truth_leading_jet_eta)<4.5&&abs(truth_second_jet_eta)<4.5&&"
                          "truth_M2Lep>76&&truth_M2Lep<106&&"
                          "(truth_event_type==1||truth_event_type==0)";
TCut SR = "met_tst>150&&dLepR<1.8&&dMetZPhi>2.2&&MetOHT>0.65&&n_bjets==0";
TCut FR = "truth_met_tst>130&&truth_dLepR<1.8&&abs(truth_dMetZPhi)>2.2&&truth_MetOHT_neu>0.65";

double x_z_pt[]     = {50, 155, 230, 300, 400, 1500};
double x_mt_zz[]    = {200., 330., 460., 600., 3000.};
double x_n_jets[]   = {0, 1, 2, 3, 11};
double x_leading_jet_pt[] = {30, 100, 300, 1500};
double x_dphill[]   = {0., 0.3, 0.6, 1., 1.8};
double x_dmetzphi[] = {2.2, 2.4, 2.6, 2.8, 3.0, 3.2};
double x_mjj[]      = {0, 400, 800, 8000};
double x_pt_zz[]    = {0, 50, 100, 150, 250, 1500};
double x_z_rapidity[] = {0, 0.4, 1.2, 1.7, 2.5};
double x_met_tst[]  = {130, 200, 250, 300, 375, 500, 2000};
double x_leading_pT_lepton[] = {};
double x_cp_zz_1[]  = {};
double x_cp_zz_2[]  = {};
*/
void set_palette()
{
    gStyle->SetOptStat(0);
    int my_palette[100];
    double stops[] = {0., 1.};
    double red[]   = {255./255, 1./255  };
    double green[] = {255./255, 88./255 };
    double blue[]  = {255./255, 209./255};
    int FI = TColor::CreateGradientColorTable(2, stops, red, green, blue, 100);
    for (int i = 0; i < 100; i++) my_palette[i] = FI + i;
    gStyle->SetPalette(100, my_palette);

    gStyle->SetPaintTextFormat(".2f");
}

double* get_binning(const TString &leaf, size_t &n_bins)
{
    if     (leaf == "Z_pT")       {n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);             return x_z_pt;}
    //else if(leaf == "dLepR")      {n_bins = sizeof(x_dlepr)/sizeof(x_dlepr[0]);           return x_dlepr;}
    else if(leaf == "met_tst")    {n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);       return x_met_tst;}
    else if(leaf == "dMetZPhi")   {n_bins = sizeof(x_dmetzphi)/sizeof(x_dmetzphi[0]);     return x_dmetzphi;}
    else if(leaf == "n_jets")     {n_bins = sizeof(x_n_jets)/sizeof(x_n_jets[0]);         return x_n_jets;}
    else if(leaf == "dphill")     {n_bins = sizeof(x_dphill)/sizeof(x_dphill[0]);         return x_dphill;}
    else if(leaf == "mjj")        {n_bins = sizeof(x_mjj)/sizeof(x_mjj[0]);               return x_mjj;}
    //else if(leaf == "isMC")       {n_bins = sizeof(x_ismc)/sizeof(x_ismc[0]);             return x_ismc;}
    else if(leaf == "leading_jet_pt")    {n_bins = sizeof(x_leading_jet_pt)/sizeof(x_leading_jet_pt[0]);       return x_leading_jet_pt;}
    else if(leaf == "leading_pT_lepton") {n_bins = sizeof(x_leading_pT_lepton)/sizeof(x_leading_pT_lepton[0]); return x_leading_pT_lepton;}
    else if(leaf == "pt_zz" || leaf == "pt_zz_pxy") {n_bins = sizeof(x_pt_zz)/sizeof(x_pt_zz[0]); return x_pt_zz;}
    else if(leaf == "Z_rapidity")                   {n_bins = sizeof(x_z_rapidity)/sizeof(x_z_rapidity[0]); return x_z_rapidity;}
    else if(leaf == "mT_ZZ" || leaf == "mT_ZZ_pxy") {n_bins = sizeof(x_mt_zz)/sizeof(x_mt_zz[0]); return x_mt_zz;}
    else if(leaf == "CP_ZZ_1" || leaf == "CP_ZZ_1") {n_bins = sizeof(x_cp_zz_1)/sizeof(x_cp_zz_1[0]); return x_cp_zz_1;}
    else if(leaf == "CP_ZZ_2" || leaf == "CP_ZZ_2") {n_bins = sizeof(x_cp_zz_2)/sizeof(x_cp_zz_2[0]); return x_cp_zz_2;}

    return nullptr;
}

double* generate_binning(const TString &leaf, size_t &n_bins, bool if_generate = true)
{
    if(!if_generate) return get_binning(leaf, n_bins);

    int bin_width = 10;

    int bin_min = 0;
    int bin_max = 0;
    int bin_med = 0;
    if     (leaf == "Z_pT")           {bin_min = 50; bin_max = 1500; bin_med = 400;}
    else if(leaf == "mT_ZZ")          {bin_min = 0;  bin_max = 3000; bin_med = 500;}
    else if(leaf == "n_jets")         {bin_min = 0;  bin_max = 11;   bin_med = 3; bin_width = 1;}
    else if(leaf == "leading_jet_pt") {bin_min = 0;  bin_max = 1500; bin_med = 200;}
    else if(leaf == "leading_pT_lepton") {bin_min = 0;  bin_max = 900; bin_med = 500;}
    n_bins = (bin_med - bin_min)/bin_width;

    double *binning = new double[n_bins + 2];
    for(int i = 0; i < n_bins + 1; i++)
        binning[i] = bin_min + i*bin_width;
    binning[n_bins + 1] = bin_max;

    n_bins += 2;
    return binning;
}

TChain* get_chain(TString channel)
{
    auto chain = new TChain("tree_PFLOW");

    MC mc;
    for(const auto &input : mc.mcs.at(channel))
        chain->Add(mc.mc_path_fiducial + "/" + input);

    return chain;
}

std::tuple<TString, TString> get_leaves_to_plot(const TString &recon_leaf, const TString &truth_leaf)
{
    auto recon_leaf_to_plot = recon_leaf;
    auto truth_leaf_to_plot = truth_leaf;
    if (recon_leaf == "dphill") recon_leaf_to_plot = "std::abs(dphill)";
    if (truth_leaf == "truth_dphill") truth_leaf_to_plot = "std::abs(truth_dphill)";
    if (recon_leaf == "dMetZPhi") recon_leaf_to_plot = "std::abs(dMetZPhi)";
    if (truth_leaf == "truth_dMetZPhi") truth_leaf_to_plot = "std::abs(truth_dMetZPhi)";
    if (recon_leaf == "Z_rapidity") recon_leaf_to_plot = "std::abs(Z_rapidity)";
    if (truth_leaf == "truth_Z_rapidity") truth_leaf_to_plot = "std::abs(truth_Z_rapidity)";
    if (recon_leaf == "mT_ZZ_pxy") recon_leaf_to_plot = "mT_ZZ";
    if (truth_leaf == "truth_mT_ZZ_pxy") truth_leaf_to_plot = "truth_mT_ZZ_pxy";
    if (recon_leaf == "pt_zz_pxy") recon_leaf_to_plot = "pt_zz";
    if (truth_leaf == "truth_pt_zz_pxy") truth_leaf_to_plot = "truth_pt_zz_pxy";
    return std::make_tuple(recon_leaf_to_plot, truth_leaf_to_plot);
}

void add_overflow(TH1* hist)
{
    const bool if_hist2d = dynamic_cast<TH2D*>(hist) != nullptr;
    auto n_bins = hist->GetNbinsX();

    for (int j = 1; j <= n_bins; j++)
    {
        auto last_bin = hist->GetBinContent(n_bins, j);
        auto over_bin = hist->GetBinContent(n_bins + 1, j);
        auto last_err = hist->GetBinError(n_bins, j);
        auto over_err = hist->GetBinError(n_bins + 1, j);
        hist->SetBinContent(n_bins, j, last_bin + over_bin);
        hist->SetBinError(n_bins, j, std::sqrt(last_err*last_err + over_err*over_err));
        hist->SetBinContent(n_bins + 1, j, 0.);
        hist->SetBinError(n_bins + 1, j, 0.);

        if (!if_hist2d) return;
    }

    for (int i = 1; i <= n_bins; i++)
    {
        auto last_bin = hist->GetBinContent(i, n_bins);
        auto over_bin = hist->GetBinContent(i, n_bins + 1);
        auto last_err = hist->GetBinError(i, n_bins);
        auto over_err = hist->GetBinError(i, n_bins + 1);
        hist->SetBinContent(i, n_bins, last_bin + over_bin);
        hist->SetBinError(i, n_bins, std::sqrt(last_err*last_err + over_err*over_err));
        hist->SetBinContent(i, n_bins + 1, 0.);
        hist->SetBinError(i, n_bins + 1, 0.);
    }

    auto last_bin = hist->GetBinContent(n_bins, n_bins);
    auto over_bin = hist->GetBinContent(n_bins + 1, n_bins + 1);
    auto last_err = hist->GetBinError(n_bins, n_bins);
    auto over_err = hist->GetBinError(n_bins + 1, n_bins + 1);
    hist->SetBinContent(n_bins, n_bins, last_bin + over_bin);
    hist->SetBinError(n_bins, n_bins, std::sqrt(last_err*last_err + over_err*over_err));
    hist->SetBinContent(n_bins + 1, n_bins + 1, 0.);
    hist->SetBinError(n_bins + 1, n_bins + 1, 0.);
}

TH1D* get_efficiency_numerator(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = false)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);
    auto [recon_leaf_to_plot, truth_leaf_to_plot] = get_leaves_to_plot(recon_leaf, truth_leaf);

    TH1D *efficiency_numerator = new TH1D("efficiency_numerator_" + recon_leaf, "", n_bins - 1, binning);
    auto name = efficiency_numerator->GetName();
    chain->Draw(recon_leaf_to_plot + ">>" + name,
                "ew_correction*tautau_correction*scale*weight"*(truth_preselection&&FR&&pre_selection&&SR));

    add_overflow(efficiency_numerator);

    std::cout << "eff num:\t" << efficiency_numerator->GetSumOfWeights() << std::endl;
    return efficiency_numerator;
}

TH1D* get_efficiency_denominator(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = false)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);
    auto [recon_leaf_to_plot, truth_leaf_to_plot] = get_leaves_to_plot(recon_leaf, truth_leaf);
    
    TH1D *efficiency_denominator = new TH1D("efficiency_denominator_" + recon_leaf, "", n_bins - 1, binning);
    auto name = efficiency_denominator->GetName();
    chain->Draw(truth_leaf_to_plot + ">>" + name,
                "ew_correction*tautau_correction*scale*weight_gen"*(truth_preselection&&FR));

    add_overflow(efficiency_denominator);

    std::cout << "eff den:\t" << efficiency_denominator->GetSumOfWeights() << std::endl;
    return efficiency_denominator;
}

TH1D* get_purity_numerator(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = false)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);
    auto [recon_leaf_to_plot, truth_leaf_to_plot] = get_leaves_to_plot(recon_leaf, truth_leaf);

    TH2D *matrix = new TH2D("matrix_" + recon_leaf, "", n_bins - 1, binning, n_bins - 1, binning);
    auto name = matrix->GetName();
    chain->Draw(recon_leaf_to_plot + ":" + truth_leaf_to_plot + ">>" + name,
                "ew_correction*tautau_correction*scale*weight"*(pre_selection&&SR));

    TH1D *purity_numerator = new TH1D("purity_numerator_" + recon_leaf, "", n_bins - 1, binning);
    for(int i = 1; i <= matrix->GetNbinsX() + 1; i++)
    {
        purity_numerator->SetBinContent(i, matrix->GetBinContent(i, i));
        purity_numerator->SetBinError(i, matrix->GetBinError(i, i));
    }

    add_overflow(purity_numerator);

    std::cout << "pur num:\t" << purity_numerator->GetSumOfWeights() << std::endl;
    return purity_numerator;
}

TH1D* get_purity_denominator(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = false)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);
    auto [recon_leaf_to_plot, truth_leaf_to_plot] = get_leaves_to_plot(recon_leaf, truth_leaf);
    
    TH1D *purity_denominator = new TH1D("purity_denominator_" + recon_leaf, "", n_bins - 1, binning);
    auto name = purity_denominator->GetName();
    chain->Draw(recon_leaf_to_plot + ">>" + name,
                "ew_correction*tautau_correction*scale*weight"*(pre_selection&&SR));

    add_overflow(purity_denominator);

    std::cout << "pur den:\t" << purity_denominator->GetSumOfWeights() << std::endl;
    return purity_denominator;
}

TH1D* get_truth(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = false)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);
    auto [recon_leaf_to_plot, truth_leaf_to_plot] = get_leaves_to_plot(recon_leaf, truth_leaf);
    
    TH1D *truth = new TH1D("truth_" + recon_leaf, "", n_bins - 1, binning);
    auto name = truth->GetName();
    chain->Draw(truth_leaf_to_plot + ">>" + name,
                "ew_correction*tautau_correction*scale*weight_gen"*(truth_preselection&&FR));

    add_overflow(truth);
    double max = truth->GetMaximum();
    truth->SetMaximum(1.25*max);

    std::cout << "truth: \t" << truth->GetSumOfWeights() << std::endl;
    return truth;
}

TH1D* get_recon(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = false)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);
    auto [recon_leaf_to_plot, truth_leaf_to_plot] = get_leaves_to_plot(recon_leaf, truth_leaf);
    
    TH1D *recon = new TH1D("recon_" + recon_leaf, "", n_bins - 1, binning);
    auto name = recon->GetName();
    chain->Draw(recon_leaf_to_plot + ">>" + name,
                "ew_correction*tautau_correction*scale*weight"*(pre_selection&&SR));

    add_overflow(recon);
    double max = recon->GetMaximum();
    recon->SetMaximum(1.25*max);

    std::cout << "recon: \t" << recon->GetSumOfWeights() << std::endl;
    return recon;
}

TH2D* get_migration(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = false)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);
    auto [recon_leaf_to_plot, truth_leaf_to_plot] = get_leaves_to_plot(recon_leaf, truth_leaf);

    TH2D *migration = new TH2D("migration_" + recon_leaf, "", n_bins - 1, binning, n_bins - 1, binning);
    auto name = migration->GetName();
    chain->Draw(truth_leaf_to_plot + ":" + recon_leaf_to_plot + ">>" + name,
                "ew_correction*tautau_correction*scale*weight"*(pre_selection&&SR&&truth_preselection&&FR));

    add_overflow(migration);

    for(int j = 1; j <= n_bins; j++)
    {
        double sum = 0.;
        for(int i = 1; i <= n_bins; i++)
            sum += migration->GetBinContent(i, j);

        for(int i = 1; i <= n_bins; i++)
        {
            migration->SetBinContent(i, j, migration->GetBinContent(i, j)/sum);
            migration->SetBinError(i, j, 0.);
        }
    }

    return migration;
}

TH1D *format_hist(TH1D* input, Color_t color, TString x_title)
{
    const int n_bins = input->GetNbinsX();
    auto binning = new double[n_bins + 1];
    for(int i = 0; i < n_bins + 1; i++) binning[i] = i;

    auto formatted = dynamic_cast<TH1D*>(input->Clone(TString::Format("%s_formatted", input->GetName())));
    formatted->SetBins(n_bins, binning);
    formatted->GetXaxis()->SetNdivisions(n_bins, 1, 1);
    for(int i = 0; i <= n_bins; i++)
        formatted->GetXaxis()->ChangeLabel(i + 1, -1, -1, -1, -1, -1, dtoa(input->GetBinLowEdge(i + 1)));

    formatted->SetMarkerStyle(kFullCircle);
    formatted->SetMarkerColor(color);
    formatted->SetMarkerSize(1);
    formatted->SetLineColor(color);
    formatted->SetLineWidth(2);
    formatted->SetMinimum(0.);
    formatted->GetXaxis()->SetTitle(x_title);
    //formatted->GetYaxis()->SetTitle("efficiency");

    return formatted;
}

TH2D *format_hist2d(TH2D* input, Color_t color, TString x_title)
{
    const int n_bins = input->GetNbinsX();
    auto binning = new double[n_bins + 1];
    for(int i = 0; i < n_bins + 1; i++) binning[i] = i;

    auto formatted = dynamic_cast<TH2D*>(input->Clone(TString::Format("%s_formatted", input->GetName())));
    formatted->SetBins(n_bins, binning, n_bins, binning);
    formatted->GetXaxis()->SetNdivisions(n_bins, 1, 1);
    formatted->GetYaxis()->SetNdivisions(n_bins, 1, 1);
    for(int i = 0; i <= n_bins; i++)
        formatted->GetXaxis()->ChangeLabel(i + 1, -1, -1, -1, -1, -1, dtoa(input->GetXaxis()->GetBinLowEdge(i + 1)));
    for(int j = 0; j <= n_bins; j++)
        formatted->GetYaxis()->ChangeLabel(j + 1, -1, -1, -1, -1, -1, dtoa(input->GetYaxis()->GetBinLowEdge(j + 1)));

    formatted->SetMarkerStyle(kFullCircle);
    formatted->SetMarkerColor(color);
    formatted->SetMarkerSize(1);
    formatted->SetLineColor(color);
    formatted->SetLineWidth(2);
    formatted->SetMinimum(0.);
    formatted->GetXaxis()->SetTitle(x_title);
    formatted->GetYaxis()->SetTitle("truth " + x_title);

    return formatted;
}

void check_all()
{
    set_palette();
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    std::vector<TString> recon_leaves = {//"Z_pT",
                                         //"mT_ZZ",
                                         //"n_jets",
                                         //"leading_pT_lepton",
                                         //"dphill",
                                         //"pt_zz",
                                         "Z_rapidity",
                                         //"CP_ZZ_1",
                                         //"CP_ZZ_2",

                                         //"mjj",
                                         //"mT_ZZ",
                                         //"leading_jet_pt",
                                         //"dphill",
                                         //"Z_rapidity",
                                         //"Z_pT",
                                         //"pt_zz",
                                        };

    auto c = new TCanvas("c", "", 800., 600.);

    TString measurement = "inclusive";
    auto chain = get_chain(measurement);
    for(const auto &recon_leaf : recon_leaves)
    {
        TString truth_leaf = "truth_" + recon_leaf;

        auto efficiency_numerator   = get_efficiency_numerator  (recon_leaf, truth_leaf, chain);
        auto efficiency_denominator = get_efficiency_denominator(recon_leaf, truth_leaf, chain);
        auto efficiency = dynamic_cast<TH1D*>(efficiency_numerator->Clone("efficiency_" + recon_leaf));
        efficiency->Divide(efficiency_denominator);
        auto efficiency_formatted = format_hist(efficiency, kBlack, title.at(recon_leaf));
        efficiency_formatted->SetMaximum(1.25);

        auto purity_numerator   = get_purity_numerator  (recon_leaf, truth_leaf, chain);
        auto purity_denominator = get_purity_denominator(recon_leaf, truth_leaf, chain);
        auto purity = dynamic_cast<TH1D*>(purity_numerator->Clone("purity_" + recon_leaf));
        purity->Divide(purity_denominator);
        auto purity_formatted = format_hist(purity, kRed + 1, title.at(recon_leaf));
        purity_formatted->SetMaximum(1.25);

        efficiency_formatted->Draw("P");
        purity_formatted->Draw("P same");

        auto legend = new TLegend(0.60, 0.81, 0.85, 0.88);
        legend->AddEntry(efficiency_formatted, "efficiency", "LP");
        legend->AddEntry(purity_formatted,     "purity",     "LP");
        legend->SetTextSize(0.03);
        legend->SetBorderSize(0);
        legend->Draw();

        auto title_in_legend = title.at(recon_leaf);
        title_in_legend.ReplaceAll(" [GeV]", "");
        auto text = new TPaveText(0.12, 0.81, 0.50, 0.89, "brNDC");
        text->SetFillColor(0);
        text->SetFillStyle(0);
        text->SetBorderSize(0);
        text->SetTextAlign(12);
        text->SetTextSize(0.03);
        text->AddText("#it{ATLAS} #bf{internal #sqrt{s}=13TeV 140fb^{-1}}");
        text->AddText("#bf{" + TString(measurement == "inclusive" ? "inclusive" : "ZZjj") + " region, " + title_in_legend + "}");
        dynamic_cast<TText*>(text->GetListOfLines()->First())->SetTextSize(0.04);
        text->Draw();

        TString name = "all_" + recon_leaf;
        c->SaveAs("fig/" + name + ".png");

        auto truth = get_truth(recon_leaf, truth_leaf, chain);
        auto truth_formatted = format_hist(truth, kBlack, "truth " + title.at(recon_leaf));
        truth_formatted->GetYaxis()->SetTitle("events/bin");
        truth_formatted->Draw("p");
        text->Draw();
        name = "truth_" + recon_leaf;
        c->SaveAs("fig/" + name + ".png");

        auto recon = get_recon(recon_leaf, truth_leaf, chain);
        auto recon_formatted = format_hist(recon, kBlack, title.at(recon_leaf));
        recon_formatted->GetYaxis()->SetTitle("events/bin");
        recon_formatted->Draw("p");
        text->Draw();
        name = "recon_" + recon_leaf;
        c->SaveAs("fig/" + name + ".png");

        auto migration = get_migration(recon_leaf, truth_leaf, chain);
        auto migration_formatted = format_hist2d(migration, kBlack, title.at(recon_leaf));
        migration_formatted->Draw("colz text");
        text->Draw();
        name = "migration_" + recon_leaf;
        c->SaveAs("fig/" + name + ".png");

        auto output = new TFile("root/" + recon_leaf + ".root", "update");
        output->cd();
        efficiency_formatted->Write("efficiency");
        purity_formatted->Write("purity");
        truth_formatted->Write("truth");
        recon_formatted->Write("recon");
        migration_formatted->Write("migration");
        output->Close();

        remove_keys(output->GetName(), {"efficiency", "purity", "truth", "recon", "migration"});
    }
}
