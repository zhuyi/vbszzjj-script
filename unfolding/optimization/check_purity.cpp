#include "inc/MC.h"
#include "inc/Setting.h"
#include "inc/Util.h"

//TCut pre_selection = "passPresel==1&&"
//                     "met_tst>70&&(event_type==0||event_type==1)&&event_3CR==0&&M2Lep>80&&M2Lep<100&&leading_pT_lepton>30&&subleading_pT_lepton>20";
//TCut truth_preselection = "truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20&&"
//                          "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5&&"
//                          "truth_M2Lep>76&&truth_M2Lep<106&&"
//                          "truth_event_type==1||truth_event_type==0";
//TCut SR = "met_tst>110&&MetOHT>0.65&&dLepR<1.8&&dMetZPhi>2.2&&n_bjets==0";
//TCut FR = "truth_met_tst>95&&truth_MetOHT_neu>0.65&&truth_dLepR<1.8&&abs(truth_dMetZPhi)>2.2";

TCut pre_selection = "passPresel==1&&"
                     "met_tst>70&&(event_type==0||event_type==1)&&event_3CR==0&&M2Lep>80&&M2Lep<100&&leading_pT_lepton>30&&subleading_pT_lepton>20&&"
                     "leading_jet_pt>30&&second_jet_pt>30";
TCut truth_preselection = "truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20&&"
                          "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5&&"
                          "truth_leading_jet_pt>30&&truth_second_jet_pt>30&&"
                          "abs(truth_leading_jet_eta)<4.5&&abs(truth_second_jet_eta)<4.5&&"
                          "truth_M2Lep>76&&truth_M2Lep<106&&"
                          "truth_event_type==1||truth_event_type==0";
TCut SR = "met_tst>150&&dLepR<1.8&&dMetZPhi>2.2&&MetOHT>0.65&&n_bjets==0";
TCut FR = "truth_met_tst>130&&truth_dLepR<1.8&&abs(truth_dMetZPhi)>2.2&&truth_MetOHT_neu>0.65";

double x_z_pt[]     = {50, 150, 200, 250, 350, 1500};
//double x_mt_zz[]    = {0, 250, 300, 400, 600, 3000};
double x_mt_zz[]    = {0, 550, 750, 3000};
double x_n_jets[]   = {0, 1, 2, 3, 11};
double x_leading_jet_pt[] = {30, 40, 60, 100, 1500};
double x_leading_pT_lepton[] = {30, 90, 110, 130, 150, 200, 900};
double x_dphill[]   = {0., 0.3, 0.6, 1., 1.8};
double x_mjj[] = {0, 200, 400, 8000};

double* get_binning(const TString &leaf, size_t &n_bins)
{
    if     (leaf == "Z_pT")       {n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);             return x_z_pt;    }
    //else if(leaf == "dLepR")      {n_bins = sizeof(x_dlepr)/sizeof(x_dlepr[0]);           return x_dlepr;   }
    //else if(leaf == "met_tst")    {n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);       return x_met_tst; }
    //else if(leaf == "dMetZPhi")   {n_bins = sizeof(x_dmetzphi)/sizeof(x_dmetzphi[0]);     return x_dmetzphi;}
    else if(leaf == "n_jets")     {n_bins = sizeof(x_n_jets)/sizeof(x_n_jets[0]);         return x_n_jets;  }
    else if(leaf == "dphill")     {n_bins = sizeof(x_dphill)/sizeof(x_dphill[0]);         return x_dphill;  }
    else if(leaf == "mT_ZZ")      {n_bins = sizeof(x_mt_zz)/sizeof(x_mt_zz[0]);           return x_mt_zz;   }
    else if(leaf == "mjj")        {n_bins = sizeof(x_mjj)/sizeof(x_mjj[0]);               return x_mjj;     }
    //else if(leaf == "isMC")       {n_bins = sizeof(x_ismc)/sizeof(x_ismc[0]);             return x_ismc;    }
    else if(leaf == "leading_jet_pt")    {n_bins = sizeof(x_leading_jet_pt)/sizeof(x_leading_jet_pt[0]);       return x_leading_jet_pt;}
    else if(leaf == "leading_pT_lepton") {n_bins = sizeof(x_leading_pT_lepton)/sizeof(x_leading_pT_lepton[0]); return x_leading_pT_lepton;}
    //else if(leaf == "pt_zz")      {n_bins = sizeof(x_pt_zz)/sizeof(x_pt_zz[0]);           return x_pt_zz;   }
    //else if(leaf == "Z_rapidity") {n_bins = sizeof(x_z_rapidity)/sizeof(x_z_rapidity[0]); return x_z_rapidity;   }
    return nullptr;
}

double* generate_binning(const TString &leaf, size_t &n_bins, bool if_generate = true)
{
    if(!if_generate) return get_binning(leaf, n_bins);

    int bin_width = 10;

    int bin_min = 0;
    int bin_max = 0;
    int bin_med = 0;
    if     (leaf == "Z_pT")           {bin_min = 50; bin_max = 1500; bin_med = 400;}
    else if(leaf == "mT_ZZ")          {bin_min = 0;  bin_max = 3000; bin_med = 500;}
    else if(leaf == "n_jets")         {bin_min = 0;  bin_max = 11;   bin_med = 3; bin_width = 1;}
    else if(leaf == "leading_jet_pt") {bin_min = 0;  bin_max = 1500; bin_med = 200;}
    else if(leaf == "leading_pT_lepton") {bin_min = 0;  bin_max = 900; bin_med = 500;}
    n_bins = (bin_med - bin_min)/bin_width;

    double *binning = new double[n_bins + 2];
    for(int i = 0; i < n_bins + 1; i++)
        binning[i] = bin_min + i*bin_width;
    binning[n_bins + 1] = bin_max;

    n_bins += 2;
    return binning;
}

TChain* get_chain()
{
    TString channel = "inclusive";
    auto chain = new TChain("tree_PFLOW");

    MC mc;
    for(const auto &input : mc.mcs.at(channel))
        chain->Add(mc.mc_path_fiducial + input);

    return chain;
}

TH1D* get_numerator(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = true)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);

    TH2D *matrix = new TH2D("matrix_" + recon_leaf, "", n_bins - 1, binning, n_bins - 1, binning);
    auto name = matrix->GetName();
    chain->Draw(recon_leaf + ":" + truth_leaf + ">>" + name,
                "ew_correction*tautau_correction*scale*weight"*(pre_selection&&SR));

    TH1D *numerator = new TH1D("numerator_" + recon_leaf, "", n_bins - 1, binning);
    for(int i = 1; i <= matrix->GetNbinsX(); i++)
    {
        numerator->SetBinContent(i, matrix->GetBinContent(i, i));
        numerator->SetBinError(i, matrix->GetBinError(i, i));
    }

    std::cout << numerator->GetSumOfWeights() << std::endl;
    return numerator;
}

TH1D* get_denominator(const TString &recon_leaf, const TString &truth_leaf, TChain* chain, bool if_generate = true)
{
    size_t n_bins;
    auto binning = generate_binning(recon_leaf, n_bins, if_generate);
    
    TH1D *denominator = new TH1D("denominator_" + recon_leaf, "", n_bins - 1, binning);
    auto name = denominator->GetName();
    chain->Draw(recon_leaf + ">>" + name,
                "ew_correction*tautau_correction*scale*weight"*(pre_selection&&SR));

    std::cout << denominator->GetSumOfWeights() << std::endl;
    return denominator;
}

TH1D *format_hist(TH1D* input)
{
    const int n_bins = input->GetNbinsX();
    auto binning = new double[n_bins + 1];
    for(int i = 0; i < n_bins + 1; i++) binning[i] = i;

    auto formatted = dynamic_cast<TH1D*>(input->Clone(TString::Format("%s_formatted", input->GetName())));
    formatted->SetBins(n_bins, binning);
    formatted->GetXaxis()->SetNdivisions(n_bins, 1, 1);
    for(int i = 0; i <= n_bins; i++)
        formatted->GetXaxis()->ChangeLabel(i + 1, -1, -1, -1, -1, -1, format_numeric_label(input->GetBinLowEdge(i + 1)));
    return formatted;
}

void check_purity()
{
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    std::vector<TString> recon_leaves = {"Z_pT",
                                         "mT_ZZ",
                                         "n_jets",
                                         //"leading_jet_pt",
                                         //"leading_pT_lepton",
                                         "dphill",
                                         "mjj",
                                        };

    auto c = new TCanvas("c", "", 800., 600.);

    auto chain = get_chain();
    for(const auto &recon_leaf : recon_leaves)
    {
        TString truth_leaf = "truth_" + recon_leaf;

        auto numerator   = get_numerator  (recon_leaf, truth_leaf, chain, false);
        auto denominator = get_denominator(recon_leaf, truth_leaf, chain, false);
        auto purity = dynamic_cast<TH1D*>(numerator->Clone("purity_" + recon_leaf));
        purity->Divide(denominator);
        auto formatted = format_hist(purity);
        formatted->Draw("P");
        formatted->GetXaxis()->SetTitle(title.at(recon_leaf));
        formatted->GetYaxis()->SetTitle("purity");
        formatted->SetMarkerStyle(kFullCircle);
        //formatted->SetMarkerColor(kBlack);
        formatted->SetMarkerSize(1);
        formatted->SetMaximum((formatted->GetMaximum() > 1.) ? 1.2 : 1.);
        formatted->SetMinimum(0.);

        TString name = "purity_" + recon_leaf;
        c->SaveAs("fig/" + name + ".png");

        auto output = new TFile("root/" + recon_leaf + ".root", "update");
        output->cd();
        formatted->Write("purity");
        output->Close();

        remove_keys(output->GetName(), {"efficiency", "purity"});
    }
}
