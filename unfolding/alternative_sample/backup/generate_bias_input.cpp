#include <filesystem>
#include <iostream>

#include "TString.h"
#include "TFile.h"
#include "TSystem.h"
#include "TKey.h"
#include "TH2D.h"
#include <ROOT/RDF/InterfaceUtils.hxx> // RNode
#include <ROOT/RDataFrame.hxx>

#include "inc/Util.h"
#include "inc/Setting.h"
#include "inc/MC.h"
#include "inc/reweight.h"

using RNode = ROOT::RDF::RNode;

const std::string path = "/lustre/collider/zhuyifan/VBSZZ/unfolding/unfolding-input-for-vbszz/EB/results/bias/vbs/";
const std::string abs_response_name = "response.root";
const std::string abs_background_name = "background.root";
const std::string abs_data_name = "data.root";
const std::string abs_script_name = "VBS_unfold.py";
const TString abs_migration_matrix_name = "migration_matrix";

std::string get_short_folder_name(const std::string &long_name)
{
    auto const pos = long_name.find_last_of('/');
    const auto short_name = long_name.substr(pos + 1);
    return short_name;
}

std::vector<std::string> get_folders()
{
    std::vector<std::string> folders;

    for (const auto & entry : std::filesystem::directory_iterator(path))
    {
        if (entry.is_directory())
            folders.push_back(get_short_folder_name(entry.path().string()));
    }

    return folders;
}

void copy_file(const std::string &folder, const std::string &abs_name)
{
    auto alternative_name = path + "/" + folder + "/alternative/" + abs_name;
    auto bias_name = path + "/" + folder + "/" + abs_name;
    std::filesystem::copy_file(alternative_name, bias_name, std::filesystem::copy_options::overwrite_existing);
}

RNode get_node(const TString &measurement, const TString &channel, const TString &syst)
{
    MC mcs;

    TString path_to_files;
    if(syst == "nominal") path_to_files = mcs.mc_path_fiducial;
    else if(syst.BeginsWith("weight")) path_to_files = mcs.mc_path_fiducial;
    else path_to_files = mcs.mc_path + "/" + syst + "/";

    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((path_to_files + "/" + file).Data());

    std::string pre_selection = settings.at(measurement).truth_preselection.GetTitle();
    std::string selection     = settings.at(measurement).FR.GetTitle();
    std::string truth_pre_selection = settings.at(measurement).pre_selection.GetTitle();
    std::string truth_selection     = settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    RNode node = data.Define("truth_abs_dphill", "abs(truth_dphill)")
                     .Define("truth_abs_Z_rapidity", "abs(truth_Z_rapidity)")
                     .Define("abs_dphill", "abs(dphill)")
                     .Define("abs_Z_rapidity", "abs(Z_rapidity)")
                     .Filter(truth_pre_selection).Filter(truth_selection)
                     .Filter(pre_selection).Filter(selection);

    return node;
}

TH2D* generate_reweighted_matrix(const TString &var, const TString &measurement, const TString &channel, const TString &syst, TH2D *matrix)
{
    if(syst.BeginsWith("alphas") || syst.BeginsWith("pdf") || syst.BeginsWith("scale"))
        return matrix;

    auto syst_weight = syst.BeginsWith("weight") ? syst : "weight_gen";
    auto var_to_plot = var;
    if(var == "mt_zz") var_to_plot = "mT_ZZ";
    else if(var == "dphill")     var_to_plot = "abs_dphill";
    else if(var == "Z_rapidity") var_to_plot = "abs_Z_rapidity";
    else if(var == "ptz")        var_to_plot = "Z_pT";
    auto truth_var_to_plot = "truth_" + var_to_plot;

    auto node = get_node(measurement, channel, syst);

    int n_binsx = matrix->GetNbinsX();
    int n_binsy = matrix->GetNbinsY();

    std::vector<double> binningx(n_binsx + 1);
    std::vector<double> binningy(n_binsy + 1);

    for(int i = 0; i <= n_binsx; i++) binningx.at(i) = matrix->GetXaxis()->GetBinLowEdge(i + 1);
    for(int j = 0; j <= n_binsy; j++) binningy.at(j) = matrix->GetYaxis()->GetBinLowEdge(j + 1);

    TString reweighted_matrix_name = var + "_" + matrix->GetName();
    auto reweighted_matrix = new TH2D(*node.Define("reweight", reweight_functions.at(measurement), {"second_jet_pt", "subleading_pT_lepton"})
                                           .Define("weight_final", ("scale*weight*ew_correction*reweight/weight_gen*" + syst_weight).Data())
                                           .Histo2D({reweighted_matrix_name, "", n_binsx, &binningx.at(0), n_binsy, &binningy.at(0)},
                                                    var_to_plot, truth_var_to_plot, "weight_final"));

    reweighted_matrix->GetXaxis()->SetTitle(matrix->GetXaxis()->GetTitle());
    reweighted_matrix->GetYaxis()->SetTitle(matrix->GetYaxis()->GetTitle());

    return reweighted_matrix;
}

int main(int argc, char *argv[])
{
    auto folders = get_folders();

    for(const auto &folder : folders)
    {
//        if(folder != "n_jets") continue;

        std::cout << folder << std::endl;

        copy_file(folder, abs_response_name);
        copy_file(folder, abs_background_name);
        copy_file(folder, abs_data_name);
        copy_file(folder, abs_script_name);

        auto bias_response_name = path + "/" + folder + "/" + abs_response_name;
        auto bias_response_file = new TFile(bias_response_name.data(), "update");

        for(auto k_obj : *bias_response_file->GetListOfKeys())
        {
            auto *key = dynamic_cast<TKey*>(k_obj);
            TString syst_name = key->GetName();

            std::cout << syst_name << std::endl;

            auto syst_dir = dynamic_cast<TDirectory*>(bias_response_file->Get(syst_name));
            syst_dir->cd();

            auto migration_matrix_name = syst_name + "/" + abs_migration_matrix_name;
            auto migration_matrix = dynamic_cast<TH2D*>(bias_response_file->Get(migration_matrix_name));

            auto reweighted_matrix = generate_reweighted_matrix(folder, "ZZjj", "alternative", syst_name, migration_matrix);
            reweighted_matrix->Write(abs_migration_matrix_name);
            remove_keys(syst_dir, {"fidcorrnum", "fidcorrden", "effcorrnum", "effcorrden", "puritynum", "migration_matrix"});

            bias_response_file->cd();
        }

        bias_response_file->Close();
    }
}
