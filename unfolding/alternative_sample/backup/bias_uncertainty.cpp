const TString path = "/lustre/collider/zhuyifan/VBSZZ/unfolding/VIPUnfolding/examples/EB/vbs/bias";

const std::map<TString, TString> vars = {
                                         //{"n_jets",         "n_jets"},
                                         {"leading_jet_pt", "leading_jet_pt"},
                                         //{"leading_pT_lepton", "leading_pT_lepton"},
                                         {"mt_zz",          "mT_ZZ"},
                                         {"mjj",            "mjj"},
                                         {"dphill",         "dphill"},
                                         {"ptz",            "Z_pT"},
                                         {"pt_zz",          "pt_zz"},
                                         {"Z_rapidity",     "Z_rapidity"}};

TString get_file_name(const TString &folder, const TString &var)
{
    return path + "/" + folder + "/Results_" + var + ".root";
}

void bias_uncertainty()
{
    for(const auto &[folder, var] : vars)
    {
        auto file = new TFile(get_file_name(folder, var), "READ");
        auto dir  = file->GetDirectory("Final");
        dir->cd();
        auto unfolded = dir->Get<TH1D>("Final_Nominal_Unfolded");

        file->cd();
        dir = file->GetDirectory("Nominal");
        dir->cd();
        auto fakedata = dir->Get<TH1D>("Nominal_MCTruth");

        TString latex_formatted = folder;

        const int n_bins = unfolded->GetNbinsX();
        for(int i = 1; i <= n_bins; i++)
        {
            //std::cout << (fakedata->GetBinContent(i) - unfolded->GetBinContent(i))/unfolded->GetBinContent(i) << "\t";
            latex_formatted += TString::Format("\t& %.3f", (fakedata->GetBinContent(i) - unfolded->GetBinContent(i))/unfolded->GetBinContent(i));
        }
        std::cout << latex_formatted << " \\\\" << std::endl;

        file->cd();
        file->Close();
    }
}
