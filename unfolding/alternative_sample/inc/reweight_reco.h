#ifndef REWEIGHT_H
#define REWEIGHT_H

#include <functional>
#include <map>

auto get_inclusive_reweight = [](const float &subleading_pT_lepton) ->double
{
    if (subleading_pT_lepton >= 0 && subleading_pT_lepton < 30) return 0.96351;
    else if (subleading_pT_lepton >= 30 && subleading_pT_lepton < 40) return 1.03118;
    else if (subleading_pT_lepton >= 40 && subleading_pT_lepton < 50) return 1.03415;
    else if (subleading_pT_lepton >= 50 && subleading_pT_lepton < 60) return 0.97947;
    else if (subleading_pT_lepton >= 60 && subleading_pT_lepton < 70) return 1.00284;
    else if (subleading_pT_lepton >= 70 && subleading_pT_lepton < 80) return 0.96609;
    else if (subleading_pT_lepton >= 80 && subleading_pT_lepton < 90) return 0.98739;
    else if (subleading_pT_lepton >= 90 && subleading_pT_lepton < 100) return 0.97765;
    else if (subleading_pT_lepton >= 100 && subleading_pT_lepton < 110) return 1.12147;
    else if (subleading_pT_lepton >= 110 && subleading_pT_lepton < 120) return 0.79615;
    else if (subleading_pT_lepton >= 120 && subleading_pT_lepton < 140) return 0.96761;
    else if (subleading_pT_lepton >= 140 && subleading_pT_lepton < 160) return 1.12714;
    else if (subleading_pT_lepton >= 160 && subleading_pT_lepton < 200) return 0.82616;
    else if (subleading_pT_lepton >= 200 && subleading_pT_lepton < 1000) return 0.89015;

//    if (subleading_pT_lepton >= 0 && subleading_pT_lepton < 30) return 0.96386;
//    else if (subleading_pT_lepton >= 30 && subleading_pT_lepton < 40) return 1.03221;
//    else if (subleading_pT_lepton >= 40 && subleading_pT_lepton < 50) return 1.03629;
//    else if (subleading_pT_lepton >= 50 && subleading_pT_lepton < 60) return 0.98215;
//    else if (subleading_pT_lepton >= 60 && subleading_pT_lepton < 70) return 1.00525;
//    else if (subleading_pT_lepton >= 70 && subleading_pT_lepton < 80) return 0.96628;
//    else if (subleading_pT_lepton >= 80 && subleading_pT_lepton < 90) return 0.98780;
//    else if (subleading_pT_lepton >= 90 && subleading_pT_lepton < 100) return 0.97626;
//    else if (subleading_pT_lepton >= 100 && subleading_pT_lepton < 110) return 1.12218;
//    else if (subleading_pT_lepton >= 110 && subleading_pT_lepton < 120) return 0.79585;
//    else if (subleading_pT_lepton >= 120 && subleading_pT_lepton < 140) return 0.96673;
//    else if (subleading_pT_lepton >= 140 && subleading_pT_lepton < 160) return 1.12529;
//    else if (subleading_pT_lepton >= 160 && subleading_pT_lepton < 200) return 0.82613;
//    else if (subleading_pT_lepton >= 200 && subleading_pT_lepton < 1000) return 0.89008;

    return 0.;
};

auto get_ZZjj_reweight = [](const float &subleading_pT_lepton) ->double
{
    if (subleading_pT_lepton >= 0 && subleading_pT_lepton < 30) return 1.05052;
    else if (subleading_pT_lepton >= 30 && subleading_pT_lepton < 40) return 1.07665;
    else if (subleading_pT_lepton >= 40 && subleading_pT_lepton < 50) return 1.14991;
    else if (subleading_pT_lepton >= 50 && subleading_pT_lepton < 60) return 1.17506;
    else if (subleading_pT_lepton >= 60 && subleading_pT_lepton < 70) return 1.08589;
    else if (subleading_pT_lepton >= 70 && subleading_pT_lepton < 80) return 1.06996;
    else if (subleading_pT_lepton >= 80 && subleading_pT_lepton < 90) return 1.35335;
    else if (subleading_pT_lepton >= 90 && subleading_pT_lepton < 100) return 1.35534;
    else if (subleading_pT_lepton >= 100 && subleading_pT_lepton < 110) return 1.00522;
    else if (subleading_pT_lepton >= 110 && subleading_pT_lepton < 120) return 0.99179;
    else if (subleading_pT_lepton >= 120 && subleading_pT_lepton < 140) return 1.22248;
    else if (subleading_pT_lepton >= 140 && subleading_pT_lepton < 160) return 0.85402;
    else if (subleading_pT_lepton >= 160 && subleading_pT_lepton < 200) return 0.87670;
    else if (subleading_pT_lepton >= 200 && subleading_pT_lepton < 1000) return 0.71052;

//    if (subleading_pT_lepton >= 0 && subleading_pT_lepton < 30) return 1.02746;
//    else if (subleading_pT_lepton >= 30 && subleading_pT_lepton < 40) return 1.09272;
//    else if (subleading_pT_lepton >= 40 && subleading_pT_lepton < 50) return 1.18464;
//    else if (subleading_pT_lepton >= 50 && subleading_pT_lepton < 60) return 1.18500;
//    else if (subleading_pT_lepton >= 60 && subleading_pT_lepton < 70) return 1.10789;
//    else if (subleading_pT_lepton >= 70 && subleading_pT_lepton < 80) return 1.08773;
//    else if (subleading_pT_lepton >= 80 && subleading_pT_lepton < 90) return 1.35755;
//    else if (subleading_pT_lepton >= 90 && subleading_pT_lepton < 100) return 1.36739;
//    else if (subleading_pT_lepton >= 100 && subleading_pT_lepton < 110) return 1.01437;
//    else if (subleading_pT_lepton >= 110 && subleading_pT_lepton < 120) return 0.98908;
//    else if (subleading_pT_lepton >= 120 && subleading_pT_lepton < 140) return 1.21898;
//    else if (subleading_pT_lepton >= 140 && subleading_pT_lepton < 160) return 0.84018;
//    else if (subleading_pT_lepton >= 160 && subleading_pT_lepton < 200) return 0.87658;
//    else if (subleading_pT_lepton >= 200 && subleading_pT_lepton < 1000) return 0.70995;

//aQGC1
//    if (subleading_pT_lepton >= 0 && subleading_pT_lepton < 30) return 0.97328;
//    else if (subleading_pT_lepton >= 30 && subleading_pT_lepton < 40) return 0.97848;
//    else if (subleading_pT_lepton >= 40 && subleading_pT_lepton < 50) return 0.98215;
//    else if (subleading_pT_lepton >= 50 && subleading_pT_lepton < 60) return 0.98042;
//    else if (subleading_pT_lepton >= 60 && subleading_pT_lepton < 70) return 0.97223;
//    else if (subleading_pT_lepton >= 70 && subleading_pT_lepton < 80) return 0.94925;
//    else if (subleading_pT_lepton >= 80 && subleading_pT_lepton < 90) return 0.94646;
//    else if (subleading_pT_lepton >= 90 && subleading_pT_lepton < 100) return 0.92390;
//    else if (subleading_pT_lepton >= 100 && subleading_pT_lepton < 110) return 0.89263;
//    else if (subleading_pT_lepton >= 110 && subleading_pT_lepton < 120) return 0.85279;
//    else if (subleading_pT_lepton >= 120 && subleading_pT_lepton < 140) return 0.78463;
//    else if (subleading_pT_lepton >= 140 && subleading_pT_lepton < 160) return 0.71128;
//    else if (subleading_pT_lepton >= 160 && subleading_pT_lepton < 200) return 0.61755;
//    else if (subleading_pT_lepton >= 200 && subleading_pT_lepton < 1000) return 0.25351;

//aQGC2
//    if (subleading_pT_lepton >= 0 && subleading_pT_lepton < 30) return 0.99272;
//    else if (subleading_pT_lepton >= 30 && subleading_pT_lepton < 40) return 0.99416;
//    else if (subleading_pT_lepton >= 40 && subleading_pT_lepton < 50) return 0.99551;
//    else if (subleading_pT_lepton >= 50 && subleading_pT_lepton < 60) return 0.99474;
//    else if (subleading_pT_lepton >= 60 && subleading_pT_lepton < 70) return 0.99221;
//    else if (subleading_pT_lepton >= 70 && subleading_pT_lepton < 80) return 0.98765;
//    else if (subleading_pT_lepton >= 80 && subleading_pT_lepton < 90) return 0.98625;
//    else if (subleading_pT_lepton >= 90 && subleading_pT_lepton < 100) return 0.98132;
//    else if (subleading_pT_lepton >= 100 && subleading_pT_lepton < 110) return 0.97065;
//    else if (subleading_pT_lepton >= 110 && subleading_pT_lepton < 120) return 0.95467;
//    else if (subleading_pT_lepton >= 120 && subleading_pT_lepton < 140) return 0.93360;
//    else if (subleading_pT_lepton >= 140 && subleading_pT_lepton < 160) return 0.90849;
//    else if (subleading_pT_lepton >= 160 && subleading_pT_lepton < 200) return 0.86762;
//    else if (subleading_pT_lepton >= 200 && subleading_pT_lepton < 1000) return 0.57819;

    return 0.;
};

std::map<TString, std::function<double(const float&)>> reweight_functions = {{"inclusive", get_inclusive_reweight},
                                                                             {"ZZjj",      get_ZZjj_reweight}};

#endif // REWEIGHT_H
