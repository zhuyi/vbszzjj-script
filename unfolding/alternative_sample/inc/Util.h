#ifndef UTIL_H
#define UTIL_H

#include <map>
#include <string>
#include <set>
#include <stdio.h>

#include "TDirectory.h"
#include "TKey.h"
#include "TFile.h"
#include "TString.h"

void remove_keys(TDirectory* dir, const std::set<std::string> &keys_to_save)
{
    std::map<std::string, int> count;

    for(auto k_obj : *dir->GetListOfKeys())
    {
        auto *key = dynamic_cast<TKey*>(k_obj);
        std::string name = key->GetName();
        int cycle = key->GetCycle();

        if (keys_to_save.find(name) == keys_to_save.end())
            continue;

        if (count.find(name) == count.end()) count[name] = cycle;
        else if (count[name] < cycle)        count[name] = cycle;
    }

    for(auto k_obj : *dir->GetListOfKeys()) 
    {
        auto *key = dynamic_cast<TKey*>(k_obj);

        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (!(count.find(name) != count.end() && count[name] == cycle))
        {
//            printf("%10s  %20s  %2d   delete\n", key->GetClassName(), name.c_str(), cycle);
            gDirectory->Delete((name + ";" + std::to_string(cycle)).c_str());
        }
//        else
//        {
//            printf("%10s  %20s  %2d   keep  \n", key->GetClassName(), name.c_str(), cycle);
//        }
    }
}

void remove_keys(TString file_name, const std::set<std::string> &keys_to_save)
{
    auto file = new TFile(file_name, "update");

    remove_keys(file, keys_to_save);

    file->Close();
}

TString dtoa(const double &label)
{
    TString formatted(TString::Format("%f", label));

    size_t n = formatted.Length();
    for(size_t i = 0; i < n; i++)
    {
        if     (formatted.EndsWith("0")) formatted.Remove(formatted.Length() - 1);
        else if(formatted.EndsWith(".")) { formatted.Remove(formatted.Length() - 1); break; }
    }

    return formatted;
}

#endif // UTIL_H
