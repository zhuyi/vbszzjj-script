const TString nominal_path = "/lustre/collider/zhuyifan/VBSZZ/unfolding/VIPUnfolding/examples/EB/vbs";
const TString bias_path = nominal_path + "/bias/";

const std::map<TString, TString> vars = {
                                         //{"n_jets",         "n_jets"},
                                         {"leading_jet_pt", "leading_jet_pt"},
                                         //{"leading_pT_lepton", "leading_pT_lepton"},
                                         {"mt_zz",          "mT_ZZ"},
                                         {"mjj",            "mjj"},
                                         {"dphill",         "dphill"},
                                         {"ptz",            "Z_pT"},
                                         {"pt_zz",          "pt_zz"},
                                         {"Z_rapidity",     "Z_rapidity"},
                                         //{"CP_ZZ_1",          "CP_ZZ_1"},
                                         //{"CP_ZZ_2",          "CP_ZZ_2"},
                                        };

TString get_file_name(const TString &path, const TString &folder, const TString &var)
{
    return path + "/" + folder + "/Results_" + var + ".root";
}

TH1D* get_unfolded(const TString &path, const TString &folder, const TString &var)
{
    auto file = new TFile(get_file_name(path, folder, var), "READ");
    if(!file->IsOpen())
    {
        std::cout << "[WARNING] ==> No file " << get_file_name(path, folder, var) << std::endl;
        return nullptr;
    }

    auto dir = file->GetDirectory("Final");
    dir->cd();
    auto unfolded = dir->Get<TH1D>("Final_Nominal_Unfolded");

    return unfolded;
}

void bias_uncertainty()
{
    for(const auto &[folder, var] : vars)
    {
        auto bias_unfolded    = get_unfolded(bias_path, folder, var);
        auto nominal_unfolded = get_unfolded(nominal_path, folder, var);

        if(!bias_unfolded || !nominal_unfolded) continue;

        TString latex_formatted = folder;

        const int n_bins = nominal_unfolded->GetNbinsX();
        for(int i = 1; i <= n_bins; i++)
        {
            //std::cout << (fakedata->GetBinContent(i) - unfolded->GetBinContent(i))/unfolded->GetBinContent(i) << "\t";
            latex_formatted += TString::Format("\t& %.3f", (bias_unfolded->GetBinContent(i) - nominal_unfolded->GetBinContent(i))/nominal_unfolded->GetBinContent(i));
        }
        std::cout << latex_formatted << " \\\\" << std::endl;
    }
}
