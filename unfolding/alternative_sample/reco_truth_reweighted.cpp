#include "inc/Util.h"
#include "inc/Setting.h"
#include "inc/MC.h"
#include "inc/reweight_reco.h"

using RNode = ROOT::RDF::RNode;

namespace setting
{
    enum level {reco, truth, both};
    enum measurement {inclusive, ZZjj};
}

RNode get_node(const TString &measurement, const TString &channel, setting::level level)
{
    MC mcs;

    TString path_to_files = mcs.mc_path_fiducial;

    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((path_to_files + "/" + file).Data());

    std::string pre_selection("true");
    std::string selection("true");
    if(level == setting::reco)
    {
        pre_selection = settings.at(measurement).truth_preselection.GetTitle();
        selection     = settings.at(measurement).FR.GetTitle();
    }
    else if(level == setting::truth)
    {
        pre_selection = settings.at(measurement).pre_selection.GetTitle();
        selection     = settings.at(measurement).SR.GetTitle();
    }
    else if(level == setting::both)
    {
        pre_selection = (settings.at(measurement).pre_selection && settings.at(measurement).truth_preselection).GetTitle();
        selection     = (settings.at(measurement).FR && settings.at(measurement).SR).GetTitle();
    }

    ROOT::RDataFrame data("tree_PFLOW", files);
    RNode node = data.Define("truth_abs_dphill", "abs(truth_dphill)")
                     .Define("truth_abs_Z_rapidity", "abs(truth_Z_rapidity)")
                     .Define("abs_dphill", "abs(dphill)")
                     .Define("abs_Z_rapidity", "abs(Z_rapidity)")
                     .Filter(pre_selection).Filter(selection);

    return node;
}

TH1D* get_hist(RNode &node, const TString &var, const TString &measurement, setting::level level, bool if_reweight)
{
    auto var_to_plot = var;
    if(var == "dphill") var_to_plot = "abs_dphill";
    else if(var == "Z_rapidity") var_to_plot = "abs_Z_rapidity";
    auto truth_var_to_plot = "truth_" + var_to_plot;

    int n_bins = settings.at(measurement).get_binning(var).size();
    auto xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(var).at(i);

    TString reweighted_hist_name = var;
    if(level == setting::truth) reweighted_hist_name += "_truth"; else reweighted_hist_name += "_reco";
    if(if_reweight) reweighted_hist_name += "_reweighted";

    TString weight_final = "scale*ew_correction*tautau_correction";
    if(if_reweight) weight_final += "*reweight";
    if(level == setting::truth) weight_final += "*weight_gen"; else weight_final += "*weight";
    auto hist = new TH1D(*node.Define("reweight", reweight_functions.at(measurement), {"subleading_pT_lepton"})
                              .Define("weight_final", weight_final.Data())
                              .Histo1D({reweighted_hist_name, "", n_bins - 1, xbins},
                                       (level == setting::truth ? truth_var_to_plot.Data() : var_to_plot.Data()), "weight_final"));

    delete [] xbins;

    return hist;
}

TH1D *get_formatted(TH1D* unfolded, const TString &x_title, const TString &y_title, const Color_t &color, const Style_t &style = kDot)
{
    const int n_bins = unfolded->GetNbinsX();
    auto binning = new double[n_bins + 1];
    for(int i = 0; i < n_bins + 1; i++) binning[i] = i;

    auto formatted = dynamic_cast<TH1D*>(unfolded->Clone(TString::Format("%s_formatted", unfolded->GetName())));
    formatted->SetBins(n_bins, binning);
    formatted->GetXaxis()->SetNdivisions(n_bins, 1, 1);
    for(int i = 0; i <= n_bins; i++)
        formatted->GetXaxis()->ChangeLabel(i + 1, -1, -1, -1, -1, -1, dtoa(unfolded->GetBinLowEdge(i + 1)));

    if(style != kDot)
    {
        formatted->SetMarkerStyle(style);
        formatted->SetMarkerColor(color);
        formatted->SetMarkerSize(1.5);
    }
    formatted->SetLineColor(color);
    formatted->SetLineWidth(2);
    formatted->SetTitle("");
    formatted->GetXaxis()->SetTitle(x_title);
    formatted->GetYaxis()->SetTitle(y_title);

    return formatted;
}

void PlotLegendPaveText(TH1D* recon_nom_unw, TH1D* recon_alt_unw, TH1D* recon_alt_rew,
                        std::vector<TString> texts,
                        int if_left = false)
{
    TLegend *legend(nullptr);
    if(if_left) legend = new TLegend(0.21, 0.66, 0.50, 0.75);
    else        legend = new TLegend(0.61, 0.66, 0.90, 0.75);
    legend->SetFillStyle(0);
    legend->SetBorderSize(0);
    legend->AddEntry(recon_nom_unw, "nominal",     "L");
    legend->AddEntry(recon_alt_unw, "alternative", "LP");
    legend->AddEntry(recon_alt_rew, "alternative reweighted", "L");

    TPaveText* textatlas(nullptr);
    if(if_left)
        textatlas = new TPaveText(0.20, 0.75, 0.60, 0.85, "brNDC");
    else
        textatlas = new TPaveText(0.60, 0.75, 0.90, 0.85, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(12);
    textatlas->SetTextSize(0.03);
    for(auto text : texts) textatlas->AddText(text);
//    dynamic_cast<TText*>(textatlas->GetListOfLines()->First())->SetTextSize(0.04);

    legend->Draw("same");
    textatlas->Draw("same");
}


void reco_truth_reweighted()
{
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    auto c = new TCanvas("c", "", 800., 600.);

    std::vector<TString> vars = {
                                 //"subleading_pT_lepton",
                                 //"n_jets",
                                 //"dphill",
                                 //"leading_jet_pt",
                                 //"leading_pT_lepton",
                                 //"mjj",
                                 //"mT_ZZ",
                                 //"pt_zz",
                                 //"Z_pT",
                                 //"Z_rapidity",
                                 "CP_ZZ_1"
                                };

    TString measurement = "inclusive";
    TString channel = "alternative";
    bool if_reweight = true;

    auto node_recon_nom = get_node(measurement, "inclusive", setting::reco);
    auto node_recon_alt = get_node(measurement, channel,     setting::reco);
    auto node_truth_nom = get_node(measurement, "inclusive", setting::truth);
    auto node_truth_alt = get_node(measurement, channel,     setting::truth);

    for(const auto &var : vars)
    {
        TString x_title = settings.at(measurement).titles.at(var);

        setting::level level = setting::reco;
        auto recon_nom_unw = get_formatted(get_hist(node_recon_nom, var, measurement, level, false),       x_title, "no. of events", kBlack);
        auto recon_alt_unw = get_formatted(get_hist(node_recon_alt, var, measurement, level, false),       x_title, "no. of events", kBlue + 1, kFullDiamond);
        auto recon_alt_rew = get_formatted(get_hist(node_recon_alt, var, measurement, level, if_reweight), x_title, "no. of events", kRed + 1);

        recon_nom_unw->Print("range");
        recon_alt_unw->Print("range");
        recon_alt_rew->Print("range");

        level = setting::truth;
        auto truth_nom_unw = get_formatted(get_hist(node_truth_nom, var, measurement, level, false),       "truth " + x_title, "no. of events", kBlack);
        auto truth_alt_unw = get_formatted(get_hist(node_truth_alt, var, measurement, level, false),       "truth " + x_title, "no. of events", kBlue + 1, kFullDiamond);
        auto truth_alt_rew = get_formatted(get_hist(node_truth_alt, var, measurement, level, if_reweight), "truth " + x_title, "no. of events", kRed + 1);

        truth_nom_unw->Print("range");
        truth_alt_unw->Print("range");
        truth_alt_rew->Print("range");

        recon_nom_unw->SetMinimum(0.);
        recon_nom_unw->SetMaximum(1.2*recon_nom_unw->GetMaximum());
        recon_nom_unw->Draw("hist");
        recon_alt_unw->Draw("E same");
        recon_alt_rew->Draw("E same");

        PlotLegendPaveText(recon_nom_unw, recon_alt_unw, recon_alt_rew, {"#it{ATLAS} #bf{internal}", "#bf{" + measurement + " region}"});

        TString name = var + "_recon_" + measurement + "_rew";
        c->SaveAs("fig/" + name + ".png");

        truth_nom_unw->SetMinimum(0.);
        truth_nom_unw->SetMaximum(1.2*truth_nom_unw->GetMaximum());
        truth_nom_unw->Draw("hist");
        truth_alt_unw->Draw("E same");
        truth_alt_rew->Draw("E same");

        PlotLegendPaveText(truth_nom_unw, truth_alt_unw, truth_alt_rew, {"#it{ATLAS} #bf{internal}", "#bf{" + measurement + " region}"});

        name = var + "_truth_" + measurement + "_rew";
        c->SaveAs("fig/" + name + ".png");
    }
}
