#include "inc/Setting.h"

TString path = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/PFLOW/";
TString nominal = "345666*.root";
TString alter   = "361604*.root";

using RNode = ROOT::RDF::RNode;

RNode get_node(const TString &file, const TString &measurement, bool if_truth = false)
{
    std::string pre_selection = if_truth ? settings.at(measurement).truth_preselection.GetTitle()
                                         : settings.at(measurement).pre_selection.GetTitle();
    std::string selection     = if_truth ? settings.at(measurement).FR.GetTitle()
                                         : settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", (path + "/" + file).Data());
    RNode node = data.Define("weightew", "scale*weight*ew_correction")
                     .Define("abs_dphill", "abs(dphill)")
                     .Define("abs_Z_rapidity", "abs(Z_rapidity)")
                     .Filter(pre_selection).Filter(selection);

    return node;
}

TH1D* get_hist(RNode &node, const TString &measurement, const TString &leaf)
{
    int n_bins = settings.at(measurement).get_binning(leaf).size();
    double *xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(leaf).at(i);

    TString leaf_to_draw(leaf);
    if     (leaf == "dphill")     leaf_to_draw = "abs_dphill";
    else if(leaf == "Z_rapidity") leaf_to_draw = "abs_Z_rapidity";
    auto hist = node.Histo1D({"hist", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "weightew");
    return dynamic_cast<TH1D*>(hist->Clone());
}

void compare_kinetics()
{
    std::vector<TString> leaves = {"n_jets",
                                   "dphill",
                                   "leading_jet_pt",
                                   "leading_pT_lepton",
                                   "mT_ZZ",
                                   "Z_pT",
                                   "pt_zz",
                                   "Z_rapidity"};

    TH1::SetDefaultSumw2();
    gStyle->SetOptStat(0);

    auto c = new TCanvas("c", "", 800., 600.);

    auto nominal_node = get_node(nominal, "inclusive", true);
    auto alter_node   = get_node(alter, "inclusive", true);

    for(const auto &leaf : leaves)
    {
        std::cout << leaf << std::endl;

        auto nominal_hist = get_hist(nominal_node, "inclusive", leaf);
        auto alter_hist   = get_hist(alter_node,   "inclusive", leaf);

        double minimum = nominal_hist->GetMinimum() < alter_hist->GetMinimum() ?
                         nominal_hist->GetMinimum() : alter_hist->GetMinimum();
        double maximum = nominal_hist->GetMaximum() > alter_hist->GetMaximum() ?
                         nominal_hist->GetMaximum() : alter_hist->GetMaximum();
        alter_hist->SetMinimum(0.8*minimum);
        alter_hist->SetMaximum(1.1*maximum);

        nominal_hist->SetLineWidth(2);
        nominal_hist->SetLineColor(kGreen + 2);
        alter_hist  ->SetLineWidth(2);
        alter_hist  ->SetLineColor(kBlack);
        alter_hist  ->SetMarkerSize(1);
        alter_hist  ->SetMarkerStyle(kFullCircle);
        alter_hist  ->SetMarkerColor(kBlack);

        auto rp = new TRatioPlot(alter_hist, nominal_hist);
        rp->SetH1DrawOpt("E");
        rp->SetH2DrawOpt("hist");
        rp->Draw();

        rp->SetLeftMargin(0.12);
        rp->SetLowBottomMargin(0.3);

        rp->GetUpperRefXaxis()->SetTitle("truth " + settings.at("inclusive").titles.at(leaf));
        rp->GetUpperRefYaxis()->SetTitle("No. of events");
        rp->GetLowerRefYaxis()->SetTitle("alternative/nominal");
        rp->GetLowerRefYaxis()->SetRangeUser(0.5, 1.5);
        rp->GetLowerRefYaxis()->SetNdivisions(5, 2, 0);

        rp->GetLowerRefGraph()->SetLineColor(kBlack);
        rp->GetLowerRefGraph()->SetMarkerStyle(21);
        rp->GetLowerRefGraph()->SetMarkerColor(kBlack);
        rp->GetLowerRefGraph()->SetMarkerSize(0.75);

        rp->GetUpperPad()->cd();
        TLegend *legend = new TLegend(0.70, 0.59, 0.90, 0.69);
        legend->AddEntry(nominal_hist, "nominal",     "L");
        legend->AddEntry(alter_hist,   "alternative", "LP");
        legend->SetTextSize(0.04);
        legend->SetBorderSize(0);
        legend->Draw();

        TString name = "compare_truth_" + leaf;
        c->SaveAs("fig/" + name + ".png");

        c->cd();
    }
}
