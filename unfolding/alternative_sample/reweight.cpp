#include "inc/Setting.h"
#include "inc/MC.h"
#include "inc/Util.h"

double binning1[] = { 30,  45,  60,  75,  90, 105, 120, 135, 150, 165, 180, 215, 250, 500, };
const int n_bins1 = sizeof(binning1)/sizeof(binning1[0]);

double binning2[] = {0,  30,  40,  50,  60,  70,  80,  90, 100, 110, 120, 140, 160, 200, 1000};
const int n_bins2 = sizeof(binning2)/sizeof(binning2[0]);

using RNode = ROOT::RDF::RNode;

RNode get_init_node(const TString &measurement, const TString &channel, bool if_truth = false)
{
    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path_fiducial + file).Data());

    std::string pre_selection = if_truth ? settings.at(measurement).truth_preselection.GetTitle()
                                         : settings.at(measurement).pre_selection.GetTitle();
    std::string selection     = if_truth ? settings.at(measurement).FR.GetTitle()
                                         : settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Define("weight_final", "scale*ew_correction*tautau_correction*weight").Filter(pre_selection).Filter(selection);
}

void reweight()
{
    TH1::SetDefaultSumw2();
    gStyle->SetOptStat(0);

    TString measurement = "inclusive";

    auto nominal_node = get_init_node(measurement, "inclusive", true);
    auto alternative_node = get_init_node(measurement, "alternative", true);

//    auto nominal_hist = new TH2D(*nominal_node.Histo2D({"inclusive", "", n_bins1 - 1, binning1, n_bins2 - 1, binning2},
//                                                       "truth_second_jet_pt", "truth_subleading_pT_lepton", "weight_final"));
//    auto alternative_hist = new TH2D(*alternative_node.Histo2D({"alternative", "", n_bins1 - 1, binning1, n_bins2 - 1, binning2},
//                                                               "truth_second_jet_pt", "truth_subleading_pT_lepton", "weight_final"));

    auto nominal_hist = new TH1D(*nominal_node.Histo1D({"inclusive", "", n_bins2 - 1, binning2},
                                                       "subleading_pT_lepton", "weight_final"));
    auto alternative_hist = new TH1D(*alternative_node.Histo1D({"alternative", "", n_bins2 - 1, binning2},
                                                   "subleading_pT_lepton", "weight_final"));

//    auto weight_hist = new TH2D(*nominal_hist);
    auto weight_hist = new TH1D(*nominal_hist);
    weight_hist->Divide(alternative_hist);
    //weight_hist->Draw("colz");
    //weight_hist->Print("range");

    TString table_str;
//    for(int i = 0; i < n_bins1 - 1; i++)
//    {
//        for(int j = 0; j < n_bins2 - 1; j++)
//        {
//            table_str += TString::Format("else if (truth_second_jet_pt >= %.0f && truth_second_jet_pt < %.0f && truth_subleading_pT_lepton >= %.0f && truth_subleading_pT_lepton < %.0f) return %.5f;\n",
//                                         binning1[i], binning1[i + 1], binning2[j], binning2[j + 1], weight_hist->GetBinContent(i + 1, j + 1));
//        }
//    }
    for(int j = 0; j < n_bins2 - 1; j++)
        table_str += TString::Format("else if (subleading_pT_lepton >= %.0f && subleading_pT_lepton < %.0f) return %.5f;\n",
                                     binning2[j], binning2[j + 1], weight_hist->GetBinContent(j + 1));
    table_str.Replace(0, 7, "if", 2);
    std::cout << table_str << std::endl;
}
