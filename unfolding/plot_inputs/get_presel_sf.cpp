#include "inc/MC.h"
#include "inc/Setting.h"

using RNode = ROOT::RDF::RNode;

namespace setting
{
}

RNode get_node(const TString &measurement, const TString &channel)
{
    MC mcs;

    TString path_to_file = mcs.mc_path_fiducial;

    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((path_to_file + "/" + file).Data());

    std::string pre_selection = settings.at(measurement).truth_preselection.GetTitle();
    std::string selection = settings.at(measurement).FR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    RNode node = data.Filter(pre_selection).Filter(selection);

    return node;
}

TString get_hist_name(const TString &measurement, const TString &channel, const TString &var)
{
    TString hist_name;
    hist_name = measurement + "_" + channel + "_" + var;

    return hist_name;
}

TH1D* get_hist(const TString &measurement, const TString &channel, const TString &var, bool if_presel = false)
{
    auto node = get_node(measurement, channel);
    *node.Count();

    int n_bins = settings.at(measurement).get_binning(var).size();
    auto xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(var).at(i);

    std::string weight_final = std::string("scale*ew_correction*tautau_correction*weight_gen") +
                               (if_presel && measurement == "inclusive" ? "*presel_factor_inclusive" : "") +
                               (if_presel && measurement == "ZZjj"      ? "*presel_factor_ZZjj"      : "");
    std::string var_to_plot  = std::string("truth_") + var.Data();
    auto hist_name = get_hist_name(measurement, channel, var);
    auto hist = new TH1D(*node.Define("weight_final", weight_final)
                              .Histo1D({hist_name, "", n_bins - 1, xbins}, var_to_plot, "weight_final"));

    delete [] xbins;

    return hist;
}

void get_presel_sf()
{
    TString measurement = "ZZjj";
    for(const auto &[var, binning] : settings.at(measurement).binnings)
    {
        if(var == "subleading_pT_lepton") continue;

        auto hist_presel  = get_hist(measurement, "inclusive", var, true);
        auto hist_orignal = get_hist(measurement, "inclusive", var, false);
        hist_presel->Divide(hist_orignal);
//        hist_presel->Print("range");

        TString output = "{\"" + TString::Format("%-19s", (var + "\",").Data()) + " {";
        auto n_bins = hist_presel->GetNbinsX();
        for(auto i = 1; i <= n_bins; i++)
            output += TString::Format("%6f,", hist_presel->GetBinContent(i));
        output += "}},\n";
        std::cout << output;
    }
}
