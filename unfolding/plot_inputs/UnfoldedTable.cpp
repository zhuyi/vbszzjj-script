const TString pattern = "[ ]+[-\\+\\d\\.]+[ ]+\\(([-\\+\\d\\.]+\\\\\%)\\) \\|";

struct Node
{
    TString unc;
    TString formatted;
};

std::map<TString, TString> headers = {{"Combined", "Comb. Unc."},
                                      {"Stat.",    "Stat. Unc."},
                                      {"Sys.",     "Sys. Unc. "},
                                      {"Bkg.",     "Bkg. Unc. "}};

std::vector<Node> formatteds = {};

TString GetPath(TString region, TString var)
{
    return "/lustre/collider/zhuyifan/VBSZZ/unfolding/VIPUnfolding/examples/EB/" + region + "/" + var + "/";
}

TString GetName(TString var)
{
    if(var == "mt_zz")    var = "mT_ZZ";
    else if(var == "ptz") var = "Z_pT";
    return "Results_" + var + ".txt";
}

void UnfoldedTable()
{
    TString region = "inclusive";
    TString var = "n_jets";
    TString file = GetPath(region, var) + "/" + GetName(var);

    std::ifstream ifs;
    ifs.open(file, std::ios::in);
    if (!ifs.is_open())
    {
        std::cerr << "fail to open " << file << std::endl;
        return;
    }

    TString line;
    while(line.ReadLine(ifs))
    {
        TString line_copy(line);

        if(line_copy.BeginsWith("|") && line_copy.Contains("Unc."))
        {
            TString patterns_to_match;

            patterns_to_match += "([\\w\\.]+) Unc\\.[ ]+\\|";
            int n_bin = line_copy.CountChar('%');
            for(int i = 0; i < n_bin; i++) patterns_to_match += pattern;

            auto sub_string_array = TPRegexp(patterns_to_match).MatchS(line_copy);
            auto sub_string_array_size = sub_string_array->GetLast() + 1;

            if(sub_string_array_size == 0) break;

            auto header = headers.at(dynamic_cast<TObjString*>(sub_string_array->At(1))->GetString());

            TString formatted;
            formatted += header + " ";
            for(int i = 2; i < sub_string_array_size; i++)
                formatted += TString::Format("& %s ",
                                             dynamic_cast<TObjString*>(sub_string_array->At(i))->GetString().Data());
            formatted += "\\\\";

            formatteds.push_back({header, formatted});
        }
    }

    TString final_formatted;
    for(const auto &[header, formatted] : formatteds)
    {
        final_formatted += formatted + "\n";
        if(header == headers.at("Combined")) final_formatted += "\\hline\n";
    }
    std::cout << final_formatted << std::endl;
}
