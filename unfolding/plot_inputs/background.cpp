#include "inc/Setting.h"

const TString path = "/lustre/collider/zhuyifan/VBSZZ/dev_VIP/results/EB/inclusive/background_variation/";

enum variation {bUp, bDown};
std::map<variation, TString> variation_names = {{bUp, "up"}, {bDown, "down"}};

TString get_file_name(const TString &var)
{

    return "Results_" + var + ".root";
}

std::tuple<TFile*, TFile*> get_files(const TString &channel, const TString &var)
{
    TString var_nickname(var);
    if     (var == "Z_pT")  var_nickname = "ptz";
    else if(var == "mT_ZZ") var_nickname = "mt_zz";

    return {new TFile(path + "/" + channel + "/up/"   + var_nickname + "/" + get_file_name(var), "read"),
            new TFile(path + "/" + channel + "/down/" + var_nickname + "/" + get_file_name(var), "read")};
}

TH1D* get_variation_hist(TFile *file)
{
    auto unfolded_hist = dynamic_cast<TH1D*>(file->Get("Final/Final_Nominal_Unfolded_Normalized"));
    auto mc_hist = dynamic_cast<TH1D*>(file->Get("Final/Final_MCTruth_Normalized"));

    unfolded_hist->Divide(mc_hist);
    return unfolded_hist;
}

std::tuple<TH1D*, TH1D*> get_variation_hists(const TString &channel, const TString &var)
{
    auto [up_file, down_file] = get_files(channel, var);

    return {get_variation_hist(up_file), get_variation_hist(down_file)};
}

std::vector<double> get_uncertainties(const TH1D *up_hist, const TH1D *down_hist)
{
    const auto n_bin = up_hist->GetNbinsX();
    std::vector<double> uncertainties(n_bin);

    for(auto i = 0; i < n_bin; i++)
    {
        auto up_uncertainty = up_hist->GetBinContent(i + 1) - 1.;
        auto down_uncertainty = 1. - down_hist->GetBinContent(i + 1);
//        uncertainties.at(i) = 0.5*std::hypot(up_uncertainty, down_uncertainty);
        uncertainties.at(i) = 0.5*std::abs(up_uncertainty + down_uncertainty);
    }

    return uncertainties;
}

void background()
{
    gStyle->SetOptStat(0);

    std::vector<TString> channels = {"top", "WW", "WZ", "Z0jet", "Z1jet", "Z2jet"};

    std::vector<TString> vars = {
                                 "n_jets",
                                 "dphill",
                                 "leading_pT_lepton",
                                 "mT_ZZ",
                                 "pt_zz",
                                 "Z_pT",
                                 "Z_rapidity",
                                 "CP_ZZ_1",
                                 "CP_ZZ_2",

                                 //"dphill",
                                 //"leading_jet_pt",
                                 //"mjj",
                                 //"mT_ZZ",
                                 //"pt_zz",
                                 //"Z_pT",
                                 //"Z_rapidity",
                                };

    auto c = new TCanvas("c", "", 800., 600.);

    for(const auto &channel : channels)
    {
        for(const auto &var : vars)
        {
            auto [up_hist, down_hist] = get_variation_hists(channel, var);
            auto uncertainties = get_uncertainties(up_hist, down_hist);

            up_hist->SetTitle("");
            up_hist->GetXaxis()->SetTitle(titles.at(var));
            up_hist->GetYaxis()->SetTitle("variation/nominal");
            up_hist->SetMaximum(1.15);
            up_hist->SetMinimum(0.85);
            up_hist->SetLineWidth(2);
            up_hist->SetLineColor(kRed + 2);
            down_hist->SetLineWidth(2);
            down_hist->SetLineColor(kBlue + 2);

            up_hist->Draw("hist");
            c->Update();

            auto line = new TLine(c->GetUxmax(), 1., c->GetUxmin(), 1.);
            line->SetLineColor(kBlack);
            line->Draw();
            line->SetLineStyle(kDashed);
            line->DrawLine(c->GetUxmax(), 1.025, c->GetUxmin(), 1.025);
            line->DrawLine(c->GetUxmax(), 0.975, c->GetUxmin(), 0.975);

            up_hist->Draw("same hist");
            down_hist->Draw("same hist");

            auto legend = new TLegend(0.65, 0.75, 0.90, 0.83);
            legend->AddEntry(up_hist, "up", "L");
            legend->AddEntry(down_hist, "down", "L");
            legend->SetTextSize(0.03);
            legend->SetBorderSize(0);
            legend->Draw();

            auto text = new TPaveText(0.12, 0.70, 0.50, 0.83, "brNDC");
            text->SetFillColor(0);
            text->SetFillStyle(0);
            text->SetBorderSize(0);
            text->SetTextAlign(12);
            text->SetTextSize(0.03);
            text->AddText("#it{ATLAS} #bf{Internal}");
            text->AddText("#bf{#sqrt{s}= 13 TeV 140 fb^{-1}, inclusive region}");
            text->AddText("#bf{" + channel + " variation}");
            dynamic_cast<TText*>(text->GetListOfLines()->First())->SetTextSize(0.04);
            text->Draw();

            for(const auto &uncertainty : uncertainties)
                std::cout << uncertainty << ",\t";
            std::cout << std::endl;

            auto save_name = "background_variation_" + channel + "_" + var;
            c->SaveAs("fig/" + save_name + ".png");
        }
    }
}
