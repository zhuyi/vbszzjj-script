#include "inc/util.h"
#include "inc/bias.h"

const int max_bin = 8;
const double lumi = 140.06894;
//const std::map<TString, double> xss = {{"inclusive", 20.08}, {"ZZjj", 0.94}};
const std::map<TString, double> xss = {{"inclusive", 21.8}, {"ZZjj", 0.96}};

namespace pattern
{
    enum group_type {pBin, pResult, pSyst};
    enum pattern_type {pName, pNumber};

    const TString bin_name_pattern = " +(Bin)+ +\\|";
    const TString bin_number_pattern = " +([-\\d\\.]+ +- [-\\d\\.]+) +\\|";
    const TString result_name_pattern = " +(Results)+ +\\|";
    const TString result_number_pattern = " +([-\\de\\.]+) +\\|";
    const TString syst_name_pattern = " +([\\w\\d\\(\\)\\._ ]+) +\\|";
    const TString syst_number_pattern = " +[-\\+\\de\\.]+ +\\(([-\\+\\d\\.]+\\\\\%)\\) \\|";

    std::map<std::tuple<group_type, pattern_type>, const TString*> patterns = {{{pBin,    pName},   &bin_name_pattern},
                                                                               {{pBin,    pNumber}, &bin_number_pattern},
                                                                               {{pResult, pName},   &result_name_pattern},
                                                                               {{pResult, pNumber}, &result_number_pattern},
                                                                               {{pSyst,   pName},   &syst_name_pattern},
                                                                               {{pSyst,   pNumber}, &syst_number_pattern}
                                                                              };



    group_type get_type(const TString &line)
    {
        if     (line.Contains("Bin"))     return pBin;
        else if(line.Contains("Results")) return pResult;
        return pSyst;
    }

    TString get_pattern(const group_type &group_type_in, const pattern_type &pattern_type_in)
    {
        return *patterns.at({group_type_in, pattern_type_in});
    }
}

const std::map<TString, std::vector<TString>> group_names = {//{"MC stat", {"Stat. Unc. (MC)"}},
                                                             {"stat", {"Stat. Unc."}},
                                                             {"syst",  {"Sys. Unc."}},
                                                             {"total", {"Combined Unc."}},
                                                             {"jet",  {"JET", "FT"}},
                                                             {"muon", {"MUON"}},
                                                             {"elec", {"EL"}},
                                                             {"egamma", {"EG"}},
                                                             {"MET",  {"MET"}},
                                                             {"pileup", {"PRW"}},
                                                             {"theo", {"pdf", "scale", "alphas"}},
                                                             {"bias", {"bias"}},
                                                             {"bkg",  {"Background"}},
                                                             //{"unfolding", {"Unfolding"}}
                                                             {"result", {"Results"}},
                                                             {"bin",  {"Bin"}},
                                                            };

class Node
{
public:
    Node() = default;
    Node(const TString &name) : name_(name) {}
    Node(const TString &name, const std::vector<double> &numbers) : name_(name), numbers_(numbers) {}
    Node(TString &&name, std::vector<double> &&numbers) : name_(std::move(name)), numbers_(std::move(numbers)) {}
    Node(const Node &node) : name_(node.name_), numbers_(node.numbers_) {}
    Node(Node &&node) : name_(std::move(node.name_)), numbers_(std::move(node.numbers_)) {}

    Node operator=(const Node &node)
    {
        if(this == &node) return *this;

        name_ = node.name_;
        numbers_ = node.numbers_;
        return *this;
    }

    void normalize()
    {
        double sum = 0.;
        for(const auto &number : numbers_) sum += number;
        for(auto &number : numbers_) number /= sum;
    }

    void scale(const double &scale)
    {
        for(auto &number : numbers_)
            number *= scale;
    }

    void scale(const std::vector<double> &scales)
    {
        for(auto&& [scale, number] : make_container_ref_tuple(scales, numbers_))
            number *= scale;
    }

    void add(const Node &node)
    {
        if(!numbers_.size())
        {
            numbers_ = node.numbers_;
            return;
        }

        auto min_size = numbers_.size() > node.numbers_.size() ? node.numbers_.size() : numbers_.size();

        for(auto i = 0; i < min_size; i++)
            numbers_.at(i) = std::hypot(numbers_.at(i), node.numbers_.at(i));
    }

    TString name() const { return name_; }
    std::vector<double> numbers() const { return numbers_; }
    
private:
    TString name_;
    std::vector<double> numbers_;
};

class Group
{
public:
    Group(const TString &name) : name_(name) {}
    Group(const Group &group) : name_(group.name_), nodes_(group.nodes_), numbers_(group.numbers_) {}
    Group(Group &&group) : name_(std::move(group.name_)), nodes_(std::move(group.nodes_)), numbers_(std::move(group.numbers_)) {}

    void set_name(const TString &name) { name_ = name; }
    void add_node(const Node &node, bool if_update = false)
    {
        nodes_.push_back(node);

        if(if_update) update_numbers();
    }
    void add(const Group &group)
    {
        nodes_.insert(nodes_.end(), group.nodes_.begin(), group.nodes_.end());
        update_numbers();
    }

    void update_numbers()
    {
        if(!nodes_.size()) return;

        numbers_.clear();
        numbers_.reserve(nodes_.at(0).numbers().size());

        if(nodes_.size() == 1)
        {
            numbers_ = nodes_.at(0).numbers();
            return;
        }

        auto n_bin = nodes_.at(0).numbers().size();
        for(size_t i = 0; i < n_bin; i++)
        {
            double number(0.);

            for(const auto& node : nodes_)
                number += node.numbers().at(i)*node.numbers().at(i);

            numbers_.push_back(std::sqrt(number));
        }
    }

    void normalize()
    {
        for(auto& node : nodes_) node.normalize();

        update_numbers();
    }

    void scale(const double &scale)
    {
        for(auto& node : nodes_) node.scale(scale);

        update_numbers();
    }

    void scale(const std::vector<double> &scales)
    {
        for(auto& node : nodes_) node.scale(scales);

        update_numbers();
    }

    void print(const TString &last_term = "", const TString &separator = "", const TString &ending = "") const
    {
        TString line;

        line += TString::Format("%-9s\t%s ", name_.Data(), separator.Data());
        for(const auto &number : numbers_)
        {
/*
            int precision = 2;
            if     (number == 0.)  precision = 2;
            else if(number < 1e-4) precision = 5;
            else if(number < 1e-3) precision = 4;
            else if(number < 1e-2) precision = 3;
            line += TString::Format("%." + TString::Itoa(precision, 10) + "f\t%s ", number, separator.Data());
*/
            line += TString::Format("%f\t%s ", number, separator.Data());
        }
        line += last_term;
        line.Replace(line.Length() - 2, separator.Length(), "");
        line += ending;

        std::cout << line << std::endl;
    }

    void print_latex(const TString &last_term = "") const
    {
        print(last_term, "&", "\\\\");
    }

    TString name() const { return name_; }
    std::vector<double> numbers() const { return numbers_; }

private:
    TString name_;
    std::vector<Node> nodes_;
    std::vector<double> numbers_;
};

class Table
{
public:
    Table(const TString &measurement, std::map<TString, Group> &&syst_groups) : measurement_(measurement), syst_groups_(std::move(syst_groups)) {}

    void get_summary(const std::vector<double> *scales = nullptr)
    {
        summary_groups_.clear();

        if(scales)
        {
            syst_groups_.at("result").scale(*scales);
            syst_groups_.at("result").normalize();
        }

        syst_groups_.at("result").scale(xss.at(measurement_));

        syst_groups_.at("bias").scale(100.);

        std::vector<double> bin_widths;
        auto lowers = syst_groups_.at("bin lower").numbers();
        auto uppers = syst_groups_.at("bin upper").numbers();
        for(auto&& [lower, upper] : make_container_ref_tuple(lowers, uppers))
            bin_widths.push_back(1./(upper - lower));
        syst_groups_.at("result").scale(bin_widths);

        auto lower  = syst_groups_.extract("bin lower");
        auto upper  = syst_groups_.extract("bin upper");
        auto result = syst_groups_.extract("result");
        auto bkg    = syst_groups_.extract("bkg");
        auto stat   = syst_groups_.extract("stat");
        auto syst   = syst_groups_.extract("syst");
        auto total  = syst_groups_.extract("total");

        summary_groups_.insert(std::move(lower));
        summary_groups_.insert(std::move(upper));
        summary_groups_.insert(std::move(result));
        summary_groups_.insert(std::move(bkg));
        summary_groups_.insert(std::move(stat));
        summary_groups_.insert(std::move(syst));
        summary_groups_.insert(std::move(total));

        summary_groups_.at("syst") .add(syst_groups_.at("bias"));
        summary_groups_.at("total").add(syst_groups_.at("bias"));
    }

    std::optional<TString> get_group_sum(const TString &group_name)
    {
        Group *group = nullptr;
        if(syst_groups_.count(group_name))
            group = &syst_groups_.at(group_name);
        if(summary_groups_.count(group_name) && group_name != "result" && !group_name.Contains("bin"))
            group = &summary_groups_.at(group_name);
        if(!group) return std::nullopt;

        double sum;
        auto results = summary_groups_.at("result").numbers();
        auto lowers = summary_groups_.at("bin lower").numbers();
        auto uppers = summary_groups_.at("bin upper").numbers();
        auto numbers = group->numbers();
        for(auto&& [result, lower, upper, number] : make_container_ref_tuple(results, lowers, uppers, numbers))
            sum += result*(upper - lower)/xss.at(measurement_)*number*number;
        return TString::Format("%.2f", std::sqrt(sum));
    }

    void print(const TString &separator = "", const TString &ending = "",
               const TString &top_rule = "---", const TString &mid_rule = "---", const TString &bottom_rule = "---")
    {
        TString last_term = "";

        std::cout << top_rule << std::endl;
        std::cout << get_bin_str(separator + " ", separator, ending) << std::endl;
        std::cout << mid_rule << std::endl;
        summary_groups_.at("result").print(separator + " ", separator, ending);
        std::cout << mid_rule << std::endl;

        for(const auto &[group_name, group] : syst_groups_)
            group.print(get_group_sum(group_name).value() + " " + separator + " ", separator, ending);

        std::cout << mid_rule << std::endl;
        summary_groups_.at("bkg").print(get_group_sum("bkg").value() + " " + separator + " ", separator, ending);
        summary_groups_.at("stat").print(get_group_sum("stat").value() + " " + separator + " ", separator, ending);
        summary_groups_.at("syst").print(get_group_sum("syst").value() + " " + separator + " ", separator, ending);
        std::cout << mid_rule << std::endl;
        summary_groups_.at("total").print(get_group_sum("total").value() + " " + separator + " ", separator, ending);
        std::cout << bottom_rule << std::endl;
    }

    void print_latex()
    {
        print("&", "\\\\", "\\toprule", "\\midrule", "\\bottomrule");
    }

private:
    TString get_bin_str(const TString &last_term, const TString &separator, const TString &ending)
    {
        TString bin_str;

        auto lowers = summary_groups_.at("bin lower").numbers();
        auto uppers = summary_groups_.at("bin upper").numbers();
        bin_str += TString::Format("bin      \t%s ", separator.Data());
        for(auto&& [lower, upper] : make_container_ref_tuple(lowers, uppers))
            bin_str += TString::Format("%.2f - %.2f\t%s ", lower, upper, separator.Data());
        bin_str += last_term;
        bin_str.Replace(bin_str.Length() - 2, ending.Length(), "");
        bin_str += ending;

        return bin_str;
    }

    TString measurement_;

    std::map<TString, Group> syst_groups_;
    std::map<TString, Group> summary_groups_;
};

TString get_var_nickname(const TString &var)
{
    TString var_nickname(var);
    if     (var == "mt_zz") var_nickname = "mT_ZZ";
    else if(var == "ptz")   var_nickname = "Z_pT";
    return var_nickname;
}

TString get_path(TString measurement, const TString &var)
{
    if(measurement == "zzjj" || measurement == "ZZjj") measurement = "vbs";
//    return "/lustre/collider/zhuyifan/VBSZZ/unfolding/VIPUnfolding/examples/EB/" + measurement + "/" + var + "/";
//    return "/lustre/collider/zhuyifan/VBSZZ/dev_VIP/results/EB/" + measurement + "/" + var + "/";
    return "/lustre/collider/zhuyifan/VBSZZ/dev_VIP/results/unblind/" + measurement + "/" + var + "/";
//    return "/lustre/collider/zhuyifan/VBSZZ/dev_VIP/results/EB/" + measurement + "/background_variation/one/" + var + "/";
}

TString get_name(const TString &var)
{
    return "Results_" + get_var_nickname(var) + ".txt";
}

std::optional<TString> split_line(const TString &line, const TString &match_pattern, std::vector<TString> &splits, int start_i = 1)
{
    auto sub_string_array = TPRegexp(match_pattern).MatchS(line);
    auto sub_string_array_size = sub_string_array->GetLast() + 1;
    if(sub_string_array_size == 0) return std::nullopt;

    splits.reserve(splits.size() + sub_string_array_size - 1);
    for(int i = start_i; i < sub_string_array_size; i++)
        splits.push_back(dynamic_cast<TObjString*>(sub_string_array->At(i))->GetString()); 

    return dynamic_cast<TObjString*>(sub_string_array->At(0))->GetString();
}

bool if_contains_syst(const TString &line)
{
    for(const auto &[group_name, name_slices] : group_names)
    {
        for(const auto &name_slice : name_slices)
        {
            if(line.Contains(name_slice) && !line.Contains("Stat. Unc. (MC)"))
                return true;
        }
    }

    return false;
}

std::tuple<Node, Node> get_bin_nodes(const std::vector<TString> &bin_strs)
{
    const TString bin_edge_pattern = "([-\\d\\.]+)";
    const TString match_pattern = bin_edge_pattern + " +- " + bin_edge_pattern;

    auto n_bins = bin_strs.size() - 1;
    std::vector<double> lower_edges(n_bins);
    std::vector<double> upper_edges(n_bins);

    std::vector<TString> edges;
    for(int i = 0; i < n_bins; i++)
    {
        edges.clear();
        auto edge_result = split_line(bin_strs.at(i + 1), match_pattern, edges, 0);
        if(!edge_result.has_value()) continue;

        lower_edges.at(i) = edges.at(1).Atof();
        upper_edges.at(i) = edges.at(2).Atof();
    }

    return {Node("Bin_Lower", std::move(lower_edges)), Node("Bin_Upper", std::move(upper_edges))}; 
}

std::vector<Node> get_nodes(const TString &measurement, const TString &var)
{
    auto file = get_path(measurement, var) + "/" + get_name(var);
    std::ifstream ifs;
    ifs.open(file, std::ios::in);
    if (!ifs.is_open())
    {
        std::cerr << "fail to open " << file << std::endl;
        return {};
    }

    std::vector<Node> nodes;
    std::vector<TString> splits;
    std::vector<double> numbers;

    nodes.reserve(109);

    TString line;
    while(line.ReadLine(ifs))
    {
        splits.clear();
        numbers.clear();

        TString line_copy(line);

        if(!line_copy.BeginsWith("|") ||
           (!line_copy.Contains("Systematic") && !line_copy.Contains("Uncertainty") && !if_contains_syst(line_copy))
          )
            continue;

        auto line_type = pattern::get_type(line_copy);

        int n_bin = line_copy.CountChar('|') - 2;
        int match_bin = n_bin > max_bin ? max_bin : n_bin;

        TString match_pattern = pattern::get_pattern(line_type, pattern::pName);
        for(int i = 0; i < match_bin; i++) match_pattern += pattern::get_pattern(line_type, pattern::pNumber);

        auto split_result = split_line(line_copy, match_pattern, splits);
        if(!split_result.has_value()) continue;
        auto syst_name = splits.at(0);

        if(n_bin > max_bin)
        {
            auto first_match = split_result.value();
            line_copy.ReplaceAll(first_match, "");

            match_pattern = pattern::get_pattern(line_type, pattern::pNumber);
            for(int i = 1; i < n_bin - max_bin; i++) match_pattern += pattern::get_pattern(line_type, pattern::pNumber);
            split_result = split_line(line_copy, match_pattern, splits);
        }

        if(line_type == pattern::pBin)
        {
            auto [lower_node, upper_node] = get_bin_nodes(splits);
            nodes.emplace_back(std::move(lower_node));
            nodes.emplace_back(std::move(upper_node));
            continue;
        }

        numbers.reserve(splits.size() - 1);
        for(int i = 1; i < splits.size(); i++)
            numbers.push_back(splits.at(i).Atof());
        
        nodes.emplace_back(std::move(syst_name), std::move(numbers));
    }

    nodes.emplace_back("bias", bias.at(measurement).at(var));

    ifs.close();

//    for(const auto& node : nodes)
//    {
//        std::cout << node.name() << std::endl;
//        for(const auto& number : node.numbers()) std::cout << number << "\t";
//        std::cout << std::endl;
//    }

    return nodes;
}

std::optional<TString> get_group_name(const Node &node)
{
    if     (node.name() == "Bin_Lower") return "bin lower";
    else if(node.name() == "Bin_Upper") return "bin upper";

    for(const auto &[group_name, name_slices] : group_names)
    {
        for(const auto &name_slice : name_slices)
        {
            if(node.name().Contains(name_slice))
                return group_name;
        }
    }

    return std::nullopt;
}

std::map<TString, Group> get_groups(const std::vector<Node> &nodes)
{
    if(!nodes.size()) return {};

    std::map<TString, Group> groups;

    for(const auto &node : nodes)
    {
        auto group_name = get_group_name(node);
        if(!group_name.has_value()) continue;

        if(groups.count(group_name.value())) groups.at(group_name.value()).add_node(node);
        else
        {
            Group group(group_name.value());
            group.add_node(node);
            groups.insert({group_name.value(), std::move(group)});
        }
    }

    for(auto &[group_name, group] : groups) group.update_numbers();

    return groups;
}

void unc_breakdown_post()
{
    TString measurement = "ZZjj";
    std::map<TString, std::vector<TString>> vars = {{"inclusive",
                                                     {
                                                      "n_jets",
                                                      "dphill",
                                                      "leading_pT_lepton",
                                                      "mt_zz",
                                                      "ptz",
                                                      "pt_zz",
                                                      "Z_rapidity",
                                                      "CP_ZZ_1",
                                                      "CP_ZZ_2",
                                                     }
                                                    },

                                                    {"ZZjj",
                                                     {
                                                      "dphill",
                                                      "leading_jet_pt",
                                                      "mjj",
                                                      "mt_zz",
                                                      "ptz",
                                                      "pt_zz",
                                                      "Z_rapidity",
                                                     }
                                                    }
                                                   };

    for(const auto &var : vars.at(measurement))
    {
        std::cout << var << std::endl;

        auto nodes = get_nodes(measurement, var);
        auto groups = get_groups(nodes);

        Table table(measurement, std::move(groups));
        table.get_summary();
        table.print_latex();

        std::cout << std::endl;

        std::cout << std::endl;
    }
}
