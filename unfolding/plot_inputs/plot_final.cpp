#include "inc/presel_weight.h"
#include "inc/bias.h"

namespace setting
{
    enum range_fix {original, floating, fixed};
}

const double lumi = 140.06894;

const TString mc_name = "Nominal/Nominal_MCTruth";
const TString unfolded_name = "Final/Final_Nominal_Unfolded";
const TString normaliz_name = "Final/Final_Nominal_Unfolded_Normalized";
const TString stats_name = "Final/Final_Statistical_Uncertainty";
const TString total_name = "Final/Final_CombinedUncertainty";
const TString background_name = "Nominal/Nominal_Background";

std::map<TString, std::array<TString, 2>> titles = {{"met_tst",    {"E^{missing}_{T} [GeV]", "d#sigma/dE^{missing}_{T} [fb/GeV]"}},
                                                    {"dMetZPhi",   {"#Delta#Phi(E^{missing}_{T, }},Z)", "d#sigma/#Delta#Phi(E^{missing}_{T, }},Z) [fb]"}},
                                                    {"dLepR",      {"#DeltaR(l,l)", "d#sigma/d#DeltaR(l,l) [fb]"}},
                                                    {"Z_pT",       {"p_{T}^{Z} [GeV]", "d#sigma/dp_{T}^{Z} [fb/GeV]"}},
                                                    {"n_jets",     {"No. of jets", "d#sigma/dN [fb]"}},
                                                    {"dphill",     {"#Delta#Phi(l,l)", "d#sigma/d#Delta#Phi(l,l) [fb]"}},
                                                    {"mT_ZZ",      {"m_{T}^{Z,Z} [GeV]", "d#sigma/dm_{T}^{Z,Z} [fb/GeV]"}},
                                                    {"mjj",        {"m_{jj} [GeV]", "d#sigma/dm_{jj} [fb/GeV]"}},
                                                    {"leading_jet_pt", {"p_{T}^{leading jet} [GeV]", "d#sigma/dp_{T}^{leading jet} [fb/GeV]"}},
                                                    {"leading_pT_lepton", {"p_{T}^{leading lepton} [GeV]", "d#sigma/dp_{T}^{leading lepton} [fb/GeV]"}},
                                                    {"pt_zz",      {"p_{T}^{Z,Z} [GeV]", "d#sigma/dp_{T}^{Z,Z} [fb/GeV]"}},
                                                    {"Z_rapidity", {"|y^{Z}|", "d#sigma/dy^{Z} [fb]"}},
                                                    {"CP_ZZ_1",    {"sin(#phi_{1})cos(#theta_{1})", "d#sigma/dsin(#phi_{1})cos(#theta_{1}) [fb]"}},
                                                    {"CP_ZZ_2",    {"sin(#phi_{2})cos(#theta_{2})", "d#sigma/dsin(#phi_{2})cos(#theta_{2}) [fb]"}},
                                                   };

std::map<TString, TString> nicknames = {{"met_tst",    "met_tst"},
                                        {"dMetZPhi",   "dMetZPhi"},
                                        {"dLepR",      "dLepR"},
                                        {"Z_pT",       "ptz"},
                                        {"n_jets",     "n_jets"},
                                        {"dphill",     "dphill"},
                                        {"mT_ZZ",      "mt_zz"},
                                        {"mjj",        "mjj"},
                                        {"leading_jet_pt", "leading_jet_pt"},
                                        {"leading_pT_lepton", "leading_pT_lepton"},
                                        {"pt_zz",      "pt_zz"},
                                        {"Z_rapidity", "Z_rapidity"},
                                        {"CP_ZZ_1",    "CP_ZZ_1"},
                                        {"CP_ZZ_2",    "CP_ZZ_2"},
                                       };

TString dtoa(const double &label)
{
    TString formatted(TString::Format("%f", label));

    size_t n = formatted.Length();
    for(size_t i = 0; i < n; i++)
    {
        if     (formatted.EndsWith("0")) formatted.Remove(formatted.Length() - 1);
        else if(formatted.EndsWith(".")) { formatted.Remove(formatted.Length() - 1); break; }
    }

    return formatted;
}

TFile* get_file(const TString &var, const TString &measurement)
{
    TString name = "Results_" + var + ".root";
//    TString path = "/lustre/collider/zhuyifan/VBSZZ/unfolding/VIPUnfolding/examples/EB/" + (measurement == "ZZjj" ? "vbs" : measurement) + "/" + nicknames.at(var);
    TString path = "/lustre/collider/zhuyifan/VBSZZ/dev_VIP/results/unblind/" + (measurement == "ZZjj" ? "vbs" : measurement) + "/" + nicknames.at(var);
//    TString path = "/lustre/collider/zhuyifan/VBSZZ/dev_VIP/results/EB/" + (measurement == "ZZjj" ? "vbs" : measurement) + "/" + nicknames.at(var);
    auto file = new TFile(path + "/" + name, "read");

    return file;
}

TH1D* get_hist(TFile *file, const TString &hist_name)
{
    auto hist = dynamic_cast<TH1D*>(file->Get(hist_name));

    return hist;
}

TH1D *get_formatted(TH1D* unfolded, const TString &x_title, const TString &y_title, const Color_t &color, const Style_t &style = kDot, std::vector<double> *scales = nullptr)
{
    const int n_bins = unfolded->GetNbinsX();
    auto binning = new double[n_bins + 1];
    for(int i = 0; i < n_bins + 1; i++) binning[i] = i;

    auto formatted = dynamic_cast<TH1D*>(unfolded->Clone(TString::Format("%s_formatted", unfolded->GetName())));
    formatted->SetBins(n_bins, binning);
    formatted->GetXaxis()->SetNdivisions(n_bins, 1, 1);
    for(int i = 0; i <= n_bins; i++)
        formatted->GetXaxis()->ChangeLabel(i + 1, -1, -1, -1, -1, -1, " ");

    formatted->Scale(1./lumi);
    for(int i = 1; i <= n_bins; i++)
        formatted->SetBinContent(i, formatted->GetBinContent(i)/unfolded->GetBinWidth(i)*((scales == nullptr) ? 1. : scales->at(i - 1)));
    for(int i = 1; i <= n_bins; i++)
        formatted->SetBinError(i, 0.);
    if(style != kDot)
    {
        formatted->SetMarkerStyle(style);
        formatted->SetMarkerColor(color);
        formatted->SetMarkerSize(1.5);
    }
    formatted->SetLineColor(color);
    formatted->SetLineWidth(2);
    formatted->SetTitle("");
    formatted->GetXaxis()->SetTitle(x_title);
    formatted->GetYaxis()->SetTitle(y_title);

    double total_xs(0.);
    for(int i = 1; i <= n_bins; i++)
        total_xs += formatted->GetBinContent(i)*unfolded->GetBinWidth(i);
    std::cout << "total xs: " << total_xs << " fb" << std::endl;

    return formatted;
}

void add_unc(TH1D* hist, const std::vector<double> &uncs)
{
    int nbin = hist->GetNbinsX();
    for(int i = 1; i < nbin; i++)
        hist->SetBinContent(i, std::hypot(hist->GetBinContent(i), uncs.at(i - 1)));
}

std::vector<TBox*> get_boxs(TH1D* formatted, TH1D* unc,
                            double &max, double &min, const Color_t &color, const Style_t &style,
                            setting::range_fix fix_range = setting::original)
{
    gStyle->SetHatchesLineWidth(2);

    std::vector<TBox*> boxs;

    const int n_bins = formatted->GetNbinsX();

    std::vector<int> min_is;
    for(int i = 1; i <= n_bins; i++)
    {
        double left   = formatted->GetBinLowEdge(i);
        double right  = formatted->GetBinLowEdge(i + 1);
        double top    = formatted->GetBinContent(i)*(1 + unc->GetBinContent(i));
        double bottom = formatted->GetBinContent(i)*(1 - unc->GetBinContent(i));

        boxs.emplace_back(new TBox(left, bottom, right, top));
        boxs.back()->SetFillStyle(style);
        boxs.back()->SetFillColor(color);

        if (fix_range == setting::fixed)
        {
            if (bottom < min) min_is.push_back(i - 1);
            continue;
        }

        if (min > bottom) min = bottom;
        if (max < top)    max = top;
        if (fix_range == setting::floating && bottom < 0.) min_is.push_back(i - 1);
    }

    if (fix_range == setting::fixed && min_is.size())
    {
        for(const auto &min_i : min_is) boxs.at(min_i)->SetY1(min);
        for(const auto &min_i : min_is) boxs.at(min_i)->SetY2(max);
    }

    if (fix_range == setting::floating && min_is.size())
    {
        min = 0.01*formatted->GetMinimum();
        for(const auto &min_i : min_is) boxs.at(min_i)->SetY1(min);
    }

    return boxs;
}

void draw_legend(const TString &var, TH1D *mc_formatted, TH1D *formatted, TBox *stats_box, TBox *total_box, TString measurement)
{
    auto legend = new TLegend(0.70, 0.72, 0.90, 0.88);
    legend->AddEntry(formatted, "unfolded",  "P");
    legend->AddEntry(mc_formatted, "MC truth", "L");
    legend->AddEntry(stats_box, "stat. unc.", "F");
    legend->AddEntry(total_box, "total unc.", "F");
    legend->SetTextSize(0.04);
    legend->SetBorderSize(0);
    legend->Draw();

    auto title_in_legend = titles.at(var).at(0);
    title_in_legend.ReplaceAll(" [GeV]", "");
    auto text = new TPaveText(0.12, 0.75, 0.50, 0.89, "brNDC");
    text->SetFillColor(0);
    text->SetFillStyle(0);
    text->SetBorderSize(0);
    text->SetTextAlign(12);
    text->SetTextSize(0.04);
//    text->AddText("#it{ATLAS} #bf{Working in Progress}");
    text->AddText("#it{ATLAS} #bf{internal}");
    text->AddText("#bf{#sqrt{s}= 13 TeV 140 fb^{-1}, " + measurement + ", " + title_in_legend + "}");
//    text->AddText("#bf{blinded, fake data}");
    text->Draw();
}

TH1D* get_unithist(TH1D *formatted, TH1D *unfolded, const TString &x_title, const double &text_scale)
{
    auto unithist = dynamic_cast<TH1D*>(formatted->Clone(TString(formatted->GetName()) + "_unithist"));
    unithist->Divide(unithist);

    for(int i = 0; i <= formatted->GetNbinsX(); i++)
        unithist->GetXaxis()->ChangeLabel(i + 1, -1, -1, -1, -1, -1, dtoa(unfolded->GetBinLowEdge(i + 1)));

    unithist->GetYaxis()->SetTitleOffset(0.53);
    unithist->GetXaxis()->SetTitle(x_title);
    unithist->GetYaxis()->SetTitle("relative unc.");
    unithist->GetYaxis()->SetNdivisions(6, 5, 1);
    //unithist->SetMinimum(-0.5);
    //unithist->SetMaximum(2.5);

    unithist->GetXaxis()->SetLabelSize(unithist->GetXaxis()->GetLabelSize()*text_scale);
    unithist->GetYaxis()->SetLabelSize(unithist->GetYaxis()->GetLabelSize()*text_scale);
    unithist->GetXaxis()->SetTitleSize(unithist->GetXaxis()->GetTitleSize()*text_scale);
    unithist->GetYaxis()->SetTitleSize(unithist->GetYaxis()->GetTitleSize()*text_scale);

    return unithist;
}

void plot_final()
{
    const double division = 0.25;

    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);
    TGaxis::SetMaxDigits(3);

    auto c = new TCanvas("c", "", 800., 800.);

    auto p1 = new TPad("p1", "", 0., division, 1., 1.);
    p1->Draw();
    p1->SetLogy();

    c->cd();
    auto p2 = new TPad("p2", "", 0., 0., 1., division + 0.05);
    p2->Draw();
    p2->SetTopMargin(0.03);
    p2->SetBottomMargin(0.25);

    c->cd();

    TString measurement = "ZZjj";
    std::vector<TString> vars = {
                                 //"n_jets",
                                 //"dphill",
                                 //"leading_pT_lepton",
                                 //"mT_ZZ",
                                 //"pt_zz",
                                 //"Z_pT",
                                 //"Z_rapidity",
                                 //"CP_ZZ_1",
                                 //"CP_ZZ_2",

                                 //"dphill",
                                 "leading_jet_pt",
                                 //"mjj",
                                 //"mT_ZZ",
                                 //"pt_zz",
                                 //"Z_pT",
                                 //"Z_rapidity",
                                };

    for(const auto &var : vars)
    {
//        const bool if_ZZjj_yZ = measurement == "ZZjj" && var == "Z_rapidity";

        auto file = get_file(var, measurement);
        if(!file || !file->IsOpen()) continue;

        auto mc = get_hist(file, mc_name);   
        auto unfolded = get_hist(file, unfolded_name);
        auto normaliz = get_hist(file, normaliz_name);
        auto background = get_hist(file, background_name);
        auto total = get_hist(file, total_name);
        auto stats = get_hist(file, stats_name);
        total->Divide(normaliz);
        stats->Divide(normaliz);
        add_unc(total, bias.at(measurement).at(var));

        std::vector<double>* presel_weight_measurement = &presel_weights.at(measurement).at(var);
//        std::vector<double>* presel_weight_measurement = nullptr;
        auto mc_formatted = get_formatted(mc, "", "", kGreen - 1);
        auto formatted = get_formatted(unfolded, "", titles.at(var).at(1), kBlack, kFullCircle);

        double min = 0.1*formatted->GetMinimum();
        double max = formatted->GetMaximum();
        auto stats_boxs = get_boxs(formatted, stats, max, min, kGray + 2,  3345, setting::floating);
        auto total_boxs = get_boxs(formatted, total, max, min, kBlue - 10, 1001, setting::floating);

        p1->cd();
        max *= 20.;
//        if(if_ZZjj_yZ) max = 1000.;
        formatted->SetMaximum(max);
        formatted->SetMinimum(min);
        formatted->Draw("axis");
        for(const auto &box : total_boxs) box->Draw("same");
        for(const auto &box : stats_boxs) box->Draw("same");
        formatted->Draw("P E1 same");
        mc_formatted->Draw("E same");

        TString legend_measurement(measurement);
        if(legend_measurement == "vbs") legend_measurement = "ZZjj";
        draw_legend(var, mc_formatted, formatted, stats_boxs.at(0), total_boxs.at(0), legend_measurement);

        min = /*if_ZZjj_yZ ? -1.5 :*/ 1.;
        max = /*if_ZZjj_yZ ?  3.5 :*/ 1.;
        auto p2_text_scale = (1. - division)/division*0.9;
        auto unithist = get_unithist(formatted, unfolded, titles.at(var).at(0), p2_text_scale);
        auto total_ratios = get_boxs(unithist, total, max, min, kBlue - 10, 1001/*, if_ZZjj_yZ ? setting::fixed : setting::original*/);
        auto stats_ratios = get_boxs(unithist, stats, max, min, kGray + 2,  3345/*, if_ZZjj_yZ ? setting::fixed : setting::original*/);

        p2->cd();
        double p2_interval = (1. - min > max - 1. ) ? 1. - min : max - 1.;
        p2_interval = /*if_ZZjj_yZ ? 2.5 :*/ static_cast<int>(p2_interval*2)*0.5 + 0.5;
        unithist->SetMinimum(1. - p2_interval);
        unithist->SetMaximum(1. + p2_interval);
        unithist->Draw("axis");
        for(const auto &ratio : total_ratios) ratio->Draw("same");
        for(const auto &ratio : stats_ratios) ratio->Draw("same");
        unithist->Draw("same");

        c->cd();
        c->SaveAs("final_" + nicknames.at(var) + ".png");
        c->SaveAs("final_" + nicknames.at(var) + ".C");

        if(min < 0.) p1->SetLogy();

        TString xs_line = TString::Format("%-9s\t& ", "xs");
        auto n_bin = formatted->GetNbinsX();
        for(int i = 1; i <= n_bin; i++)
            xs_line += TString::Format("%f\t& ", formatted->GetBinContent(i));
        xs_line.Replace(xs_line.Length() - 2, 1, "");
        xs_line += "\\\\\n\\bottomrule";
        std::cout << xs_line << std::endl;

//        file->Close();
    }
}
