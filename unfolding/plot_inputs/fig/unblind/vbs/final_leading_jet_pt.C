void final_leading_jet_pt()
{
//=========Macro generated from canvas: c/
//=========  (Thu Feb  6 19:59:48 2025) by ROOT version 6.24/00
   TCanvas *c = new TCanvas("c", "",200,56,800,800);
   gStyle->SetOptStat(0);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "",0,0.25,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-0.375,-7.885352,3.375,0.1138185);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetLogy();
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   Double_t xAxis1[4] = {0, 1, 2, 3}; 
   
   TH1D *Final_Nominal_Unfolded_formatted__1 = new TH1D("Final_Nominal_Unfolded_formatted__1","",3, xAxis1);
   Final_Nominal_Unfolded_formatted__1->SetBinContent(1,0.009197794);
   Final_Nominal_Unfolded_formatted__1->SetBinContent(2,0.001466862);
   Final_Nominal_Unfolded_formatted__1->SetBinContent(3,1.950733e-05);
   Final_Nominal_Unfolded_formatted__1->SetMinimum(8.214193e-08);
   Final_Nominal_Unfolded_formatted__1->SetMaximum(0.2060162);
   Final_Nominal_Unfolded_formatted__1->SetEntries(30490);
   Final_Nominal_Unfolded_formatted__1->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted__1->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted__1->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted__1->GetXaxis()->SetRange(1,3);
   Final_Nominal_Unfolded_formatted__1->GetXaxis()->SetNdivisions(10103);
   Final_Nominal_Unfolded_formatted__1->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__1->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__1->GetYaxis()->SetTitle("d#sigma/dp_{T}^{leading jet} [fb/GeV]");
   Final_Nominal_Unfolded_formatted__1->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__1->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__1->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__1->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted__1->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__1->Draw("axis");
   TBox *box = new TBox(0,0.008094777,1,0.01030081);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(1,0.001099999,2,0.001833725);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(2,8.214193e-08,3,3.893251e-05);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(0,0.008305992,1,0.0100896);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(1,0.00115476,2,0.001778964);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(2,1.79292e-06,3,3.722173e-05);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   Double_t xAxis2[4] = {0, 1, 2, 3}; 
   
   TH1D *Final_Nominal_Unfolded_formatted__2 = new TH1D("Final_Nominal_Unfolded_formatted__2","",3, xAxis2);
   Final_Nominal_Unfolded_formatted__2->SetBinContent(1,0.009197794);
   Final_Nominal_Unfolded_formatted__2->SetBinContent(2,0.001466862);
   Final_Nominal_Unfolded_formatted__2->SetBinContent(3,1.950733e-05);
   Final_Nominal_Unfolded_formatted__2->SetMinimum(8.214193e-08);
   Final_Nominal_Unfolded_formatted__2->SetMaximum(0.2060162);
   Final_Nominal_Unfolded_formatted__2->SetEntries(30490);
   Final_Nominal_Unfolded_formatted__2->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted__2->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted__2->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted__2->GetXaxis()->SetRange(1,3);
   Final_Nominal_Unfolded_formatted__2->GetXaxis()->SetNdivisions(10103);
   Final_Nominal_Unfolded_formatted__2->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__2->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__2->GetYaxis()->SetTitle("d#sigma/dp_{T}^{leading jet} [fb/GeV]");
   Final_Nominal_Unfolded_formatted__2->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__2->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__2->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__2->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted__2->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__2->Draw("P E1 same");
   Double_t xAxis3[4] = {0, 1, 2, 3}; 
   
   TH1D *Nominal_MCTruth_formatted__3 = new TH1D("Nominal_MCTruth_formatted__3","",3, xAxis3);
   Nominal_MCTruth_formatted__3->SetBinContent(1,0.00920593);
   Nominal_MCTruth_formatted__3->SetBinContent(2,0.001331892);
   Nominal_MCTruth_formatted__3->SetBinContent(3,2.26016e-05);
   Nominal_MCTruth_formatted__3->SetEntries(7);

   ci = TColor::GetColor("#336633");
   Nominal_MCTruth_formatted__3->SetLineColor(ci);
   Nominal_MCTruth_formatted__3->SetLineWidth(2);
   Nominal_MCTruth_formatted__3->GetXaxis()->SetRange(1,3);
   Nominal_MCTruth_formatted__3->GetXaxis()->SetNdivisions(10103);
   Nominal_MCTruth_formatted__3->GetXaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__3->GetXaxis()->SetTitleOffset(1);
   Nominal_MCTruth_formatted__3->GetXaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__3->GetYaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__3->GetYaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__3->GetZaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__3->GetZaxis()->SetTitleOffset(1);
   Nominal_MCTruth_formatted__3->GetZaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__3->Draw("E same");
   
   TLegend *leg = new TLegend(0.7,0.72,0.9,0.88,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("Final_Nominal_Unfolded_formatted","unfolded","P");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(42);
   entry=leg->AddEntry("Nominal_MCTruth_formatted","MC truth","L");

   ci = TColor::GetColor("#336633");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TBox","stat. unc.","F");

   ci = TColor::GetColor("#666666");
   entry->SetFillColor(ci);
   entry->SetFillStyle(3345);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TBox","total unc.","F");

   ci = TColor::GetColor("#ccccff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   
   TPaveText *pt = new TPaveText(0.12,0.75,0.5,0.89,"brNDC");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextSize(0.04);
   TText *pt_LaTex = pt->AddText("#it{ATLAS} #bf{internal}");
   pt_LaTex = pt->AddText("#bf{#sqrt{s}= 13 TeV 140 fb^{-1}, ZZjj, p_{T}^{leading jet}}");
   pt->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "",0,0,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-0.375,-0.6944444,3.375,2.083333);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetTopMargin(0.03);
   p2->SetBottomMargin(0.25);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   Double_t xAxis4[4] = {0, 1, 2, 3}; 
   
   TH1D *Final_Nominal_Unfolded_formatted_unithist__4 = new TH1D("Final_Nominal_Unfolded_formatted_unithist__4","",3, xAxis4);
   Final_Nominal_Unfolded_formatted_unithist__4->SetBinContent(1,1);
   Final_Nominal_Unfolded_formatted_unithist__4->SetBinContent(2,1);
   Final_Nominal_Unfolded_formatted_unithist__4->SetBinContent(3,1);
   Final_Nominal_Unfolded_formatted_unithist__4->SetMinimum(0);
   Final_Nominal_Unfolded_formatted_unithist__4->SetMaximum(2);
   Final_Nominal_Unfolded_formatted_unithist__4->SetEntries(3);
   Final_Nominal_Unfolded_formatted_unithist__4->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted_unithist__4->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted_unithist__4->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted_unithist__4->GetXaxis()->SetTitle("p_{T}^{leading jet} [GeV]");
   Final_Nominal_Unfolded_formatted_unithist__4->GetXaxis()->SetRange(1,3);
   Final_Nominal_Unfolded_formatted_unithist__4->GetXaxis()->SetNdivisions(10103);
   Final_Nominal_Unfolded_formatted_unithist__4->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__4->GetXaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__4->GetXaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__4->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__4->GetYaxis()->SetTitle("relative unc.");
   Final_Nominal_Unfolded_formatted_unithist__4->GetYaxis()->SetNdivisions(10506);
   Final_Nominal_Unfolded_formatted_unithist__4->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__4->GetYaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__4->GetYaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__4->GetYaxis()->SetTitleOffset(0.53);
   Final_Nominal_Unfolded_formatted_unithist__4->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__4->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__4->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted_unithist__4->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__4->Draw("axis");
   box = new TBox(0,0.8800781,1,1.119922);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(1,0.7498997,2,1.2501);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(2,0.004210825,3,1.995789);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(0,0.9030418,1,1.096958);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(1,0.7872315,2,1.212769);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(2,0.09191006,3,1.90809);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   Double_t xAxis5[4] = {0, 1, 2, 3}; 
   
   TH1D *Final_Nominal_Unfolded_formatted_unithist__5 = new TH1D("Final_Nominal_Unfolded_formatted_unithist__5","",3, xAxis5);
   Final_Nominal_Unfolded_formatted_unithist__5->SetBinContent(1,1);
   Final_Nominal_Unfolded_formatted_unithist__5->SetBinContent(2,1);
   Final_Nominal_Unfolded_formatted_unithist__5->SetBinContent(3,1);
   Final_Nominal_Unfolded_formatted_unithist__5->SetMinimum(0);
   Final_Nominal_Unfolded_formatted_unithist__5->SetMaximum(2);
   Final_Nominal_Unfolded_formatted_unithist__5->SetEntries(3);
   Final_Nominal_Unfolded_formatted_unithist__5->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted_unithist__5->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted_unithist__5->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted_unithist__5->GetXaxis()->SetTitle("p_{T}^{leading jet} [GeV]");
   Final_Nominal_Unfolded_formatted_unithist__5->GetXaxis()->SetRange(1,3);
   Final_Nominal_Unfolded_formatted_unithist__5->GetXaxis()->SetNdivisions(10103);
   Final_Nominal_Unfolded_formatted_unithist__5->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__5->GetXaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__5->GetXaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__5->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__5->GetYaxis()->SetTitle("relative unc.");
   Final_Nominal_Unfolded_formatted_unithist__5->GetYaxis()->SetNdivisions(10506);
   Final_Nominal_Unfolded_formatted_unithist__5->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__5->GetYaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__5->GetYaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__5->GetYaxis()->SetTitleOffset(0.53);
   Final_Nominal_Unfolded_formatted_unithist__5->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__5->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__5->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted_unithist__5->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__5->Draw("same");
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
