void final_pt_zz()
{
//=========Macro generated from canvas: c/
//=========  (Thu Feb  6 12:15:20 2025) by ROOT version 6.20/00
   TCanvas *c = new TCanvas("c", "",122,56,800,800);
   gStyle->SetOptStat(0);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "",0,0.25,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-0.5,-4.756335,4.5,1.680729);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetLogy();
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   Double_t xAxis21[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Final_Nominal_Unfolded_formatted__21 = new TH1D("Final_Nominal_Unfolded_formatted__21","",4, xAxis21);
   Final_Nominal_Unfolded_formatted__21->SetBinContent(1,0.5081961);
   Final_Nominal_Unfolded_formatted__21->SetBinContent(2,0.135007);
   Final_Nominal_Unfolded_formatted__21->SetBinContent(3,0.03588569);
   Final_Nominal_Unfolded_formatted__21->SetBinContent(4,0.0007715637);
   Final_Nominal_Unfolded_formatted__21->SetMinimum(7.715637e-05);
   Final_Nominal_Unfolded_formatted__21->SetMaximum(10.88986);
   Final_Nominal_Unfolded_formatted__21->SetEntries(11104.11);
   Final_Nominal_Unfolded_formatted__21->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted__21->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted__21->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted__21->GetXaxis()->SetRange(1,4);
   Final_Nominal_Unfolded_formatted__21->GetXaxis()->SetNdivisions(10104);
   Final_Nominal_Unfolded_formatted__21->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__21->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__21->GetYaxis()->SetTitle("d#sigma/dp_{T}^{Z,Z} [fb/GeV]");
   Final_Nominal_Unfolded_formatted__21->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__21->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__21->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__21->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted__21->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__21->Draw("axis");
   TBox *box = new TBox(0,0.4718991,1,0.5444931);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(1,0.1101757,2,0.1598384);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(2,0.0239435,3,0.04782789);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(3,0.0005743869,4,0.0009687405);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(0,0.4946712,1,0.5217211);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(1,0.1229404,2,0.1470737);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(2,0.03019452,3,0.04157687);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(3,0.0006557496,4,0.0008873778);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   Double_t xAxis22[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Final_Nominal_Unfolded_formatted__22 = new TH1D("Final_Nominal_Unfolded_formatted__22","",4, xAxis22);
   Final_Nominal_Unfolded_formatted__22->SetBinContent(1,0.5081961);
   Final_Nominal_Unfolded_formatted__22->SetBinContent(2,0.135007);
   Final_Nominal_Unfolded_formatted__22->SetBinContent(3,0.03588569);
   Final_Nominal_Unfolded_formatted__22->SetBinContent(4,0.0007715637);
   Final_Nominal_Unfolded_formatted__22->SetMinimum(7.715637e-05);
   Final_Nominal_Unfolded_formatted__22->SetMaximum(10.88986);
   Final_Nominal_Unfolded_formatted__22->SetEntries(11104.11);
   Final_Nominal_Unfolded_formatted__22->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted__22->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted__22->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted__22->GetXaxis()->SetRange(1,4);
   Final_Nominal_Unfolded_formatted__22->GetXaxis()->SetNdivisions(10104);
   Final_Nominal_Unfolded_formatted__22->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__22->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__22->GetYaxis()->SetTitle("d#sigma/dp_{T}^{Z,Z} [fb/GeV]");
   Final_Nominal_Unfolded_formatted__22->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__22->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__22->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__22->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted__22->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__22->Draw("P E1 same");
   Double_t xAxis23[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Nominal_MCTruth_formatted__23 = new TH1D("Nominal_MCTruth_formatted__23","",4, xAxis23);
   Nominal_MCTruth_formatted__23->SetBinContent(1,0.4456203);
   Nominal_MCTruth_formatted__23->SetBinContent(2,0.1413375);
   Nominal_MCTruth_formatted__23->SetBinContent(3,0.03635953);
   Nominal_MCTruth_formatted__23->SetBinContent(4,0.0007024464);
   Nominal_MCTruth_formatted__23->SetEntries(9);

   ci = TColor::GetColor("#336633");
   Nominal_MCTruth_formatted__23->SetLineColor(ci);
   Nominal_MCTruth_formatted__23->SetLineWidth(2);
   Nominal_MCTruth_formatted__23->GetXaxis()->SetRange(1,4);
   Nominal_MCTruth_formatted__23->GetXaxis()->SetNdivisions(10104);
   Nominal_MCTruth_formatted__23->GetXaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__23->GetXaxis()->SetTitleOffset(1);
   Nominal_MCTruth_formatted__23->GetXaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__23->GetYaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__23->GetYaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__23->GetZaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__23->GetZaxis()->SetTitleOffset(1);
   Nominal_MCTruth_formatted__23->GetZaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__23->Draw("E same");
   
   TLegend *leg = new TLegend(0.7,0.72,0.9,0.88,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("Final_Nominal_Unfolded_formatted","unfolded","P");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(42);
   entry=leg->AddEntry("Nominal_MCTruth_formatted","MC truth","L");

   ci = TColor::GetColor("#336633");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TBox","stat. unc.","F");

   ci = TColor::GetColor("#666666");
   entry->SetFillColor(ci);
   entry->SetFillStyle(3345);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TBox","total unc.","F");

   ci = TColor::GetColor("#ccccff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   
   TPaveText *pt = new TPaveText(0.12,0.75,0.5,0.89,"brNDC");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextSize(0.04);
   TText *pt_LaTex = pt->AddText("#it{ATLAS} #bf{internal}");
   pt_LaTex = pt->AddText("#bf{#sqrt{s}= 13 TeV 140 fb^{-1}, inclusive, p_{T}^{Z,Z}}");
   pt->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "",0,0,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-0.5,0.1527778,4.5,1.541667);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetTopMargin(0.03);
   p2->SetBottomMargin(0.25);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   Double_t xAxis24[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Final_Nominal_Unfolded_formatted_unithist__24 = new TH1D("Final_Nominal_Unfolded_formatted_unithist__24","",4, xAxis24);
   Final_Nominal_Unfolded_formatted_unithist__24->SetBinContent(1,1);
   Final_Nominal_Unfolded_formatted_unithist__24->SetBinContent(2,1);
   Final_Nominal_Unfolded_formatted_unithist__24->SetBinContent(3,1);
   Final_Nominal_Unfolded_formatted_unithist__24->SetBinContent(4,1);
   Final_Nominal_Unfolded_formatted_unithist__24->SetMinimum(0.5);
   Final_Nominal_Unfolded_formatted_unithist__24->SetMaximum(1.5);
   Final_Nominal_Unfolded_formatted_unithist__24->SetEntries(4);
   Final_Nominal_Unfolded_formatted_unithist__24->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted_unithist__24->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted_unithist__24->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted_unithist__24->GetXaxis()->SetTitle("p_{T}^{Z,Z} [GeV]");
   Final_Nominal_Unfolded_formatted_unithist__24->GetXaxis()->SetRange(1,4);
   Final_Nominal_Unfolded_formatted_unithist__24->GetXaxis()->SetNdivisions(10104);
   Final_Nominal_Unfolded_formatted_unithist__24->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__24->GetXaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__24->GetXaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__24->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__24->GetYaxis()->SetTitle("relative unc.");
   Final_Nominal_Unfolded_formatted_unithist__24->GetYaxis()->SetNdivisions(10506);
   Final_Nominal_Unfolded_formatted_unithist__24->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__24->GetYaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__24->GetYaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__24->GetYaxis()->SetTitleOffset(0.53);
   Final_Nominal_Unfolded_formatted_unithist__24->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__24->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__24->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted_unithist__24->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__24->Draw("axis");
   box = new TBox(0,0.9285768,1,1.071423);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(1,0.8160739,2,1.183926);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(2,0.6672157,3,1.332784);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(3,0.7444452,4,1.255555);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(0,0.9733864,1,1.026614);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(1,0.910622,2,1.089378);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(2,0.8414083,3,1.158592);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(3,0.8498969,4,1.150103);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   Double_t xAxis25[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Final_Nominal_Unfolded_formatted_unithist__25 = new TH1D("Final_Nominal_Unfolded_formatted_unithist__25","",4, xAxis25);
   Final_Nominal_Unfolded_formatted_unithist__25->SetBinContent(1,1);
   Final_Nominal_Unfolded_formatted_unithist__25->SetBinContent(2,1);
   Final_Nominal_Unfolded_formatted_unithist__25->SetBinContent(3,1);
   Final_Nominal_Unfolded_formatted_unithist__25->SetBinContent(4,1);
   Final_Nominal_Unfolded_formatted_unithist__25->SetMinimum(0.5);
   Final_Nominal_Unfolded_formatted_unithist__25->SetMaximum(1.5);
   Final_Nominal_Unfolded_formatted_unithist__25->SetEntries(4);
   Final_Nominal_Unfolded_formatted_unithist__25->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted_unithist__25->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted_unithist__25->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted_unithist__25->GetXaxis()->SetTitle("p_{T}^{Z,Z} [GeV]");
   Final_Nominal_Unfolded_formatted_unithist__25->GetXaxis()->SetRange(1,4);
   Final_Nominal_Unfolded_formatted_unithist__25->GetXaxis()->SetNdivisions(10104);
   Final_Nominal_Unfolded_formatted_unithist__25->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__25->GetXaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__25->GetXaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__25->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__25->GetYaxis()->SetTitle("relative unc.");
   Final_Nominal_Unfolded_formatted_unithist__25->GetYaxis()->SetNdivisions(10506);
   Final_Nominal_Unfolded_formatted_unithist__25->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__25->GetYaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__25->GetYaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__25->GetYaxis()->SetTitleOffset(0.53);
   Final_Nominal_Unfolded_formatted_unithist__25->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__25->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__25->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted_unithist__25->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__25->Draw("same");
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
