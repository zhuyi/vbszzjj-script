void final_mt_zz()
{
//=========Macro generated from canvas: c/
//=========  (Thu Feb  6 12:15:12 2025) by ROOT version 6.20/00
   TCanvas *c = new TCanvas("c", "",122,56,800,800);
   gStyle->SetOptStat(0);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "",0,0.25,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-0.5,-4.848259,4.5,0.8217041);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetLogy();
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   Double_t xAxis16[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Final_Nominal_Unfolded_formatted__16 = new TH1D("Final_Nominal_Unfolded_formatted__16","",4, xAxis16);
   Final_Nominal_Unfolded_formatted__16->SetBinContent(1,0.0852238);
   Final_Nominal_Unfolded_formatted__16->SetBinContent(2,0.05254499);
   Final_Nominal_Unfolded_formatted__16->SetBinContent(3,0.01588836);
   Final_Nominal_Unfolded_formatted__16->SetBinContent(4,0.0005232837);
   Final_Nominal_Unfolded_formatted__16->SetMinimum(5.232837e-05);
   Final_Nominal_Unfolded_formatted__16->SetMaximum(1.797661);
   Final_Nominal_Unfolded_formatted__16->SetEntries(439664);
   Final_Nominal_Unfolded_formatted__16->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted__16->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted__16->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted__16->GetXaxis()->SetRange(1,4);
   Final_Nominal_Unfolded_formatted__16->GetXaxis()->SetNdivisions(10104);
   Final_Nominal_Unfolded_formatted__16->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__16->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__16->GetYaxis()->SetTitle("d#sigma/dm_{T}^{Z,Z} [fb/GeV]");
   Final_Nominal_Unfolded_formatted__16->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__16->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__16->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__16->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted__16->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__16->Draw("axis");
   TBox *box = new TBox(0,0.08056456,1,0.08988305);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(1,0.04795782,2,0.05713216);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(2,0.01425587,3,0.01752085);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(3,0.0004505661,4,0.0005960014);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(0,0.08288042,1,0.08756719);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(1,0.04945803,2,0.05563195);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(2,0.01454243,3,0.01723429);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(3,0.0004658036,4,0.0005807638);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   Double_t xAxis17[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Final_Nominal_Unfolded_formatted__17 = new TH1D("Final_Nominal_Unfolded_formatted__17","",4, xAxis17);
   Final_Nominal_Unfolded_formatted__17->SetBinContent(1,0.0852238);
   Final_Nominal_Unfolded_formatted__17->SetBinContent(2,0.05254499);
   Final_Nominal_Unfolded_formatted__17->SetBinContent(3,0.01588836);
   Final_Nominal_Unfolded_formatted__17->SetBinContent(4,0.0005232837);
   Final_Nominal_Unfolded_formatted__17->SetMinimum(5.232837e-05);
   Final_Nominal_Unfolded_formatted__17->SetMaximum(1.797661);
   Final_Nominal_Unfolded_formatted__17->SetEntries(439664);
   Final_Nominal_Unfolded_formatted__17->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted__17->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted__17->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted__17->GetXaxis()->SetRange(1,4);
   Final_Nominal_Unfolded_formatted__17->GetXaxis()->SetNdivisions(10104);
   Final_Nominal_Unfolded_formatted__17->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__17->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__17->GetYaxis()->SetTitle("d#sigma/dm_{T}^{Z,Z} [fb/GeV]");
   Final_Nominal_Unfolded_formatted__17->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__17->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__17->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__17->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted__17->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__17->Draw("P E1 same");
   Double_t xAxis18[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Nominal_MCTruth_formatted__18 = new TH1D("Nominal_MCTruth_formatted__18","",4, xAxis18);
   Nominal_MCTruth_formatted__18->SetBinContent(1,0.0798981);
   Nominal_MCTruth_formatted__18->SetBinContent(2,0.04846383);
   Nominal_MCTruth_formatted__18->SetBinContent(3,0.01416152);
   Nominal_MCTruth_formatted__18->SetBinContent(4,0.0004546058);
   Nominal_MCTruth_formatted__18->SetEntries(9);

   ci = TColor::GetColor("#336633");
   Nominal_MCTruth_formatted__18->SetLineColor(ci);
   Nominal_MCTruth_formatted__18->SetLineWidth(2);
   Nominal_MCTruth_formatted__18->GetXaxis()->SetRange(1,4);
   Nominal_MCTruth_formatted__18->GetXaxis()->SetNdivisions(10104);
   Nominal_MCTruth_formatted__18->GetXaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__18->GetXaxis()->SetTitleOffset(1);
   Nominal_MCTruth_formatted__18->GetXaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__18->GetYaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__18->GetYaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__18->GetZaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__18->GetZaxis()->SetTitleOffset(1);
   Nominal_MCTruth_formatted__18->GetZaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__18->Draw("E same");
   
   TLegend *leg = new TLegend(0.7,0.72,0.9,0.88,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("Final_Nominal_Unfolded_formatted","unfolded","P");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(42);
   entry=leg->AddEntry("Nominal_MCTruth_formatted","MC truth","L");

   ci = TColor::GetColor("#336633");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TBox","stat. unc.","F");

   ci = TColor::GetColor("#666666");
   entry->SetFillColor(ci);
   entry->SetFillStyle(3345);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TBox","total unc.","F");

   ci = TColor::GetColor("#ccccff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   
   TPaveText *pt = new TPaveText(0.12,0.75,0.5,0.89,"brNDC");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextSize(0.04);
   TText *pt_LaTex = pt->AddText("#it{ATLAS} #bf{internal}");
   pt_LaTex = pt->AddText("#bf{#sqrt{s}= 13 TeV 140 fb^{-1}, inclusive, m_{T}^{Z,Z}}");
   pt->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "",0,0,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-0.5,0.1527778,4.5,1.541667);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetTopMargin(0.03);
   p2->SetBottomMargin(0.25);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   Double_t xAxis19[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Final_Nominal_Unfolded_formatted_unithist__19 = new TH1D("Final_Nominal_Unfolded_formatted_unithist__19","",4, xAxis19);
   Final_Nominal_Unfolded_formatted_unithist__19->SetBinContent(1,1);
   Final_Nominal_Unfolded_formatted_unithist__19->SetBinContent(2,1);
   Final_Nominal_Unfolded_formatted_unithist__19->SetBinContent(3,1);
   Final_Nominal_Unfolded_formatted_unithist__19->SetBinContent(4,1);
   Final_Nominal_Unfolded_formatted_unithist__19->SetMinimum(0.5);
   Final_Nominal_Unfolded_formatted_unithist__19->SetMaximum(1.5);
   Final_Nominal_Unfolded_formatted_unithist__19->SetEntries(4);
   Final_Nominal_Unfolded_formatted_unithist__19->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted_unithist__19->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted_unithist__19->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted_unithist__19->GetXaxis()->SetTitle("m_{T}^{Z,Z} [GeV]");
   Final_Nominal_Unfolded_formatted_unithist__19->GetXaxis()->SetRange(1,4);
   Final_Nominal_Unfolded_formatted_unithist__19->GetXaxis()->SetNdivisions(10104);
   Final_Nominal_Unfolded_formatted_unithist__19->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__19->GetXaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__19->GetXaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__19->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__19->GetYaxis()->SetTitle("relative unc.");
   Final_Nominal_Unfolded_formatted_unithist__19->GetYaxis()->SetNdivisions(10506);
   Final_Nominal_Unfolded_formatted_unithist__19->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__19->GetYaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__19->GetYaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__19->GetYaxis()->SetTitleOffset(0.53);
   Final_Nominal_Unfolded_formatted_unithist__19->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__19->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__19->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted_unithist__19->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__19->Draw("axis");
   box = new TBox(0,0.9453294,1,1.054671);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(1,0.9127001,2,1.0873);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(2,0.8972526,3,1.102747);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(3,0.8610359,4,1.138964);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(0,0.9725031,1,1.027497);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(1,0.9412511,2,1.058749);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(2,0.9152883,3,1.084712);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(3,0.890155,4,1.109845);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   Double_t xAxis20[5] = {0, 1, 2, 3, 4}; 
   
   TH1D *Final_Nominal_Unfolded_formatted_unithist__20 = new TH1D("Final_Nominal_Unfolded_formatted_unithist__20","",4, xAxis20);
   Final_Nominal_Unfolded_formatted_unithist__20->SetBinContent(1,1);
   Final_Nominal_Unfolded_formatted_unithist__20->SetBinContent(2,1);
   Final_Nominal_Unfolded_formatted_unithist__20->SetBinContent(3,1);
   Final_Nominal_Unfolded_formatted_unithist__20->SetBinContent(4,1);
   Final_Nominal_Unfolded_formatted_unithist__20->SetMinimum(0.5);
   Final_Nominal_Unfolded_formatted_unithist__20->SetMaximum(1.5);
   Final_Nominal_Unfolded_formatted_unithist__20->SetEntries(4);
   Final_Nominal_Unfolded_formatted_unithist__20->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted_unithist__20->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted_unithist__20->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted_unithist__20->GetXaxis()->SetTitle("m_{T}^{Z,Z} [GeV]");
   Final_Nominal_Unfolded_formatted_unithist__20->GetXaxis()->SetRange(1,4);
   Final_Nominal_Unfolded_formatted_unithist__20->GetXaxis()->SetNdivisions(10104);
   Final_Nominal_Unfolded_formatted_unithist__20->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__20->GetXaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__20->GetXaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__20->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__20->GetYaxis()->SetTitle("relative unc.");
   Final_Nominal_Unfolded_formatted_unithist__20->GetYaxis()->SetNdivisions(10506);
   Final_Nominal_Unfolded_formatted_unithist__20->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__20->GetYaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__20->GetYaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__20->GetYaxis()->SetTitleOffset(0.53);
   Final_Nominal_Unfolded_formatted_unithist__20->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__20->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__20->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted_unithist__20->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__20->Draw("same");
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
