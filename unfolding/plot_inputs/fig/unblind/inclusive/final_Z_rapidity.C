void final_Z_rapidity()
{
//=========Macro generated from canvas: c/
//=========  (Thu Feb  6 12:15:36 2025) by ROOT version 6.20/00
   TCanvas *c = new TCanvas("c", "",122,56,800,800);
   gStyle->SetOptStat(0);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "",0,0.25,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-0.625,-1.201933,5.625,2.817776);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetLogy();
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   Double_t xAxis31[6] = {0, 1, 2, 3, 4, 5}; 
   
   TH1D *Final_Nominal_Unfolded_formatted__31 = new TH1D("Final_Nominal_Unfolded_formatted__31","",5, xAxis31);
   Final_Nominal_Unfolded_formatted__31->SetBinContent(1,12.05841);
   Final_Nominal_Unfolded_formatted__31->SetBinContent(2,11.10094);
   Final_Nominal_Unfolded_formatted__31->SetBinContent(3,9.299852);
   Final_Nominal_Unfolded_formatted__31->SetBinContent(4,5.809517);
   Final_Nominal_Unfolded_formatted__31->SetBinContent(5,1.585033);
   Final_Nominal_Unfolded_formatted__31->SetMinimum(0.1585033);
   Final_Nominal_Unfolded_formatted__31->SetMaximum(260.4986);
   Final_Nominal_Unfolded_formatted__31->SetEntries(439667);
   Final_Nominal_Unfolded_formatted__31->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted__31->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted__31->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted__31->GetXaxis()->SetRange(1,5);
   Final_Nominal_Unfolded_formatted__31->GetXaxis()->SetNdivisions(10105);
   Final_Nominal_Unfolded_formatted__31->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__31->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__31->GetYaxis()->SetTitle("d#sigma/dy^{Z} [fb]");
   Final_Nominal_Unfolded_formatted__31->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__31->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__31->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__31->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted__31->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__31->Draw("axis");
   TBox *box = new TBox(0,11.0919,1,13.02493);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(1,10.53115,2,11.67073);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(2,8.635972,3,9.963732);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(3,5.239322,4,6.379711);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(4,1.187948,5,1.982117);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(0,11.37007,1,12.74676);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(1,10.68778,2,11.5141);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(2,8.745107,3,9.854598);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(3,5.373465,4,6.245568);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(4,1.273588,5,1.896478);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   Double_t xAxis32[6] = {0, 1, 2, 3, 4, 5}; 
   
   TH1D *Final_Nominal_Unfolded_formatted__32 = new TH1D("Final_Nominal_Unfolded_formatted__32","",5, xAxis32);
   Final_Nominal_Unfolded_formatted__32->SetBinContent(1,12.05841);
   Final_Nominal_Unfolded_formatted__32->SetBinContent(2,11.10094);
   Final_Nominal_Unfolded_formatted__32->SetBinContent(3,9.299852);
   Final_Nominal_Unfolded_formatted__32->SetBinContent(4,5.809517);
   Final_Nominal_Unfolded_formatted__32->SetBinContent(5,1.585033);
   Final_Nominal_Unfolded_formatted__32->SetMinimum(0.1585033);
   Final_Nominal_Unfolded_formatted__32->SetMaximum(260.4986);
   Final_Nominal_Unfolded_formatted__32->SetEntries(439667);
   Final_Nominal_Unfolded_formatted__32->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted__32->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted__32->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted__32->GetXaxis()->SetRange(1,5);
   Final_Nominal_Unfolded_formatted__32->GetXaxis()->SetNdivisions(10105);
   Final_Nominal_Unfolded_formatted__32->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__32->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__32->GetYaxis()->SetTitle("d#sigma/dy^{Z} [fb]");
   Final_Nominal_Unfolded_formatted__32->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__32->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__32->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted__32->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted__32->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted__32->Draw("P E1 same");
   Double_t xAxis33[6] = {0, 1, 2, 3, 4, 5}; 
   
   TH1D *Nominal_MCTruth_formatted__33 = new TH1D("Nominal_MCTruth_formatted__33","",5, xAxis33);
   Nominal_MCTruth_formatted__33->SetBinContent(1,10.96377);
   Nominal_MCTruth_formatted__33->SetBinContent(2,10.59193);
   Nominal_MCTruth_formatted__33->SetBinContent(3,8.933872);
   Nominal_MCTruth_formatted__33->SetBinContent(4,4.765025);
   Nominal_MCTruth_formatted__33->SetBinContent(5,1.1271);
   Nominal_MCTruth_formatted__33->SetEntries(11);

   ci = TColor::GetColor("#336633");
   Nominal_MCTruth_formatted__33->SetLineColor(ci);
   Nominal_MCTruth_formatted__33->SetLineWidth(2);
   Nominal_MCTruth_formatted__33->GetXaxis()->SetRange(1,5);
   Nominal_MCTruth_formatted__33->GetXaxis()->SetNdivisions(10105);
   Nominal_MCTruth_formatted__33->GetXaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__33->GetXaxis()->SetTitleOffset(1);
   Nominal_MCTruth_formatted__33->GetXaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__33->GetYaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__33->GetYaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__33->GetZaxis()->SetLabelFont(42);
   Nominal_MCTruth_formatted__33->GetZaxis()->SetTitleOffset(1);
   Nominal_MCTruth_formatted__33->GetZaxis()->SetTitleFont(42);
   Nominal_MCTruth_formatted__33->Draw("E same");
   
   TLegend *leg = new TLegend(0.7,0.72,0.9,0.88,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("Final_Nominal_Unfolded_formatted","unfolded","P");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(42);
   entry=leg->AddEntry("Nominal_MCTruth_formatted","MC truth","L");

   ci = TColor::GetColor("#336633");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TBox","stat. unc.","F");

   ci = TColor::GetColor("#666666");
   entry->SetFillColor(ci);
   entry->SetFillStyle(3345);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TBox","total unc.","F");

   ci = TColor::GetColor("#ccccff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   
   TPaveText *pt = new TPaveText(0.12,0.75,0.5,0.89,"brNDC");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextSize(0.04);
   TText *pt_LaTex = pt->AddText("#it{ATLAS} #bf{internal}");
   pt_LaTex = pt->AddText("#bf{#sqrt{s}= 13 TeV 140 fb^{-1}, inclusive, |y^{Z}|}");
   pt->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "",0,0,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-0.625,0.1527778,5.625,1.541667);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetTopMargin(0.03);
   p2->SetBottomMargin(0.25);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   Double_t xAxis34[6] = {0, 1, 2, 3, 4, 5}; 
   
   TH1D *Final_Nominal_Unfolded_formatted_unithist__34 = new TH1D("Final_Nominal_Unfolded_formatted_unithist__34","",5, xAxis34);
   Final_Nominal_Unfolded_formatted_unithist__34->SetBinContent(1,1);
   Final_Nominal_Unfolded_formatted_unithist__34->SetBinContent(2,1);
   Final_Nominal_Unfolded_formatted_unithist__34->SetBinContent(3,1);
   Final_Nominal_Unfolded_formatted_unithist__34->SetBinContent(4,1);
   Final_Nominal_Unfolded_formatted_unithist__34->SetBinContent(5,1);
   Final_Nominal_Unfolded_formatted_unithist__34->SetMinimum(0.5);
   Final_Nominal_Unfolded_formatted_unithist__34->SetMaximum(1.5);
   Final_Nominal_Unfolded_formatted_unithist__34->SetEntries(5);
   Final_Nominal_Unfolded_formatted_unithist__34->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted_unithist__34->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted_unithist__34->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted_unithist__34->GetXaxis()->SetTitle("|y^{Z}|");
   Final_Nominal_Unfolded_formatted_unithist__34->GetXaxis()->SetRange(1,5);
   Final_Nominal_Unfolded_formatted_unithist__34->GetXaxis()->SetNdivisions(10105);
   Final_Nominal_Unfolded_formatted_unithist__34->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__34->GetXaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__34->GetXaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__34->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__34->GetYaxis()->SetTitle("relative unc.");
   Final_Nominal_Unfolded_formatted_unithist__34->GetYaxis()->SetNdivisions(10506);
   Final_Nominal_Unfolded_formatted_unithist__34->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__34->GetYaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__34->GetYaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__34->GetYaxis()->SetTitleOffset(0.53);
   Final_Nominal_Unfolded_formatted_unithist__34->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__34->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__34->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted_unithist__34->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__34->Draw("axis");
   box = new TBox(0,0.9198471,1,1.080153);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(1,0.9486718,2,1.051328);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(2,0.9286139,3,1.071386);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(3,0.9018517,4,1.098148);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(4,0.7494786,5,1.250521);

   ci = TColor::GetColor("#ccccff");
   box->SetFillColor(ci);
   box->Draw();
   box = new TBox(0,0.9429159,1,1.057084);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(1,0.9627813,2,1.037219);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(2,0.940349,3,1.059651);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(3,0.9249419,4,1.075058);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   box = new TBox(4,0.8035088,5,1.196491);

   ci = TColor::GetColor("#666666");
   box->SetFillColor(ci);
   box->SetFillStyle(3345);
   box->Draw();
   Double_t xAxis35[6] = {0, 1, 2, 3, 4, 5}; 
   
   TH1D *Final_Nominal_Unfolded_formatted_unithist__35 = new TH1D("Final_Nominal_Unfolded_formatted_unithist__35","",5, xAxis35);
   Final_Nominal_Unfolded_formatted_unithist__35->SetBinContent(1,1);
   Final_Nominal_Unfolded_formatted_unithist__35->SetBinContent(2,1);
   Final_Nominal_Unfolded_formatted_unithist__35->SetBinContent(3,1);
   Final_Nominal_Unfolded_formatted_unithist__35->SetBinContent(4,1);
   Final_Nominal_Unfolded_formatted_unithist__35->SetBinContent(5,1);
   Final_Nominal_Unfolded_formatted_unithist__35->SetMinimum(0.5);
   Final_Nominal_Unfolded_formatted_unithist__35->SetMaximum(1.5);
   Final_Nominal_Unfolded_formatted_unithist__35->SetEntries(5);
   Final_Nominal_Unfolded_formatted_unithist__35->SetLineWidth(2);
   Final_Nominal_Unfolded_formatted_unithist__35->SetMarkerStyle(20);
   Final_Nominal_Unfolded_formatted_unithist__35->SetMarkerSize(1.5);
   Final_Nominal_Unfolded_formatted_unithist__35->GetXaxis()->SetTitle("|y^{Z}|");
   Final_Nominal_Unfolded_formatted_unithist__35->GetXaxis()->SetRange(1,5);
   Final_Nominal_Unfolded_formatted_unithist__35->GetXaxis()->SetNdivisions(10105);
   Final_Nominal_Unfolded_formatted_unithist__35->GetXaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__35->GetXaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__35->GetXaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__35->GetXaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__35->GetYaxis()->SetTitle("relative unc.");
   Final_Nominal_Unfolded_formatted_unithist__35->GetYaxis()->SetNdivisions(10506);
   Final_Nominal_Unfolded_formatted_unithist__35->GetYaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__35->GetYaxis()->SetLabelSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__35->GetYaxis()->SetTitleSize(0.0945);
   Final_Nominal_Unfolded_formatted_unithist__35->GetYaxis()->SetTitleOffset(0.53);
   Final_Nominal_Unfolded_formatted_unithist__35->GetYaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__35->GetZaxis()->SetLabelFont(42);
   Final_Nominal_Unfolded_formatted_unithist__35->GetZaxis()->SetTitleOffset(1);
   Final_Nominal_Unfolded_formatted_unithist__35->GetZaxis()->SetTitleFont(42);
   Final_Nominal_Unfolded_formatted_unithist__35->Draw("same");
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
