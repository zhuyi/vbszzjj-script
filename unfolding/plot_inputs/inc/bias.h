#ifndef BIAS_H
#define BIAS_H

#include <map>
#include <vector>

#include "TString.h"

using bias_map = std::map<TString, std::vector<double>>;
/*
bias_map inclusive_bias = {{"CP_ZZ_1", {0.0113279, 0.0192906, 0.00519818, 0.01418, 0.0145808, 0.0359395, 0.012939}},
                           {"CP_ZZ_2", {0.00850229, 0.00238806, 0.00861009, 0.0150919, 0.00239091, 0.0191327, 0.00373367, 0.000660502, 0.00500626}},
                           {"dphill",  {0.00059952, 0.00551133, 0.00977199, 0.00972763, 0.00902062}},
                           {"leading_pT_lepton", {0.00357356, 0.0356295, 0.00793167, 0.0110104, 0.0139064, 0.00417163}},
                           {"mt_zz",   {0.0168269, 0.000615006, 0.000622278, 0.00190718}},
                           {"n_jets",  {0.00786925, 0.0371058, 0.0483668, 0.0718673}},
                           {"ptz",     {0.0116851, 0.0126103, 0.00924784, 0.0146431, 0.0182039}},
                           {"pt_zz",   {0.000605327, 0.290043, 0.0577889, 0.004914}},
                           {"Z_rapidity", {0.0098401, 0.0113493, 0.00308261, 0.00610128, 0.00606796}}};

bias_map ZZjj_bias = {{"dphill",  {0.0712897, 0.0494678, 0.169122, 0.205882, 0.0960699}},
                      {"leading_jet_pt", {0.660725, 0.0868455, 0.0355422}},
                      {"mjj",     {0.326059, 0.0494368, 0.}},
                      {"mt_zz",   {0.266586, 0.158422, 0.00690955, 0.00483676}},
                      {"ptz",     {0.186957, 0.115459, 0.0457038, 0.0301568, 0.0221402}},
                      {"pt_zz",   {0.731692, 0.635567, 0.182575, 0.0171569, 0.00436681}},
                      {"Z_rapidity", {0.106832, 0.0416934, 0.0176722, 0.0313631, 0.0362854}}};
*/
bias_map inclusive_bias = {{"CP_ZZ_1",    {0.056, 0.006, 0.007, 0.012, 0.041, 0.045, 0.012}},
                           {"CP_ZZ_2",    {0.057, 0.010, 0.043, 0.000, 0.012, 0.010, 0.053, 0.029, 0.017}},
                           {"dphill",     {0.003, 0.008, 0.010, 0.027, 0.031}},
                           {"leading_pT_lepton", {0.027, 0.014, 0.027, 0.028, 0.017, 0.005}},
                           {"mt_zz",      {0.025, 0.018, 0.002, 0.001}},
                           {"n_jets",     {0.005, 0.035, 0.087, 0.080}},
                           {"pt_zz",      {0.004, 0.036, 0.079, 0.085}},
                           {"ptz",        {0.035, 0.008, 0.008, 0.006, 0.007}},
                           {"Z_rapidity", {0.030, 0.012, 0.010, 0.000, 0.023}}};

bias_map ZZjj_bias = {{"dphill",     {0.038, 0.021, 0.064, 0.060, 0.063}},
                      {"leading_jet_pt", {0.029, 0.057, 0.066}},
                      {"mjj",        {0.062, 0.183, 0.214}},
                      {"mt_zz",      {0.063, 0.069, 0.043, 0.093}},
                      {"pt_zz",      {0.061, 0.008, 0.046, 0.113, 0.001}},
                      {"ptz",        {0.085, 0.058, 0.129, 0.145, 0.024}},
                      {"Z_rapidity", {0.042, 0.059, 0.058, 0.026}}};

std::map<TString, bias_map> bias = {{"inclusive", inclusive_bias},
                                    {"ZZjj",      ZZjj_bias}};

#endif // BIAS_H
