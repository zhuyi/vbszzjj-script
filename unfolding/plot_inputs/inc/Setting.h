#ifndef SETTING_H
#define SETTING_H

#include "TString.h"
#include "TCut.h"

struct Setting
{
    TString measurement;

    TCut pre_selection;
    TCut truth_preselection;

    TCut SR;
    TCut FR;

    std::map<TString, std::vector<double>> binnings;

    std::map<TString, TString> titles;

    bool operator==(const TString &rhs)
    {
        return measurement == rhs;
    }

    std::vector<double> get_binning(const TString &var) const
    {
        if (binnings.count(var))
            return binnings.at(var);
        return {};
    }
};

const Setting inclusive {"inclusive", // measurement

//                         "passPresel==1&&"
                         "met_tst>70&&(event_type==0||event_type==1)&&event_3CR==0&&"
                         "M2Lep>80&&M2Lep<100&&leading_pT_lepton>30&&subleading_pT_lepton>20", // pre_selection

                         "truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20&&"
                         "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5&&"
                         "truth_M2Lep>76&&truth_M2Lep<106&&"
                         "(truth_event_type==1||truth_event_type==0)", // truth_preselection

                         "met_tst>110&&MetOHT>0.65&&abs(dLepR)<1.8&&abs(dMetZPhi)>2.2&&n_bjets==0", // SR
                         "truth_met_tst>95&&truth_MetOHT_neu>0.65&&abs(truth_dLepR)<1.8&&abs(truth_dMetZPhi)>1.8", // FR

                         {
                          {"Z_pT",              {50, 155, 230, 300, 400, 1500}},
                          {"mT_ZZ",             {200., 350., 450., 600., 3000.}},
                          {"n_jets",            {0., 1., 2., 3., 11.}},
                          //{"leading_jet_pt",    {30., 40., 60., 100., 1500.}},
                          {"leading_jet_pt",    {30, 65, 100, 150, 250, 1500}},
                          {"leading_pT_lepton", {30., 90., 110., 130., 160., 200., 900.}},
                          {"dphill",            {0., 0.3, 0.6, 1., 1.4, 1.8}},
                          {"pt_zz",             {0., 30., 60., 100., 1500.}},
                          {"Z_rapidity",        {0, 0.4, 1.2, 1.7, 2.2, 2.7}},
                          {"CP_ZZ_1",           {-1., -0.6, -0.3, -0.1, 0.1, 0.3, 0.6, 1.}},
                          {"CP_ZZ_2",           {-1., -0.75, -0.5, -0.3, -0.1, 0.1, 0.3, 0.5, 0.75, 1.}},

//                          {"isMC", {-1, 2}},
                          {"subleading_pT_lepton", {0,  30,  40,  50,  60,  70,  80,  90, 110, 120, 140, 160, 200, 1000}},
                          {"dMetZPhi",          {1.8, 2.2, 2.5, 2.65, 2.8, 2.9, 3.0, 3.1, 3.2}}
                          
                         }, // binnings

                         {
                          {"Z_pT",              "p_{T}^{Z} [GeV]"},
                          {"mT_ZZ",             "m_{T}^{ZZ} [GeV]"},
                          {"n_jets",            "No. of jets"},
                          {"leading_jet_pt",    "p_{T}^{leading jet} [GeV]"},
                          {"leading_pT_lepton", "p_{T}^{leading lepton} [GeV]"},
                          {"dphill",            "#Delta#Phi(l,l)"},
                          {"pt_zz",             "p_{T}^{ZZ} [GeV]"},
                          {"Z_rapidity",        "y(Z)"},
                          {"CP_ZZ_1",           "sin(#phi_{1})cos(#theta_{1})"},
                          {"CP_ZZ_2",           "sin(#phi_{2})cos(#theta_{2})"},

                          {"subleading_pT_lepton", "p_{T}^{subleading lepton} [GeV]"},
                          {"dMetZPhi",          "#Delta#Phi(MET,Z)"},
                         } // titles
                        };

const Setting ZZjj      {"ZZjj", // measurement

//                         "passPresel==1&&"
                         "met_tst>70&&(event_type==0||event_type==1)&&event_3CR==0&&"
                         "M2Lep>80&&M2Lep<100&&"
                         "leading_pT_lepton>30&&subleading_pT_lepton>20&&"
                         "leading_jet_pt>30&&second_jet_pt>30", // pre_selection

                         "truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20&&"
                         "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5&&"
//                         "truth_leading_jet_pt>30&&truth_n_jets>=2",
                         "truth_leading_jet_pt>30&&truth_second_jet_pt>30&&"
                         "abs(truth_leading_jet_eta)<4.5&&abs(truth_second_jet_eta)<4.5&&"
                         "truth_M2Lep>76&&truth_M2Lep<106&&"
                         "(truth_event_type==1||truth_event_type==0)", // truth_preselection

                         "met_tst>150&&abs(dLepR)<1.8&&abs(dMetZPhi)>2.2&&MetOHT>0.65&&n_bjets==0", // SR
                         "truth_met_tst>130&&abs(truth_dLepR)<1.8&&abs(truth_dMetZPhi)>1.8&&truth_MetOHT_neu>0.65", // FR

                         {
                          {"Z_pT",              {50, 155, 230, 300, 400, 1500}},
                          {"mT_ZZ",             {200., 350., 450., 600., 3000.}},
                          {"leading_jet_pt",    {30., 100., 300., 1500.}},
                          {"dphill",            {0., 0.3, 0.6, 1., 1.4, 1.8}},
                          {"pt_zz",             {0., 50., 100., 150., 250., 1500.}},
                          {"Z_rapidity",        {0, 0.4, 1.2, 1.7, 2.2, 2.7}},
                          {"mjj",               {0., 400., 800., 8000.}},
//                          {"CP_ZZ_1",           {-1., -0.75, -0.5, -0.3, -0.1, 0.1, 0.3, 0.5, 0.75, 1.}},
//                          {"CP_ZZ_1",           {-1., -0.6, -0.3, -0.1, 0.1, 0.3, 0.6, 1.}},
//                          {"CP_ZZ_2",           {-1., -0.75, -0.5, -0.3, -0.1, 0.1, 0.3, 0.5, 0.75, 1.}},

                          {"subleading_pT_lepton", {0,  30,  40,  50,  60,  70,  80,  90, 100, 110, 120, 140, 160, 200, 1000}},
                          {"dMetZPhi",          {1.8, 2.2, 2.5, 2.65, 2.8, 2.9, 3.0, 3.1, 3.2}}
                         }, // binnings

                         {
                          {"Z_pT",              "p_{T}^{Z} [GeV]"},
                          {"mT_ZZ",             "m_{T}^{ZZ} [GeV]"},
                          {"leading_jet_pt",    "p_{T}^{leading jet} [GeV]"},
                          {"dphill",            "#Delta#Phi(l,l)"},
                          {"pt_zz",             "p_{T}^{ZZ} [GeV]"},
                          {"Z_rapidity",        "y(Z)"},
                          {"mjj",               "m_{jj} [GeV]"},

                          {"subleading_pT_lepton", "p_{T}^{subleading lepton} [GeV]"},
                          {"dMetZPhi",          "#Delta#Phi(MET,Z)"},
                         } // binnings
                        };

std::map<TString, const Setting> settings = {{"inclusive", inclusive}, {"ZZjj", ZZjj}};

std::map<TString, TString> titles = {
                                     {"dphill",            "#Delta#Phi(l,l)"},
                                     {"leading_jet_pt",    "p_{T}^{leading jet} [GeV]"},
                                     {"leading_pT_lepton", "p_{T}^{leading lepton} [GeV]"},
                                     {"mjj",               "m_{jj} [GeV]"},
                                     {"mT_ZZ",             "m_{T}^{ZZ} [GeV]"},
                                     {"mt_zz",             "m_{T}^{ZZ} [GeV]"},
                                     {"pt_zz",             "p_{T}^{ZZ} [GeV]"},
                                     {"ptz",               "p_{T}^{Z} [GeV]"},
                                     {"n_jets",            "No. of jets"},
                                     {"Z_rapidity",        "y(Z)"},
                                     {"Z_pT",              "p_{T}^{Z} [GeV]"},
                                     {"CP_ZZ_1",           "sin(#phi)cos(#theta) 1"},
                                     {"CP_ZZ_2",           "sin(#phi)cos(#theta) 2"},
                                    };

#endif // SETTING_H
