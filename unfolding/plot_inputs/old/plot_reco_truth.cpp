void plot_reco_truth(TString mode = "")
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    TCanvas *c = new TCanvas("c", "", 800, 600);
    c->SetLogy();

    TFile *file = new TFile("response.root", "read");
    TH1F *fidcorrden = nullptr;
    TH1F *effcorrden = nullptr;
    file->GetObject("nominal/fidcorrden", fidcorrden);
    file->GetObject("nominal/effcorrden", effcorrden);

    //effcorrden->SetMaximum(1000.);
    fidcorrden->SetLineWidth(2);
    fidcorrden->SetLineColor(kBlack);
    effcorrden->SetTitle(" ");
    effcorrden->SetLineWidth(2);
    effcorrden->SetLineColor(kBlue);

//    effcorrden->Draw();
//    fidcorrden->Draw("same");

    double max = fidcorrden->GetMaximum() > effcorrden->GetMaximum() ? 1.2*fidcorrden->GetMaximum() : 1.2*effcorrden->GetMaximum();
    double min = fidcorrden->GetMinimum() < effcorrden->GetMinimum() ? 0.5*fidcorrden->GetMinimum() : 0.5*effcorrden->GetMinimum();
    effcorrden->SetMaximum(max);
    effcorrden->SetMinimum(min);

    TRatioPlot *rp = new TRatioPlot(effcorrden, fidcorrden);
    rp->SetH1DrawOpt("hist");
    rp->SetH2DrawOpt("hist");
    rp->Draw();

    rp->SetLowBottomMargin(0.4);
    rp->SetLeftMargin(0.13);
    rp->SetSeparationMargin(0.);

    //rp->GetUpperRefXaxis()->SetTitle("E_{T}^{missing} [GeV]");
    //rp->GetUpperRefXaxis()->SetTitle("p_{T}^{Z} [GeV]");
    //rp->GetUpperRefXaxis()->SetTitle("No. of jets");
    rp->GetUpperRefXaxis()->SetTitle("#Delta#Phi(l,l)");
    rp->GetUpperRefYaxis()->SetTitle("events/bin");
    rp->GetUpperRefYaxis()->SetTitleSize(0.03);
    rp->GetUpperRefYaxis()->SetTitleOffset(1.9);
    rp->GetUpperRefYaxis()->SetLabelSize(0.025);
    rp->GetLowerRefXaxis()->SetTitleSize(0.03);
    rp->GetLowerRefXaxis()->SetTitleOffset(1.5);
    rp->GetLowerRefXaxis()->SetLabelSize(0.025);
    rp->GetLowerRefYaxis()->SetTitle("#frac{truth}{reco}");
    rp->GetLowerRefYaxis()->SetTitleSize(0.03);
    rp->GetLowerRefYaxis()->SetTitleOffset(1.9);
    //rp->GetLowerRefYaxis()->SetRangeUser(0.92, 1.08);
    rp->GetLowerRefYaxis()->SetLabelSize(0.025);

    rp->GetLowerRefGraph()->SetMarkerStyle(21);
    rp->GetLowerRefGraph()->SetMarkerColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerSize(1.);

    rp->GetUpperPad()->Update();
    rp->GetUpperPad()->cd();
    TPaveText* textatlas = new TPaveText(0.49, 0.70, 0.90, 0.88, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(12);
    textatlas->SetTextSize(0.04);
    textatlas->AddText("#it{ATLAS} #bf{internal}");
    textatlas->AddText("#bf{#intLdt = 139fb^{-1}, #sqrt{s} = 13TeV, ee+#mu#mu, mc16ade}");
    textatlas->Draw();

    TLegend *legend = new TLegend(0.70, 0.59, 0.90, 0.69);
    legend->AddEntry(fidcorrden, "reco distribution",  "L");
    legend->AddEntry(effcorrden, "truth distribution", "L");
    legend->SetTextSize(0.04);
    legend->SetBorderSize(0);
    legend->Draw();

    TString name("");
    if(mode == "") name = "reco_vs_truth.png";
    else           name = mode + "_reco_vs_truth.png";
    c->SaveAs("fig/" + name);
}
