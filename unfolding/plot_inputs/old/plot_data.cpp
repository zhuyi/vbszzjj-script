void plot_data(TString mode = "")
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    TCanvas *c = new TCanvas("c", "", 800, 600);

    TFile *response = new TFile("response.root", "read");
    TFile *back = new TFile("background.root", "read");
    TFile *data = new TFile("data.root", "read");
    TH1F *fidcorrden = nullptr;
    TH1F *back_rec   = nullptr;
    TH1F *data_rec   = nullptr;
    response->GetObject("nominal/fidcorrden", fidcorrden);
    back->GetObject("nominal/reconstructed", back_rec);
    data->GetObject("nominal/reconstructed", data_rec);

    std::cout << "inclusive signals: " << fidcorrden->GetSumOfWeights() << std::endl;
    std::cout << "background: " << back_rec->GetSumOfWeights() << std::endl;
    std::cout << "data: " << data_rec->GetSumOfWeights() << std::endl;

    back_rec->Add(fidcorrden);
    back_rec->SetTitle("");
    back_rec->SetLineWidth(2);
    back_rec->SetLineColor(kBlue);
    back_rec->SetMinimum(fidcorrden->GetMinimum()*.5);
    back_rec->SetMaximum(back_rec->GetMaximum()*1.2);
    data_rec->SetMarkerStyle(20);
    //data_rec->SetLineWidth(2);
    //data_rec->SetLineColor(kBlue);

    TRatioPlot *rp = new TRatioPlot(back_rec, data_rec);
    rp->SetH1DrawOpt("hist");
    rp->SetH2DrawOpt("p e1");
    rp->Draw("same");

    rp->GetUpperPad()->SetLogy();
    //rp->GetLowerPad()->SetLogy();

    rp->SetLowBottomMargin(0.4);
    rp->SetLeftMargin(0.12);
    rp->SetSeparationMargin(0.);

    //rp->GetUpperRefXaxis()->SetTitle("E_{T}^{missing} [GeV]");
    //rp->GetUpperRefXaxis()->SetTitle("p_{T}^{Z} [GeV]");
    rp->GetUpperRefXaxis()->SetTitle("No. of jets");
    rp->GetUpperRefYaxis()->SetTitle("events/bin");
    rp->GetUpperRefYaxis()->SetTitleSize(0.03);
    rp->GetUpperRefYaxis()->SetTitleOffset(1.9);
    rp->GetUpperRefYaxis()->SetLabelSize(0.025);
    rp->GetLowerRefXaxis()->SetTitleSize(0.03);
    rp->GetLowerRefXaxis()->SetTitleOffset(1.5);
    rp->GetLowerRefXaxis()->SetLabelSize(0.025);
    rp->GetLowerRefYaxis()->SetTitle("#frac{mc}{data}");
    rp->GetLowerRefYaxis()->SetTitleSize(0.03);
    rp->GetLowerRefYaxis()->SetTitleOffset(1.9);
    //rp->GetLowerRefYaxis()->SetRangeUser(5e-1, 5);
    rp->GetLowerRefYaxis()->SetLabelSize(0.025);

    rp->GetLowerRefGraph()->SetMarkerStyle(21);
    rp->GetLowerRefGraph()->SetMarkerColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerSize(1.);

    rp->GetUpperPad()->Update();
    rp->GetUpperPad()->cd();

    fidcorrden->SetLineColor(kGreen + 3);
    fidcorrden->SetLineWidth(2);
    fidcorrden->Draw("same hist");

    TPaveText* textatlas = new TPaveText(0.47, 0.68, 0.90, 0.88, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(12);
    textatlas->SetTextSize(0.04);
    textatlas->AddText("#it{ATLAS} #bf{internal}");
    textatlas->AddText("#bf{#intLdt = 139fb^{-1}, #sqrt{s} = 13TeV, ee+#mu#mu, mc16ade}");
    dynamic_cast<TText*>(textatlas->GetListOfLines()->First())->SetTextSize(0.05);
    textatlas->Draw();

    TLegend *legend = new TLegend(0.63, 0.55, 0.90, 0.68);
    legend->AddEntry(back_rec,   "MC bkg. distribution", "L");
    legend->AddEntry(fidcorrden, "MC sig. distribution", "L");
    legend->AddEntry(data_rec,   "data distribution",    "PE");
    legend->SetTextSize(0.04);
    legend->SetBorderSize(0);
    legend->Draw();

    TString name("");
    if(mode == "") name = "data.png";
    else           name = mode + "_data.png";
    c->SaveAs("fig/" + name);
}
