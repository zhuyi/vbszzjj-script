void set_palette()
{
    gStyle->SetOptStat(0);
    int my_palette[100];
    double stops[] = {0., 0.5, 1.};
    double red[]   = {255./255, 255./255, 1./255};
    double green[] = {178./255, 255./255, 88./255};
    double blue[]  = {0./255,   255./255, 209./255};
    int FI = TColor::CreateGradientColorTable(3, stops, red, green, blue, 100);
    for (int i = 0; i<100; i++) my_palette[i] = FI + i;
    gStyle->SetPalette(100, my_palette);
}

void format_matrix(TH2D *matrix, TH1D *baseline)
{
    std::vector<double> bins;
    for(int i = 1; i <= baseline->GetNbinsX() + 1; i++)
        bins.push_back(baseline->GetXaxis()->GetBinLowEdge(i));

    matrix->SetTitle("");
    matrix->GetXaxis()->SetNdivisions(baseline->GetNbinsX(), 0, 0);
    matrix->GetYaxis()->SetNdivisions(baseline->GetNbinsX(), 0, 0);
    for(int i = 0; i <= baseline->GetNbinsX(); i++)
    {
        matrix->GetXaxis()->ChangeLabel(i + 1, -1, -1, -1, -1, -1, TString::Format("%.0f", bins.at(i)));
        matrix->GetYaxis()->ChangeLabel(i + 1, -1, -1, -1, -1, -1, TString::Format("%.0f", bins.at(i)));
    }
    matrix->GetXaxis()->SetTitle(baseline->GetXaxis()->GetTitle());
    matrix->GetYaxis()->SetTitle(baseline->GetXaxis()->GetTitle());
}

void plot_correlation()
{
    set_palette();
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    auto file = new TFile("Results_n_jets.root", "READ");

    auto baseline = file->Get<TH1D>("Final/Final_Nominal_Unfolded");
    auto combined = file->Get<TH2D>("Final/Final_CombinedCorrelationMatrix");
    auto systematic = file->Get<TH2D>("Final/Final_SystematicCorrelationMatrix");
    format_matrix(combined,   baseline);
    format_matrix(systematic, baseline);

    auto c = new TCanvas("c", "", 800., 600.);
    combined->Draw("colz text");
    c->SaveAs("fig/combined_correlation.png");
    systematic->Draw("colz text");
    c->SaveAs("fig/systematic_correlation.png");
}
