void set_palette()
{
    gStyle->SetOptStat(0);
    int my_palette[100];
    double stops[] = {0., 1.};
    double red[]   = {255./255, 1./255  };
    double green[] = {255./255, 88./255 };
    double blue[]  = {255./255, 209./255};
    int FI = TColor::CreateGradientColorTable(2, stops, red, green, blue, 100);
    for (int i = 0; i < 100; i++) my_palette[i] = FI + i;
    gStyle->SetPalette(100, my_palette);

    gStyle->SetPaintTextFormat(".2f");
}

void format_matrix(TH2D *matrix)
{
    matrix->SetTitle("");

    int nbinx = matrix->GetNbinsX();
    int nbiny = matrix->GetNbinsY();
    for(int j = 0; j < nbiny; j++)
    {
        double nevent = 0.;

        for(int i = 0; i < nbinx; i++)
            nevent += matrix->GetBinContent(i + 1, j + 1);

        for(int i = 0; i < nbinx; i++)
            matrix->SetBinContent(i + 1, j + 1, matrix->GetBinContent(i + 1, j + 1)/nevent);
    }
}

void plot_migration(TString path)
{
    set_palette();
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    TString name = "response.root";
    if (path.Length()) name = path + "/" + name;
    auto file = new TFile(name, "READ");
    if (!file->IsOpen())
    {
        std::cerr << "[Error] ==> Failed opening the file " + name << std::endl;
        exit(-1);
    }

    auto migration = file->Get<TH2D>("nominal/migration_matrix");
    format_matrix(migration);

    auto c = new TCanvas("c", "", 800., 600.);
    migration->Draw("colz text");
    c->SaveAs("fig/migration.png");
}
