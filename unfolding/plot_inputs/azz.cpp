#include "inc/MC.h"
#include "inc/Setting.h"

namespace setting
{
    enum space {full, fiducial};
}

using RNode = ROOT::RDF::RNode;

RNode get_init_node(const TString &measurement, const TString &channel, setting::space space)
{
    MC mcs;
    std::vector<std::string> files;

    std::string path = (space == setting::full) ? mcs.mc_path_fullspace.Data() : mcs.mc_path_fiducial.Data();

    for(const auto &file : mcs.mcs.at(channel))
        files.push_back(path + file.Data());

    std::string pre_selection = settings.at(measurement).truth_preselection.GetTitle();
    std::string selection     = settings.at(measurement).FR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Define("truth_abs_Z_rapidity", "abs(truth_Z_rapidity)")
               .Define("abs_dphill", "abs(dphill)")
               .Define("abs_Z_rapidity", "abs(Z_rapidity)")
               .Define("weight_final", "scale*ew_correction*tautau_correction*weight")
               .Filter(pre_selection).Filter(selection);
}

TH1D* get_hist(RNode &node, const TString &var, const TString &measurement)
{
    auto var_to_plot = var;
    if(var == "dphill") var_to_plot = "abs_dphill";
    else if(var == "Z_rapidity") var_to_plot = "abs_Z_rapidity";

    int n_bins = settings.at(measurement).get_binning(var).size();
    auto xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(var).at(i);

    TString reweighted_hist_name = var;

    TString weight_final = "scale*ew_correction*tautau_correction";
    auto hist = new TH1D(*node.Histo1D({reweighted_hist_name, "", n_bins - 1, xbins}, var_to_plot.Data(), "weight_final"));

    delete [] xbins;

    return hist;
}

void azz()
{
    TString measurement = "inclusive";
    TString channel     = "inclusive";

    auto node_full = get_init_node(measurement, channel, setting::full);
    auto node_fidu = get_init_node(measurement, channel, setting::fiducial);

    std::vector<TString> vars = {"n_jets",
//                                 "dphill",
//                                 "leading_jet_pt",
//                                 "leading_pT_lepton",
//                                 "mT_ZZ",
//                                 "pt_zz",
//                                 "Z_pT",
//                                 "Z_rapidity"
                                };

    for(const auto &var : vars)
    {
        auto hist_full = get_hist(node_full, var, measurement);
        auto hist_fidu = get_hist(node_fidu, var, measurement);

        hist_full->Print("range");
        hist_fidu->Print("range");

        int n_bins = hist_full->GetNbinsX();

        std::cout << "{\"" + var + "\", {";
        for(int i = 1; i <= n_bins; i++)
            std::cout << TString::Format("%.3f,", hist_full->GetBinContent(i)/hist_fidu->GetBinContent(i));
        std::cout << "}}," << std::endl;
    }
}
