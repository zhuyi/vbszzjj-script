#!/bin/bash

curr_path=${PWD}
result_path="/lustre/collider/zhuyifan/VBSZZ/unfolding/unfolding-input-for-vbszz/EB/results/background_variation/inclusive"
submit_path="/lustre/collider/zhuyifan/VBSZZ/unfolding/unfolding-input-for-vbszz/EB/submit/background_variation/inclusive"

channels=("top" "Z0jet" "Z1jet" "Z2jet" "WZ" "WW")
declare -A channel_patterns
channel_patterns["top"]="top"
channel_patterns["Z0jet"]="Zjets 0 jet"
channel_patterns["Z1jet"]="      1 jet"
channel_patterns["Z2jet"]="      2 jet"
channel_patterns["WZ"]="WZ"
channel_patterns["WW"]="WW"
declare -A cvs
cvs["top"]="0.98"
cvs["Z0jet"]="1.16"
cvs["Z1jet"]="1.12"
cvs["Z2jet"]="0.98"
cvs["WZ"]="1.00"
cvs["WW"]="1.29"
vars=("CP_ZZ_1"  "CP_ZZ_2"  "dphill"  "leading_pT_lepton"  "mt_zz"  "n_jets"  "ptz"  "pt_zz"  "Z_rapidity")

function substitute_sf
{
    local dir=${1}

    for var in "${vars[@]}"
    do
        echo "substituting ${var} ..."

        cd "inc_${var}"

        for channel in "${channels[@]}"
        do
            if [[ ${channel} = ${dir} ]]; then continue; fi

            sed -i "s:[[:digit:]]\.[[:digit:]]\+, # ${channel_patterns[${channel}]}:${cvs[${channel}]},:g" unfolding_ee_input.yaml
        done

        cd ..
    done
} 

for channel_dir in "${channels[@]}"
do
    echo ${channel_dir}

    echo "y" | rm -rf ${channel_dir}

    cp -r all ${channel_dir}

    cd ${channel_dir}
    cd up
    substitute_sf ${channel_dir}

    cd ..
    cd down
    substitute_sf ${channel_dir}

    cd ..
    cd ..

    mkdir -vp ${result_path}/${channel_dir}/up
    mkdir -vp ${result_path}/${channel_dir}/down
    for var in "${vars[@]}"; do mkdir -vp ${result_path}/${channel_dir}/up/${var}; done
    for var in "${vars[@]}"; do mkdir -vp ${result_path}/${channel_dir}/down/${var}; done

    mkdir -vp ${submit_path}/${channel_dir}/up/log
    mkdir -vp ${submit_path}/${channel_dir}/down/log

    cp ${submit_path}/all/up/*.* ${submit_path}/${channel_dir}/up
    cp ${submit_path}/all/down/*.* ${submit_path}/${channel_dir}/down

    sed -i "s:all:${channel_dir}:g" ${submit_path}/${channel_dir}/*/*.*
done

cd ${curr_path}
