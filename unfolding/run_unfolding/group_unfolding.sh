#!/bin/bash

if [[ ${1} == "inclusive" ]]; then
    source run.sh inclusive n_jets
    source run.sh inclusive dphill
#    source run.sh inclusive leading_jet_pt
    source run.sh inclusive leading_pT_lepton
    source run.sh inclusive mt_zz
    source run.sh inclusive ptz
    source run.sh inclusive pt_zz
    source run.sh inclusive Z_rapidity
    source run.sh inclusive CP_ZZ_1
    source run.sh inclusive CP_ZZ_2
elif [[ ${1} == "vbs" ]]; then
    source run.sh vbs dphill
    source run.sh vbs leading_jet_pt
    source run.sh vbs mjj
    source run.sh vbs mt_zz
    source run.sh vbs ptz
    source run.sh vbs pt_zz
    source run.sh vbs Z_rapidity
fi
