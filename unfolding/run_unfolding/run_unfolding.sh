#!/bin/bash

echo "****************************************************************************************************"
echo "* $2"
echo "****************************************************************************************************"
sleep 2s

orignal=/lustre/collider/zhuyifan/VBSZZ/unfolding/unfolding-input-for-vbszz/EB/
region=$1
variation=
mkdir -vp ${region}

cp ${orignal}/results/${variation}/${region}/$2/*.root .
cp ${orignal}/results/${variation}/${region}/$2/*.py .

if   [[ $2 == dphill || $2 == one ]]; then
    python VBS_unfold.py 2> /dev/null
    sed -i "s:abs(dphill):dphill:g" abs\(dphill\)_config.xml
    mv abs\(dphill\)_config.xml dphill_config.xml
    VIPUnfoldBase dphill_config.xml
elif [[ $2 == Z_rapidity ]]; then
    python VBS_unfold.py 2> /dev/null
    if [[ -f "std::abs(Z_rapidity)_config.xml" ]]; then
        sed -i "s:std\:\:abs(Z_rapidity):Z_rapidity:g" std\:\:abs\(Z_rapidity\)_config.xml
        mv std\:\:abs\(Z_rapidity\)_config.xml Z_rapidity_config.xml
    fi
    if [[ -f "abs(Z_rapidity)_config.xml" ]]; then
        sed -i "s:abs(Z_rapidity):Z_rapidity:g" abs\(Z_rapidity\)_config.xml
        mv abs\(Z_rapidity\)_config.xml Z_rapidity_config.xml
    fi
    VIPUnfoldBase Z_rapidity_config.xml
else
    python VBS_unfold.py
fi

mkdir -vp ${region}/${variation}/$2
mv *.root ${region}/${variation}/$2
mv *.xml  ${region}/${variation}/$2
mv *.txt  ${region}/${variation}/$2
mv *.py   ${region}/${variation}/$2
