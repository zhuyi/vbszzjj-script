TString gg  = "345723*.root";
TString qq  = "345666*.root";
TString EWK = "363724*.root";

double binning[] = {0., 1., 2., 3., 11.};
double weights[] = {-1., 0., 1., 1.};

size_t n_bin = sizeof(binning)/sizeof(binning[0]) - 1;

TCut get_binned_syst_weight()
{
    TString cut;
    for(size_t i = 0; i <= n_bin; i++)
        cut += "(n_jets>=" + TString::Format("%f", binning[i]) +
               "&&n_jets<" + TString::Format("%f", binning[i + 1]) +
               ")*" + TString::Format("%f", weights[i]) +
               (i < n_bin ? "+" : "");
    return cut.Data();
}

TH1D* get_hist(const TString &channel, const TString &folder, const TCut &cut)
{
    auto tree = new TChain("tree_PFLOW");
    tree->Add("/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/" + folder + "/" + channel);
    auto hist = new TH1D("hist_" + channel + "_" + folder, "", n_bin, binning);
    TString name = hist->GetName();
    tree->Draw("n_jets>>" + name, "weight_gen*scale*ew_correction*tautau_correction"*cut);
    return hist;
}

void test_tstring_binned_unc()
{
    auto cut = get_binned_syst_weight();
    std::cout << cut.GetTitle() << std::endl;
    auto hist = get_hist(qq, "Signal_nocut", cut);
    auto c = new TCanvas("c", "", 800, 600);
    hist->Draw("hist");
}
