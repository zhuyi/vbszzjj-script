#include <vector>
#include <algorithm>
#include <iostream>

#include "TStyle.h"
#include "TString.h"
#include "TChain.h"
#include "TCut.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "THStack.h"
#include "TRatioPlot.h"
#include "TLegend.h"
#include "TPad.h"
#include "TPaveText.h"
#include "TLine.h"

#include "inc/Data.h"
#include "inc/MC.h"

enum {mumu, ee};
enum {is_data, is_mc};

std::map<TString, int> colors = {{"Zjets",  kMagenta + 2},
                                 {"llvv",   kBlue - 6},
                                 {"inclusive", kBlue - 6},
                                 //{"EWKZZ",  kBlue - 6},
                                 //{"QCDZZ",  kYellow - 4},
                                 {"WZ",     kCyan - 6},
                                 {"top",    kAzure - 3},
                                 {"llll",   kYellow + 2},
                                 {"llqq",   kGray + 3},
                                 {"lllljj", kGray + 2},
                                 {"ttbarV", kViolet - 4},
                                 {"WW",     kGreen + 2},
                                 {"Wt",     kSpring + 4},
                                 {"VVV",    kOrange + 10},
                                 {"Ztt",    kRed + 1}
                                };

std::map<TString, TString> axis_label = {{"met_signif", "E^{missing}_{T} signif"},
                                         {"met_tst",    "E^{missing}_{T}"},
                                         {"dMetZPhi",   "#Delta(E^{missing}_{T},#Phi)"}
                                        };

double x_met_sig[] = {0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5, 15, 15.5, 16, 16.5, 17, 17.5, 18, 18.5, 19, 19.5, 20, 20.5, 21}; //met_signif
double x_met_tst[] = {60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300}; //met_tst

double GetHistError(const TH1D *hist)
{
    double err = 0.;
    for(int i = 0; i < hist->GetSumw2N(); i++) err += hist->GetSumw2()->At(i);
    return sqrt(err);
}

void DoReco(TChain *tree, TH1D *hist, const TString &leaf, const int &if_mc, const TString &event_type_str)
{
    TCut inclusive("");
    inclusive = "met_tst>70&&M2Lep>80&&M2Lep<100&&n_bjets==0&&dLepR<1.7&&dMetZPhi>2.2&&MetOHT>0.65&&event_type==" + event_type_str;

    if(if_mc)
        tree->Draw(leaf + ">>" + hist->GetName(), "scale*weight"*inclusive);
    else
        tree->Draw(leaf + ">>" + hist->GetName(), inclusive);
}

void DrawHStack(const TString &leaf,
                const TRatioPlot *rp, THStack *hs, TH1D *data_hist)
{
    TLine *cut_line = new TLine();
    cut_line->SetLineStyle(kDashed);
    cut_line->SetLineColor(kRed);

    auto box = new TBox();
    box->SetFillColor(kGray);

    rp->GetUpperPad()->cd();
    if(leaf == "met_tst")
        box->DrawBox(70., rp->GetUpperPad()->GetUymin(), 90., rp->GetUpperPad()->GetUymax());

    hs->Draw("same hist");
    data_hist->SetLineColor(kBlack);
    data_hist->SetMarkerColor(kBlack);
    data_hist->Draw("same e");

    if(leaf == "met_tst")
        cut_line->DrawLine(100., rp->GetUpperPad()->GetUymin(), 100., rp->GetUpperPad()->GetUymax());

    rp->GetLowerPad()->cd();
    if(leaf == "met_tst")
    {
        cut_line->DrawLine(100., rp->GetLowerPad()->GetUymin(), 100., rp->GetLowerPad()->GetUymax());

        cut_line->SetLineColor(kGray + 2);
        cut_line->DrawLine(90., rp->GetLowerPad()->GetUymin(), 90., rp->GetLowerPad()->GetUymax());
        cut_line->DrawLine(70., rp->GetLowerPad()->GetUymin(), 70., rp->GetLowerPad()->GetUymax());
    }
}

void DrawLegendText(TH1D *data_hist, std::vector<TH1D*> mc_hists, const std::vector<TString> &text_strings)
{
    TLegend *legend(nullptr);
    legend = new TLegend(0.50, 0.25, 0.90, 0.70);
    legend->SetFillStyle(0);
    legend->SetBorderSize(0);
    for(auto hist = mc_hists.rbegin(); hist != mc_hists.rend(); hist++)
        legend->AddEntry(*hist, (*hist)->GetName(), "f");
    legend->AddEntry(data_hist, "data", "l");

    TPaveText* textatlas(nullptr);
    textatlas = new TPaveText(0.50, 0.73, 0.90, 0.87, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(12);
    textatlas->SetTextSize(0.04);
    textatlas->AddText(text_strings.at(0));
    textatlas->AddText(text_strings.at(1));

    legend->Draw();
    textatlas->Draw();
}

void Full(TString leaf, int event_type)
{
    //gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    const TString event_type_str(TString::Itoa(event_type, 10));

    int n_bins = 0;
    double *x_bins;
    if (leaf == "met_signif")   {x_bins = x_met_sig;   n_bins = sizeof(x_met_sig)/sizeof(x_met_sig[0]);}
    if (leaf == "met_tst")      {x_bins = x_met_tst;   n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);}

    Data datas;
    TChain *data_tree = new TChain("tree_PFLOW");
    TH1D *data_hist = new TH1D("data", "", n_bins - 1, x_bins);
    for(auto data : datas.datas)
        data_tree->Add(datas.data_path + data);
    DoReco(data_tree, data_hist, leaf, is_data, event_type_str);

    data_hist->SetLineColor(kWhite);
    data_hist->SetMarkerColor(kWhite);
    data_hist->SetMarkerStyle(kFullSquare);
    data_hist->SetMarkerSize(0.75);
    //data_hist->SetMaximum(1.1*data_hist->GetMaximum());

    MC mcs;
    THStack *hs = new THStack("hs", "");
    std::vector<TChain*> mc_trees;
    std::vector<TH1D*> mc_hists;
    for(auto mc : mcs.mcs)
    {
        //if(mc.first == "inclusive") continue;

        mc_trees.emplace_back(new TChain("tree_PFLOW"));
        for(auto file : mc.second)
            mc_trees.back()->Add(mcs.mc_path_nominal + "/" + file);

        mc_hists.emplace_back(new TH1D(mc.first, "", n_bins - 1, x_bins));

        DoReco(mc_trees.back(), mc_hists.back(), leaf, is_mc, event_type_str);

        if(mc.first.Contains("WZ")  && event_type == ee)   mc_hists.back()->Scale(1.02);
        if(mc.first.Contains("WW")  && event_type == ee)   mc_hists.back()->Scale(1.23);
        if(mc.first.Contains("top") && event_type == ee)   mc_hists.back()->Scale(1.06);
        if(mc.first.Contains("WZ")  && event_type == mumu) mc_hists.back()->Scale(1.02);
        if(mc.first.Contains("WW")  && event_type == mumu) mc_hists.back()->Scale(1.23);
        if(mc.first.Contains("top") && event_type == mumu) mc_hists.back()->Scale(1.06);

        mc_hists.back()->SetLineColor(colors.at(mc.first));
        mc_hists.back()->SetLineWidth(2);
        mc_hists.back()->SetFillColor(colors.at(mc.first));
    }

    std::sort(mc_hists.begin(),
         mc_hists.end(),
         [&](const TH1D *hist1, const TH1D *hist2) -> bool
         {
             return hist1->GetSumOfWeights() < hist2->GetSumOfWeights();
         }
        );

    for(auto hist : mc_hists)
        hs->Add(hist);

    auto mc_hist = dynamic_cast<TH1D*>(mc_hists.at(0)->Clone("mc_hist"));
    for(int i = 1; i < mc_hists.size(); i++)
        mc_hist->Add(mc_hists.at(i));
    mc_hist->SetLineColor(kWhite);

    TCanvas *c = new TCanvas("c", "c", 800, 600);
    c->cd();

    TRatioPlot *rp = new TRatioPlot(data_hist, mc_hist);
    rp->SetH1DrawOpt("e");
    rp->SetH2DrawOpt("e");
    rp->Draw();

    rp->SetLeftMargin(0.12);
    rp->SetSeparationMargin(0.02);

    double upper_max = data_hist->GetMaximum() > hs->GetMaximum() ? data_hist->GetMaximum() : hs->GetMaximum();
    rp->GetUpperRefXaxis()->SetTitle(axis_label.at(leaf));
    rp->GetUpperRefYaxis()->SetTitle("events/bin");
    dynamic_cast<TH1D*>(rp->GetUpperRefObject())->SetMaximum(1.1*upper_max);
    rp->GetUpperPad()->Update();

    //rp->GetLowerPad()->SetLogy();
    rp->GetLowerRefYaxis()->SetRangeUser(0., 2.);
    rp->GetLowerRefYaxis()->SetTitle("#frac{data}{MC}");
    rp->GetLowerRefGraph()->SetLineColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerStyle(kFullSquare);
    rp->GetLowerRefGraph()->SetMarkerColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerSize(0.6);
    rp->GetLowerPad()->Update();

    DrawHStack(leaf, rp, hs, data_hist);

    rp->GetUpperPad()->cd();
    std::vector<TString> text_strings = {"#it{ATLAS} #bf{internal}",
                                         "#intLdt = 139fb^{-1}, #sqrt{s} = 13TeV, " + (TString)(event_type_str == "0" ? "Z#rightarrow#mu#mu" : "Z#rightarrowee")};
    DrawLegendText(data_hist, mc_hists, text_strings);

    c->SaveAs("fig/distribution_inc_" + leaf + "_" + event_type_str + ".png");
/*
    double yield_total = 0.;
    double yield_bkg = 0.;
    double err_total = 0.;
    double err_bkg = 0.;
    for(auto hist : mc_hists)
    {
        double yield = hist->GetSumOfWeights();
        double err = GetHistError(hist);

        std::cout << hist->GetName() << "\t"
                  << yield << " +/- "
                  << err << std::endl;

        yield_total += yield;
        err_total += err*err;

        if(!((TString)(hist->GetName())).Contains("llvv"))
        {
            yield_bkg += yield;
            err_bkg += err*err;
        }
    }
    std::cout << "bkg\t" << yield_bkg << " +/- " << std::sqrt(err_bkg) << std::endl;
    std::cout << "total\t" << yield_total << " +/- " << std::sqrt(err_total) << std::endl;
    std::cout << "data\t" << data_hist->GetEntries() << " +/- " << std::sqrt(data_hist->GetEntries()) << std::endl;
*/
}

int main(int argc, char *argv[])
{
    Full(argv[1], ee);
    Full(argv[1], mumu);
}
