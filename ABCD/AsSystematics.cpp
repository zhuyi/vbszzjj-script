#include <iostream>
#include <vector>
#include <map>

#include "TStyle.h"
#include "TColor.h"
#include "TString.h"
#include "TCut.h"
#include "TH1D.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TPaveText.h"

#include "inc/Data.h"
#include "inc/MC.h"
#include "inc/Setting.h"

enum {mumu, ee};
enum {right, left};

TString pdf_nominal = "weight_gen";
std::vector<TString> pdf_variations = {"weight_var_th_MUR1_MUF1_PDF270000",
                                       "weight_var_th_MUR1_MUF1_PDF269000"};
std::map<TString, int> colors = {{"weight_gen", kBlack},
                                 {"weight_var_th_MUR1_MUF1_PDF270000", kRed + 1},
                                 {"weight_var_th_MUR1_MUF1_PDF269000", kBlue + 2}};
std::map<TString, TString> leg_label = {{"weight_gen", "nominal"},
                                        {"weight_var_th_MUR1_MUF1_PDF270000", "#alpha_{S} up"},
                                        {"weight_var_th_MUR1_MUF1_PDF269000", "#alpha_{S} down"}};

void FindHistsMinMax(std::vector<TH1D*> &hists,
                       double &y_max, double &y_min)
{
    for(auto hist : hists) 
    {
        if(y_max < hist->GetMaximum()) y_max = hist->GetMaximum();
        if(y_min > hist->GetMinimum()) y_min = hist->GetMinimum();
    }
}

void PlotLegendPaveText(TH1D *hist_nominal, std::vector<TH1D*> hists_variation,
                        std::vector<TString> texts,
                        int if_left = false)
{
    TLegend *legend(nullptr);
    if(if_left) legend = new TLegend(0.21, 0.66, 0.50, 0.75);
    else        legend = new TLegend(0.67, 0.66, 1.00, 0.75);
    legend->SetFillStyle(0);
    legend->SetBorderSize(0);
    legend->AddEntry(hist_nominal, leg_label.at(hist_nominal->GetName()), "l");
    for(const auto &hist : hists_variation)
        legend->AddEntry(hist, leg_label.at(hist->GetName()), "l");

    TPaveText* textatlas(nullptr);
    if(if_left)
        textatlas = new TPaveText(0.20, 0.75, 0.60, 0.85, "brNDC");
    else
        textatlas = new TPaveText(0.45, 0.75, 0.90, 0.85, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(32);
    textatlas->SetTextSize(0.03);
    for(auto text : texts) textatlas->AddText(text);
    dynamic_cast<TText*>(textatlas->GetListOfLines()->First())->SetTextSize(0.04);

    legend->Draw();
    textatlas->Draw();
}

void AsSystematics(TString channel, TString leaf, int event_type, TString region)
{
//Plot varitation
    //TCut weight_total = (scale + "/" + pdf_nominal)*(pre_selection&&regions.at(region)&&("event_type==" + TString::Itoa(event_type, 10)));
    TCut weight_total = (scale + "/" + pdf_nominal)*(pre_selection&&regions.at(region));

    int n_bins = 0;
    double *xbins;
    if(leaf == "Z_pT")     {xbins = x_z_pt;     n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);}
    if(leaf == "dLepR")    {xbins = x_dlepr;    n_bins = sizeof(x_dlepr)/sizeof(x_dlepr[0]);}
    if(leaf == "met_tst")  {xbins = x_met_tst;  n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);}
    if(leaf == "dMetZPhi") {xbins = x_dmetzphi; n_bins = sizeof(x_dmetzphi)/sizeof(x_dmetzphi[0]);}
    if(leaf == "n_jets")   {xbins = x_n_jets;   n_bins = sizeof(x_n_jets)/sizeof(x_n_jets[0]);}
    if(leaf == "dphill")   {xbins = x_dphill;   n_bins = sizeof(x_dphill)/sizeof(x_dphill[0]);}
    if(leaf == "mT_ZZ")    {xbins = x_mt_zz;    n_bins = sizeof(x_mt_zz)/sizeof(x_mt_zz[0]);}
    if(leaf == "mjj")      {xbins = x_mjj;      n_bins = sizeof(x_mjj)/sizeof(x_mjj[0]);}

    TString leaf_to_draw(leaf);
    if(leaf == "dphill") leaf_to_draw = "std::abs(dphill)";

    MC mcs;
    auto tree = new TChain("tree_PFLOW");
    for(auto file : mcs.mcs.at(channel))
        tree->Add(mcs.mc_path_fiducial + file);

    auto hist_nominal = new TH1D(pdf_nominal, "", n_bins - 1, xbins);
    tree->Draw(leaf_to_draw + ">>" + hist_nominal->GetName(), (TCut)pdf_nominal*weight_total);
    hist_nominal->SetLineWidth(2);
    hist_nominal->SetLineColor(colors.at(hist_nominal->GetName()));

    std::vector<TH1D*> hists_variation;
    for(auto pdf_variation : pdf_variations)
    {
        hists_variation.emplace_back(new TH1D(pdf_variation, "", n_bins - 1, xbins));
        tree->Draw(leaf_to_draw + ">>" + hists_variation.back()->GetName(), (TCut)pdf_variation*weight_total);
        hists_variation.back()->SetLineWidth(2);
        hists_variation.back()->SetLineColor(colors.at(hists_variation.back()->GetName()));
    }

    hists_variation.front()->GetYaxis()->SetTitle("events/bin");
    //hists_variation.front()->GetXaxis()->SetTitle("m_{T}(Z,Z)");
    hists_variation.front()->GetXaxis()->SetTitle(title.at(leaf));

    auto c = new TCanvas("c", "", 800, 600);
    c->SetLogy();
    for(const auto &hist : hists_variation) hist->Draw("hist same");
    hist_nominal->Draw("hist same");

    std::vector<TString> texts = {"#bf{#it{ATLAS}} internal",
                                  "#intLdt = 139fb^{-1}, #sqrt{s} = 13TeV, mc16ade, region " + region};
    PlotLegendPaveText(hist_nominal, hists_variation, texts);

    //TString name = "as_variation_" + leaf + "_" + channel + "_" + region + "_" + TString::Itoa(event_type, 10);
    //TString name = "as_variation_" + leaf + "_" + channel + "_" + region;
    TString name = "as_variation_" + leaf + "_vbs_" + region;
    c->SaveAs("fig/" + name + ".png");

//Calculate uncertainties
    //double nominal = hist_nominal->GetSumOfWeights();
    //std::cout << "nominal: " << nominal << std::endl;
    //
    //std::vector<double> variations;
    //for(const auto &hist : hists_variation)
    //    variations.push_back(hist->GetSumOfWeights() - nominal);
    //
    //std::sort(variations.begin(), variations.end());
    //std::cout << "alpha_s uncertainty: " << (0.5*(variations.back() - variations.front()))/nominal << std::endl;

    std::cout << "\n" << leaf << std::endl;
    std::vector<double> variations;
    for(int i = 0; i < hist_nominal->GetNbinsX(); ++i)
    {
        double nominal = hist_nominal->GetBinContent(i + 1);
    
        std::vector<double> variations_in_bin;
        for(const auto &hist : hists_variation)
            variations_in_bin.push_back(hist->GetBinContent(i + 1) - nominal);
    
        std::sort(variations_in_bin.begin(), variations_in_bin.end());
        variations.push_back((0.5*(variations_in_bin.back() - variations_in_bin.front()))/nominal);
        //std::cout << (0.5*(variations_in_bin.back() - variations_in_bin.front()))/nominal << "	";
    }
    //std::cout << std::endl;
    for(const auto variation : variations) std::cout << variation << ",  ";
    std::cout << std::endl;
    for(const auto variation : variations) std::cout << "-" << variation << ", ";
    std::cout << std::endl;

//Plot relative variation
    c->ResetDrawn();
    c->SetLogy(false);

    for(const auto &hist : hists_variation)
    {
        hist->Add(hist_nominal, -1.);
        hist->Divide(hist_nominal);
        hist->Scale(100.);
    }
    hist_nominal->Add(hist_nominal, -1.);
    hist_nominal->Divide(hist_nominal);
    hist_nominal->Scale(100.);

    double y_max = -INFINITY;
    double y_min = INFINITY;
    FindHistsMinMax(hists_variation, y_max, y_min);
    hists_variation.front()->SetMaximum(0.5*(y_max + y_min) + 1.5*(y_max - y_min));
    hists_variation.front()->SetMinimum(0.5*(y_max + y_min) - 0.6*(y_max - y_min));
    hists_variation.front()->GetYaxis()->SetTitle("#frac{variation - nominal}{nominal} [%]");

    for(const auto &hist : hists_variation) hist->Draw("hist same");
    hist_nominal->Draw("hist same");

    //TString name_relative = "as_relative_variation_" + leaf + "_" + channel + "_" + region + "_" + TString::Itoa(event_type, 10);
    //TString name_relative = "as_relative_variation_" + leaf + "_" + channel + "_" + region;
    TString name_relative = "as_relative_variation_" + leaf + "_vbs_" + region;
    c->SaveAs("fig/" + name_relative + ".png");

    c->Clear();
}

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    TString channel(argv[1]);
    //AsSystematics(channel, "Z_pT", ee, "A");
    //AsSystematics(channel, "Z_pT", mumu, "A");
    //AsSystematics(channel, "n_jets", ee, "A");
    //AsSystematics(channel, "dphill", ee, "A");
    AsSystematics(channel, "mT_ZZ",  ee, "A");
    AsSystematics(channel, "mjj",    ee, "A");
}
