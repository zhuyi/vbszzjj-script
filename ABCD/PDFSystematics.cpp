#include <iostream>
#include <vector>
#include <map>

#include "TStyle.h"
#include "TColor.h"
#include "TString.h"
#include "TCut.h"
#include "TH1D.h"
#include "TBox.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TPaveText.h"

#include "inc/Data.h"
#include "inc/MC.h"
#include "inc/Setting.h"

enum {mumu, ee};
enum {right, left};

TString pdf_nominal = "weight_var_th_MUR1_MUF1_PDF261000";
std::vector<TString> pdf_variations = {"weight_var_th_MUR1_MUF1_PDF261001",
                                       "weight_var_th_MUR1_MUF1_PDF261002",
                                       "weight_var_th_MUR1_MUF1_PDF261003",
                                       "weight_var_th_MUR1_MUF1_PDF261004",
                                       "weight_var_th_MUR1_MUF1_PDF261005",
                                       "weight_var_th_MUR1_MUF1_PDF261006",
                                       "weight_var_th_MUR1_MUF1_PDF261007",
                                       "weight_var_th_MUR1_MUF1_PDF261008",
                                       "weight_var_th_MUR1_MUF1_PDF261009",
                                       "weight_var_th_MUR1_MUF1_PDF261010",
                                       "weight_var_th_MUR1_MUF1_PDF261011",
                                       "weight_var_th_MUR1_MUF1_PDF261012",
                                       "weight_var_th_MUR1_MUF1_PDF261013",
                                       "weight_var_th_MUR1_MUF1_PDF261014",
                                       "weight_var_th_MUR1_MUF1_PDF261015",
                                       "weight_var_th_MUR1_MUF1_PDF261016",
                                       "weight_var_th_MUR1_MUF1_PDF261017",
                                       "weight_var_th_MUR1_MUF1_PDF261018",
                                       "weight_var_th_MUR1_MUF1_PDF261019",
                                       "weight_var_th_MUR1_MUF1_PDF261020",
                                       "weight_var_th_MUR1_MUF1_PDF261021",
                                       "weight_var_th_MUR1_MUF1_PDF261022",
                                       "weight_var_th_MUR1_MUF1_PDF261023",
                                       "weight_var_th_MUR1_MUF1_PDF261024",
                                       "weight_var_th_MUR1_MUF1_PDF261025",
                                       "weight_var_th_MUR1_MUF1_PDF261026",
                                       "weight_var_th_MUR1_MUF1_PDF261027",
                                       "weight_var_th_MUR1_MUF1_PDF261028",
                                       "weight_var_th_MUR1_MUF1_PDF261029",
                                       "weight_var_th_MUR1_MUF1_PDF261030",
                                       "weight_var_th_MUR1_MUF1_PDF261031",
                                       "weight_var_th_MUR1_MUF1_PDF261032",
                                       "weight_var_th_MUR1_MUF1_PDF261033",
                                       "weight_var_th_MUR1_MUF1_PDF261034",
                                       "weight_var_th_MUR1_MUF1_PDF261035",
                                       "weight_var_th_MUR1_MUF1_PDF261036",
                                       "weight_var_th_MUR1_MUF1_PDF261037",
                                       "weight_var_th_MUR1_MUF1_PDF261038",
                                       "weight_var_th_MUR1_MUF1_PDF261039",
                                       "weight_var_th_MUR1_MUF1_PDF261040",
                                       "weight_var_th_MUR1_MUF1_PDF261041",
                                       "weight_var_th_MUR1_MUF1_PDF261042",
                                       "weight_var_th_MUR1_MUF1_PDF261043",
                                       "weight_var_th_MUR1_MUF1_PDF261044",
                                       "weight_var_th_MUR1_MUF1_PDF261045",
                                       "weight_var_th_MUR1_MUF1_PDF261046",
                                       "weight_var_th_MUR1_MUF1_PDF261047",
                                       "weight_var_th_MUR1_MUF1_PDF261048",
                                       "weight_var_th_MUR1_MUF1_PDF261049",
                                       "weight_var_th_MUR1_MUF1_PDF261050",
                                       "weight_var_th_MUR1_MUF1_PDF261051",
                                       "weight_var_th_MUR1_MUF1_PDF261052",
                                       "weight_var_th_MUR1_MUF1_PDF261053",
                                       "weight_var_th_MUR1_MUF1_PDF261054",
                                       "weight_var_th_MUR1_MUF1_PDF261055",
                                       "weight_var_th_MUR1_MUF1_PDF261056",
                                       "weight_var_th_MUR1_MUF1_PDF261057",
                                       "weight_var_th_MUR1_MUF1_PDF261058",
                                       "weight_var_th_MUR1_MUF1_PDF261059",
                                       "weight_var_th_MUR1_MUF1_PDF261060",
                                       "weight_var_th_MUR1_MUF1_PDF261061",
                                       "weight_var_th_MUR1_MUF1_PDF261062",
                                       "weight_var_th_MUR1_MUF1_PDF261063",
                                       "weight_var_th_MUR1_MUF1_PDF261064",
                                       "weight_var_th_MUR1_MUF1_PDF261065",
                                       "weight_var_th_MUR1_MUF1_PDF261066",
                                       "weight_var_th_MUR1_MUF1_PDF261067",
                                       "weight_var_th_MUR1_MUF1_PDF261068",
                                       "weight_var_th_MUR1_MUF1_PDF261069",
                                       "weight_var_th_MUR1_MUF1_PDF261070",
                                       "weight_var_th_MUR1_MUF1_PDF261071",
                                       "weight_var_th_MUR1_MUF1_PDF261072",
                                       "weight_var_th_MUR1_MUF1_PDF261073",
                                       "weight_var_th_MUR1_MUF1_PDF261074",
                                       "weight_var_th_MUR1_MUF1_PDF261075",
                                       "weight_var_th_MUR1_MUF1_PDF261076",
                                       "weight_var_th_MUR1_MUF1_PDF261077",
                                       "weight_var_th_MUR1_MUF1_PDF261078",
                                       "weight_var_th_MUR1_MUF1_PDF261079",
                                       "weight_var_th_MUR1_MUF1_PDF261080",
                                       "weight_var_th_MUR1_MUF1_PDF261081",
                                       "weight_var_th_MUR1_MUF1_PDF261082",
                                       "weight_var_th_MUR1_MUF1_PDF261083",
                                       "weight_var_th_MUR1_MUF1_PDF261084",
                                       "weight_var_th_MUR1_MUF1_PDF261085",
                                       "weight_var_th_MUR1_MUF1_PDF261086",
                                       "weight_var_th_MUR1_MUF1_PDF261087",
                                       "weight_var_th_MUR1_MUF1_PDF261088",
                                       "weight_var_th_MUR1_MUF1_PDF261089",
                                       "weight_var_th_MUR1_MUF1_PDF261090",
                                       "weight_var_th_MUR1_MUF1_PDF261091",
                                       "weight_var_th_MUR1_MUF1_PDF261092",
                                       "weight_var_th_MUR1_MUF1_PDF261093",
                                       "weight_var_th_MUR1_MUF1_PDF261094",
                                       "weight_var_th_MUR1_MUF1_PDF261095",
                                       "weight_var_th_MUR1_MUF1_PDF261096",
                                       "weight_var_th_MUR1_MUF1_PDF261097",
                                       "weight_var_th_MUR1_MUF1_PDF261098",
                                       "weight_var_th_MUR1_MUF1_PDF261099",
                                       "weight_var_th_MUR1_MUF1_PDF261100"
                                      };

std::map<TString, int> colors = {{"nominal",        kBlack},
                                 {"variation_up",   kBlue - 3},
                                 {"variation_down", kRed - 4}};

void FindHistsMinMax(std::vector<TH1D*> &hists,
                       double &y_max, double &y_min)
{
    for(auto hist : hists) 
    {
        if(y_max < hist->GetMaximum()) y_max = hist->GetMaximum();
        if(y_min > hist->GetMinimum()) y_min = hist->GetMinimum();
    }
}

void FindMinimumHist(const std::vector<TH1D*> &hists_variation, TH1D* hist_minimum)
{
    int n_bins = hist_minimum->GetNbinsX();

    for(int i = 1; i <= n_bins; ++i)
    {
        double minimum(INFINITY);
        for(const auto &hist : hists_variation)
        {
            if(minimum > hist->GetBinContent(i))
                minimum = hist->GetBinContent(i);
        }

        hist_minimum->SetBinContent(i, minimum);
    }
}

void FindMaximumHist(const std::vector<TH1D*> &hists_variation, TH1D* hist_maximum)
{
    int n_bins = hist_maximum->GetNbinsX();

    for(int i = 1; i <= n_bins; ++i)
    {
        double maximum(-INFINITY);
        for(const auto &hist : hists_variation)
        {
            if(maximum < hist->GetBinContent(i))
                maximum = hist->GetBinContent(i);
        }

        hist_maximum->SetBinContent(i, maximum);
    }
}

void FindVariationBoxes(std::vector<TBox*> &boxes_variation,
                        const TH1D *hist_maximum, const TH1D *hist_minimum)
{
    int n_bins = hist_maximum->GetNbinsX();

    for(int i = 1; i <= n_bins; ++i)
    {
        double x_min_box = hist_maximum->GetBinCenter(i) - 0.5*hist_maximum->GetBinWidth(i);
        double x_max_box = hist_maximum->GetBinCenter(i) + 0.5*hist_maximum->GetBinWidth(i);
        double y_min_box = hist_minimum->GetBinContent(i);
        double y_max_box = hist_maximum->GetBinContent(i);
        boxes_variation.push_back(new TBox(x_min_box, y_min_box, x_max_box, y_max_box));
        boxes_variation.back()->SetFillColor(kBlue + 3);
        boxes_variation.back()->SetFillStyle(3004);
    }
}

void PlotLegendPaveText(TH1D *hist_nominal, TBox *box_variation,
                        std::vector<TString> texts,
                        int if_left = false)
{
    TLegend *legend(nullptr);
    if(if_left) legend = new TLegend(0.19, 0.66, 0.50, 0.75);
    else        legend = new TLegend(0.65, 0.66, 1.00, 0.75);
    legend->SetFillStyle(0);
    legend->SetBorderSize(0);
    legend->AddEntry(hist_nominal, "nominal", "l");
    legend->AddEntry(box_variation, "PDF variations", "f");

    TPaveText* textatlas(nullptr);
    if(if_left)
        textatlas = new TPaveText(0.20, 0.75, 0.60, 0.85, "brNDC");
    else
        textatlas = new TPaveText(0.45, 0.75, 0.90, 0.85, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(32);
    textatlas->SetTextSize(0.03);
    for(auto text : texts) textatlas->AddText(text);
    dynamic_cast<TText*>(textatlas->GetListOfLines()->First())->SetTextSize(0.04);

    legend->Draw();
    textatlas->Draw();
}

void PDFSystematics(TString channel, TString leaf, int event_type, TString region)
{
//Plot varitation
    //TCut weight_total = (scale + "/" + pdf_nominal)*(pre_selection&&regions.at(region)&&("event_type==" + TString::Itoa(event_type, 10)));
    TCut weight_total = (scale + "/" + pdf_nominal)*(pre_selection&&regions.at(region));

    int n_bins = 0;
    double *xbins;
    if(leaf == "Z_pT")     {xbins = x_z_pt;     n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);}
    if(leaf == "dLepR")    {xbins = x_dlepr;    n_bins = sizeof(x_dlepr)/sizeof(x_dlepr[0]);}
    if(leaf == "met_tst")  {xbins = x_met_tst;  n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);}
    if(leaf == "dMetZPhi") {xbins = x_dmetzphi; n_bins = sizeof(x_dmetzphi)/sizeof(x_dmetzphi[0]);}
    if(leaf == "n_jets")   {xbins = x_n_jets;   n_bins = sizeof(x_n_jets)/sizeof(x_n_jets[0]);}
    if(leaf == "dphill")   {xbins = x_dphill;   n_bins = sizeof(x_dphill)/sizeof(x_dphill[0]);}
    if(leaf == "mT_ZZ")    {xbins = x_mt_zz;    n_bins = sizeof(x_mt_zz)/sizeof(x_mt_zz[0]);}
    if(leaf == "mjj")      {xbins = x_mjj;      n_bins = sizeof(x_mjj)/sizeof(x_mjj[0]);}

    TString leaf_to_draw(leaf);
    if(leaf == "dphill") leaf_to_draw = "std::abs(dphill)";

    MC mcs;
    auto tree = new TChain("tree_PFLOW");
    for(auto file : mcs.mcs.at(channel))
        tree->Add(mcs.mc_path_fiducial + file);

    auto hist_nominal = new TH1D(pdf_nominal, "", n_bins - 1, xbins);
    tree->Draw(leaf_to_draw + ">>" + hist_nominal->GetName(), (TCut)pdf_nominal*weight_total);
    hist_nominal->SetLineWidth(2);
    hist_nominal->SetLineColor(colors.at("nominal"));

    std::vector<TH1D*> hists_variation;
    for(auto pdf_variation : pdf_variations)
    {
        std::cerr << pdf_variation << std::endl;

        hists_variation.emplace_back(new TH1D(pdf_variation, "", n_bins - 1, xbins));
        tree->Draw(leaf_to_draw + ">>" + hists_variation.back()->GetName(), (TCut)pdf_variation*weight_total);
    }

    hists_variation.front()->GetYaxis()->SetTitle("events/bin");
    hists_variation.front()->GetXaxis()->SetTitle(title.at(leaf));

    auto c = new TCanvas("c", "", 800, 600);
    c->SetLogy();

    auto hist_minimum = new TH1D("hist_minimum", "", n_bins - 1, xbins);
    FindMinimumHist(hists_variation, hist_minimum);
    auto hist_maximum = new TH1D("hist_maximum", "", n_bins - 1, xbins);
    FindMaximumHist(hists_variation, hist_maximum);

    hist_nominal->GetYaxis()->SetTitle("events/bin");
    hist_nominal->GetXaxis()->SetTitle(title.at(leaf));
    hist_nominal->Draw("hist same");

    std::vector<TBox*> boxes_variation;
    FindVariationBoxes(boxes_variation, hist_maximum, hist_minimum);
    for(const auto &box : boxes_variation) box->Draw();

    hist_nominal->Draw("hist same ][");

    std::vector<TString> texts = {"#bf{#it{ATLAS}} internal",
                                  "#intLdt = 139fb^{-1}, #sqrt{s} = 13TeV, mc16ade, region " + region};
    PlotLegendPaveText(hist_nominal, boxes_variation.front(), texts);

    //TString name = "pdf_variation_" + leaf + "_" + channel + "_" + region + "_" + TString::Itoa(event_type, 10);
    //TString name = "pdf_variation_" + leaf + "_" + channel + "_" + region;
    TString name = "pdf_variation_" + leaf + "_vbs_" + region;
    c->SaveAs("fig/" + name + ".png");

//Calculate uncertainties
    //double nominal = hist_nominal->GetSumOfWeights();
    //std::cout << "nominal: " << nominal << std::endl;

    //std::vector<double> variations;
    //for(const auto &hist : hists_variation)
    //    variations.push_back(hist->GetSumOfWeights() - nominal);

    //double sum(0.);
    //for(const auto &variation : variations)
    //    sum += variation*variation;
    //std::cout << "PDF uncertainty: " << std::sqrt(sum/pdf_variations.size())/nominal << std::endl;

    std::cout << "\n" << leaf << std::endl;
    std::vector<double> variations;
    std::vector<double> envelops_max;
    std::vector<double> envelops_min;
    for(int i = 0; i < hist_nominal->GetNbinsX(); ++i)
    {
        double nominal = hist_nominal->GetBinContent(i + 1);

        double variation(0.);
        double envelop_max(-INFINITY);
        double envelop_min( INFINITY);
        for(const auto &hist : hists_variation)
        {
            double variation_per_hist = hist->GetBinContent(i + 1) - nominal;
            variation += variation_per_hist*variation_per_hist;

            if(envelop_max < variation_per_hist) envelop_max = variation_per_hist;
            if(envelop_min > variation_per_hist) envelop_min = variation_per_hist;
        }

        variation = std::sqrt(variation/pdf_variations.size())/nominal; 
        variations.push_back(variation);

        envelops_max.push_back(envelop_max/nominal);
        envelops_min.push_back(envelop_min/nominal);

        //std::cout << variation << "	";
    }
    //std::cout << std::endl;
    for(const auto variation : variations) std::cout << variation << ",  ";
    std::cout << std::endl;
    for(const auto variation : variations) std::cout << "-" << variation << ", ";
    std::cout << std::endl;

//Plot relative variation

    c->ResetDrawn();
    c->SetLogy(false);

    for(const auto &hist : hists_variation)
    {
        hist->Add(hist_nominal, -1.);
        hist->Divide(hist_nominal);
        hist->Scale(100.);
    }
    hist_nominal->Add(hist_nominal, -1.);
    hist_nominal->Divide(hist_nominal);
    hist_nominal->Scale(100.);

    auto hist_minimum_ratio = new TH1D("hist_minimum_ratio", "", n_bins - 1, xbins);
    FindMinimumHist(hists_variation, hist_minimum_ratio);
    auto hist_maximum_ratio = new TH1D("hist_maximum_ratio", "", n_bins - 1, xbins);
    FindMaximumHist(hists_variation, hist_maximum_ratio);
    std::vector<TBox*> boxes_variation_ratio;
    FindVariationBoxes(boxes_variation_ratio, hist_maximum_ratio, hist_minimum_ratio);

    double y_max = -INFINITY;
    double y_min = INFINITY;
    FindHistsMinMax(hists_variation, y_max, y_min);
    hist_nominal->SetMaximum(0.5*(y_max + y_min) + 1.2*(y_max - y_min));
    hist_nominal->SetMinimum(0.5*(y_max + y_min) - 0.6*(y_max - y_min));
    hist_nominal->GetXaxis()->SetTitle(title.at(leaf));
    hist_nominal->GetYaxis()->SetTitle("#frac{variation - nominal}{nominal} [%]");

    hist_nominal->Draw("axis");
    for(const auto &box : boxes_variation_ratio) box->Draw();
    hist_nominal->Draw("hist same");
    PlotLegendPaveText(hist_nominal, boxes_variation_ratio.front(), texts);

    //TString name_relative = "pdf_relative_variation_" + leaf + "_" + channel + "_" + region + "_" + TString::Itoa(event_type, 10);
    //TString name_relative = "pdf_relative_variation_" + leaf + "_" + channel + "_" + region;
    TString name_relative = "pdf_relative_variation_" + leaf + "_vbs_" + region;
    c->SaveAs("fig/" + name_relative + ".png");

    c->Clear();
}

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    //gStyle->SetHatchesSpacing(1);
    TH1::SetDefaultSumw2(kTRUE);

    TString channel(argv[1]);
    //PDFSystematics(channel, "Z_pT", ee, "A");
    //PDFSystematics(channel, "Z_pT", mumu, "A");
    //PDFSystematics(channel, "n_jets", ee, "A");
    //PDFSystematics(channel, "dphill", ee, "A");
    //PDFSystematics(channel, "mT_ZZ", ee, "A");
    PDFSystematics(channel, "mjj",   ee, "A");
}
