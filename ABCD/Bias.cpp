#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>

#include "TStyle.h"
#include "TString.h"
#include "TChain.h"
#include "TCut.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "THStack.h"
#include "TRatioPlot.h"
#include "TLegend.h"
#include "TPad.h"
#include "TPaveText.h"
#include "TLine.h"

#include "inc/Data.h"
#include "inc/MC.h"
#include "inc/Setting.h"

int verbose = 2; 

enum {mumu, ee};
enum {is_data, is_mc};

std::map<TString, int> colors = {{"Zjets",  kMagenta + 2},
                                 {"inclusive", kBlue - 6},
                                 {"EWKZZ",  kBlue - 6},
                                 {"QCDZZ",  kYellow - 4},
                                 {"WZ",     kCyan - 6},
                                 {"top",    kAzure - 3},
                                 {"llll",   kYellow + 2},
                                 {"llqq",   kGray + 3},
                                 {"lllljj", kGray + 2},
                                 {"ttbarV", kViolet - 4},
                                 {"WW",     kGreen + 2},
                                 {"Wt",     kSpring + 4},
                                 {"VVV",    kOrange + 10},
                                 {"Ztt",    kRed + 1},
                                 {"Wjets",  kRed - 7},
                                };

std::map<TString, double> yields_data   = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_total  = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_signal = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_zjets  = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_other_bkg = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> errors_stat   = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};

void ConstructRegions(int argc, char *argv[], int channel)
{
    if     (argc != 3 && argc != 2)
    {
        //if     (channel == ee)   pre_selection += "MetOHT>0.270&&dMetZPhi>2.34"; //ee
        //else if(channel == mumu) pre_selection += "MetOHT>0.320&&dMetZPhi>2.32"; //mumu
        if     (channel == ee)   pre_selection += "MetOHT>0.35&&dMetZPhi>2."; //ee
        else if(channel == mumu) pre_selection += "MetOHT>0.40&&dMetZPhi>2."; //mumu
    }
    else if(argc == 2)
    {
        //pre_selection += "MetOHT>0.33";
        //pre_selection += argv[1];
    }
    else if(argc == 3)
    {
        pre_selection += argv[1];
        pre_selection += argv[2];
    }

    //std::cout << pre_selection << std::endl;
}

double GetHistError(const TH1D *hist)
{
    double err = 0.;
    for(int i = 0; i < hist->GetSumw2N(); i++) err += hist->GetSumw2()->At(i);
    return sqrt(err);
}

void DoReco(TChain *tree, TH1D *hist, TString region, TString leaf,
            int if_mc, const TString &type)
{
    //std::cout << "Plot " << hist->GetName() << " ..." << std::endl;

    TCut inclusive("");

    tree->Draw(leaf + ">>" + hist->GetName(),
               //scale*(regions.at(region)&&pre_selection&&("event_type==" + type))
               scale*(regions.at(region)&&pre_selection)
              );


}

void DrawLegendText(TH1D *data_hist, std::vector<TH1D*> mc_hists, const std::vector<TString> &text_strings)
{
    double left(0.15);
    double width(0.4);

    TLegend *legend(nullptr);
    legend = new TLegend(left, 0.25, left + width, 0.70);
    legend->SetFillStyle(0);
    legend->SetBorderSize(0);
    for(auto hist = mc_hists.rbegin(); hist != mc_hists.rend(); hist++)
        legend->AddEntry(*hist, (*hist)->GetName(), "f");
    legend->AddEntry(data_hist, "data", "l");

    TPaveText* textatlas(nullptr);
    textatlas = new TPaveText(left, 0.73, left + width, 0.87, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(12);
    textatlas->SetTextSize(0.04);
    textatlas->AddText(text_strings.at(0));
    textatlas->AddText(text_strings.at(1));

    legend->Draw();
    textatlas->Draw();
}

void GetYields(const TH1D *data_hist, const std::vector<TH1D*> &mc_hists, const TString &region)
{
    double yield_total = 0.;
    double yield_bkg = 0.;
    double yield_other = 0.;
    double yield_other_bkg = 0.;
    double yield_zjets = 0.;
    double err_total = 0.;
    double err_bkg = 0.;
    double err_other = 0.;
    double err_other_bkg = 0.;
    double err_zjets = 0.;
    for(auto hist : mc_hists)
    {
        double yield = hist->GetSumOfWeights();
        double err = GetHistError(hist);

        if(verbose >= 2)
            std::cout << hist->GetName() << "\t"
                      << TString::Format("%.3f", yield) << " +/- "
                      << TString::Format("%.3f", err) << std::endl;

        yield_total += yield;
        err_total += err*err;

        if(!((TString)(hist->GetName())).Contains("EWKZZ") && !((TString)(hist->GetName())).Contains("inclusive"))
        {
            yield_bkg += yield;
            err_bkg += err*err;
        }

        if(((TString)(hist->GetName())).Contains("Zjets"))
        {
            yield_zjets += yield;
            err_zjets += err*err;
        }
    }

    yield_other = yield_total - yield_zjets;
    yield_other_bkg = yield_bkg - yield_zjets;
    err_other = err_total - err_zjets;
    err_other_bkg = err_bkg - err_zjets;

    double yield_data = data_hist->GetSumOfWeights();

    if(verbose >= 2)
    {
        std::cout << TString::Format("bkg\t%.3f +/- %.3f",   yield_bkg,       std::sqrt(err_bkg))       << std::endl;
        std::cout << TString::Format("oth b\t%.3f +/- %.3f", yield_other_bkg, std::sqrt(err_other_bkg)) << std::endl;
        std::cout << TString::Format("other\t%.3f +/- %.3f", yield_other,     std::sqrt(err_other))     << std::endl;
        std::cout << TString::Format("total\t%.3f +/- %.3f", yield_total,     std::sqrt(err_total))     << std::endl;
        std::cout << TString::Format("data\t%.3f +/- %.3f",  yield_data,      sqrt(yield_data))         << std::endl;
        std::cout << TString::Format("data/MC\t%.3f", yield_data/yield_total) << std::endl;
        if(region != "A") std::cout << TString::Format("data driven/MC\t%.3f", (yield_data - yield_other_bkg)/yield_zjets) << std::endl;
        else              std::cout << "data driven/MC\t" << std::endl;
        std::cout << std::endl;
    }

    yields_data.at(region)   = data_hist->GetSumOfWeights();
    yields_total.at(region)  = yield_total;
    yields_signal.at(region) = yields_total.at(region) - yield_bkg;
    yields_zjets.at(region)  = yield_zjets;
    yields_other_bkg.at(region) = yield_bkg - yield_zjets;
    errors_stat.at(region)   = err_zjets/yield_zjets/yield_zjets;
}

void Bias(TString leaf, TString region, int event_type)
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    const TString event_type_str(TString::Itoa(event_type, 10));

    int n_bins = 0;
    double *xbins;
    if(leaf == "Z_pT")     {xbins = x_z_pt;     n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);}
    if(leaf == "dLepR")    {xbins = x_dlepr;    n_bins = sizeof(x_dlepr)/sizeof(x_dlepr[0]);}
    if(leaf == "met_tst")  {xbins = x_met_tst;  n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);}
    if(leaf == "dMetZPhi") {xbins = x_dmetzphi; n_bins = sizeof(x_dmetzphi)/sizeof(x_dmetzphi[0]);}
    if(leaf == "n_jets")   {xbins = x_n_jets;   n_bins = sizeof(x_n_jets)/sizeof(x_n_jets[0]);}



    Data datas;
    TChain *data_tree = new TChain("tree_PFLOW");
    TH1D *data_hist = new TH1D("data", "", n_bins - 1, xbins);
    for(auto data : datas.datas)
        data_tree->Add(datas.data_path + data);
    DoReco(data_tree, data_hist, region, leaf, is_data, event_type_str);

    data_hist->SetLineColor(kBlack);
    data_hist->SetMarkerColor(kBlack);
    data_hist->SetMarkerStyle(kFullSquare);
    data_hist->SetMarkerSize(0.75);
    //data_hist->SetMaximum(1.1*data_hist->GetMaximum());

    MC mcs;
    THStack *hs = new THStack("hs", "");
    std::vector<TChain*> mc_trees;
    std::vector<TH1D*> mc_hists;
    for(const auto &mc : mcs.mcs)
    {
        std::cout << mc.first << std::endl;
        mc_trees.emplace_back(new TChain("tree_PFLOW"));
        for(auto file : mc.second)
            mc_trees.back()->Add(mcs.mc_path_nominal + file);

        mc_hists.emplace_back(new TH1D(mc.first, "", n_bins - 1, xbins));

        DoReco(mc_trees.back(), mc_hists.back(), region, leaf, is_mc, event_type_str);

        //if(mc.first.Contains("WZ")  && event_type == ee)   mc_hists.back()->Scale(1.02);
        //if(mc.first.Contains("WW")  && event_type == ee)   mc_hists.back()->Scale(1.23);
        //if(mc.first.Contains("top") && event_type == ee)   mc_hists.back()->Scale(1.06);
        //if(mc.first.Contains("WZ")  && event_type == mumu) mc_hists.back()->Scale(1.02);
        //if(mc.first.Contains("WW")  && event_type == mumu) mc_hists.back()->Scale(1.23);
        //if(mc.first.Contains("top") && event_type == mumu) mc_hists.back()->Scale(1.06);
        mc_hists.back()->SetLineColor(colors.at(mc.first));
        mc_hists.back()->SetLineWidth(2);
        mc_hists.back()->SetFillColor(colors.at(mc.first));
    }

    std::sort(mc_hists.begin(),
         mc_hists.end(),
         [&](const TH1D *hist1, const TH1D *hist2) -> bool
         {
             return hist1->GetSumOfWeights() < hist2->GetSumOfWeights();
         }
        );

    for(auto hist : mc_hists)
        hs->Add(hist);

    auto mc_hist = dynamic_cast<TH1D*>(mc_hists.at(0)->Clone("mc_hist"));
    mc_hist->SetLineColor(kWhite);
    for(int i = 1; i < mc_hists.size(); i++)
        mc_hist->Add(mc_hists.at(i));

    TCanvas *c = new TCanvas("c", "c", 800, 600);
    c->cd();

    data_hist->SetMinimum(1.);
    TRatioPlot *rp = new TRatioPlot(data_hist, mc_hist);
    rp->SetH1DrawOpt("e");
    rp->SetH2DrawOpt("e");
    rp->Draw();

    rp->SetLeftMargin(0.12);
    rp->SetSeparationMargin(0.02);

    double upper_max = data_hist->GetMaximum() > hs->GetMaximum() ? data_hist->GetMaximum() : hs->GetMaximum();
    rp->GetUpperRefXaxis()->SetTitle(title.at(leaf));
    rp->GetUpperRefYaxis()->SetTitle("events/bin");
    //dynamic_cast<TH1D*>(rp->GetUpperRefObject())->SetMaximum(1.1*upper_max);
    rp->GetUpperPad()->SetLogy();
    rp->GetUpperPad()->Update();

    //rp->GetLowerPad()->SetLogy();
    rp->GetLowerRefYaxis()->SetRangeUser(0.5, 1.5);
    rp->GetLowerRefYaxis()->SetTitle("#frac{data}{MC}");
    rp->GetLowerRefGraph()->SetLineColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerStyle(kFullSquare);
    rp->GetLowerRefGraph()->SetMarkerColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerSize(0.6);
    rp->GetLowerPad()->Update();

    rp->GetUpperPad()->cd();
    hs->Draw("hist same");
    data_hist->Draw("E1 same");
    std::vector<TString> text_strings = {"#it{ATLAS} #bf{internal}",
                                         "#intLdt = 139fb^{-1}, #sqrt{s} = 13TeV, " + (TString)(event_type_str == "0" ? "Z#rightarrow#mu#mu" : "Z#rightarrowee")};
    DrawLegendText(data_hist, mc_hists, text_strings);

    TString name = "distribution_" + leaf + "_region_" + region + "_" + event_type_str + "_advance";
    //TString name = "distribution_region_" + region + "_" + event_type_str;
    if(verbose == 3) c->SaveAs("fig/" + name + "_VBS.png");
    if(verbose == 3) c->SaveAs("fig/" + name + "_VBS.pdf");

    GetYields(data_hist, mc_hists, region);
}

double Extraction()
{
    //double a = yields_total.at("A") - yields_signal.at("A") - yields_other_bkg.at("A");
    double a = yields_zjets.at("A");
    //double a = yields_total.at("A") - yields_other_bkg.at("A");
    double b = yields_data.at("B") - yields_other_bkg.at("B");
    double c = yields_data.at("C") - yields_other_bkg.at("C");
    double d = yields_data.at("D") - yields_other_bkg.at("D");

    if(verbose)
        std::cout << "Zjets yields\tdata driven\tMC\tpurity" << std::endl
                  << TString::Format("a:\t\t    \t\t%.3f\t%.3f",    yields_zjets.at("A"), yields_zjets.at("A")/yields_total.at("A")) << std::endl
                  << TString::Format("b:\t\t%.3f\t\t%.3f\t%.3f", b, yields_zjets.at("B"), yields_zjets.at("B")/yields_total.at("B")) << std::endl
                  << TString::Format("c:\t\t%.3f\t\t%.3f\t%.3f", c, yields_zjets.at("C"), yields_zjets.at("C")/yields_total.at("C")) << std::endl
                  << TString::Format("d:\t\t%.3f\t%.3f\t%.3f",   d, yields_zjets.at("D"), yields_zjets.at("D")/yields_total.at("D")) << std::endl;

    double epsilon = yields_zjets.at("A")/yields_zjets.at("C")/(yields_zjets.at("B")/yields_zjets.at("D")) - 1.;
    double error_stat = std::sqrt(errors_stat.at("B") + errors_stat.at("C") + errors_stat.at("D"));
    double extracted = b*c/d*(1 + epsilon);
    if(verbose)
        std::cout << "epsilon	" << epsilon << std::endl
                  << TString::Format("extracted Zjets in SR(C*B/D): %.3f +/- %.3f", extracted, error_stat*extracted) << std::endl;

    double epsilon_data_plus_1 = 1./c/(b/d);
    //return epsilon_data_plus_1;
    return epsilon;
}

int main(int argc, char *argv[])
{
    verbose = 3;

//    ConstructRegions(argc, argv, ee);
// 
//    Bias("n_jets", "A", ee);
//    Bias("n_jets", "B", ee);
//    Bias("n_jets", "C", ee);
//    Bias("n_jets", "D", ee);
//    std::cout << Extraction() << std::endl << std::endl;

//    std::cout << std::endl;

    ConstructRegions(argc, argv, mumu);
  
    Bias("n_jets", "A", mumu);
    Bias("n_jets", "B", mumu);
    Bias("n_jets", "C", mumu);
    Bias("n_jets", "D", mumu);
    std::cout << Extraction() << std::endl << std::endl;
}
