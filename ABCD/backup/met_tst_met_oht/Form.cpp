#include <iostream>
#include <string>
#include <vector>

#include "TStyle.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLine.h"
#include "TText.h"
#include "TPaveText.h"
#include "TLegend.h"

void Form()
{
    gStyle->SetOptStat(0);
    gStyle->SetPalette(kDeepSea);

    TCanvas *c = new TCanvas("c", "", 800, 600);

    //TString xlab[] = {"70", " ", " ", "90", "110", " ", " ", " ", "#infty"};
    //TString ylab[] = {"0", "2.3", "#pi"};
    //TString xlab[] = {" ", "&& 70<E_{T}^{missing}<90", " ", " ", " ", " ", "&& E_{T}^{missing}>100GeV", " ", " "};
    //TString ylab[] = {"No", "Yes", " "};
    //TString xlab[] = {" ", "&& 70<E_{T}^{missing}<90", " ", " ", " ", " ", "&& E_{T}^{missing}>100GeV", " ", " "};
    //TString ylab[] = {"No", "Yes", " "};
    TString xlab[] = {" ", "E_{T}^{missing}<85GeV",      " ", " ", "E_{T}^{missing}>110GeV",  " "};
    TString ylab[] = {" ", "0.3<E_{T}^{missing}/HT<0.5", " ", " ", "E_{T}^{missing}/HT>0.65", " "};
    const int nbinx = sizeof(xlab)/sizeof(xlab[0]) - 1;
    const int nbiny = sizeof(ylab)/sizeof(ylab[0]) - 1;
    //double xbin[nbinx + 1] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    //double ybin[nbiny + 1] = {0, 1, 2};
    double xbin[nbinx + 1] = {0, 1, 2, 3, 4, 5};
    double ybin[nbinx + 1] = {0, 1, 2, 3, 4, 5};
    TH2D *tabl = new TH2D("tabl", "", nbinx, xbin, nbiny, ybin);
    //tabl->SetBinContent(5, 2, 1);
    //tabl->SetBinContent(6, 2, 1);
    //tabl->SetBinContent(7, 2, 1);
    //tabl->SetBinContent(8, 2, 1);
    tabl->SetBinContent(4, 4, 1);
    tabl->SetBinContent(4, 5, 1);
    tabl->SetBinContent(5, 4, 1);
    tabl->SetBinContent(5, 5, 1);
    //tabl->SetCanExtend(TH1::kAllAxes);

    TAxis *axix = tabl->GetXaxis();
    TAxis *axiy = tabl->GetYaxis();
    for(int i = 0; i < nbinx + 1; i++)
        axix->ChangeLabel(i + 1, -1, -1, 22, -1, -1, xlab[i]);
    for(int j = 0; j < nbiny + 1; j++)
        //axiy->ChangeLabel(j + 1, -1, -1, -1, -1, -1, ylab[j]);
        axiy->ChangeLabel(j + 1, 90, -1, 21, -1, -1, ylab[j]);
    //axix->CenterLabels();
    //axiy->CenterLabels();
    axix->SetTickLength(0.);
    axiy->SetTickLength(0.);
    axix->SetNdivisions(nbinx + 1);
    axiy->SetNdivisions(nbiny + 1);
    //axix->SetTitle("E_{T}^{missing} significance");
    //axiy->SetTitle("#Delta#Phi(Z,E_{T}^{missing})");
    //axix->SetTitle("MetOHT>0.65");
    //axiy->SetTitle("dLepR<1.7 && #Delta#Phi(Z,E_{T}^{missing})>2.2 && n_bjets==0");

    tabl->Draw("col");
    gPad->Update();

    TLine *line = new TLine();
    line->SetLineColor(kGray + 2);
    line->SetLineStyle(kDashed);
    //line->DrawLine(3., 0., 3., 2.);
    //line->DrawLine(4., 0., 4., 2.);
    //line->DrawLine(0., 1., 8., 1.);
    line->DrawLine(2., 0., 2., 5.);
    line->DrawLine(3., 0., 3., 5.);
    line->DrawLine(0., 2., 5., 2.);
    line->DrawLine(0., 3., 5., 3.);

    TPaveText* textatlas = new TPaveText(0.13, 0.77, 0.30, 0.87, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(12);
    textatlas->SetTextSize(0.03);
    textatlas->AddText("#it{ATLAS} #bf{internal}");
    textatlas->AddText("#bf{ABCD definition}");
    dynamic_cast<TText*>(textatlas->GetListOfLines()->First())->SetTextSize(0.04);
    textatlas->Draw();

    double lmar = gPad->GetLeftMargin();
    double rmar = gPad->GetRightMargin();
    double tmar = gPad->GetTopMargin();
    double bmar = gPad->GetBottomMargin();
    TText *text = new TText();
    text->SetTextSize(0.03);
    //text->DrawText(6. , 1.5, "A");
    //text->DrawText(1.5, 1.5, "B");
    //text->DrawText(6. ,  .5, "C");
    //text->DrawText(1.5,  .5, "D");
    text->DrawText(4. , 4. , "A");
    text->DrawText(1. , 4. , "B");
    text->DrawText(4. , 1. , "C");
    text->DrawText(1. , 1. , "D");

    //c->SaveAs("fig/region.png");
    c->SaveAs("fig/advance.png");
}

int main(int argc, char *argv[])
{
    Form();
}
