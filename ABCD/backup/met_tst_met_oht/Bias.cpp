#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>

#include "TStyle.h"
#include "TString.h"
#include "TChain.h"
#include "TCut.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "THStack.h"
#include "TRatioPlot.h"
#include "TLegend.h"
#include "TPad.h"
#include "TPaveText.h"
#include "TLine.h"

#include "inc/Data.h"
#include "inc/MC.h"

enum {mumu, ee};
enum {is_data, is_mc};

std::map<TString, int> colors = {{"Zjets",  kMagenta + 2},
                                 {"inclusive", kBlue - 6},
                                 //{"EWKZZ", kBlue - 6},
                                 //{"QCDZZ", kYellow - 4},
                                 {"WZ",     kCyan - 6},
                                 {"top",    kAzure - 3},
                                 {"llll",   kYellow + 2},
                                 {"llqq",   kGray + 3},
                                 {"lllljj", kGray + 2},
                                 {"ttbarV", kViolet - 4},
                                 {"WW",     kGreen + 2},
                                 {"Wt",     kSpring + 4},
                                 {"VVV",    kOrange + 10},
                                 {"Ztt",    kRed + 1},
                                 {"Wjets",  kRed - 7}
                                };

TString scale = "scale*weight";
TCut pre_selection = "met_tst>70&&leading_pT_lepton>30&&subleading_pT_lepton>20&&M2Lep>80&&M2Lep<110&&dLepR<1.8&&n_bjets==0&&dMetZPhi>2.7";
TCut var1_1 = "met_tst>110";
TCut var1_2 = "met_tst<85";
TCut var2_1 = "MetOHT>0.65";
TCut var2_2 = "MetOHT>0.3&&MetOHT<0.5";

std::map<TString, TCut> regions = {{"A", var1_1&&var2_1},
                                   {"B", var1_2&&var2_1},
                                   {"C", var1_1&&var2_2},
                                   {"D", var1_2&&var2_2},
                                  };

std::map<TString, TString> axis_label = {{"met_signif", "E^{missing}_{T} signif"},
                                         {"met_tst",    "E^{missing}_{T}"},
                                         {"dMetZPhi",   "#Delta(E^{missing}_{T},#Phi)"},
                                         {"dLepR",      "#DeltaR(l,l)"},
                                         {"Z_pT",       "p_{T}^{Z}"}
                                        };

double x_z_pt[]     = {0., 100., 160., 250., 750., 1600.};
//double x_dlepr[]    = {0., 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4};
double x_dlepr[]    = {0., 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0};
double x_met_tst[]  = {70., 90., 110., 130., 150., 170., 190., 210., 230., 250., 300., 500., 1000.};
double x_dmetzphi[] = {0., 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2};

std::map<TString, double> yields_data  = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_total = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_signal = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_zjets = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_other_bkg = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};

void ConstructRegions(int argc, char *argv[])
{
    if     (argc == 2)
    {
        var1_2 = argv[1];
        regions.at("B") = var1_2&&var2_1;
        regions.at("D") = var1_2&&var2_2;
    }
    else if(argc == 3)
    {
        var1_2 = argv[1];
        var2_2 = (TString)"MetOHT>0.3&&" + argv[2];
        regions.at("B") = var1_2&&var2_1;
        regions.at("C") = var1_1&&var2_2;
        regions.at("D") = var1_2&&var2_2;
    }
}

double GetHistError(const TH1D *hist)
{
    double err = 0.;
    for(int i = 0; i < hist->GetSumw2N(); i++) err += hist->GetSumw2()->At(i);
    return sqrt(err);
}

void DoReco(TChain *tree, TH1D *hist, TString region, TString leaf,
            int if_mc, const TString &type)
{
    //std::cout << "Plot " << hist->GetName() << " ..." << std::endl;

    TCut inclusive("");

    tree->Draw(leaf + ">>" + hist->GetName(),
               scale*(regions.at(region)&&pre_selection&&("event_type==" + type))
               //scale*(regions.at(region)&&pre_selection)
              );
}

void DrawLegendText(TH1D *data_hist, std::vector<TH1D*> mc_hists, const std::vector<TString> &text_strings)
{
    double left(0.15);
    double width(0.4);

    TLegend *legend(nullptr);
    legend = new TLegend(left, 0.25, left + width, 0.70);
    legend->SetFillStyle(0);
    legend->SetBorderSize(0);
    for(auto hist = mc_hists.rbegin(); hist != mc_hists.rend(); hist++)
        legend->AddEntry(*hist, (*hist)->GetName(), "f");
    legend->AddEntry(data_hist, "data", "l");

    TPaveText* textatlas(nullptr);
    textatlas = new TPaveText(left, 0.73, left + width, 0.87, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(12);
    textatlas->SetTextSize(0.04);
    textatlas->AddText(text_strings.at(0));
    textatlas->AddText(text_strings.at(1));

    legend->Draw();
    textatlas->Draw();
}

void GetYields(const TH1D *data_hist, const std::vector<TH1D*> &mc_hists, const TString &region)
{
    //std::cout << region << std::endl;

    double yield_total = 0.;
    double yield_bkg = 0.;
    double yield_other_bkg = 0.;
    double yield_zjets = 0.;
    double err_total = 0.;
    double err_bkg = 0.;
    double err_other_bkg = 0.;
    double err_zjets = 0.;
    for(auto hist : mc_hists)
    {
        double yield = hist->GetSumOfWeights();
        double err = GetHistError(hist);
/*
        std::cout << hist->GetName() << "\t"
                  << yield << " +/- "
                  << err << std::endl;
*/
        yield_total += yield;
        err_total += err*err;

        if(!((TString)(hist->GetName())).Contains("ZZ") && !((TString)(hist->GetName())).Contains("inclusive"))
        {
            yield_bkg += yield;
            err_bkg += err*err;
        }

        if(((TString)(hist->GetName())).Contains("Zjets"))
        {
            yield_zjets += yield;
            err_zjets += err*err;
        }
    }
    yield_other_bkg = yield_bkg - yield_zjets;
    err_other_bkg = err_bkg - err_zjets;

    //std::cout << "bkg\t" << yield_bkg << " +/- " << std::sqrt(err_bkg) << std::endl;
    //std::cout << "total\t" << yield_total << " +/- " << std::sqrt(err_total) << std::endl;
    //std::cout << "data\t" << data_hist->GetEntries() << " +/- " << sqrt(data_hist->GetEntries()) << std::endl;
    //if(region != "A") std::cout << "data driven/MC\t" << (data_hist->GetEntries() - yield_other_bkg)/yield_zjets << std::endl;
    //else              std::cout << "data driven/MC\t" << std::endl;
    //std::cout << std::endl;

    yields_data.at(region)  = data_hist->GetEntries();
    yields_total.at(region) = yield_total;
    yields_signal.at(region) = yields_total.at(region) - yield_bkg;
    yields_zjets.at(region) = yield_zjets;
    yields_other_bkg.at(region) = yield_bkg - yield_zjets;
}

void Bias(TString leaf, TString region, int event_type)
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    const TString event_type_str(TString::Itoa(event_type, 10));

    int n_bins = 0;
    double *xbins;
    if(leaf == "Z_pT")     {xbins = x_z_pt;     n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);}
    if(leaf == "dLepR")    {xbins = x_dlepr;    n_bins = sizeof(x_dlepr)/sizeof(x_dlepr[0]);}
    if(leaf == "met_tst")  {xbins = x_met_tst;  n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);}
    if(leaf == "dMetZPhi") {xbins = x_dmetzphi; n_bins = sizeof(x_dmetzphi)/sizeof(x_dmetzphi[0]);}

    Data datas;
    TChain *data_tree = new TChain("tree_PFLOW");
    TH1D *data_hist = new TH1D("data", "", n_bins - 1, xbins);
    for(auto data : datas.datas)
        data_tree->Add(datas.data_path + data);
    DoReco(data_tree, data_hist, region, leaf, is_data, event_type_str);

    data_hist->SetLineColor(kBlack);
    data_hist->SetMarkerColor(kBlack);
    data_hist->SetMarkerStyle(kFullSquare);
    data_hist->SetMarkerSize(0.75);
    //data_hist->SetMaximum(1.1*data_hist->GetMaximum());

    MC mcs;
    THStack *hs = new THStack("hs", "");
    std::vector<TChain*> mc_trees;
    std::vector<TH1D*> mc_hists;
    for(const auto &mc : mcs.mcs)
    {
        mc_trees.emplace_back(new TChain("tree_PFLOW"));
        for(auto file : mc.second)
            mc_trees.back()->Add(mcs.mc_path_nominal + file);

        mc_hists.emplace_back(new TH1D(mc.first, "", n_bins - 1, xbins));

        DoReco(mc_trees.back(), mc_hists.back(), region, leaf, is_mc, event_type_str);

        if(mc.first.Contains("WZ")  && event_type == ee)   mc_hists.back()->Scale(1.02);
        if(mc.first.Contains("WW")  && event_type == ee)   mc_hists.back()->Scale(1.23);
        if(mc.first.Contains("top") && event_type == ee)   mc_hists.back()->Scale(1.06);
        if(mc.first.Contains("WZ")  && event_type == mumu) mc_hists.back()->Scale(1.02);
        if(mc.first.Contains("WW")  && event_type == mumu) mc_hists.back()->Scale(1.23);
        if(mc.first.Contains("top") && event_type == mumu) mc_hists.back()->Scale(1.06);
        mc_hists.back()->SetLineColor(colors.at(mc.first));
        mc_hists.back()->SetLineWidth(2);
        mc_hists.back()->SetFillColor(colors.at(mc.first));
    }

    std::sort(mc_hists.begin(),
         mc_hists.end(),
         [&](const TH1D *hist1, const TH1D *hist2) -> bool
         {
             return hist1->GetSumOfWeights() < hist2->GetSumOfWeights();
         }
        );


    for(auto hist : mc_hists)
        hs->Add(hist);

    auto mc_hist = dynamic_cast<TH1D*>(mc_hists.at(0)->Clone("mc_hist"));
    mc_hist->SetLineColor(kWhite);
    for(int i = 1; i < mc_hists.size(); i++)
        mc_hist->Add(mc_hists.at(i));

    TCanvas *c = new TCanvas("c", "c", 800, 600);
    c->cd();

    data_hist->SetMinimum(1.);
    TRatioPlot *rp = new TRatioPlot(data_hist, mc_hist);
    rp->SetH1DrawOpt("e");
    rp->SetH2DrawOpt("e");
    rp->Draw();

    rp->SetLeftMargin(0.12);
    rp->SetSeparationMargin(0.02);

    double upper_max = data_hist->GetMaximum() > hs->GetMaximum() ? data_hist->GetMaximum() : hs->GetMaximum();
    rp->GetUpperRefXaxis()->SetTitle(axis_label.at(leaf));
    rp->GetUpperRefYaxis()->SetTitle("events/bin");
    //dynamic_cast<TH1D*>(rp->GetUpperRefObject())->SetMaximum(1.1*upper_max);
    rp->GetUpperPad()->SetLogy();
    rp->GetUpperPad()->Update();

    //rp->GetLowerPad()->SetLogy();
    rp->GetLowerRefYaxis()->SetRangeUser(0.5, 1.5);
    rp->GetLowerRefYaxis()->SetTitle("#frac{data}{MC}");
    rp->GetLowerRefGraph()->SetLineColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerStyle(kFullSquare);
    rp->GetLowerRefGraph()->SetMarkerColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerSize(0.6);
    rp->GetLowerPad()->Update();

    rp->GetUpperPad()->cd();
    hs->Draw("hist same");
    data_hist->Draw("E1 same");
    std::vector<TString> text_strings = {"#it{ATLAS} #bf{internal}",
                                         "#intLdt = 139fb^{-1}, #sqrt{s} = 13TeV, " + (TString)(event_type_str == "0" ? "Z#rightarrow#mu#mu" : "Z#rightarrowee")};
    DrawLegendText(data_hist, mc_hists, text_strings);

    TString name = "distribution_" + leaf + "_region_" + region + "_" + event_type_str + "_advance";
    //TString name = "distribution_region_" + region + "_" + event_type_str;
    //c->SaveAs("fig/" + name + ".png");

    GetYields(data_hist, mc_hists, region);
}

double Extraction()
{
    //double a = yields_total.at("A") - yields_signal.at("A") - yields_other_bkg.at("A");
    double a = yields_zjets.at("A");
    //double a = yields_total.at("A") - yields_other_bkg.at("A");
    double b = yields_data.at("B") - yields_other_bkg.at("B");
    double c = yields_data.at("C") - yields_other_bkg.at("C");
    double d = yields_data.at("D") - yields_other_bkg.at("D");
/*
    std::cout << "Zjets yields\tdata driven\tMC" << std::endl
              << "a:\t\t"         "\t\t" << yields_zjets.at("A") << std::endl
              << "b:\t\t" << b << "\t\t" << yields_zjets.at("B") << std::endl
              << "c:\t\t" << c << "\t\t" << yields_zjets.at("C") << std::endl
              << "d:\t\t" << d << "\t\t" << yields_zjets.at("D") << std::endl;
*/
    double epsilon = yields_zjets.at("A")/yields_zjets.at("C")/(yields_zjets.at("B")/yields_zjets.at("D")) - 1.;
//    std::cout << "epsilon	" << epsilon << std::endl;
//    std::cout << "extracted Zjets in SR(C*B/D): " << b*c/d*(1 + epsilon) << std::endl;

    return epsilon;
}

int main(int argc, char *argv[])
{
    ConstructRegions(argc, argv);

    Bias("dLepR", "A", ee);
    Bias("dLepR", "B", ee);
    Bias("dLepR", "C", ee);
    Bias("dLepR", "D", ee);
    std::cout << Extraction() << std::endl;

//    std::cout << std::endl;
/*
    Bias("dLepR", "A", mumu);
    Bias("dLepR", "B", mumu);
    Bias("dLepR", "C", mumu);
    Bias("dLepR", "D", mumu);
    std::cout << Extraction() << std::endl;
*/
}
