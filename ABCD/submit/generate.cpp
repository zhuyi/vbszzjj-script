#include "define.h"

TString get_cuts(int i, int j)
{
    double cut_i = i*(up_i - dw_i)/n_i + dw_i;
    double cut_j = j*(up_j - dw_j)/n_j + dw_j;

    std::cout << cut_i << ", " << cut_j << std::endl;

    //return TString::Format("\"met_tst<%.1f\" \"MetOHT<%.2f\"", cut_i, cut_j);
    return TString::Format("\"MetOHT>%.4f\" \"dMetZPhi>%.4f\"", cut_i, cut_j);
}

void print_script(int i, int j)
{
    auto script_name = TString::Format("script/%d_%d.sh", i, j);
    std::ofstream script(script_name);

    script << "#!/bin/bash" << std::endl;
    script << std::endl;   
    script << "source /home/zhuyifan/DarkShine/dp.env" << std::endl;   
    script << "export PATH=\"/lustre/collider/zhuyifan/VBSZZ/ABCD/EB/bin:$PATH\"" << std::endl;
    script << "bias " + get_cuts(i, j) << std::endl;

    script.close();

    system("chmod 744 " + script_name);
}

void print_condor(const std::vector<TString> &jobs)
{
    TString condor_name = "submit.condor";
    std::ofstream condor(condor_name);

    condor << "Executable = script/$(job).sh" << std::endl;
    condor << "Error      = log/$(job).err"   << std::endl;
    condor << "Output     = log/$(job).out"   << std::endl;
    condor << "Log        = log/$(job).log"   << std::endl;
    condor << std::endl;
    condor << "+JobFlavour = \"testmatch\"" << std::endl;
    condor << "Queue job from (" << std::endl;
    for(const auto &job : jobs) condor << "                " + job << std::endl;
    condor << "               )" << std::endl;

    condor.close();
}

void generate()
{
    std::vector<TString> jobs;

    for(int i = 0; i <= n_i; i++)
    {
        for(int j = 0; j <= n_j; j++)
        {
            print_script(i, j);
            jobs.push_back(TString::Format("%d_%d", i, j));
        }
    }

    print_condor(jobs);
}
