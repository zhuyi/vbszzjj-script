#include "define.h"

TString read_output(int i, int j)
{
    auto output_name = TString::Format("log/%d_%d.out", i, j);
    std::ifstream output(output_name);

    TString line;
    line.ReadLine(output);

    output.close();

    return line;
}

void collect()
{
    std::cout << ",";
    for(int j = 0; j <= n_j; j++)
        std::cout << j*(up_j - dw_j)/n_j + dw_j << ",";
    std::cout << std::endl;

    for(int i = 0; i <= n_i; i++)
    {
        std::cout << i*(up_i - dw_i)/n_i + dw_i << ",";
        for(int j = 0; j <= n_j; j++)
            std::cout << read_output(i, j) + ",";
        std::cout << std::endl;
    }
}
