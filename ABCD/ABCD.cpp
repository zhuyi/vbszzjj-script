#include <vector>
#include <map>
#include <algorithm>
#include <iostream>

#include "TStyle.h"
#include "TString.h"
#include "TChain.h"
#include "TCut.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "THStack.h"
#include "TRatioPlot.h"
#include "TLegend.h"
#include "TPad.h"
#include "TPaveText.h"
#include "TLine.h"

#include "inc/Data.h"
#include "inc/MC.h"

enum {mumu, ee};
enum {is_data, is_mc};

std::map<TString, int> colors = {{"Zjets",  kMagenta + 2},
                                 {"inclusive", kBlue - 6},
                                 //{"EWKZZ", kBlue - 6},
                                 //{"QCDZZ", kYellow - 4},
                                 {"WZ",     kCyan - 6},
                                 {"top",    kAzure - 3},
                                 {"llll",   kYellow + 2},
                                 {"llqq",   kGray + 3},
                                 {"lllljj", kGray + 2},
                                 {"ttbarV", kViolet - 4},
                                 {"WW",     kGreen + 2},
                                 {"Wt",     kSpring + 4},
                                 {"VVV",    kOrange + 10},
                                 {"Ztt",    kRed + 1},
                                 {"Wjets",  kRed - 7}
                                };

TString scale = "scale*weight";
//TCut pre_selection = "met_tst>70&&leading_pT_lepton>30&&subleading_pT_lepton>20&&M2Lep>80&&M2Lep<110&&dLepR<1.8&&n_bjets==0&&dMetZPhi>2.2";
//TCut var1 = "met_tst>110&&dMetZPhi>2.7&&MetOHT>0.65";
//TCut var2 = "met_tst<85&&MetOHT>0.3&&MetOHT<0.5";
//TCut var1_1 = "met_tst>110&&dMetZPhi>2.7";
//TCut var1_2 = "met_tst>70&&met_tst<85";
//TCut var2_1 = "MetOHT>0.65";
//TCut var2_2 = "MetOHT>0.3&&MetOHT<0.5";

TCut pre_selection = "met_tst>70&&leading_pT_lepton>30&&subleading_pT_lepton>20&&M2Lep>80&&M2Lep<110&&MetOHT>0.2&&dMetZPhi>2.2";
TCut var1_1 = "met_tst>110&&MetOHT>0.65";
TCut var1_2 = !var1_1;
TCut var2_1 = "dLepR<1.8&&dMetZPhi>2.7&&n_bjets==0";
TCut var2_2 = !var2_1;

std::map<TString, TCut> regions = {{"A", var1_1&&var2_1},
                                   {"B", var1_2&&var2_1},
                                   {"C", var1_1&&var2_2},
                                   {"D", var1_2&&var2_2},
                                  };

std::map<TString, TString> axis_label = {{"met_signif", "E^{missing}_{T} signif"},
                                         {"dMetZPhi",   "#Delta(E^{missing}_{T},#Phi)"},
                                         {"met_tst",    "E^{missing}_{T}"},
                                         {"Z_pT",       "p_{T}^{Z}"}
                                        };

double x_z_pt[] = {0., 100., 160., 250., 750., 1600.};

std::map<TString, double> yields_data  = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_other = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};
std::map<TString, double> yields_zjets = {{"A", 0.}, {"B", 0.}, {"C", 0.}, {"D", 0.}};

double GetHistError(const TH1D *hist)
{
    double err = 0.;
    for(int i = 0; i < hist->GetSumw2N(); i++) err += hist->GetSumw2()->At(i);
    return sqrt(err);
}

void DoReco(TChain *tree, TH1D *hist, TString region, TString leaf,
            int if_mc, int type)
{
    //std::cout << "Plot " << hist->GetName() << " ..." << std::endl;

    TCut inclusive("");

    tree->Draw(leaf + ">>" + hist->GetName(),
               scale*(regions.at(region)&&pre_selection&&(TString)("event_type==" + TString::Itoa(type, 10)))
               //scale*(regions.at(region)&&pre_selection)
              );
}

void ABCD(TString leaf, TString region, int event_type)
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    int n_bins = 0;
    double *xbins;
    if(leaf == "Z_pT") {xbins = x_z_pt; n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);}

    Data datas;
    TChain *data_tree = new TChain("tree_PFLOW");
    TH1D *data_hist = new TH1D("data", "", n_bins - 1, xbins);
    for(auto data : datas.datas)
        data_tree->Add(datas.data_path + data);
    DoReco(data_tree, data_hist, region, leaf, is_data, event_type);

    data_hist->SetLineColor(kBlack);
    data_hist->SetMarkerColor(kBlack);
    data_hist->SetMarkerStyle(kFullSquare);
    data_hist->SetMarkerSize(0.75);
    //data_hist->SetMaximum(1.1*data_hist->GetMaximum());

    MC mcs;
    THStack *hs = new THStack("hs", "");
    std::vector<TChain*> mc_trees;
    std::vector<TH1D*> mc_hists;
    for(auto mc : mcs.mcs)
    {
        mc_trees.emplace_back(new TChain("tree_PFLOW"));
        for(auto file : mc.second)
            mc_trees.back()->Add(mcs.mc_path_nominal + file);

        mc_hists.emplace_back(new TH1D(mc.first, "", n_bins - 1, xbins));

        DoReco(mc_trees.back(), mc_hists.back(), region, leaf, is_mc, event_type);

        //if(mc.first.Contains("Zjets") && (region == "B" || region == "D")) mc_hists.back()->Scale(1.3);
        if(mc.first.Contains("WW")    && (region == "B" || region == "D")) mc_hists.back()->Scale(1.23);
        if(mc.first.Contains("top")   && (region == "B" || region == "D")) mc_hists.back()->Scale(1.06);

        mc_hists.back()->SetLineColor(colors.at(mc.first));
        mc_hists.back()->SetLineWidth(2);
        mc_hists.back()->SetFillColor(colors.at(mc.first));
    }

    std::sort(mc_hists.begin(),
         mc_hists.end(),
         [&](const TH1D *hist1, const TH1D *hist2) -> bool
         {
             return hist1->GetSumOfWeights() < hist2->GetSumOfWeights();
         }
        );

    for(auto hist : mc_hists)
        hs->Add(hist);

    TLegend *legend(nullptr);
    if(leaf == "dMetZPhi")
        legend = new TLegend(0.20, 0.25, 0.60, 0.70);
    else
        legend = new TLegend(0.50, 0.25, 0.90, 0.70);
    legend->SetFillStyle(0);
    legend->SetBorderSize(0);
    for(auto hist = mc_hists.rbegin();
             hist != mc_hists.rend();
             ++hist)
        legend->AddEntry(*hist, mcs.label.at((*hist)->GetName()), "f");
    legend->AddEntry(data_hist, "data", "l");

    TPaveText* textatlas(nullptr);
    if(leaf == "dMetZPhi") 
        textatlas = new TPaveText(0.20, 0.73, 0.60, 0.87, "brNDC");
    else
        textatlas = new TPaveText(0.50, 0.73, 0.90, 0.87, "brNDC");
    textatlas->SetFillColor(0);
    textatlas->SetFillStyle(0);
    textatlas->SetBorderSize(0);
    textatlas->SetTextAlign(12);
    textatlas->SetTextSize(0.04);
    textatlas->AddText("#bf{#it{ATLAS}} internal");
    textatlas->AddText("#intLdt = 139fb^{-1}, #sqrt{s} = 13TeV, mc16ade");

    TCanvas *c = new TCanvas("c", "c", 800, 600);
    c->cd();

    TRatioPlot *rp = new TRatioPlot(hs, data_hist);
    rp->Draw();

    rp->SetLeftMargin(0.12);
    rp->SetSeparationMargin(0.02);

    double upper_max = data_hist->GetMaximum() > hs->GetMaximum() ? data_hist->GetMaximum() : hs->GetMaximum();
    rp->GetUpperRefXaxis()->SetTitle(axis_label.at(leaf));
    rp->GetUpperRefYaxis()->SetTitle("events/bin");
    //dynamic_cast<THStack*>(rp->GetUpperRefObject())->SetMaximum(1.1*upper_max);
    rp->GetUpperPad()->SetLogy();
    rp->GetUpperPad()->Update();

    //rp->GetLowerPad()->SetLogy();
    rp->GetLowerRefYaxis()->SetRangeUser(0.5, 1.5);
    rp->GetLowerRefYaxis()->SetTitle("#frac{mc}{data}");
    rp->GetLowerRefGraph()->SetLineColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerStyle(kFullSquare);
    rp->GetLowerRefGraph()->SetMarkerColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerSize(0.6);
    rp->GetLowerPad()->Update();

    rp->GetUpperPad()->cd();
    legend->Draw();
    textatlas->Draw();

    //TString name = "distribution_region_" + region + "_" + TString::Itoa(event_type, 10) + "_advance";
    TString name = "distribution_region_" + region + "_" + TString::Itoa(event_type, 10);
    //c->SaveAs("fig/" + name + ".png");

    double yield_total = 0.;
    double yield_bkg = 0.;
    double yield_other = 0.;
    double yield_zjets = 0.;
    double err_total = 0.;
    double err_bkg = 0.;
    double err_other = 0.;
    double err_zjets = 0.;
    for(auto hist : mc_hists)
    {
        double yield = hist->GetSumOfWeights();
        double err = GetHistError(hist);

        std::cout << hist->GetName() << "\t"
                  << yield << " +/- "
                  << err << std::endl;

        yield_total += yield;
        err_total += err*err;

        if(!((TString)(hist->GetName())).Contains("ZZ") && !((TString)(hist->GetName())).Contains("inclusive"))
        {
            yield_bkg += yield;
            err_bkg += err*err;
        }

        if(!((TString)(hist->GetName())).Contains("Zjets"))
        {
            yield_other += yield;
            err_other += err*err;
        }
        else
        {
            yield_zjets += yield;
            err_zjets += err*err;
        }
    }
    std::cout << "bkg\t" << yield_bkg << " +/- " << std::sqrt(err_bkg) << std::endl;
    std::cout << "total\t" << yield_total << " +/- " << std::sqrt(err_total) << std::endl;
    std::cout << "data\t" << data_hist->GetSumOfWeights() << " +/- " << sqrt(data_hist->GetSumOfWeights()) << std::endl;

    yields_data.at(region)  = data_hist->GetSumOfWeights();
    yields_other.at(region) = yield_other;
    yields_zjets.at(region) = yield_zjets;
}

void Extraction()
{
    double b = yields_data.at("B") - yields_other.at("B");
    double c = yields_data.at("C") - yields_other.at("C");
    double d = yields_data.at("D") - yields_other.at("D");
    std::cout << "extracted  truth" << std::endl
              << "b: " << b << " " << yields_zjets.at("B") << std::endl
              << "c: " << c << " " << yields_zjets.at("C") << std::endl
              << "d: " << d << " " << yields_zjets.at("D") << std::endl;
    std::cout << "reonstructed Zjets: " << b*c/d << std::endl;
}

int main(int argc, char *argv[])
{
    ABCD("Z_pT", "A", ee);
    ABCD("Z_pT", "B", ee);
    ABCD("Z_pT", "C", ee);
    ABCD("Z_pT", "D", ee);
    Extraction();

    std::cout << std::endl;
    ABCD("Z_pT", "A", mumu);
    ABCD("Z_pT", "B", mumu);
    ABCD("Z_pT", "C", mumu);
    ABCD("Z_pT", "D", mumu);
    Extraction();
}
