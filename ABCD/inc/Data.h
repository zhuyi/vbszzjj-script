#ifndef DATA_H
#define DATA_H

#include <vector>

#include "TString.h"

struct Data
{
    //TString data_path = "/lustre/collider/zhuyifan/VBSZZ/Minitrees/Data_Reduced/";
    TString data_path = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/Data/";

    std::vector<TString> datas = {"data15_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data15_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data15_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data15_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data15_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodA.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data16_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data17_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data17_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data17_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data17_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data17_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data17_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data17_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodM.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodO.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root",
                                  "data18_13TeV.periodQ.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251.root"};
};

#endif
