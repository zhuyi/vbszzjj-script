#include "inc/MC.h"
#include "inc/Data.h"

const TCut pre = "met_tst>70&&leading_pT_lepton>30&&subleading_pT_lepton>20&&M2Lep>80&&M2Lep<110&&dLepR<1.8&&n_bjets==0&&dMetZPhi>2.2";
//const TCut SR = pre&&"met_tst>110&&MetOHT>0.65&&dMetZPhi>2.7";
//const TCut CR = pre&&"met_tst<85&&MetOHT>0.3&&MetOHT<0.5";
const TCut SR = pre&&"met_tst>110&&dMetZPhi>2.7";
const TCut CR = pre&&"met_tst<85";

//TString met_tst = "(9, 0, 9, 25, 0, 200)";
//TString met_oht = "(9, 0, 9, 25, 0, 2)";

void VSNjets()
{
    auto canvas = new TCanvas("c", "c", 800, 600);
    auto met_tst = new TH2D("met_tst", ";# of jets;MET[GeV]", 9, 0, 9, 25, 60, 90);
    auto met_oht = new TH2D("met_oht", ";# of jets;MET/HT",   9, 0, 9, 25, .2, .6);

    MC mcs;
    Data datas;

    auto data = new TChain("tree_PFLOW");
    //for(const auto &file : datas.datas)
    //    data->Add(datas.data_path + file);
    for(const auto &file : mcs.mcs.at("inclusive"))
        data->Add(mcs.mc_path_nominal + file);

    data->Draw("met_tst:n_jets>>met_tst", CR, "colz");
    //canvas->SaveAs("fig/met_n_jets_data.png");
    canvas->SaveAs("fig/met_n_jets_inclusive.png");

    data->Draw("MetOHT:n_jets>>met_oht", CR, "colz");
    //canvas->SaveAs("fig/metoht_n_jets_data.png");
    canvas->SaveAs("fig/metoht_n_jets_inclusive.png");

    auto mc = new TChain("tree_PFLOW");
    //for(const auto &channel : mcs.mcs)
    //{
    //    for(const auto &file : channel.second)
    //        mc->Add(mcs.mc_path_nominal + file);
    //}
    for(const auto &file : mcs.mcs.at("Zjets"))
        data->Add(mcs.mc_path_nominal + file);

    mc->Draw("met_tst:n_jets>>met_tst", CR, "colz");
    //canvas->SaveAs("fig/met_n_jets_mc.png");
    canvas->SaveAs("fig/met_n_jets_zjets.png");

    mc->Draw("MetOHT:n_jets>>met_oht", CR, "colz");
    //canvas->SaveAs("fig/metoht_n_jets_mc.png");
    canvas->SaveAs("fig/metoht_n_jets_zjets.png");
}
