#!/bin/bash

#cd /lustre/collider/zhuyifan/VBSZZ/ABCD/EB
#source /home/zhuyifan/DarkShine/dp.env

if   [ $1 == "inclusive" ]; then
    g++ -o bin/inclusive Inclusive.cpp `root-config --cflags --libs`
    bin/inclusive met_signif
    bin/inclusive dMetZPhi
elif [ $1 == "full" ]; then
    g++ -o bin/full Full.cpp `root-config --cflags --libs`
    bin/full met_tst
elif [ $1 == "abcd" ]; then
    g++ -o bin/abcd ABCD.cpp `root-config --cflags --libs`
    bin/abcd
elif [ $1 == "osb" ]; then
    g++ -o bin/osb OSB.cpp `root-config --cflags --libs`
    bin/osb
elif [ $1 == "compare" ]; then
    g++ -o bin/compare Compare.cpp `root-config --cflags --libs`
    bin/compare
elif [ $1 == "form" ]; then
    g++ -o bin/form Form.cpp `root-config --cflags --libs`
    bin/form
elif [ $1 == "bias" ]; then
    g++ -o bin/bias Bias.cpp `root-config --cflags --libs`
    bin/bias

elif [ $1 == "pdf" ]; then
    g++ -o bin/pdf PDFSystematics.cpp `root-config --cflags --libs`
    bin/pdf $2
elif [ $1 == "scale" ]; then
    g++ -o bin/scale ScaleSystematics.cpp `root-config --cflags --libs`
    bin/scale $2
elif [ $1 == "as" ]; then
    g++ -o bin/as AsSystematics.cpp `root-config --cflags --libs`
    bin/as $2
fi
