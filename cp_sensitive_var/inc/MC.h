#ifndef MC_H
#define MC_H

#include <vector>
#include <map>

#include "TString.h"

struct MC
{
    TString mc_path = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/";
    TString mc_path_nominal = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/PFLOW/";
    //TString mc_path_fiducial = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/Signal_nocut/";
    TString mc_path_fiducial = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/PFLOW/";
    //TString mc_path_fiducial = "/eos/home-z/zhuyi/VBS/fiducial/Signal_nocut/";
    TString mc_path_fullspace = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/Signal_nocut/";

    std::map<TString, std::vector<TString>> mcs = {
        //{"EWKZZ",  {"*364285*.root"}},
        {"EWKZZ",  {"*363724*.root"}},
        {"QCDZZ",  {"*345723*.root", "*345666*.root"}},
        //{"qqZZ",   {"*345666*.root"}},
        //{"qqZZ_alt", {"*361604*.root"}},
        //{"ggZZ",   {"*345723*.root"}},
        {"inclusive", {"*363724*.root", "*345723*.root", "*345666*.root"}},
        {"alternative", {"*363724*.root", "*345723*.root", "*361604*.root"}},
        {"aQGC1", {"*363724*.root", "*515529*.root", "*515530*.root", "*345723*.root", "*345666*.root"}},
        {"aQGC2", {"*363724*.root", "*515531*.root", "*515532*.root", "*345723*.root", "*345666*.root"}},
        {"Zjets",  {"*364114*.root", "*364115*.root", "*364116*.root", "*364117*.root", "*364118*.root", "*364119*.root", "*364120*.root", "*364121*.root", "*364122*.root", "*364123*.root", "*364124*.root", "*364125*.root", "*364126*.root", "*364127*.root", "*364100*.root", "*364101*.root", "*364102*.root", "*364103*.root", "*364104*.root", "*364105*.root", "*364106*.root", "*364107*.root", "*364108*.root", "*364109*.root", "*364110*.root", "*364111*.root", "*364112*.root", "*364113*.root"}},

        {"WZ",     {"*363358*.root", "*364253*.root", "*364284*.root"}},
        {"top",    {"*410644*.root", "*410645*.root", "*410658*.root", "*410659*.root", "*410472*.root"}},
        {"llll",   {"*345701*.root", "*345706*.root", "*364250*.root"}},
        {"llqq",   {"*363356*.root"}},
        {"ttbarV", {"*410155*.root", "*410156*.root", "*410157*.root", "*410081*.root"}},
        {"WW",     {"*361600*.root", "*361606*.root", "*345718*.root"}},
        {"Wt",     {"*410648*.root", "*410649*.root"}},
        {"VVV",    {"*363507*.root", "*363508*.root", "*363509*.root", "*364242*.root", "*364243*.root", "*364244*.root", "*364245*.root", "*364246*.root", "*364247*.root", "*364248*.root", "*364249*.root"}},
//        {"Zjets",  {"*364114*.root", "*364115*.root", "*364116*.root", "*364117*.root", "*364118*.root", "*364119*.root", "*364120*.root", "*364121*.root", "*364122*.root", "*364123*.root", "*364124*.root", "*364125*.root", "*364126*.root", "*364127*.root", "*364100*.root", "*364101*.root", "*364102*.root", "*364103*.root", "*364104*.root", "*364105*.root", "*364106*.root", "*364107*.root", "*364108*.root", "*364109*.root", "*364110*.root", "*364111*.root", "*364112*.root", "*364113*.root"}},
        {"Ztt",    {"*364128*.root", "*364129*.root", "*364130*.root", "*364131*.root", "*364132*.root", "*364133*.root", "*364134*.root", "*364135*.root", "*364136*.root", "*364137*.root", "*364138*.root", "*364139*.root", "*364140*.root", "*364141*.root"}},

        //{"backgrounds", {"*363358*.root", "*364253*.root", "*364284*.root",
        //                 "*410644*.root", "*410645*.root", "*410658*.root", "*410659*.root", "*410472*.root",
        //                 "*345701*.root", "*345706*.root", "*364250*.root",
        //                 "*363356*.root",
        //                 "*410155*.root", "*410156*.root", "*410157*.root", "*410081*.root",
        //                 "*361600*.root", "*361606*.root", "*345718*.root",
        //                 "*410648*.root", "*410649*.root",
        //                 "*363507*.root", "*363508*.root", "*363509*.root", "*364242*.root", "*364243*.root", "*364244*.root", "*364245*.root", "*364246*.root", "*364247*.root", "*364248*.root", "*364249*.root",
        //                 "*364114*.root", "*364115*.root", "*364116*.root", "*364117*.root", "*364118*.root", "*364119*.root", "*364120*.root", "*364121*.root", "*364122*.root", "*364123*.root", "*364124*.root", "*364125*.root", "*364126*.root", "*364127*.root", "*364100*.root", "*364101*.root", "*364102*.root", "*364103*.root", "*364104*.root", "*364105*.root", "*364106*.root", "*364107*.root", "*364108*.root", "*364109*.root", "*364110*.root", "*364111*.root", "*364112*.root", "*364113*.root",
        //                 "*364128*.root", "*364129*.root", "*364130*.root", "*364131*.root", "*364132*.root", "*364133*.root", "*364134*.root", "*364135*.root", "*364136*.root", "*364137*.root", "*364138*.root", "*364139*.root", "*364140*.root", "*364141*.root"}}
    };

    std::map<TString, TString> label = {{"EWKZZ",  "ZZjj"},
                                        {"QCDZZ",  "QCD ZZ"},
                                        {"inclusive", "inclusive ZZ"},
                                        {"alternative", "alternative ZZ"},
                                        {"WZ",     "WZ"},
                                        {"top",    "top"},
                                        {"llll",   "llll"},
                                        {"llqq",   "llqq"},
                                        {"lllljj", "lllljj"},
                                        {"ttbarV", "t#bar{t}V"},
                                        {"WW",     "WW"},
                                        {"Wt",     "Wt"},
                                        {"VVV",    "VVV"},
                                        {"Zjets",  "Z+jets"},
                                        //{"Wjets",  "W+jets"},
                                        {"Ztt",    "Z#tau#tau"}};
};

#endif
