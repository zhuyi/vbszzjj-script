#include "inc/Setting.h"
#include "inc/MC.h"

using RNode = ROOT::RDF::RNode;

const auto Z_mass = TDatabasePDG::Instance()->GetParticle(23)->Mass();

RNode GetInitNode(const TString &measurement, const TString &channel, bool if_truth = false)
{
    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path_fiducial + file).Data());

    std::string pre_selection = if_truth ? settings.at(measurement).truth_preselection.GetTitle()
                                         : settings.at(measurement).pre_selection.GetTitle();
    std::string selection     = if_truth ? settings.at(measurement).FR.GetTitle()
                                         : settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Filter(pre_selection).Filter(selection);
}

auto CP_ZZ_1 = [](float met_px_tst, float met_py_tst, float met_tst,
                  float lepplus_pt, float lepplus_eta, float lepplus_phi, float lepplus_m,
                  float lepminus_pt, float lepminus_eta, float lepminus_phi, float lepminus_m
                 )->double
               {
                   TLorentzVector met, lp, lm;

                   met.SetPxPyPzE(met_px_tst,
                                  met_py_tst,
                                  0.,
                                  std::sqrt(met_px_tst*met_px_tst + met_py_tst*met_py_tst + Z_mass*Z_mass));

                   lp.SetPtEtaPhiM(lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m);
                   lm.SetPtEtaPhiM(lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);

                   TVector3 v_zzrest;
                   v_zzrest.SetXYZ(-(lp + lm + met).Px()/(lp + lm + met).E(),
                                   -(lp + lm + met).Py()/(lp + lm + met).E(),
                                   -(lp + lm + met).Pz()/(lp + lm + met).E());
                   auto ll_zzrest = lp + lm;
                   auto met_zzrest = met;
                   ll_zzrest.Boost(v_zzrest);

                   TVector3 ex, ey, ez;
                   ez.SetXYZ(ll_zzrest.Px()/ll_zzrest.P(), ll_zzrest.Py()/ll_zzrest.P(), ll_zzrest.Pz()/ll_zzrest.P());
                   ex.SetMagThetaPhi(1.,
                                     (ll_zzrest.Theta() < TMath::Pi()/2 ? 1. : 3.)*TMath::Pi()/2 - ll_zzrest.Theta(),
                                     ll_zzrest.Phi() + (ll_zzrest.Phi() > 0. ? -1. : 1.)*TMath::Pi());
//                   std::cout << ez.Dot(ex) << std::endl;
                   ey = ez.Cross(ex);

                   TVector3 v_llrest;
                   v_llrest.SetXYZ(-(lp + lm).Px()/(lp + lm).E(), -(lp + lm).Py()/(lp + lm).E(), -(lp + lm).Pz()/(lp + lm).E());
                   auto lm_llrest = lm;
                   lm_llrest.Boost(v_llrest);
               
                   TVector3 lm_fin, lm_llrest_3D;
                   lm_llrest_3D.SetXYZ(lm_llrest.Px(), lm_llrest.Py(), lm_llrest.Pz());
                   lm_fin.SetXYZ(lm_llrest_3D*ex, lm_llrest_3D*ey, lm_llrest_3D*ez);
                   auto phi = lm_fin.Phi();
                   auto theta = lm_fin.Theta();

                   return std::sin(phi)*std::cos(theta);
               };

auto CP_ZZ_2 = [](float lepplus_pt, float lepplus_eta, float lepplus_phi, float lepplus_m,
                  float lepminus_pt, float lepminus_eta, float lepminus_phi, float lepminus_m
                 )->double
               {
                   TLorentzVector lp, lm;

                   lp.SetPtEtaPhiM(lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m);
                   lm.SetPtEtaPhiM(lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);
                   
                   auto ll = lp + lm;

                   TVector3 ex, ey, ez;
//                   ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), 0.);
//                   ex.SetXYZ(0., 0., ll.Pz()/std::abs(ll.Pz()));
//                   ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), ll.Pz()/ll.P());
//                   ex.SetXYZ(0., -ll.Py()/ll.P(), ll.P()/ll.Pz() - ll.Pz()/ll.P() - ll.Px()*ll.Px()/ll.Pz()/ll.P());
                   ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), ll.Pz()/ll.P());
//                   if (ll.Theta()<TMath::Pi()/2)
//                   {
//                     if (ll.Phi()>0) ex.SetMagThetaPhi(1, TMath::Pi()/2-ll.Theta(), ll.Phi()-TMath::Pi());
//                     if (ll.Phi()<0) ex.SetMagThetaPhi(1, TMath::Pi()/2-ll.Theta(), ll.Phi()+TMath::Pi());
//                   }
//                   if (ll.Theta()>TMath::Pi()/2)
//                   {
//                     if (ll.Phi()>0) ex.SetMagThetaPhi(1, 3*TMath::Pi()/2-ll.Theta(), ll.Phi()-TMath::Pi());
//                     if (ll.Phi()<0) ex.SetMagThetaPhi(1, 3*TMath::Pi()/2-ll.Theta(), ll.Phi()+TMath::Pi());
//                   }
                   ex.SetMagThetaPhi(1.,
                                     (ez.Theta() < TMath::Pi()/2 ? 1. : 3.)*TMath::Pi()/2 - ez.Theta(),
                                     ez.Phi() + (ez.Phi() > 0. ? -1. : 1.)*TMath::Pi());
                   std::cout << ez.Dot(ex) << std::endl;
                   ey = ez.Cross(ex);

                   TVector3 v_llrest;
                   v_llrest.SetXYZ(-ll.Px()/ll.E(), -ll.Py()/ll.E(), -ll.Pz()/ll.E());
                   auto lm_llrest = lm;
                   lm_llrest.Boost(v_llrest);
               
                   TVector3 lm_fin, lm_llrest_3D;
                   lm_llrest_3D.SetXYZ(lm_llrest.Px(), lm_llrest.Py(), lm_llrest.Pz());
                   lm_fin.SetXYZ(lm_llrest_3D*ex, lm_llrest_3D*ey, lm_llrest_3D*ez);
                   auto phi = lm_fin.Phi();
                   auto theta = lm_fin.Theta();

                   return std::sin(phi)*std::cos(theta);
               };

void cp_sensitive_var()
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    TString measurement = "ZZjj";
    TString channel = "inclusive";

    auto node = GetInitNode(measurement, channel);
    std::cout << *node.Count() << std::endl;

    auto hist1 = new TH1D(*node.Define("CP_ZZ_11",
                                       CP_ZZ_1,
                                       {"met_px_tst", "met_py_tst", "met_tst", "lepplus_pt", "lepplus_eta", "lepplus_phi", "lepplus_m", "lepminus_pt", "lepminus_eta", "lepminus_phi", "lepminus_m"}
                                      )
                               .Histo1D({"CP_ZZ_11", "", 100, -1, 1}, "CP_ZZ_11", "weight")
                         );

    auto hist2 = new TH1D(*node.Define("CP_ZZ_21",
                                       CP_ZZ_2,
                                       {"lepplus_pt", "lepplus_eta", "lepplus_phi", "lepplus_m", "lepminus_pt", "lepminus_eta", "lepminus_phi", "lepminus_m"}
                                      )
                               .Histo1D({"CP_ZZ_21", "", 100, -1, 1}, "CP_ZZ_21", "weight")
                         );

//    auto c = new TCanvas("c", "", 800., 600.);
//    hist1->Draw();
//    hist1->Scale(1./hist1->GetSumOfWeights());
//    hist1->GetXaxis()->SetTitle("sin(#phi)cos(#theta)");
//    hist1->GetYaxis()->SetTitle("a.u.");
//    c->SaveAs("CPZZ1.png");

//    hist2->Draw();
//    hist2->Scale(1./hist2->GetSumOfWeights());
//    hist2->GetXaxis()->SetTitle("sin(#phi)cos(#theta)");
//    hist2->GetYaxis()->SetTitle("a.u.");
//    c->SaveAs("CPZZ2.png");
}
