struct Node
{
    double value;
    Node* left = nullptr;
    Node* right = nullptr;
    Node* up = nullptr;
    Node* down = nullptr;
};

template<size_t M, size_t N>
class Link
{
public:
    Link() {nodes = new Node[M*N];}

private:
    Node* nodes = nullptr;
};
