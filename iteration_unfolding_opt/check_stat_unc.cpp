#include "inc/Util.h"

const TString path = "/lustre/collider/zhuyifan/VBSZZ/unfolding/VIPUnfolding/examples/EB/inclusive/iteration/";

std::map<TString, TString> nicknames = {
                                        {"n_jets", "n_jets"},
                                        {"mT_ZZ",  "mt_zz"},
                                        {"leading_jet_pt", "leading_jet_pt"},
                                        {"Z_pT",    "ptz"},
                                       };

std::vector<int> iterations = {1, 2, 3, 5, 10};

TFile* get_file(const TString &leaf)
{
    auto file_name = "Results_" + leaf + ".root";
    return new TFile(path + "/" + nicknames.at(leaf) + "/" + file_name, "read");
}

std::vector<double> get_unfoldeds(const TH1D* unfolded_hist)
{
    std::vector<double> unfoldeds;

    int n_bins = unfolded_hist->GetNbinsX();
    unfoldeds.reserve(n_bins);

    for(int i = 1; i <= n_bins; i++)
        unfoldeds.push_back(unfolded_hist->GetBinContent(i));

    return unfoldeds;
}

std::vector<double> get_stat_uncs(const TH1D* unfolded_hist, const TH1D* stat_unc_hist)
{
    std::vector<double> stat_uncs;

    int n_bins = stat_unc_hist->GetNbinsX();
    stat_uncs.reserve(n_bins);

    for(int i = 1; i <= n_bins; i++)
        stat_uncs.push_back(stat_unc_hist->GetBinContent(i)/unfolded_hist->GetBinContent(i)*100.);

    return stat_uncs;
}

TString get_formatted(const vector<double> &inputs) 
{
    TString formatted;

    for(const auto &input : inputs)
        formatted += TString::Format("%.5f\t", input);
    return formatted;
}

std::vector<TLine*> get_lines(const TH1D* truth_hist)
{
    std::vector<TLine*> lines;

    int n_bins = truth_hist->GetNbinsX();
    for(int i = 0; i < n_bins; i++)
    {
        double truth_unc = truth_hist->GetBinError(i + 1)/truth_hist->GetBinContent(i + 1);
        lines.push_back(new TLine(iterations.front(), truth_unc*100., iterations.back(), truth_unc*100.));
        lines.back()->SetLineColor(kRed);
        lines.back()->SetLineStyle(kDashed);
    }

    return lines;
}

std::vector<TGraph*> get_graphs(const std::map<int, std::vector<double>> &total_stat_uncs)
{
    std::vector<TGraph*> graphs;
    const auto n = total_stat_uncs.size();
    auto n_graphs = total_stat_uncs.begin()->second.size();

    for(size_t j = 0; j < n_graphs; j++)
    {
        double x[n];
        double y[n];

        int i = 0;
        for(const auto &[iteration, stat_uncs] : total_stat_uncs)
        {
            x[i] = iteration;
            y[i] = stat_uncs.at(j);
            ++i;
        }

        graphs.push_back(new TGraph(n, x, y));
        graphs.back()->SetName("bin" + TString::Itoa(j, 10));
        graphs.back()->SetTitle("");
        graphs.back()->GetYaxis()->SetTitle("Stat Unc [%]");
        graphs.back()->GetXaxis()->SetTitle("Iteration");
        graphs.back()->GetHistogram()->SetMinimum(0.);
        graphs.back()->GetHistogram()->SetMaximum(20.);
    }

    return graphs;
}

void check_stat_unc()
{
    std::vector<TString> leaves = {
                                   "n_jets",
                                   "mT_ZZ",
                                   "leading_jet_pt",
                                   "Z_pT",
                                  };

    auto c = new TCanvas("c", "", 800., 600.);

    for(const auto &leaf : leaves)
    {
        auto file = get_file(leaf);

        std::map<int, std::vector<double>> total_stat_uncs;

        for(const auto iteration : iterations)
        {
            auto unfolded_name = "AlternativeUnfolding_Bayesian" + TString::Itoa(iteration, 10) + "_Unfolded_Normalized";
            auto stat_unc_name = "AlternativeUnfolding_Bayesian" + TString::Itoa(iteration, 10) + "_StatUncertainty";
            auto unfolded_hist = dynamic_cast<TH1D*>(file->Get("AlternativeUnfolding/" + unfolded_name));
            auto stat_unc_hist = dynamic_cast<TH1D*>(file->Get("AlternativeUnfolding/" + stat_unc_name));
            if(!stat_unc_hist || !unfolded_hist) continue;
            
            auto unfoldeds = get_unfoldeds(unfolded_hist);
            auto stat_uncs = get_stat_uncs(unfolded_hist, stat_unc_hist);
            auto formatted = get_formatted(stat_uncs);
            std::cout << formatted << std::endl;
            total_stat_uncs.insert({iteration, stat_uncs});
        }

        auto truth_hist = dynamic_cast<TH1D*>(file->Get("Nominal/Nominal_MCTruth"));
        auto lines  = get_lines(truth_hist);
        auto graphs = get_graphs(total_stat_uncs);

        auto output = new TFile("root/stat_unc/" + leaf + ".root", "recreate");
        for(const auto &[graph, line] : make_container_ref_tuple(graphs, lines))
        {
            graph->Draw("AL");
            line->Draw("same");

            TString name = leaf + "_" + graph->GetName();
            c->SaveAs("fig/stat_unc/" + name + ".png");

            graph->Write();
        }

        output->Close();
        file->Close();
    }
}
