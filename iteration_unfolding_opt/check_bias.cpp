//#include "inc/Util.h"

const TString path = "/lustre/collider/zhuyifan/VBSZZ/unfolding/VIPUnfolding/examples/EB/inclusive/iteration/";

std::map<TString, TString> nicknames = {
                                        {"n_jets", "n_jets"},
                                        {"mT_ZZ",  "mt_zz"},
                                        {"leading_jet_pt", "leading_jet_pt"},
                                        {"Z_pT",    "ptz"},
                                       };

std::map<TString, TString> axis_titles = {
                                          {"n_jets", "No. of jets"},
                                          {"mT_ZZ",  "m^{ZZ}_{T} [GeV]"},
                                          {"leading_jet_pt", "p^{leading jet}_{T} [GeV]"},
                                          {"Z_pT",    "p^{Z}_{T} [GeV]"},
                                         };

const std::vector<int> colors  = {
                                  TColor::GetColor("#000000"),
                                  TColor::GetColor("#ff00ff"),
                                  TColor::GetColor("#00ffff"),
                                  TColor::GetColor("#ffcc00"),
                                  TColor::GetColor("#0000ff"),
                                 };

std::vector<int> iterations = {1, 2, 3, 5, 10};

size_t n_toys = 1000;

using toy_summary = std::vector<std::vector<double>>;

TString get_toy_output_name(const size_t &toy_j)
{
    return "toy_" + TString::ULLtoa(toy_j, 10);
}

TFile* get_nominal_file(const TString &leaf)
{
    auto file_name = "response.root";
    return new TFile(path + "/" + nicknames.at(leaf) + "/" + file_name, "read");
}

TFile* get_data_file(const TString &leaf)
{
    auto file_name = "data.root";
    return new TFile(path + "/" + nicknames.at(leaf) + "/" + file_name, "read");
}

double randomize_toy_bin(const double &bin_content, TRandom *rdm)
{
    return rdm->Gaus(bin_content, std::sqrt(bin_content));
}

TString get_formatted(const vector<double> &inputs)
{
    TString formatted;

    for(const auto &input : inputs)
        formatted += TString::Format("%.5f\t", input);
    return formatted;
}

void init_total_toys(toy_summary &total_toys, const int &n_bins)
{
    total_toys.clear();
    total_toys.reserve(n_bins);
    for(int i = 0; i < n_bins; i++) total_toys.push_back(std::vector<double>(n_toys));
}

toy_summary get_total_truth_toys(const TH1F *nominal_hist, TRandom *rdm)
{
    toy_summary total_truth_toys;

    auto n_bins = nominal_hist->GetNbinsX();
    init_total_toys(total_truth_toys, n_bins);

    for(auto i = 0; i < n_bins; i++)
    {
        for(auto j = 0; j < n_toys; j++)
        {
            auto toy_bin = randomize_toy_bin(nominal_hist->GetBinContent(i + 1), rdm);
            total_truth_toys.at(i).at(j) = toy_bin;
        }
    }

    return total_truth_toys;
}

TH1F* get_toy_hist(const std::vector<double> &toys, const TString &name, double *binning)
{
    auto n_bins = toys.size();

    auto toy_hist = new TH1F(name, "", n_bins, binning);
    for(auto i = 0; i < n_bins; i++)
    {
        toy_hist->SetBinContent(i + 1, toys.at(i));
    }

    return toy_hist;
}

std::vector<double> get_matrix_normalized(const TH2D *migration_matrix)
{
    std::vector<double> matrix_normalized;
    auto n_bins = migration_matrix->GetNbinsX();
    matrix_normalized.reserve(n_bins*n_bins);

    for(auto r_i = 0; r_i < n_bins; r_i++)
    {
        double sum_of_row(0.);

        for(auto c_i = 0; c_i < n_bins; c_i++)
            sum_of_row += migration_matrix->GetBinContent(c_i + 1, r_i + 1);

        for(auto c_i = 0; c_i < n_bins; c_i++)
            matrix_normalized.push_back(migration_matrix->GetBinContent(c_i + 1, r_i + 1)/sum_of_row);
    }

    //for(int r_i = 0; r_i < n_bins; r_i++)
    //{
    //    for(int c_i = 0; c_i < n_bins; c_i++)
    //        std::cout << matrix_normalized.at(n_bins*(n_bins - 1 - r_i) + c_i) << "\t";
    //    std::cout << std::endl;
    //}

    return matrix_normalized;
}

std::vector<double> get_recon_toys(const std::vector<double> &truth_toys, const std::vector<double> &matrix_normalized)
{
    auto n_bins = truth_toys.size();

    std::vector<double> recon_toys(n_bins);
    for(auto r_i = 0; r_i < n_bins; r_i++)
    {
        double recon_toy(0.);
        for(auto c_i = 0; c_i < n_bins; c_i++)
            recon_toy += matrix_normalized.at(n_bins*(r_i) + c_i)*truth_toys.at(c_i);
        recon_toys.at(r_i) = recon_toy;
    }

    return recon_toys;
}

bool generate_toys(const TString &leaf, TRandom *rdm, toy_summary &total_truth_toys, toy_summary &total_recon_toys)
{
    auto file = get_nominal_file(leaf);

    auto nominal_truth_name = "nominal/effcorrden"; // truth
    auto nominal_effcorrnum_name = "nominal/effcorrnum";
    auto nominal_recon_name = "nominal/fidcorrden"; // recon
    auto nominal_fidcorrnum_name = "nominal/fidcorrnum";
    auto nominal_puritynum_name  = "nominal/puritynum";
    auto nominal_migration_name  = "nominal/migration_matrix";
    auto nominal_truth_hist = dynamic_cast<TH1F*>(file->Get(nominal_truth_name));
    auto nominal_effcorrnum_hist = dynamic_cast<TH1F*>(file->Get(nominal_effcorrnum_name));
    auto nominal_recon_hist = dynamic_cast<TH1F*>(file->Get(nominal_recon_name));
    auto nominal_fidcorrnum_hist = dynamic_cast<TH1F*>(file->Get(nominal_fidcorrnum_name));
    auto nominal_puritynum_hist  = dynamic_cast<TH1F*>(file->Get(nominal_puritynum_name));
    auto nominal_migration_hist  = dynamic_cast<TH2D*>(file->Get(nominal_migration_name));

    auto data_file = get_data_file(leaf);
    auto data_hist = dynamic_cast<TH1F*>(data_file->Get("nominal/reconstructed"));

    if(!nominal_truth_hist || !nominal_effcorrnum_hist || !nominal_recon_hist || !nominal_fidcorrnum_hist ||
       !nominal_puritynum_hist || !nominal_migration_hist ||
       !data_hist)
        return false;

    const auto n_bins = nominal_truth_hist->GetNbinsX();
    auto binning = new double[n_bins + 1];
    for(int i = 0; i <= n_bins; i++)
        binning[i] = nominal_truth_hist->GetBinLowEdge(i + 1);

    total_truth_toys = get_total_truth_toys(nominal_truth_hist, rdm);
    init_total_toys(total_recon_toys, n_bins);
    auto matrix_normalized = get_matrix_normalized(nominal_migration_hist);

    for(auto j = 0; j < n_toys; j++)
    {
        if(j%100 == 0) std::cerr << "===> Generating truth toy " << j << "\t..." << std::endl;

        auto toy_output_name = get_toy_output_name(j);
        auto toy_output_path = "root/temp/" + leaf + "/" + toy_output_name + "/";
        gSystem->Exec(TString::Format("mkdir -vp %s", toy_output_path.Data()));

        auto output = new TFile(toy_output_path + "/" + "response.root", "recreate");

        //std::err << get_formatted(truth_toys) << std::endl;
        auto curr_dir = output->mkdir("nominal");
        curr_dir->cd();

        std::vector<double> truth_toys;
        truth_toys.reserve(n_bins);
        for(int i = 0; i < n_bins; i++) truth_toys.push_back(total_truth_toys.at(i).at(j));

        auto recon_toys = get_recon_toys(truth_toys, matrix_normalized);
        auto recon_toys_hist = get_toy_hist(recon_toys, "fidcorrden", binning);
        for(int i = 0; i < n_bins; i++)
            total_recon_toys.at(i).at(j) = recon_toys.at(i);

        auto truth_toys_hist = get_toy_hist(truth_toys, "effcorrden", binning);
        //truth_toys_hist->SetEntries(nominal_truth_hist->GetEntries());

        nominal_truth_hist->Write();
        //truth_toys_hist->Write();
        nominal_effcorrnum_hist->Write();
        recon_toys_hist->Write();
        nominal_fidcorrnum_hist->Write();
        nominal_puritynum_hist->Write();
        nominal_migration_hist->Write();
        output->cd();

        curr_dir = output->mkdir("data");
        curr_dir->cd();
        auto toy_data = dynamic_cast<TH1F*>(data_hist->Clone());
        for(auto i = 0; i < n_bins; i++)
            toy_data->SetBinContent(i + 1,
                                    toy_data->GetBinContent(i + 1) - nominal_recon_hist->GetBinContent(i + 1)
                                        + total_recon_toys.at(i).at(j));
        toy_data->Write();
        output->cd();

        output->Close();
    }

    delete [] binning; binning = nullptr;

    file->Close();

    return true;
}

std::tuple<double, double> get_hist_max_min(const std::vector<double> &hist_contents)
{
    return std::make_tuple(*std::max_element(hist_contents.begin(), hist_contents.end()),
                           *std::min_element(hist_contents.begin(), hist_contents.end()));
}

bool get_toy_summary_plots(const toy_summary &total_toys, const TString &scheme, const TString &leaf, const TString &axis_title = "")
{
    TCanvas *c = nullptr;
    if(!gROOT->FindObject("c")) c = new TCanvas("c", "", 600., 800.);
    else                        c = dynamic_cast<TCanvas*>(gROOT->FindObject("c"));

    auto n_bins = total_toys.size();
    for(auto i = 0; i < n_bins; i++)
    {
        auto bin_summary_name = leaf + "_bin_" + TString::Itoa(i, 10) + "_" + scheme;

        double max, min;
        std::tie(max, min) = get_hist_max_min(total_toys.at(i));

        auto bin_summary_hist = new TH1D(bin_summary_name, "", 10, min, max);

        for(const auto &toy : total_toys.at(i)) bin_summary_hist->Fill(toy);

        bin_summary_hist->Draw();
        bin_summary_hist->GetYaxis()->SetTitle("N toys");
        bin_summary_hist->GetXaxis()->SetTitle(axis_title.Length() == 0 ? axis_titles.at(leaf) : axis_title);

        c->SaveAs("fig/bias/" + leaf + "/" + bin_summary_name + ".png");
    }

    return true;
}

bool get_toy_scripts(const TString &leaf)
{
    for(auto j = 0; j < n_toys; j++)
    //for(auto j = 0; j < 2; j++)
    {
        auto script_path_j = TString::Format("root/script/%s/toy_%d", leaf.Data(), j);

        gSystem->Exec(TString::Format("mkdir -vp %s", script_path_j.Data()));
        gSystem->Exec(TString::Format("cp root/script/template/VBS_unfold.py %s/VBS_unfold.py", script_path_j.Data()));
        gSystem->Exec(TString::Format("sed -i s/LEAF/%s/g %s/VBS_unfold.py", leaf.Data(), script_path_j.Data()));
        gSystem->Exec(TString::Format("sed -i s/ID/%d/g %s/VBS_unfold.py", j, script_path_j.Data()));
    }

    return true;
}

bool execute(const TString &curr_path, const TString &leaf, const size_t &n_tries = 0)
{
    for(auto j = 0; j < (n_tries == 0 ? n_toys : n_tries); j++)
    //for(auto j = 0; j < 2; j++)
    {
        auto script_path_j = TString::Format("root/script/%s/toy_%d/", leaf.Data(), j);

        gSystem->cd(script_path_j.Data());
        gSystem->Exec("pwd");
        try
        {
            gSystem->Exec("python VBS_unfold.py");
        }
        catch(const std::exception&)
        {
            gSystem->cd(curr_path.Data());
            exit(-1);
        }
    
        gSystem->cd(curr_path.Data());    

        std::cerr << "Toy " << j << " unfolded ...\n" << std::endl;
    }
    return true; 
}

void init_total_biass_itrs(std::map<int, toy_summary> &total_biass_itrs, const size_t &n_bins)
{
    total_biass_itrs.clear();
    for(const auto &iteration : iterations) total_biass_itrs.insert({iteration, toy_summary()});
    for(auto &total_biass_itr : total_biass_itrs) init_total_toys(total_biass_itr.second, n_bins);
}

bool get_bias_summary_plots(const toy_summary &total_truth_toys, const std::map<int, toy_summary> &total_biass_itrs, const TString &leaf,
                            const size_t &n_tries)
{
    TCanvas *c = nullptr;
    if(!gROOT->FindObject("c")) c = new TCanvas("c", "", 600., 800.);
    else                        c = dynamic_cast<TCanvas*>(gROOT->FindObject("c"));

    auto n_bins = total_truth_toys.size();
    for(auto i = 0; i < n_bins; i++)
    {
        TLegend *legend = new TLegend(0.75, 0.75, 0.90, 0.75 + 0.025*iterations.size());

        auto bin_summary_name = leaf + "_bin_" + TString::Itoa(i, 10);

        double up, down;
        std::tie(up, down) = get_hist_max_min(total_truth_toys.at(i));

        int k(0);
        std::vector<TH1D*> bin_summary_hist_itrs;
        for(const auto &iteration : iterations)
        {
            bin_summary_hist_itrs.push_back(new TH1D(bin_summary_name + "_itr" + TString::Itoa(iteration, 10), "", 10, down, up));
            auto ref_hist = new TH1D(bin_summary_name + "_itr" + TString::Itoa(iteration, 10) + "_ref", "", 10, down, up);

            for(auto j = 0; j < (n_tries == 0 ? n_toys : n_tries); j++)
            {
                bin_summary_hist_itrs.back()->Fill(total_truth_toys.at(i).at(j),
                                                   total_biass_itrs.at(iteration).at(i).at(j));
                ref_hist->Fill(total_truth_toys.at(i).at(j));
            }

            for(int itr_bin = 0; itr_bin < ref_hist->GetNbinsX(); itr_bin++)
            {
                if(ref_hist->GetBinContent(itr_bin + 1) == 0.) bin_summary_hist_itrs.back()->SetBinContent(itr_bin + 1, 0.);
                else
                {
                    double scaled = bin_summary_hist_itrs.back()->GetBinContent(itr_bin + 1)/ref_hist->GetBinContent(itr_bin + 1);
                    bin_summary_hist_itrs.back()->SetBinContent(itr_bin + 1, scaled);
                }
            }

            bin_summary_hist_itrs.back()->SetLineColor(colors.at(k%colors.size()));
            bin_summary_hist_itrs.back()->SetLineWidth(2);
            legend->AddEntry(bin_summary_hist_itrs.back(), "itr " + TString::Itoa(iteration, 10), "l");

            k++;
        }

        double max = -INFINITY, min = INFINITY;
        for(const auto &bin_summary_hist_itr : bin_summary_hist_itrs)
        {
            if(bin_summary_hist_itr->GetMaximum() > max) max = bin_summary_hist_itr->GetMaximum();
            if(bin_summary_hist_itr->GetMinimum() < min) min = bin_summary_hist_itr->GetMinimum();
        }

        bin_summary_hist_itrs.at(0)->SetMinimum(min);
        bin_summary_hist_itrs.at(0)->SetMaximum(max);
        bin_summary_hist_itrs.at(0)->Draw("hist");
        bin_summary_hist_itrs.at(0)->GetYaxis()->SetTitle("N toys");
        bin_summary_hist_itrs.at(0)->GetXaxis()->SetTitle("unfolded/bin");

        for(const auto &bin_summary_hist_itr : bin_summary_hist_itrs)
            bin_summary_hist_itr->Draw("same hist");

        legend->SetTextSize(0.025);
        legend->SetBorderSize(0);
        legend->Draw("same");

        c->SaveAs("fig/bias/" + leaf + "/" + bin_summary_name + "_bias.png");
    }

    return true;
}

void post_process(const TString &leaf, const toy_summary &total_truth_toys, std::map<int, toy_summary> &total_biass_itrs,
                  const size_t &n_tries = 0)
{
    auto n_bins = total_truth_toys.size();

    init_total_biass_itrs(total_biass_itrs, n_bins);

    for(auto j = 0; j < (n_tries == 0 ? n_toys : n_tries); j++)
    {
        auto script_path_j = TString::Format("root/script/%s/toy_%d/", leaf.Data(), j);
        auto unfolded_name = "Results_" + leaf + ".root";
        auto unfolded_file = new TFile(script_path_j + "/" + unfolded_name, "read");

        for(const auto &iteration : iterations)
        {
            auto iteration_name = "AlternativeUnfolding/AlternativeUnfolding_Bayesian" + TString::Itoa(iteration, 10) + "_Unfolded";
            auto iteration_hist = dynamic_cast<TH1D*>(unfolded_file->Get(iteration_name));

            for(auto i = 0; i < n_bins; i++)
            {
                auto bias = (iteration_hist->GetBinContent(i + 1) - total_truth_toys.at(i).at(j))/total_truth_toys.at(i).at(j);
                total_biass_itrs.at(iteration).at(i).at(j) = bias;
                //total_biass_itrs.at(iteration).at(i).at(j) = iteration_hist->GetBinContent(i + 1);
                //std::cout << iteration_hist->GetBinContent(i + 1) << "\t";
                          //<< total_truth_toys.at(i).at(j) << "\t"
                          //<< bias << "\t";
            }
            //std::cout << std::endl;
        }
    }

    get_bias_summary_plots(total_truth_toys, total_biass_itrs, leaf, n_tries);
}

void check_bias()
{
    gStyle->SetOptStat(0);
    //TH1::SetDefaultSumw2();

    const bool if_generate_truth = true;
    const bool if_plot_truth = if_generate_truth && false;
    const bool if_plot_recon = if_generate_truth && false;
    const bool if_update_script = true;
    const bool if_unfold = true;
    const bool if_post_process = true;
    const size_t n_tries = 0;

    gSystem->Exec("pwd");
    gSystem->Exec("mkdir -vp root; mkdir -vp root/bias; mkdir -vp root/temp");
    gSystem->Exec("mkdir -vp fig;  mkdir -vp fig/bias;");

    std::vector<TString> leaves = {
                                   "n_jets",
                                   //"mT_ZZ",
                                   //"leading_jet_pt",
                                   //"Z_pT",
                                  };

    TCanvas *c = nullptr;
    if(if_plot_truth || if_plot_recon || if_post_process) c = new TCanvas("c", "", 800., 600.);

    TRandom rdm;

    for(const auto &leaf : leaves)
    {
        gSystem->Exec(TString::Format("mkdir -vp root/bias/%s; mkdir -vp root/temp/%s", leaf.Data(), leaf.Data()));
        gSystem->Exec(TString::Format("mkdir -vp fig/bias/%s;", leaf.Data()));

        toy_summary total_truth_toys;
        toy_summary total_recon_toys;
        std::map<int, toy_summary> total_biass_itrs;

        if(if_generate_truth && !generate_toys(leaf, &rdm, total_truth_toys, total_recon_toys)) continue;
        else
        {
            if(if_plot_truth)    get_toy_summary_plots(total_truth_toys, "truth_toy", leaf, "yields/bin");
            if(if_plot_recon)    get_toy_summary_plots(total_recon_toys, "recon_toy", leaf, "yields/bin");
            if(if_update_script) get_toy_scripts(leaf);
            if(if_unfold)        execute(gSystem->pwd(), leaf, n_tries);
            if(if_post_process)  post_process(leaf, total_truth_toys, total_biass_itrs, n_tries);
        }
    }
}
