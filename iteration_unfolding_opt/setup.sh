#!/bin/bash

unfolding_dir="/lustre/collider/zhuyifan/VBSZZ/unfolding/"

echo $PWD

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase # use your path
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#lsetup "root 6.20.02-x86_64-centos7-gcc8-opt" 

. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.18.02/x86_64-centos7-gcc48-opt/bin/thisroot.sh

source ${unfolding_dir}/RooUnfold/build/setup.sh
source ${unfolding_dir}/BootstrapGenerator/build/setup.sh
source ${unfolding_dir}/VIPUnfolding/build/setup.sh

#echo $PATH
