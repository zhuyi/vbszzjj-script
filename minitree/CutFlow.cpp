#include "inc/MC.h"

//TString file = "345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10201_p4252.root";
//TString path = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/Cutflow/";

std::vector<TCut> fid_cuts = {
                              "true",
                              "truth_leading_pT_lepton>30",
                              "truth_subleading_pT_lepton>20",
                              "abs(truth_leading_eta_lepton)<2.5",
                              "abs(truth_subleading_eta_lepton)<2.5",
                              "truth_leading_jet_pt>30",
                              "truth_second_jet_pt>30",
                              "abs(truth_leading_jet_eta)<4.5",
                              "abs(truth_second_jet_eta)<4.5",
                              "truth_M2Lep>76&&truth_M2Lep<106",
                              "truth_met_tst>130",
                              "abs(truth_dLepR)<1.8",
                              "abs(truth_dMetZPhi)>2.2",
                              "truth_MetOHT_neu>0.65",
                             };

std::vector<TCut> rec_cuts = {
                              "true",
                              "passPresel==1",
                              "met_tst>70",
                              "leading_pT_lepton>30",
                              "subleading_pT_lepton>20",
                              "M2Lep>80&&M2Lep<100",
                              "met_tst>110",
                              "abs(dLepR)<1.8",
                              "abs(dMetZPhi)>2.2",
                              "n_bjets==0",
                              "MetOHT>0.65"
};

using RNode = ROOT::RDF::RNode;

void CutFlow()
{
    TString channel = "QCDZZ";

    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path_fullspace + file).Data());

    ROOT::RDataFrame data("tree_PFLOW", files);
    RNode node = data.Define("final_weight", "weight_gen*scale*ew_correction*tautau_correction");
                     //.Define("weightew", "weight_gen*ew_correction")
                     //.Define("pileup",   "weight_gen*weight_pileup")
                     //.Define("pileupew", "weight_gen*weight_pileup*ew_correction");

    double x[] = {0, 1, 2, 3, 11};

    for(const auto cut : fid_cuts)
    {
        node = node.Filter(cut.GetTitle());
        std::cout << TString::Format("%36s,\t%7llu,\t", cut.GetTitle(), *(node.Count()));

        auto weighted_hist = dynamic_cast<TH1D*>(node.Histo1D({"weighted", "", 1, -1, 2}, "isMC", "final_weight")->Clone());
        std::cout << weighted_hist->GetSumOfWeights() << ",\n";
    }
/*
    node = data.Define("weightew", "weight*ew_correction");

    for(const auto cut : rec_cuts)
    {
        node = node.Filter(cut.GetTitle());
        std::cout << *(node.Count()) << ",\t";
    }
*/
}
