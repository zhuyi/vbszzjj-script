#include <libgen.h>
#include <set>
#include <map>
#include <string>
#include <iostream>
#include <cmath>

#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TMath.h"
#include "TKey.h"
#include "TPRegexp.h"

TString new_name_0;
TString new_name_1;
TString new_name_2;
TString new_name_3;

TString GetNewName(const TString &old_name, const int &n_jets)
{
    auto sub_string_array = TPRegexp("([._\\w\\d]+).root").MatchS(old_name);
    return (dynamic_cast<TObjString*>(sub_string_array->At(1)))->GetString() + "_jet" + TString::Format("%d", n_jets) + ".root";
}

void split(const TString &filename)
{
    //if(!filename.Contains("Zee") && !filename.Contains("Zmumu"))
    //    return;

    TFile oldfile(filename);
    TTree *oldtree_llvv;
    bool if_sherpa = filename.Contains("Sherpa_221") || filename.Contains("Sherpa_222");
    bool if_correction = filename.Contains("345666");

    double scale(1.);
    oldfile.GetObject("tree_PFLOW", oldtree_llvv);
    oldtree_llvv->SetBranchStatus("*", 1);

    int n_jets;
    oldtree_llvv->SetBranchAddress("n_jets", &n_jets);

    TFile newfile_0(new_name_0, "recreate");
    auto newtree_llvv_0 = oldtree_llvv->CloneTree(0);
    TFile newfile_1(new_name_1, "recreate");
    auto newtree_llvv_1 = oldtree_llvv->CloneTree(0);
    TFile newfile_2(new_name_2, "recreate");
    auto newtree_llvv_2 = oldtree_llvv->CloneTree(0);
    TFile newfile_3(new_name_3, "recreate");
    auto newtree_llvv_3 = oldtree_llvv->CloneTree(0);

    for(int i = 0; i < oldtree_llvv->GetEntries(); ++i)
    {
        oldtree_llvv->GetEntry(i);

        if     (n_jets == 0) newtree_llvv_0->Fill();
        else if(n_jets == 1) newtree_llvv_1->Fill();
        else if(n_jets == 2) newtree_llvv_2->Fill();
        else if(n_jets >= 3) newtree_llvv_3->Fill();
    }

    newfile_0.cd();
    newtree_llvv_0->Write("tree_PFLOW");
    newfile_0.Write();
    newfile_1.cd();
    newtree_llvv_1->Write("tree_PFLOW");
    newfile_1.Write();
    newfile_2.cd();
    newtree_llvv_2->Write("tree_PFLOW");
    newfile_2.Write();
    newfile_3.cd();
    newtree_llvv_3->Write("tree_PFLOW");
    newfile_3.Write();
}

void remove_keys(std::string fname, std::set<std::string> &keys_to_save)
{
    TFile *f = new TFile(fname.c_str(), "update");

    std::map<std::string, int> count;
    TIter next1(f->GetListOfKeys());
    TIter next2(f->GetListOfKeys());
    TKey *key;
    while((key = (TKey*)next1()))
    {
        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (keys_to_save.find(name) != keys_to_save.end())
        {
            if (count.find(name) == count.end()) count[name] = cycle;
            else if (count[name] < cycle)        count[name] = cycle;
        }
    }
    while ((key = (TKey*)next2()))
    {
        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (!(count.find(name) != count.end() && count[name] == cycle))
        {
            printf("%10s  %20s  %2d   delete\n", key->GetClassName(), name.c_str(), cycle);
            gDirectory->Delete((name + ";" + std::to_string(cycle)).c_str());
        }
        else
        {
            printf("%10s  %20s  %2d   keep  \n", key->GetClassName(), name.c_str(), cycle);
        }

    }
    f->Close();
}


int main(int argc, char* argv[])
{
    TString path;
    TString filename;

    path = argv[1];
    filename = argv[2];

    if(!filename.Contains("Zee") && !filename.Contains("Zmumu"))
        return -1;

    gSystem->cd("/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/" + path + "/Zjets/");

    new_name_0 = GetNewName(filename, 0);
    new_name_1 = GetNewName(filename, 1);
    new_name_2 = GetNewName(filename, 2);
    new_name_3 = GetNewName(filename, 3);

    split(filename);
    std::set<std::string> keys = {
         "tree_PFLOW",
         "tree_emCR_PFLOW",
         "tree_3lCR_PFLOW",
         "tree_4lCR_PFLOW",
         "hInfo",
    };
    remove_keys(filename.Data(), keys);
    remove_keys(new_name_0.Data(), keys);
    remove_keys(new_name_1.Data(), keys);
    remove_keys(new_name_2.Data(), keys);
    remove_keys(new_name_3.Data(), keys);

    return 0;
}
