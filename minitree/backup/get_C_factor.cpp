TString gg  = "345723*.root";
TString qq  = "345666*.root";
TString EWK = "363724*.root";
TString altQCD = "361604*.root";
TString altEWK = "364285*.root";

std::vector<TCut> fid_cuts = {"truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20",
                              "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5",
                              "truth_M2Lep>76&&truth_M2Lep<106",
                              "truth_event_type==1||truth_event_type==0",

                              "truth_met_tst>95",
                              "truth_dLepR<1.8",
                              "abs(truth_dMetZPhi)>2.2",
                              "truth_MetOHT>0.65",
//                              "truth_n_jets>0"
                             };
//std::vector<TCut> fid_cuts = {"truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20",
//                              "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5",
//                              "abs(truth_leading_jet_pt)>30&&abs(truth_second_jet_pt)>30",
//                              "abs(truth_leading_jet_eta)<4.5&&abs(truth_second_jet_eta)<4.5",
//                              "truth_M2Lep>76&&truth_M2Lep<106",
//                              "truth_met_tst>130",
//                              "abs(truth_dLepR)<1.8",
//                              "abs(truth_dMetZPhi)>2.2",
//                              "truth_MetOHT>0.65",
//                             };

std::vector<TCut> reco_cuts = {"met_tst>70",
                               "event_3CR==0",
                               "M2Lep>80&&M2Lep<100",
                               "leading_pT_lepton>30&&subleading_pT_lepton>20",
                               "event_type==1||event_type==0",
                    
                               "met_tst>110",
                               "MetOHT>0.65",
                               "abs(dLepR)<1.8",
                               "abs(dMetZPhi)>2.2",
                               "n_bjets==0"
                              };

TH1D* GetYield(const TString &channel, const TString &folder, const TCut &cut)
{
    auto tree = new TChain("tree_PFLOW");
    tree->Add("/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/" + folder + "/" + channel);
    auto hist = new TH1D("hist_" + channel + "_" + folder, "", 10, -5, 5);
    TString name = hist->GetName();
    tree->Draw("isMC>>" + name, "weight_gen*scale*ew_correction*tautau_correction"*cut);
    return hist;
}

void get_C_factor()
{
    gStyle->SetOptStat(0);

    TCut cut;
    for(const auto &curr : fid_cuts) cut = cut&&curr;

    auto hist_gg_1  = GetYield(gg,  "Signal_nocut", cut);
    auto hist_qq_1  = GetYield(qq,  "Signal_nocut", cut);
    auto hist_EWK_1 = GetYield(EWK, "Signal_nocut", cut);
    auto hist_altQCD_1 = GetYield(altQCD, "Signal_nocut", cut);
    auto hist_altEWK_1 = GetYield(altEWK, "Signal_nocut", cut);
    auto hist_gg_2  = GetYield(gg,  "PFLOW", cut);
    auto hist_qq_2  = GetYield(qq,  "PFLOW", cut);
    auto hist_EWK_2 = GetYield(EWK, "PFLOW", cut);
    auto hist_altQCD_2 = GetYield(altQCD, "PFLOW", cut);
    auto hist_altEWK_2 = GetYield(altEWK, "PFLOW", cut);

    std::cout << "gg: "  << hist_gg_1->GetSumOfWeights()/hist_gg_2->GetSumOfWeights() << std::endl;
    std::cout << "qq: "  << hist_qq_1->GetSumOfWeights()/hist_qq_2->GetSumOfWeights() << std::endl;
    std::cout << "EWK: " << hist_EWK_1->GetSumOfWeights()/hist_EWK_2->GetSumOfWeights() << std::endl;
//    std::cout << "alt QCD: " << hist_altQCD_1->GetSumOfWeights()/hist_altQCD_2->GetSumOfWeights() << std::endl;
//    std::cout << "alt EWK: " << hist_altEWK_1->GetSumOfWeights()/hist_altEWK_2->GetSumOfWeights() << std::endl;
//    std::cout << "gg: "  << hist_gg_1->GetSumOfWeights()/140.06894 << "fb" << std::endl;
//    std::cout << "qq: "  << hist_qq_1->GetSumOfWeights()/140.06894 << "fb" << std::endl;
//    std::cout << "EWK: " << hist_EWK_1->GetSumOfWeights()/140.06894 << "fb" << std::endl;
//    std::cout << "alt QCD: "  << hist_altQCD_1->GetSumOfWeights()/140.06894 << "fb" << std::endl;
//    std::cout << "alt EWK: "  << hist_altEWK_1->GetSumOfWeights()/140.06894 << "fb" << std::endl;

    auto c = new TCanvas("c", "", 800, 600);
    hist_gg_1->Draw("hist");
    hist_gg_2->Draw("hist same");
    hist_gg_1->SetLineColor(kRed);
}
