#include "inc/presel.h"
#include "inc/Setting.h"
#include "inc/MC.h"

using RNode = ROOT::RDF::RNode;

namespace setting
{
    enum level {reco, truth};
//    enum region {full, presel};
}

std::map<TString, std::function<double(const TString&, const float&)>> get_presels = {{"inclusive", get_presel_factor_inclusive},
                                                                                      {"ZZjj",      get_presel_factor_ZZjj}};

const std::vector<TString> names = {
                                    "345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10201_p4252.root",
                                    "345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10724_p4252.root",
                                    "345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r9364_p4252.root",
                                    "345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10201_p4252.root",
                                    "345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10724_p4252.root",
                                    "345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r9364_p4252.root",
                                    "363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10201_p4252.root",
                                    "363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10724_p4252.root",
                                    "363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r9364_p4252.root",
                                   };

RNode get_node(const TString &measurement, const TString &name)
{
    MC mcs;

    TString path_to_file = mcs.mc_path_fiducial;

    TString file = path_to_file + "/" + name;

    auto pre_selection = settings.at(measurement).truth_preselection.GetTitle();
    auto selection = settings.at(measurement).FR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", file.Data());
    RNode node = data.Filter(pre_selection).Filter(selection);

    return node;
}

TString get_hist_name(const TString &measurement, const TString &name, bool if_presel)
{
    TString hist_name;
    hist_name = measurement + "_" + name + "_truth" + (if_presel ? "_presel" : "");

    return hist_name;
}

std::string get_var_to_plot(const TString &var)
{
    auto var_to_plot = "truth_" + var;
    return var_to_plot.Data();
}

TH1D* get_hist(const TString &var, const TString &measurement, const TString &name, bool if_presel)
{
    auto node = get_node(measurement, name);
    *node.Count();

    int n_bins = settings.at(measurement).get_binning(var).size();
    auto xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(var).at(i);

    std::string weight_final = std::string("scale*ew_correction*tautau_correction*weight_gen");
    if(if_presel) weight_final += "*presel_factor_" + measurement;
    auto var_to_plot = get_var_to_plot(var);
    auto hist_name = get_hist_name(measurement, name, if_presel);
    auto hist = new TH1D(*node.Define("weight_final", weight_final)
                              .Histo1D({hist_name, "", n_bins - 1, xbins}, var_to_plot, "weight_final"));

    delete [] xbins;

    return hist;
}

void draw_legend(const TString &var, TString measurement, TH1D *hist_1, TH1D *hist_2)
{
    auto legend = new TLegend(0.45, 0.59, 0.85, 0.71);
    legend->AddEntry(hist_1, "reweighted nominal", "LP");
    legend->AddEntry(hist_2, "nominal",            "LP");
    legend->SetTextSize(0.05);
    legend->SetBorderSize(0);
    legend->Draw();

    auto title_in_legend = settings.at(measurement).titles.at(var);
    title_in_legend.ReplaceAll(" [GeV]", "");
    auto text = new TPaveText(0.45, 0.73, 0.85, 0.85, "brNDC");
    text->SetFillColor(0);
    text->SetFillStyle(0);
    text->SetBorderSize(0);
    text->SetTextAlign(12);
    text->SetTextSize(0.05);
    text->AddText("#it{ATLAS} #bf{internal #sqrt{s}=13TeV 140fb^{-1}}");
    text->AddText("#bf{" + measurement + " region, " + title_in_legend + "}");
    text->Draw();
}

void plot_presel()
{
    gStyle->SetOptStat(0);

    TString var = "mT_ZZ";
    TString measurement = "ZZjj";

    auto hist_1 = get_hist(var, measurement, names.at(0), true);
    auto hist_2 = get_hist(var, measurement, names.at(0), false);

    hist_1->SetLineColor(kBlack);
    hist_1->SetLineWidth(2);
    hist_1->SetMaximum(1.25*hist_1->GetMaximum());
    hist_1->SetMinimum(0.);
    hist_2->SetLineWidth(2);

    hist_1->GetXaxis()->SetTitle("truth " + settings.at(measurement).titles.at(var));
    hist_1->GetYaxis()->SetTitle("events/bin");

    auto c = new TCanvas("c", "", 800., 600.);

    auto rp = new TRatioPlot(hist_1, hist_2);
    rp->SetH1DrawOpt("E1");
    rp->SetH2DrawOpt("E1");
    rp->Draw();

    rp->SetSeparationMargin(0.);
    rp->GetLowerRefGraph()->SetMarkerStyle(kFullCircle);
//    rp->GetLowerRefGraph()->SetMarkerSize(1);
    rp->GetLowerRefYaxis()->SetRangeUser(0.8, 1.4);
    rp->SetGridlines({1.0, 1.2});

    rp->GetUpperPad()->cd();
    draw_legend(var, measurement, hist_1, hist_2);

    c->SaveAs("presel_" + var + "_" + measurement + ".png");
}
