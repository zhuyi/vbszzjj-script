#!/bin/bash

curr_path=$PWD

cd /lustre/collider/zhuyifan/VBSZZ/NewMinitrees/script
source ~/DarkShine/dp.env

if [[ $1 == "reduce" ]]
then
    g++ -o bin/reduce reduce.cpp `root-config --cflags --libs --evelibs`

#    cp ../MC/PFLOW/*.root ../Reduced/
# 
#    for file in ` ls ../Reduced/*.root `
#    do
#        if [[ $file == *".root" ]]
#        then
#            echo $file
#            ./bin/reduce $file
#        fi
#    done
elif [[ $1 == "fid" ]]
then
    g++ -o bin/add_var add_var.cpp `root-config --cflags --libs --evelibs`

#    cp ../Reduced/PFLOW/*345723*.root ../Fiducial/PFLOW
#    cp ../Reduced/PFLOW/*345666*.root ../Fiducial/PFLOW
#    cp ../Reduced/PFLOW/*364285*.root ../Fiducial/PFLOW
#
#    cd ../Fiducial/PFLOW
# 
#    for file in ` ls *.root `
#    do
#        if [[ $file == *".root" ]]
#        then
#            echo $file
#            ../../script/bin/add_var $file
#        fi
#    done
elif [[ $1 == "compare" ]]
then
    g++ -o bin/compare_if_prod_selection src/compare_if_prod_selection.cpp `root-config --cflags --libs --evelibs`

    ./bin/compare_if_prod_selection
elif [[ $1 == "rename" ]]
then
    g++ -o bin/rename rename.cpp `root-config --cflags --libs --evelibs`

    #cp ../Fiducial/PFLOW/not_rename/363724*.root ../Fiducial/PFLOW/
    #bin/rename 363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r9364_p4252.root
elif [[ $1 == "split" ]]
then
    g++ -o bin/split split.cpp `root-config --cflags --libs --evelibs`
fi

cd $curr_path
