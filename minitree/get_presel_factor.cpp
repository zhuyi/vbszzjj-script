#include "inc/MC.h"
#include "inc/Setting.h"

using RNode = ROOT::RDF::RNode;

const double xmin = 2.2;
const double xmax = TMath::Pi();

namespace setting
{
    enum level {reco, truth};
    enum region {full, presel};
}

const std::vector<TString> names = {
                                    "345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10201_p4252.root",
                                    "345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10724_p4252.root",
                                    "345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r9364_p4252.root",
                                    "345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10201_p4252.root",
                                    "345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10724_p4252.root",
                                    "345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r9364_p4252.root",
                                    "361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10201_r10210_p4252.root",
                                    "361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10724_r10726_p4252.root",
                                    "361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_s3126_r9364_r9315_p4252.root",
                                    "363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10201_p4252.root",
                                    "363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10724_p4252.root",
                                    "363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r9364_p4252.root",
                                   };

RNode get_node(const TString &measurement, const TString &name, setting::region region, setting::level level)
{
    MC mcs;

    TString path_to_file;
    if(region == setting::full) path_to_file = mcs.mc_path_fullspace;
    else                        path_to_file = mcs.mc_path_fiducial;

    TString file = path_to_file + "/" + name;

    std::string pre_selection = (level == setting::truth) ? settings.at(measurement).truth_preselection.GetTitle() :
                                                            settings.at(measurement).pre_selection.GetTitle();
    std::string selection = (level == setting::truth) ? settings.at(measurement).FR.GetTitle() :
                                                        settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", file.Data());
    RNode node = data.Filter(pre_selection).Filter(selection);

    return node;
}

TString get_hist_name(const TString &measurement, const TString &name, setting::region region, setting::level level)
{
    TString hist_name;
    hist_name = measurement + "_" + name +
                (region == setting::full  ? "full"  : "presel") + "_" +
                (level  == setting::truth ? "truth" : "reco");

    return hist_name;
}

TH1D* get_hist(const TString &measurement, const TString &var, const TString &name, setting::region region, setting::level level)
{
    const std::string var_str = var.Data();

    auto node = get_node(measurement, name, region, level);
    *node.Count();

    int n_bins = settings.at(measurement).get_binning(var).size();
    auto xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(var).at(i);

    std::string weight_final = std::string("scale*ew_correction*tautau_correction*") + (level == setting::truth ? "weight_gen" : "weight");
    std::string var_to_plot  = (level == setting::truth ? "truth_" + var_str : var_str);
    auto hist_name = get_hist_name(measurement, name, region, level);
    auto hist = new TH1D(*node.Define("weight_final", weight_final)
                              .Define("abs_" + var_to_plot, "std::abs(" + var_to_plot + ")")
                              .Histo1D({hist_name, "", n_bins - 1, xbins}, "abs_" + var_to_plot, "weight_final"));

    delete [] xbins;

    return hist;
}

void get_presel_factor()
{
    gStyle->SetOptStat(0);

    auto c = new TCanvas("c", "", 800., 600.);

    TString measurement = "inclusive";
    TString var = "dMetZPhi";

    TString table_str;

    double xs_pre  = 0.;
    double xs_post = 0.;
    for(const auto name : names)
    {
        std::cout << name << std::endl;

        auto hist_nominal_truth = get_hist(measurement,   var, name, setting::presel, setting::truth);
        auto hist_fullspace_truth = get_hist(measurement, var, name, setting::full,   setting::truth);
        xs_pre += hist_fullspace_truth->GetSumOfWeights();

        auto factor_truth = new TH1D(*hist_fullspace_truth);
        factor_truth->Divide(hist_nominal_truth);
        factor_truth->SetMinimum(0.8);
        factor_truth->SetMaximum(1.8);
        factor_truth->GetYaxis()->SetTitle("#frac{w/o preseletionw}{w/ preselection}");
        factor_truth->GetXaxis()->SetTitle("truth #Delta(E^{miss}_{T}) [rad]");
        factor_truth->Draw();

        double total_rate = factor_truth->GetSumOfWeights();
/*
        auto get_par4 = [&](double *par)
                        {
                            return (total_rate*(xmax - xmin) - 1./5.*par[0]*(xmax*xmax*xmax*xmax*xmax - xmin*xmin*xmin*xmin*xmin)
                                                             - 1./4.*par[1]*(xmax*xmax*xmax*xmax - xmin*xmin*xmin*xmin)
                                                             - 1./3.*par[2]*(xmax*xmax*xmax - xmin*xmin*xmin)
                                                             - 1./2.*par[3]*(xmax*xmax - xmin*xmin))/(xmax - xmin);
                        };
*/
        auto fit_func = [&](double *x, double *par)
                        {
//                            return par[0]*std::sin(par[1]*x[0] + par[2]) + par[3];

                            return par[0]*x[0]*x[0]*x[0]*x[0]*x[0]
                                 + par[1]*x[0]*x[0]*x[0]*x[0]
                                 + par[2]*x[0]*x[0]*x[0]
                                 + par[3]*x[0]*x[0]
                                 + par[4]*x[0]
                                 + par[5];
                        };

        auto *func = new TF1("fit", fit_func, xmin, xmax, 4);
        func->SetParNames("a0", "a1", "a2", "a3", "a4", "a5");
        func->SetParameters(1., 1., 1., 1., 1., 1.);
//        func->SetParNames("a0", "a1", "a2", "a3");
//        func->SetParameters(1., 1., 1., 1.);

        auto text = new TPaveText(0.10, 0.80, 0.90, 0.87, "brNDC");
        text->SetFillColor(0);
        text->SetFillStyle(0);
        text->SetBorderSize(0);
        text->SetTextAlign(12);
        text->SetTextSize(0.03);
        text->AddText(name);
        text->AddText(measurement);
        text->Draw();

        factor_truth->Fit(func->GetName(), "Q");
        c->SaveAs("presel_" + measurement + "_" + name + ".png");

        auto n_bins = factor_truth->GetNbinsX();
        auto scale_factors = new double[n_bins];
        for(int i = 1; i <= n_bins; i++) scale_factors[i - 1] = func->Eval(factor_truth->GetBinCenter(i));

        for(int i = 1; i <= n_bins; i++) xs_post += scale_factors[i - 1]*hist_nominal_truth->GetBinContent(i);
/*
        TString table_str;
        for(int i = 1; i <= n_bins; i++)
            table_str += TString::Format("    else if (truth_dMetZPhi >= %.1f && truth_dMetZPhi < %.1f) return %.5f;\n",
                                         factor_truth->GetBinLowEdge(i),
                                         factor_truth->GetBinLowEdge(i + 1),
                                         scale_factors[i - 1]);
        table_str.Replace(4, 7, "if", 2);
        std::cout << table_str << std::endl;
*/
        auto pars = func->GetParameters();
//        auto par4 = get_par4(pars);
        std::string var_str = ("truth_" + var).Data();
        table_str += TString::Format("if(filename.Contains(\"%s\"))\n", name.Data());
        table_str += TString::Format("    return %f*var*var*var*var*var + %f*var*var*var*var + %f*var*var*var + %f*var*var + %f*var + %f;\n",
                                     pars[0], pars[1], pars[2], pars[3], pars[4], pars[5]);
//        table_str += TString::Format("    return %f*std::sin(%f*var + %f) + %f;\n",
//                                     pars[0], pars[1], pars[2], pars[3]);

        delete [] scale_factors;
    }

    std::cout << xs_pre/140.  << " fb" << std::endl;
    std::cout << xs_post/140. << " fb" << std::endl;

    std::cout << table_str << std::endl;


}
