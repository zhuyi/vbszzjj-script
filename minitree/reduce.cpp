#include <libgen.h>
#include <set>
#include <map>
#include <string>
#include <iostream>
#include <cmath>

#include "TSystem.h"
#include "TFile.h"
#include "TH1D.h"
#include "TTree.h"
#include "TString.h"
#include "TMath.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TKey.h"
#include "TDatabasePDG.h"

#include "inc/Syst.h"

const auto Z_mass = TDatabasePDG::Instance()->GetParticle(23)->Mass();

double get_ew_correction(const double &truth_met)
{
    if     ( 50. < truth_met && truth_met <   60.) return 0.95049841;
    else if( 60. < truth_met && truth_met <   70.) return 0.94603957;
    else if( 70. < truth_met && truth_met <   80.) return 0.94116453;
    else if( 80. < truth_met && truth_met <   90.) return 0.93749643;
    else if( 90. < truth_met && truth_met <  100.) return 0.93427814;
    else if(100. < truth_met && truth_met <  120.) return 0.92784113;
    else if(120. < truth_met && truth_met <  140.) return 0.91831967;
    else if(140. < truth_met && truth_met <  160.) return 0.90832711;
    else if(160. < truth_met && truth_met <  180.) return 0.89836000;
    else if(180. < truth_met && truth_met <  200.) return 0.88769693;
    else if(200. < truth_met && truth_met <  250.) return 0.87195970;
    else if(250. < truth_met && truth_met <  300.) return 0.84713314;
    else if(300. < truth_met && truth_met <  350.) return 0.82531651;
    else if(350. < truth_met && truth_met <  400.) return 0.80451086;
    else if(400. < truth_met && truth_met <  450.) return 0.78480511;
    else if(450. < truth_met && truth_met <  500.) return 0.76750962;
    else if(500. < truth_met && truth_met <  600.) return 0.74312196;
    else if(600. < truth_met && truth_met <  700.) return 0.71224842;
    else if(700. < truth_met && truth_met <  800.) return 0.68098102;
    else if(800. < truth_met)                      return 0.66162730;

    return 1.;
}

double get_cp_zz_1(float met_px_tst, float met_py_tst, float met_tst,
                   float lepplus_pt, float lepplus_eta, float lepplus_phi, float lepplus_m,
                   float lepminus_pt, float lepminus_eta, float lepminus_phi, float lepminus_m
                  )
{
    TLorentzVector met, lp, lm;

    met.SetPxPyPzE(met_px_tst,
                   met_py_tst,
                   0.,
                   std::sqrt(met_px_tst*met_px_tst + met_py_tst*met_py_tst + Z_mass*Z_mass));

    lp.SetPtEtaPhiM(lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m);
    lm.SetPtEtaPhiM(lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);

    TVector3 v_zzrest;
    v_zzrest.SetXYZ(-(lp + lm + met).Px()/(lp + lm + met).E(),
                    -(lp + lm + met).Py()/(lp + lm + met).E(),
                    -(lp + lm + met).Pz()/(lp + lm + met).E());
    auto ll_zzrest = lp + lm;
    auto met_zzrest = met;
    ll_zzrest.Boost(v_zzrest);

    TVector3 ex, ey, ez;
    ez.SetXYZ(ll_zzrest.Px()/ll_zzrest.P(), ll_zzrest.Py()/ll_zzrest.P(), ll_zzrest.Pz()/ll_zzrest.P());
    ex.SetMagThetaPhi(1.,
                      (ll_zzrest.Theta() < TMath::Pi()/2 ? 1. : 3.)*TMath::Pi()/2 - ll_zzrest.Theta(),
                      ll_zzrest.Phi() + (ll_zzrest.Phi() > 0. ? -1. : 1.)*TMath::Pi());
    ey = ez.Cross(ex);

    TVector3 v_llrest;
    v_llrest.SetXYZ(-(lp + lm).Px()/(lp + lm).E(), -(lp + lm).Py()/(lp + lm).E(), -(lp + lm).Pz()/(lp + lm).E());
    auto lm_llrest = lm;
    lm_llrest.Boost(v_llrest);

    TVector3 lm_fin, lm_llrest_3D;
    lm_llrest_3D.SetXYZ(lm_llrest.Px(), lm_llrest.Py(), lm_llrest.Pz());
    lm_fin.SetXYZ(lm_llrest_3D*ex, lm_llrest_3D*ey, lm_llrest_3D*ez);
    auto phi = lm_fin.Phi();
    auto theta = lm_fin.Theta();

    return std::sin(phi)*std::cos(theta);
}

double get_cp_zz_2(float lepplus_pt, float lepplus_eta, float lepplus_phi, float lepplus_m,
                   float lepminus_pt, float lepminus_eta, float lepminus_phi, float lepminus_m
                  )
{
    TLorentzVector lp, lm;

    lp.SetPtEtaPhiM(lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m);
    lm.SetPtEtaPhiM(lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);

    auto ll = lp + lm;

    TVector3 ex, ey, ez;
//    ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), 0.);
//    ex.SetXYZ(0., 0., ll.Pz()/std::abs(ll.Pz()));
//    ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), ll.Pz()/ll.P());
//    ex.SetXYZ(0., -ll.Py()/ll.P(), ll.P()/ll.Pz() - ll.Pz()/ll.P() - ll.Px()*ll.Px()/ll.Pz()/ll.P());
    ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), ll.Pz()/ll.P());
    ex.SetMagThetaPhi(1.,
                      (ez.Theta() < TMath::Pi()/2 ? 1. : 3.)*TMath::Pi()/2 - ez.Theta(),
                      ez.Phi() + (ez.Phi() > 0. ? -1. : 1.)*TMath::Pi());
    ey = ez.Cross(ex);

    TVector3 v_llrest;
    v_llrest.SetXYZ(-ll.Px()/ll.E(), -ll.Py()/ll.E(), -ll.Pz()/ll.E());
    auto lm_llrest = lm;
    lm_llrest.Boost(v_llrest);

    TVector3 lm_fin, lm_llrest_3D;
    lm_llrest_3D.SetXYZ(lm_llrest.Px(), lm_llrest.Py(), lm_llrest.Pz());
    lm_fin.SetXYZ(lm_llrest_3D*ex, lm_llrest_3D*ey, lm_llrest_3D*ez);
    auto phi = lm_fin.Phi();
    auto theta = lm_fin.Theta();

    return std::sin(phi)*std::cos(theta);
}

double get_aQGC_xs(TString filename, TH1D* hInfo)
{
    double xs;

    if      (filename.Contains("515527")) xs =  20.23*0.001;
    else if (filename.Contains("515528")) xs = 240.12*0.001;
    else if (filename.Contains("515529")) xs =   9.86*0.001;
    else if (filename.Contains("515530")) xs = 612.79*0.001;
    else if (filename.Contains("515531")) xs =  14.42*0.001;
    else if (filename.Contains("515532")) xs = 147.57*0.001;
    else if (filename.Contains("515533")) xs =  11.45*0.001;
    else if (filename.Contains("515534")) xs = 156.66*0.001;
    else if (filename.Contains("515535")) xs =   2.29*0.001;
    else if (filename.Contains("515536")) xs =  67.78*0.001;
    else if (filename.Contains("515537")) xs =   3.37*0.001;
    else if (filename.Contains("515538")) xs =  14.40*0.001;
    else if (filename.Contains("515539")) xs =   0.18*0.001;
    else if (filename.Contains("515540")) xs = 232.59*0.001;
    else if (filename.Contains("515541")) xs =   0.26*0.001;
    else if (filename.Contains("515542")) xs = 202.83*0.001;
    else    xs = hInfo->GetBinContent(1);

    return xs;
}

void reduce(std::string filename)
{
    TString filename_tstr(filename);

    TFile oldfile(filename.c_str());
    TH1D  *hInfo;
    TTree *oldtree_llvv;
    bool if_sherpa = filename_tstr.Contains("Sherpa_221") || filename_tstr.Contains("Sherpa_222");
    bool if_correction = filename_tstr.Contains("345666");

    double scale(1.);

    oldfile.GetObject("hInfo", hInfo);
    if(hInfo)
    {
        double xs = get_aQGC_xs(filename_tstr, hInfo);

        scale = xs*2.0/hInfo->GetEntries()/hInfo->GetBinContent(2);
//        if(filename.find("r9364") != std::string::npos)       scale *= (3.21956 + 32.9653);
//        else if(filename.find("r10201") != std::string::npos) scale *= 44.3074;
//        else if(filename.find("r10724") != std::string::npos) scale *= 58.4501;
        if(filename.find("r9364") != std::string::npos)       scale *= (3.24454 + 33.4022);
        else if(filename.find("r10201") != std::string::npos) scale *= 44.6306;
        else if(filename.find("r10724") != std::string::npos) scale *= 58.7916;
    }

    oldfile.GetObject("tree_PFLOW", oldtree_llvv);
    oldtree_llvv->SetBranchStatus("*", 1);

    float weight;
    float weight_gen;
    float met_Truth;
    float met_tst;
    float met_px_tst;
    float met_py_tst;
    float Z_pT;
    float dMetZPhi;
    float lepplus_pt;
    float lepplus_eta;
    float lepplus_phi;
    float lepplus_m;
    float lepminus_pt;
    float lepminus_eta;
    float lepminus_phi;
    float lepminus_m;
    oldtree_llvv->SetBranchAddress("weight",       &weight);
    oldtree_llvv->SetBranchAddress("weight_gen",   &weight_gen);
    oldtree_llvv->SetBranchAddress("met_Truth",    &met_Truth);
    oldtree_llvv->SetBranchAddress("met_tst",      &met_tst);
    oldtree_llvv->SetBranchAddress("met_px_tst",   &met_px_tst);
    oldtree_llvv->SetBranchAddress("met_py_tst",   &met_py_tst);
    oldtree_llvv->SetBranchAddress("Z_pT",         &Z_pT);
    oldtree_llvv->SetBranchAddress("dMetZPhi",     &dMetZPhi);
    oldtree_llvv->SetBranchAddress("lepplus_pt",   &lepplus_pt);
    oldtree_llvv->SetBranchAddress("lepplus_eta",  &lepplus_eta);
    oldtree_llvv->SetBranchAddress("lepplus_phi",  &lepplus_phi);
    oldtree_llvv->SetBranchAddress("lepplus_m",    &lepplus_m);
    oldtree_llvv->SetBranchAddress("lepminus_pt",  &lepminus_pt);
    oldtree_llvv->SetBranchAddress("lepminus_eta", &lepminus_eta);
    oldtree_llvv->SetBranchAddress("lepminus_phi", &lepminus_phi);
    oldtree_llvv->SetBranchAddress("lepminus_m",   &lepminus_m);

    bool  syst_flags  [syst_names.size()];
    bool  theo_flags  [theo_names.size()];
    float syst_weights[syst_names.size()];
    float theo_weights[theo_names.size()];
    for(int i = 0; i < syst_names.size(); i++)
    {
        syst_flags[i] = false;

        if(oldtree_llvv->FindBranch(syst_names.at(i)))
        {
            syst_flags[i] = true;
            oldtree_llvv->SetBranchAddress(syst_names.at(i), &syst_weights[i]);   
        }
    }
    for(int i = 0; i < theo_names.size(); i++)
    {
        theo_flags[i] = false;

        if(oldtree_llvv->FindBranch(theo_names.at(i)))
        {
            theo_flags[i] = true;
            oldtree_llvv->SetBranchAddress(theo_names.at(i), &theo_weights[i]);   
        }
    }

    TFile newfile(basename(&filename[0]), "recreate");
    auto newtree_llvv = oldtree_llvv->CloneTree(0);

    int   passPresel;
    float ew_correction;
    float tautau_correction;
    float dphill;
    float pt_zz;
    float CP_ZZ_1;
    float CP_ZZ_2;
    float weight_orig;
    float weight_gen_orig;
    if(!oldtree_llvv->FindBranch("passPresel")) newtree_llvv->Branch("passPresel", &passPresel);
    newtree_llvv->Branch("ew_correction",     &ew_correction);
    newtree_llvv->Branch("tautau_correction", &tautau_correction);
    newtree_llvv->Branch("scale",             &scale);
    newtree_llvv->Branch("dphill",            &dphill);
    newtree_llvv->Branch("pt_zz",             &pt_zz);
    newtree_llvv->Branch("CP_ZZ_1",           &CP_ZZ_1);
    newtree_llvv->Branch("CP_ZZ_2",           &CP_ZZ_2);
    newtree_llvv->Branch("weight_orig",       &weight_orig);
    newtree_llvv->Branch("weight_gen_orig",   &weight_gen_orig);

    int abnormal_count = 0;
    TVector3 lepplus, lepminus;
    for(int i = 0; i < oldtree_llvv->GetEntries(); ++i)
    {
        oldtree_llvv->GetEntry(i);

        passPresel = 1;
        weight_orig     = weight;
        weight_gen_orig = weight_gen;

        bool if_abnormal = false;
        if(!std::isfinite(weight) || !std::isfinite(weight_gen))
        {
            if_abnormal = true;

            weight = 0.;
            weight_gen = 0.;

            for(int j = 0; j < theo_names.size(); j++)
            {
                if(theo_flags[j]) theo_weights[j] = 0.;
            }
            for(int j = 0; j < syst_names.size(); j++)
            {
                if(syst_flags[j]) syst_weights[j] = 0.;
            }
        }

        if(if_sherpa && std::abs(weight_gen) > 100.)
        {
            weight /= weight_gen;
            for(int j = 0; j < syst_names.size(); j++)
            {
                if(syst_flags[j])
                    syst_weights[j] /= weight_gen;
            }

            weight_gen = 1.;
        }

        for(int j = 0; j < theo_names.size(); j++)
        {
            if(theo_flags[j] && if_sherpa && std::abs(theo_weights[j]) > 100.)
            {
                std::cout << TString::Format("%d %s: %f", i, theo_names.at(j).Data(), theo_weights[j]) << std::endl;
                theo_weights[j] = 1.;
            }
        }

        lepplus.SetPtEtaPhi(lepplus_pt, lepplus_eta, lepplus_phi);
        lepminus.SetPtEtaPhi(lepminus_pt, lepminus_eta, lepminus_phi);
        dphill = lepplus.DeltaPhi(lepminus);
        CP_ZZ_1 = get_cp_zz_1(met_px_tst, met_py_tst, met_tst,
                              lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m,
                              lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);
        CP_ZZ_2 = get_cp_zz_2(lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m,
                              lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);

        if(if_correction) ew_correction = get_ew_correction(met_Truth);
        else              ew_correction = 1.;

        if(filename_tstr.Contains("363724")) tautau_correction = 3.;
        else                                 tautau_correction = 1.;

        pt_zz = std::sqrt(met_tst*met_tst + Z_pT*Z_pT + 2*met_tst*Z_pT*std::cos(dMetZPhi));

        newtree_llvv->Fill();

        if(if_abnormal) abnormal_count++;
    }

    newtree_llvv->Write("tree_PFLOW");
    newfile.Write();

    std::cout << oldtree_llvv->GetEntries() << "\tabn:" << abnormal_count << std::endl;
}

void remove_keys(std::string fname, std::set<std::string> &keys_to_save)
{
    TFile *f = new TFile(fname.c_str(), "update");

    std::map<std::string, int> count;
    TIter next1(f->GetListOfKeys());
    TIter next2(f->GetListOfKeys());
    TKey *key;
    while((key = (TKey*)next1()))
    {
        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (keys_to_save.find(name) != keys_to_save.end())
        {
            if (count.find(name) == count.end()) count[name] = cycle;
            else if (count[name] < cycle)        count[name] = cycle;
        }
    }
    while ((key = (TKey*)next2()))
    {
        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (!(count.find(name) != count.end() && count[name] == cycle))
        {
            printf("%10s  %20s  %2d   delete\n", key->GetClassName(), name.c_str(), cycle);
            gDirectory->Delete((name + ";" + std::to_string(cycle)).c_str());
        }
        else
        {
            printf("%10s  %20s  %2d   keep  \n", key->GetClassName(), name.c_str(), cycle);
        }

    }
    f->Close();
}


int main(int argc, char* argv[])
{
    TString path;
    std::string filename;

    //if(argc == 3) return -1;

    path = argv[1];
    filename = argv[2];

    gSystem->cd("/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Reduced/" + path);

    reduce(filename);
    std::set<std::string> keys = {
         "tree_PFLOW",
         "tree_emCR_PFLOW",
         "tree_3lCR_PFLOW",
         "tree_4lCR_PFLOW",
         "hInfo",
    };
    remove_keys(basename(&filename[0]), keys);

    return 0;
}
