#include "inc/presel.h"

#include <libgen.h>
#include <set>
#include <map>
#include <tuple>
#include <string>
#include <algorithm>
#include <numeric>
#include <iostream>

#include "TSystem.h"
#include "TFile.h"
#include "TH1D.h"
#include "TTree.h"
#include "TString.h"
#include "TMath.h"
#include "TVector3.h"
#include "TDatabasePDG.h"
#include "TLorentzVector.h"
#include "TKey.h"

const auto Z_mass = TDatabasePDG::Instance()->GetParticle(23)->Mass();

class particle
{
public:
    particle() = default;
    ~particle() = default;

    void set_pt_eta_phi_E_pid(double pt, double eta, double phi, double e, int pid)
    {
        _particle.SetPtEtaPhiE(pt, eta, phi, e);
        _pid = pid;
    }

    double delta_R(const particle &other) const
    { return _particle.DeltaR(other._particle); }

    double pt()  const { return _particle.Pt();  }
    double eta() const { return _particle.Eta(); }
    double phi() const { return _particle.Phi(); }
    double E()   const { return _particle.E();   }
    double pid() const { return _pid;            }
    double charge() const { return TDatabasePDG::Instance()->GetParticle(_pid)->Charge(); }

private:
    TLorentzVector _particle;
    int _pid = 0;
};

double get_cp_zz_1(float met_px_tst, float met_py_tst, float met_tst,
                   float lepplus_pt, float lepplus_eta, float lepplus_phi, float lepplus_m,
                   float lepminus_pt, float lepminus_eta, float lepminus_phi, float lepminus_m
                  )
{
    TLorentzVector met, lp, lm;

    met.SetPxPyPzE(met_px_tst,
                   met_py_tst,
                   0.,
                   std::sqrt(met_px_tst*met_px_tst + met_py_tst*met_py_tst + Z_mass*Z_mass));

    lp.SetPtEtaPhiM(lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m);
    lm.SetPtEtaPhiM(lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);

    TVector3 v_zzrest;
    v_zzrest.SetXYZ(-(lp + lm + met).Px()/(lp + lm + met).E(),
                    -(lp + lm + met).Py()/(lp + lm + met).E(),
                    -(lp + lm + met).Pz()/(lp + lm + met).E());
    auto ll_zzrest = lp + lm;
    auto met_zzrest = met;
    ll_zzrest.Boost(v_zzrest);

    TVector3 ex, ey, ez;
    ez.SetXYZ(ll_zzrest.Px()/ll_zzrest.P(), ll_zzrest.Py()/ll_zzrest.P(), ll_zzrest.Pz()/ll_zzrest.P());
    ex.SetMagThetaPhi(1.,
                      (ll_zzrest.Theta() < TMath::Pi()/2 ? 1. : 3.)*TMath::Pi()/2 - ll_zzrest.Theta(),
                      ll_zzrest.Phi() + (ll_zzrest.Phi() > 0. ? -1. : 1.)*TMath::Pi());
    ey = ez.Cross(ex);

    TVector3 v_llrest;
    v_llrest.SetXYZ(-(lp + lm).Px()/(lp + lm).E(), -(lp + lm).Py()/(lp + lm).E(), -(lp + lm).Pz()/(lp + lm).E());
    auto lm_llrest = lm;
    lm_llrest.Boost(v_llrest);

    TVector3 lm_fin, lm_llrest_3D;
    lm_llrest_3D.SetXYZ(lm_llrest.Px(), lm_llrest.Py(), lm_llrest.Pz());
    lm_fin.SetXYZ(lm_llrest_3D*ex, lm_llrest_3D*ey, lm_llrest_3D*ez);
    auto phi = lm_fin.Phi();
    auto theta = lm_fin.Theta();

    return std::sin(phi)*std::cos(theta);
}

double get_cp_zz_2(float lepplus_pt, float lepplus_eta, float lepplus_phi, float lepplus_m,
                   float lepminus_pt, float lepminus_eta, float lepminus_phi, float lepminus_m
                  )
{
    TLorentzVector lp, lm;

    lp.SetPtEtaPhiM(lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m);
    lm.SetPtEtaPhiM(lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);

    auto ll = lp + lm;

    TVector3 ex, ey, ez;
//    ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), 0.);
//    ex.SetXYZ(0., 0., ll.Pz()/std::abs(ll.Pz()));
//    ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), ll.Pz()/ll.P());
//    ex.SetXYZ(0., -ll.Py()/ll.P(), ll.P()/ll.Pz() - ll.Pz()/ll.P() - ll.Px()*ll.Px()/ll.Pz()/ll.P());
    ez.SetXYZ(ll.Px()/ll.P(), ll.Py()/ll.P(), ll.Pz()/ll.P());
    ex.SetMagThetaPhi(1.,
                      (ez.Theta() < TMath::Pi()/2 ? 1. : 3.)*TMath::Pi()/2 - ez.Theta(),
                      ez.Phi() + (ez.Phi() > 0. ? -1. : 1.)*TMath::Pi());
    ey = ez.Cross(ex);

    TVector3 v_llrest;
    v_llrest.SetXYZ(-ll.Px()/ll.E(), -ll.Py()/ll.E(), -ll.Pz()/ll.E());
    auto lm_llrest = lm;
    lm_llrest.Boost(v_llrest);

    TVector3 lm_fin, lm_llrest_3D;
    lm_llrest_3D.SetXYZ(lm_llrest.Px(), lm_llrest.Py(), lm_llrest.Pz());
    lm_fin.SetXYZ(lm_llrest_3D*ex, lm_llrest_3D*ey, lm_llrest_3D*ez);
    auto phi = lm_fin.Phi();
    auto theta = lm_fin.Theta();

    return std::sin(phi)*std::cos(theta);
}

void rm_jets(std::vector<particle> &jets,
             const std::vector<particle> &electrons, const std::vector<particle> &muons,
             double cut = 0.2)
{
    std::set<int> rm_jet;
    for(int j = 0; j < jets.size(); ++j)
    {
        for(auto e : electrons)
        {
            if(jets.at(j).delta_R(e) < cut)
                rm_jet.insert(j);
        }
        for(auto mu : muons)
        {
            if(jets.at(j).delta_R(mu) < cut)
                rm_jet.insert(j);
        }
    }

    for(auto rm = rm_jet.rbegin();
             rm != rm_jet.rend();
             ++rm)
        jets.erase(jets.begin() + *rm);
}

void rm_leptons(std::vector<particle> &electrons, std::vector<particle> &muons,
                const std::vector<particle> &jets,
                double cut = 0.4)
{
    std::set<int> rm_e;
    std::set<int> rm_mu;
    for(auto j : jets)
    {
        for(int e = 0; e < electrons.size(); ++e)
        {
            if(electrons.at(e).delta_R(j) < cut)
                rm_e.insert(e);
        }
        for(int mu = 0; mu < muons.size(); ++mu)
        {
            if(muons.at(mu).delta_R(j) < cut)
                rm_mu.insert(mu);
        }
    }

    for(auto rm = rm_e.rbegin();
             rm != rm_e.rend();
             ++rm)
        electrons.erase(electrons.begin() + *rm);
    for(auto rm = rm_mu.rbegin();
             rm != rm_mu.rend();
             ++rm)
        muons.erase(muons.begin() + *rm);
}

double select_leptons(int &l1_c, int &l2_c,
                      const std::vector<particle> &leptons)
{
    double closest = INFINITY;

    if(leptons.size() > 2)
    {
        for(int lep_i = 0; lep_i < leptons.size() - 1; lep_i++)
        {
            TLorentzVector v_l1;
            v_l1.SetPtEtaPhiE(leptons.at(lep_i).pt(), leptons.at(lep_i).eta(), leptons.at(lep_i).phi(), leptons.at(lep_i).E());
            for(int lep_j = lep_i + 1; lep_j < leptons.size(); lep_j++)
            {
                if(leptons.at(lep_j).pid() != -leptons.at(lep_i).pid())
                    continue;

                TLorentzVector v_l2;
                v_l2.SetPtEtaPhiE(leptons.at(lep_j).pt(), leptons.at(lep_j).eta(), leptons.at(lep_j).phi(), leptons.at(lep_j).E());

                double mll_curr = (v_l1 + v_l2).M();
                if(std::abs(Z_mass - mll_curr) < std::abs(Z_mass - closest))
                {
                    l1_c = lep_i;
                    l2_c = lep_j;
                    closest = mll_curr;
                }
                //std::cout << TString::Format("mll_curr: %f\tlep_i: %d\tlep_j: %d\tclosest: %f\tl1_c: %d\tl2_c: %d\f",
                //                             mll_curr, lep_i, lep_j, closest, l1_c, l2_c)
                //          << std::endl;
            }
        }
    }
    else if(leptons.size() == 2 && leptons.at(0).pid() == -leptons.at(1).pid())
    {
        l1_c = 0;
        l2_c = 1;

        TLorentzVector v_l1;
        v_l1.SetPtEtaPhiE(leptons.at(l1_c).pt(), leptons.at(l1_c).eta(), leptons.at(l1_c).phi(), leptons.at(l1_c).E());
        TLorentzVector v_l2;
        v_l2.SetPtEtaPhiE(leptons.at(l2_c).pt(), leptons.at(l2_c).eta(), leptons.at(l2_c).phi(), leptons.at(l2_c).E());
        closest = (v_l1 + v_l2).M();
    }

    return closest;
}

void add_var(std::string filename)
{
    TString filename_tstr(filename);
    if(!filename_tstr.Contains("363724") && !filename_tstr.Contains("364285") && !filename_tstr.Contains("364254") &&
       !filename_tstr.Contains("345723") && !filename_tstr.Contains("345666") && !filename_tstr.Contains("343233") &&
       !filename_tstr.Contains("361604") &&
       !filename_tstr.Contains("515527") && 
       !filename_tstr.Contains("515528") && 
       !filename_tstr.Contains("515529") && 
       !filename_tstr.Contains("515530") && 
       !filename_tstr.Contains("515531") && 
       !filename_tstr.Contains("515532") && 
       !filename_tstr.Contains("515533") && 
       !filename_tstr.Contains("515534") && 
       !filename_tstr.Contains("515535") && 
       !filename_tstr.Contains("515536") && 
       !filename_tstr.Contains("515537") && 
       !filename_tstr.Contains("515538") && 
       !filename_tstr.Contains("515539") && 
       !filename_tstr.Contains("515540") && 
       !filename_tstr.Contains("515541") && 
       !filename_tstr.Contains("515542"))
        return;

    TFile oldfile(filename.c_str());
    TTree *oldtree_llvv = nullptr;

    oldfile.GetObject("tree_PFLOW", oldtree_llvv);
    oldtree_llvv->SetBranchStatus("*", 1);

    float weight;
    float weight_gen;
    int event_type;
    //float met_Truth;
    //float truth_MetOHT;
    //float Z_pT;
    //float met_tst;
    //float dMetZPhi;
    std::vector<float> *truth_jets_pt(nullptr);
    std::vector<float> *truth_jets_eta(nullptr);
    std::vector<float> *truth_jets_phi(nullptr);
    std::vector<float> *truth_jets_e(nullptr);
    std::vector<float> *truth_electrons_pt(nullptr);
    std::vector<float> *truth_electrons_eta(nullptr);
    std::vector<float> *truth_electrons_phi(nullptr);
    std::vector<float> *truth_electrons_e(nullptr);
    std::vector<int>   *truth_electrons_PID(nullptr);
    std::vector<float> *truth_muons_pt(nullptr);
    std::vector<float> *truth_muons_eta(nullptr);
    std::vector<float> *truth_muons_phi(nullptr);
    std::vector<float> *truth_muons_e(nullptr);
    std::vector<int>   *truth_muons_PID(nullptr);
    std::vector<float> *truth_neutrinos_pt(nullptr);
    std::vector<float> *truth_neutrinos_eta(nullptr);
    std::vector<float> *truth_neutrinos_phi(nullptr);
    std::vector<float> *truth_neutrinos_e(nullptr);
    std::vector<int>   *truth_neutrinos_PID(nullptr);
    oldtree_llvv->SetBranchAddress("weight",              &weight);
    oldtree_llvv->SetBranchAddress("weight_gen",          &weight_gen);
    oldtree_llvv->SetBranchAddress("event_type",          &event_type);
    //oldtree_llvv->SetBranchAddress("met_Truth",           &met_Truth);
    //oldtree_llvv->SetBranchAddress("truth_MetOHT",        &truth_MetOHT);
    //oldtree_llvv->SetBranchAddress("Z_pT",                &Z_pT);
    //oldtree_llvv->SetBranchAddress("met_tst",             &met_tst);
    //oldtree_llvv->SetBranchAddress("dMetZPhi",            &dMetZPhi);
    oldtree_llvv->SetBranchAddress("truth_jets_pt",       &truth_jets_pt);
    oldtree_llvv->SetBranchAddress("truth_jets_eta",      &truth_jets_eta);
    oldtree_llvv->SetBranchAddress("truth_jets_phi",      &truth_jets_phi);
    oldtree_llvv->SetBranchAddress("truth_jets_e",        &truth_jets_e);
    oldtree_llvv->SetBranchAddress("truth_electrons_pt",  &truth_electrons_pt);
    oldtree_llvv->SetBranchAddress("truth_electrons_eta", &truth_electrons_eta);
    oldtree_llvv->SetBranchAddress("truth_electrons_phi", &truth_electrons_phi);
    oldtree_llvv->SetBranchAddress("truth_electrons_e",   &truth_electrons_e);
    oldtree_llvv->SetBranchAddress("truth_electrons_PID", &truth_electrons_PID);
    oldtree_llvv->SetBranchAddress("truth_muons_pt",      &truth_muons_pt);
    oldtree_llvv->SetBranchAddress("truth_muons_eta",     &truth_muons_eta);
    oldtree_llvv->SetBranchAddress("truth_muons_phi",     &truth_muons_phi);
    oldtree_llvv->SetBranchAddress("truth_muons_e",       &truth_muons_e);
    oldtree_llvv->SetBranchAddress("truth_muons_PID",     &truth_muons_PID);
    oldtree_llvv->SetBranchAddress("truth_neutrinos_pt",  &truth_neutrinos_pt);
    oldtree_llvv->SetBranchAddress("truth_neutrinos_eta", &truth_neutrinos_eta);
    oldtree_llvv->SetBranchAddress("truth_neutrinos_phi", &truth_neutrinos_phi);
    oldtree_llvv->SetBranchAddress("truth_neutrinos_e",   &truth_neutrinos_e);
    oldtree_llvv->SetBranchAddress("truth_neutrinos_PID",   &truth_neutrinos_PID);

    TFile newfile(basename(&filename[0]), "recreate");
    auto newtree_llvv = oldtree_llvv->CloneTree(0);

    int    truth_event_type;
    double presel_factor_inclusive;
    double presel_factor_ZZjj;
    double truth_Z_pT;
    double truth_mjj;
    double truth_M2Lep;
    double truth_M2Neu;
    double truth_dLepR;
    double truth_mT_ZZ;
    int    truth_n_jets;
    double truth_met_tst;
    double truth_dphill;
    double truth_dMetZPhi;
    double truth_leading_jet_pt;
    double truth_leading_jet_eta;
    double truth_second_jet_pt;
    double truth_second_jet_eta;
    double truth_leading_pT_lepton;
    double truth_leading_eta_lepton;
    double truth_subleading_pT_lepton;
    double truth_subleading_eta_lepton;
    double truth_leading_pT_neutrino;
    double truth_subleading_pT_neutrino;
    double truth_Z_rapidity;
    double truth_pt_zz;
    double truth_CP_ZZ_1;
    double truth_CP_ZZ_2;
    double truth_MetOHT_neu;
    newtree_llvv->Branch("truth_event_type", &truth_event_type);
    newtree_llvv->Branch("presel_factor_inclusive", &presel_factor_inclusive);
    newtree_llvv->Branch("presel_factor_ZZjj", &presel_factor_ZZjj);
    newtree_llvv->Branch("truth_Z_pT", &truth_Z_pT);
    newtree_llvv->Branch("truth_mjj", &truth_mjj);
    newtree_llvv->Branch("truth_M2Lep", &truth_M2Lep);
    newtree_llvv->Branch("truth_M2Neu", &truth_M2Neu);
    newtree_llvv->Branch("truth_dLepR", &truth_dLepR);
    newtree_llvv->Branch("truth_mT_ZZ", &truth_mT_ZZ);
    newtree_llvv->Branch("truth_n_jets", &truth_n_jets);
    newtree_llvv->Branch("truth_met_tst", &truth_met_tst);
    newtree_llvv->Branch("truth_dphill", &truth_dphill);
    newtree_llvv->Branch("truth_dMetZPhi", &truth_dMetZPhi);
    newtree_llvv->Branch("truth_leading_jet_pt",  &truth_leading_jet_pt);
    newtree_llvv->Branch("truth_leading_jet_eta", &truth_leading_jet_eta);
    newtree_llvv->Branch("truth_second_jet_pt",  &truth_second_jet_pt);
    newtree_llvv->Branch("truth_second_jet_eta", &truth_second_jet_eta);
    newtree_llvv->Branch("truth_leading_pT_lepton",  &truth_leading_pT_lepton);
    newtree_llvv->Branch("truth_leading_eta_lepton", &truth_leading_eta_lepton);
    newtree_llvv->Branch("truth_subleading_pT_lepton",  &truth_subleading_pT_lepton);
    newtree_llvv->Branch("truth_subleading_eta_lepton", &truth_subleading_eta_lepton);
    newtree_llvv->Branch("truth_leading_pT_neutrino",  &truth_leading_pT_neutrino);
    newtree_llvv->Branch("truth_subleading_pT_neutrino",  &truth_subleading_pT_neutrino);
    newtree_llvv->Branch("truth_Z_rapidity", &truth_Z_rapidity);
    newtree_llvv->Branch("truth_pt_zz", &truth_pt_zz);
    newtree_llvv->Branch("truth_CP_ZZ_1", &truth_CP_ZZ_1);
    newtree_llvv->Branch("truth_CP_ZZ_2", &truth_CP_ZZ_2);
    newtree_llvv->Branch("truth_MetOHT_neu", &truth_MetOHT_neu);

    int normal_count = 0;
    for(int i = 0; i < oldtree_llvv->GetEntries(); ++i)
    {
        oldtree_llvv->GetEntry(i);

        double pt_j1, eta_j1, phi_j1, e_j1;
        double pt_j2, eta_j2, phi_j2, e_j2;

        int l1_c = -1, l2_c = -1;
        double pt_l1_c, eta_l1_c, phi_l1_c, e_l1_c, c_l1_c;
        double pt_l2_c, eta_l2_c, phi_l2_c, e_l2_c, c_l2_c;

        int n1_c = -1, n2_c = -1;
        double pt_n1_c, eta_n1_c, phi_n1_c, e_n1_c;
        double pt_n2_c, eta_n2_c, phi_n2_c, e_n2_c;

        std::vector<particle> electrons(truth_electrons_pt->size());
        std::vector<particle> muons(truth_muons_pt->size());
        std::vector<particle> jets(truth_jets_pt->size());
        std::vector<particle> neutrinos(truth_neutrinos_pt->size());

        for(int p = 0; p < truth_electrons_pt->size(); ++p)
            electrons.at(p).set_pt_eta_phi_E_pid(truth_electrons_pt->at(p),
                                                 truth_electrons_eta->at(p),
                                                 truth_electrons_phi->at(p),
                                                 truth_electrons_e->at(p),
                                                 truth_electrons_PID->at(p));
        for(int p = 0; p < truth_muons_pt->size(); ++p)
            muons.at(p).set_pt_eta_phi_E_pid(truth_muons_pt->at(p),
                                             truth_muons_eta->at(p),
                                             truth_muons_phi->at(p),
                                             truth_muons_e->at(p),
                                             truth_muons_PID->at(p));
        for(int p = 0; p < truth_jets_pt->size(); ++p)
            jets.at(p).set_pt_eta_phi_E_pid(truth_jets_pt->at(p),
                                            truth_jets_eta->at(p),
                                            truth_jets_phi->at(p),
                                            truth_jets_e->at(p),
                                            0);
        for(int p = 0; p < truth_neutrinos_pt->size(); ++p)
            neutrinos.at(p).set_pt_eta_phi_E_pid(truth_neutrinos_pt->at(p),
                                                 truth_neutrinos_eta->at(p),
                                                 truth_neutrinos_phi->at(p),
                                                 truth_neutrinos_e->at(p),
                                                 truth_neutrinos_PID->at(p));

//rm jets
        //rm_jets(jets, electrons, muons, 0.);

//rm leptons
        //rm_leptons(electrons, muons, jets, 0.);

//select

        int l1_c_mumu = -1, l2_c_mumu = -1;
        double Z_mass_mumu = INFINITY;
        if(muons.size() >= 2)
            Z_mass_mumu = select_leptons(l1_c_mumu, l2_c_mumu, muons);

        int l1_c_ee = -1, l2_c_ee = -1;
        double Z_mass_ee = INFINITY;
        if(electrons.size() >= 2)
            Z_mass_ee = select_leptons(l1_c_ee, l2_c_ee, electrons);

        if(std::abs(Z_mass_mumu - Z_mass) < std::abs(Z_mass_ee - Z_mass))
        {
            if(l1_c_mumu >= 0 && l2_c_mumu >= 0)
            {
                truth_event_type = 0;

                l1_c = l1_c_mumu;
                l2_c = l2_c_mumu;

                pt_l1_c = muons.at(l1_c).pt();
                eta_l1_c = muons.at(l1_c).eta();
                phi_l1_c = muons.at(l1_c).phi();
                e_l1_c = muons.at(l1_c).E();
                c_l1_c = muons.at(l1_c).charge();
                pt_l2_c = muons.at(l2_c).pt();
                eta_l2_c = muons.at(l2_c).eta();
                phi_l2_c = muons.at(l2_c).phi();
                e_l2_c = muons.at(l2_c).E();
                c_l2_c = muons.at(l2_c).charge();
            }
        }
        else
        {
            if(l1_c_ee >= 0 && l2_c_ee >= 0)
            {
                truth_event_type = 1;

                l1_c = l1_c_ee;
                l2_c = l2_c_ee;

                pt_l1_c = electrons.at(l1_c).pt();
                eta_l1_c = electrons.at(l1_c).eta();
                phi_l1_c = electrons.at(l1_c).phi();
                e_l1_c = electrons.at(l1_c).E();
                c_l1_c = electrons.at(l1_c).charge();
                pt_l2_c = electrons.at(l2_c).pt();
                eta_l2_c = electrons.at(l2_c).eta();
                phi_l2_c = electrons.at(l2_c).phi();
                e_l2_c = electrons.at(l2_c).E();
                c_l2_c = electrons.at(l2_c).charge();
            }
        }

//select neutrinos
        if(neutrinos.size() >= 2)
        {
            select_leptons(n1_c, n2_c, neutrinos);

            if(n1_c >= 0 && n2_c >= 0)
            {
                pt_n1_c = neutrinos.at(n1_c).pt();
                eta_n1_c = neutrinos.at(n1_c).eta();
                phi_n1_c = neutrinos.at(n1_c).phi();
                e_n1_c = neutrinos.at(n1_c).E();
                pt_n2_c = neutrinos.at(n2_c).pt();
                eta_n2_c = neutrinos.at(n2_c).eta();
                phi_n2_c = neutrinos.at(n2_c).phi();
                e_n2_c = neutrinos.at(n2_c).E();
            }                
        }

        if(jets.size() >= 2)
        {
            std::partial_sort(jets.begin(), jets.begin() + 2, jets.end(),
                              [&](const particle &p1, const particle &p2) -> bool
                              {
                                  return p1.pt() > p2.pt();
                              });

            pt_j1 = jets.at(0).pt();
            eta_j1 = jets.at(0).eta();
            phi_j1 = jets.at(0).phi();
            e_j1 = jets.at(0).E();

            pt_j2 = jets.at(1).pt();
            eta_j2 = jets.at(1).eta();
            phi_j2 = jets.at(1).phi();
            e_j2 = jets.at(1).E();

            TLorentzVector v_j1, v_j2;
            v_j1.SetPtEtaPhiE(pt_j1, eta_j1, phi_j1, e_j1);
            v_j2.SetPtEtaPhiE(pt_j2, eta_j2, phi_j2, e_j2);
            auto jj = v_j1 + v_j2;
            truth_mjj = jj.M();
    
        }
        else
        {
            pt_j1 = jets.size() ? jets.at(0).pt() : -INFINITY;
            eta_j1 = jets.size() ? jets.at(0).eta() : -INFINITY;
            phi_j1 = jets.size() ? jets.at(0).phi() : -INFINITY;
            e_j1 = jets.size() ? jets.at(0).E() : -INFINITY;

            pt_j2 = -INFINITY;
            eta_j2 = -INFINITY;
            phi_j2 = -INFINITY;
            e_j2 = -INFINITY;

            truth_mjj  = -INFINITY;
        }

        truth_n_jets = jets.size();
        truth_leading_jet_pt = pt_j1;
        truth_leading_jet_eta = eta_j1;
        truth_second_jet_pt = pt_j2;
        truth_second_jet_eta = eta_j2;

        if(neutrinos.size() < 2 ||
           (l1_c < 0 || l2_c < 0) ||
           (n1_c < 0 || n2_c < 0))
        {
            truth_event_type = -1;
            truth_Z_pT = -INFINITY;
            truth_M2Lep = -INFINITY;
            truth_M2Neu = -INFINITY;
            truth_dLepR = -INFINITY;
            truth_mT_ZZ = -INFINITY;
            truth_met_tst = -INFINITY;
            truth_dphill = -INFINITY;
            truth_dMetZPhi = -INFINITY;
            truth_leading_pT_lepton = -INFINITY;
            truth_leading_eta_lepton = -INFINITY;
            truth_subleading_pT_lepton = -INFINITY;
            truth_subleading_eta_lepton = -INFINITY;
            truth_leading_pT_neutrino = -INFINITY;
            truth_subleading_pT_neutrino = -INFINITY;
            truth_pt_zz = -INFINITY;
            truth_MetOHT_neu = -INFINITY;

            presel_factor_inclusive = 0.;
            presel_factor_ZZjj = 0.;
        }
        else
        {
            normal_count++;

            truth_leading_pT_neutrino = pt_n1_c;
            truth_subleading_pT_neutrino = pt_n2_c;

            TLorentzVector v_l1, v_l2;
            v_l1.SetPtEtaPhiE(pt_l1_c, eta_l1_c, phi_l1_c, e_l1_c);
            v_l2.SetPtEtaPhiE(pt_l2_c, eta_l2_c, phi_l2_c, e_l2_c);
            truth_dLepR = v_l1.DeltaR(v_l2);
            truth_dphill = -v_l1.DeltaPhi(v_l2);
            auto Z_ll = v_l1 + v_l2;
            truth_M2Lep = Z_ll.M();
            truth_Z_pT = Z_ll.Pt();
            truth_Z_rapidity = Z_ll.Rapidity();
    
            TLorentzVector v_n1, v_n2;
            v_n1.SetPtEtaPhiE(pt_n1_c, eta_n1_c, phi_n1_c, e_n1_c);
            v_n2.SetPtEtaPhiE(pt_n2_c, eta_n2_c, phi_n2_c, e_n2_c);
            auto Z_miss = v_n1 + v_n2;
            truth_dMetZPhi = Z_miss.DeltaPhi(Z_ll);
            truth_met_tst = Z_miss.Pt();
            truth_M2Neu = Z_miss.M();
    
            auto ZZ = Z_ll + Z_miss;
            truth_mT_ZZ = std::sqrt(
                                    (std::sqrt(Z_mass*Z_mass + truth_Z_pT*truth_Z_pT) +
                                     std::sqrt(Z_mass*Z_mass + truth_met_tst*truth_met_tst)
                                    )*
                                    (std::sqrt(Z_mass*Z_mass + truth_Z_pT*truth_Z_pT) +
                                     std::sqrt(Z_mass*Z_mass + truth_met_tst*truth_met_tst)
                                    ) -
                                    (Z_ll.Px() + Z_miss.Px())*(Z_ll.Px() + Z_miss.Px()) -
                                    (Z_ll.Py() + Z_miss.Py())*(Z_ll.Py() + Z_miss.Py())
                                   );
            truth_pt_zz = std::sqrt(truth_met_tst*truth_met_tst + truth_Z_pT*truth_Z_pT + 2*truth_met_tst*truth_Z_pT*std::cos(truth_dMetZPhi));
    
            truth_leading_pT_lepton = pt_l1_c > pt_l2_c ? pt_l1_c : pt_l2_c;
            truth_leading_eta_lepton = pt_l1_c > pt_l2_c ? eta_l1_c : eta_l2_c;
            truth_subleading_pT_lepton = pt_l1_c > pt_l2_c ? pt_l2_c : pt_l1_c;
            truth_subleading_eta_lepton = pt_l1_c > pt_l2_c ? eta_l2_c : eta_l1_c;

            double truth_HT = truth_leading_pT_lepton + truth_subleading_pT_lepton;
            for(const auto &jet : jets) truth_HT += jet.pt();
            truth_MetOHT_neu = truth_met_tst/truth_HT;

            double lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m;
            double lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m;
            if(c_l1_c > 0)
            {
                lepplus_pt = pt_l1_c; lepplus_eta = eta_l1_c; lepplus_phi = phi_l1_c; lepplus_m = v_l1.M();
                lepminus_pt = pt_l2_c; lepminus_eta = eta_l2_c; lepminus_phi = phi_l2_c; lepminus_m = v_l2.M();
            }
            else
            {
                lepminus_pt = pt_l1_c; lepminus_eta = eta_l1_c; lepminus_phi = phi_l1_c; lepminus_m = v_l1.M();
                lepplus_pt = pt_l2_c; lepplus_eta = eta_l2_c; lepplus_phi = phi_l2_c; lepplus_m = v_l2.M();
            }
            truth_CP_ZZ_1 = get_cp_zz_1(Z_miss.Px(), Z_miss.Py(), truth_met_tst,
                                        lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m,
                                        lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);
            truth_CP_ZZ_2 = get_cp_zz_2(lepplus_pt, lepplus_eta, lepplus_phi, lepplus_m,
                                        lepminus_pt, lepminus_eta, lepminus_phi, lepminus_m);

            presel_factor_inclusive = get_presel_factor_inclusive(filename, std::abs(truth_dMetZPhi));
            presel_factor_ZZjj      = get_presel_factor_ZZjj(filename, std::abs(truth_dMetZPhi));
        }

        newtree_llvv->Fill();
    }

    std::cout << oldtree_llvv->GetEntries() << "\t" << normal_count << std::endl;

    newtree_llvv->Write("tree_PFLOW");
    newfile.Write();
}

void remove_keys(std::string fname, std::set<std::string> &keys_to_save)
{
    TFile *f = new TFile(fname.c_str(), "update");

    std::map<std::string, int> count;
    TIter next1(f->GetListOfKeys());
    TIter next2(f->GetListOfKeys());
    TKey *key;
    while((key = (TKey*)next1()))
    {
        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (keys_to_save.find(name) != keys_to_save.end())
        {
            if (count.find(name) == count.end()) count[name] = cycle;
            else if (count[name] < cycle)        count[name] = cycle;
        }
    }
    while ((key = (TKey*)next2()))
    {
        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (!(count.find(name) != count.end() && count[name] == cycle))
        {
            printf("%10s  %20s  %2d   delete\n", key->GetClassName(), name.c_str(), cycle);
            gDirectory->Delete((name + ";" + std::to_string(cycle)).c_str());
        }
        else
        {
            printf("%10s  %20s  %2d   keep  \n", key->GetClassName(), name.c_str(), cycle);
        }

    }
    f->Close();
}


int main(int argc, char* argv[])
{
    TString path;
    std::string filename;

    path = argv[1];
    filename = argv[2];

    gSystem->cd("/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/" + path);
    //gSystem->cd("/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Scale/" + path);
    std::cout << gSystem->pwd() << std::endl;

    add_var(filename);
    std::set<std::string> keys = {
         "tree_PFLOW",
         "tree_emCR_PFLOW",
         "tree_3lCR_PFLOW",
         "tree_4lCR_PFLOW",
         "hInfo",
    };
    remove_keys(basename(&filename[0]), keys);

    return 0;
}
