TString yifan_path   = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/Cutflow/";
TString aggelos_path = "/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/Cutflow/";
TString yifan   = "*.root";
TString aggelos = "*.root";

using RNode = ROOT::RDF::RNode;

std::vector<TCut> yifan_cuts = {"truth_leading_eta_lepton>-2.5&&truth_leading_eta_lepton<2.5",
                                "truth_subleading_eta_lepton>-2.5&&truth_subleading_eta_lepton<2.5",
                                "truth_leading_pT_lepton>30",
                                "truth_subleading_pT_lepton>20",
                                "truth_M2Lep>76&&truth_M2Lep<106",
                                "met_Truth>95",
                                "truth_dLepR<1.8",
                                };
std::vector<TCut> aggelos_cuts = {"leading_eta_lepton_Truth>-2.5&&leading_eta_lepton_Truth<2.5",
                                  "subleading_eta_lepton_Truth>-2.5&&subleading_eta_lepton_Truth<2.5",
                                  "leading_pT_lepton_Truth>30",
                                  "subleading_pT_lepton_Truth>20",
                                  "M2Lep_truth>76&&M2Lep_truth<106",
                                  "met_Truth>95",
                                  "dLepR_truth<1.8",
                                  };

TCut get_final_cut(const std::vector<TCut> *cuts)
{
    TCut final_cut;
    for(auto &&cut : *cuts) final_cut = final_cut&&cut;
    return final_cut;
}

RNode get_node(const TString &file, const TString &tree, const TCut &cuts)
{
    ROOT::RDataFrame data(tree.Data(), file.Data());
    RNode node = data.Filter(cuts.GetTitle());
    return node;
}

TH1D* get_hist(RNode &node, const TString &leaf)
{
    TString leaf_to_draw(leaf + "_abs");
    auto hist = node.Define(leaf_to_draw.Data(), ("abs(" + leaf + ")").Data()).Histo1D({"hist", "", 100, 0, 3.2}, leaf_to_draw.Data());
    return dynamic_cast<TH1D*>(hist->Clone());
}

void compare_truth()
{
    TH1::SetDefaultSumw2();
    gStyle->SetOptStat(0);

    auto c = new TCanvas("c", "", 800., 600.);

    auto yifan_final_cut   = get_final_cut(&yifan_cuts);
    auto aggelos_final_cut = get_final_cut(&aggelos_cuts);

    auto yifan_node   = get_node(yifan_path + "/" + yifan,     "tree_PFLOW", "truth_event_type>-1");
    auto aggelos_node = get_node(aggelos_path + "/" + aggelos, "tree_PFLOW", "truth_event_type>-1");

    auto yifan_hist   = get_hist(yifan_node,   "truth_MetOHT");
    auto aggelos_hist = get_hist(aggelos_node, "truth_MetOHT_neu");

    double minimum = yifan_hist->GetMinimum() < aggelos_hist->GetMinimum() ?
                     yifan_hist->GetMinimum() : aggelos_hist->GetMinimum();
    double maximum = yifan_hist->GetMaximum() > aggelos_hist->GetMaximum() ?
                     yifan_hist->GetMaximum() : aggelos_hist->GetMaximum();
    yifan_hist->SetMinimum(0.8*minimum);
    yifan_hist->SetMaximum(1.1*maximum);

    yifan_hist->SetLineWidth(2);
    yifan_hist->SetLineColor(kGreen + 2);
    aggelos_hist->SetLineWidth(2);
    aggelos_hist->SetLineColor(kBlack);
    aggelos_hist->SetMarkerSize(1);
    aggelos_hist->SetMarkerStyle(kFullCircle);
    aggelos_hist->SetMarkerColor(kBlack);

    auto rp = new TRatioPlot(aggelos_hist, yifan_hist);
    rp->SetH1DrawOpt("E");
    rp->SetH2DrawOpt("hist");
    rp->Draw();

    rp->SetLeftMargin(0.12);
    rp->SetLowBottomMargin(0.3);

    rp->GetUpperRefXaxis()->SetTitle("truth E^{missing}_{T}/HT");
    rp->GetUpperRefYaxis()->SetTitle("No. of events");
    rp->GetLowerRefYaxis()->SetTitle("#{from neutrino}{minitree default}");
    rp->GetLowerRefYaxis()->SetRangeUser(0.5, 1.5);
    rp->GetLowerRefYaxis()->SetNdivisions(5, 2, 0);

    rp->GetLowerRefGraph()->SetLineColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerStyle(21);
    rp->GetLowerRefGraph()->SetMarkerColor(kBlack);
    rp->GetLowerRefGraph()->SetMarkerSize(0.75);

    rp->GetUpperPad()->cd();
    TLegend *legend = new TLegend(0.70, 0.59, 0.90, 0.69);
    legend->AddEntry(yifan_hist,   "minitree default", "L");
    legend->AddEntry(aggelos_hist, "from neutrino",    "LP");
    legend->SetTextSize(0.04);
    legend->SetBorderSize(0);
    legend->Draw();

    TString name = "compare_truth_MetOHT";
    c->SaveAs("fig/" + name + ".png");

    c->cd();
}
