#include <string>
#include <map>
#include <set>
#include <array>
#include <iostream>

#include "TString.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TKey.h"

//#include "inc/Syst.h"

const TString old_weight_pdf_prefix = "Member";
const TString new_weight_pdf_prefix = "weight_var_th_MUR1_MUF1_PDF261";

const std::vector<TString> old_weight_scale = {"mur0p5muf0p5",
                                               "mur0p5muf1",
                                               "mur0p5muf2",
                                               "mur1muf0p5",
                                               //"mur1muf1",
                                               "mur1muf2",
                                               "mur2muf0p5",
                                               "mur2muf1",
                                               "mur2muf2"};
const std::vector<TString> new_weight_scale = {"weight_var_th_MUR0p5_MUF0p5_PDF261000",
                                               "weight_var_th_MUR0p5_MUF1_PDF261000",
                                               "weight_var_th_MUR0p5_MUF2_PDF261000",
                                               "weight_var_th_MUR1_MUF0p5_PDF261000",
                                               "weight_var_th_MUR1_MUF2_PDF261000",
                                               "weight_var_th_MUR2_MUF0p5_PDF261000",
                                               "weight_var_th_MUR2_MUF1_PDF261000",
                                               "weight_var_th_MUR2_MUF2_PDF261000"};

TString FormatNewId(int id)
{
    auto formated = TString::Format("%d", id);
    while(formated.Length() < 3)
        formated = "0" + formated;
    return formated;
}

void Rename(const TString &filename)
{
    TFile oldfile(filename);
    TTree *oldtree_llvv = nullptr;
    oldfile.GetObject("tree_PFLOW", oldtree_llvv);
    oldtree_llvv->SetBranchStatus("*",        1);
    oldtree_llvv->SetBranchStatus("mur1muf1", 0);

    TFile newfile(filename, "recreate");
    auto newtree_llvv = oldtree_llvv->CloneTree(0);

    std::array<TString, 101 + 8> old_branch_names;
    std::array<float,   101 + 8> old_branches;
    std::array<float,   101 + 8> new_branches;
    for(int i = 0; i <= 100; i++)
    {
        TString old_branch_name = old_weight_pdf_prefix + TString::Format("%d", i);
        TString new_branch_name = new_weight_pdf_prefix + FormatNewId(i);
        old_branch_names.at(i) = old_branch_name;

        oldtree_llvv->SetBranchAddress(old_branch_name, &old_branches.at(i));
        newtree_llvv->Branch          (new_branch_name, &new_branches.at(i));
    }
    for(int i = 101; i < old_branches.size(); i++)
    {
        old_branch_names.at(i) = old_weight_scale.at(i - 101);
        oldtree_llvv->SetBranchAddress(old_weight_scale.at(i - 101), &old_branches.at(i));
        newtree_llvv->Branch          (new_weight_scale.at(i - 101), &new_branches.at(i));
    }

    for(int i = 0; i < oldtree_llvv->GetEntries(); i++)
    //for(int i = 0; i < 10000; i++)
    {
        //if(i%1000 == 0) std::cout << i << std::endl;

        oldtree_llvv->GetEntry(i);

        for(int j = 0; j < old_branches.size(); j++)
            new_branches.at(j) = old_branches.at(j);

        newtree_llvv->Fill();
    }

    for(int i = 0; i < old_branch_names.size(); i++)
    {
        auto old_branch = newtree_llvv->GetBranch(old_branch_names.at(i));
        newtree_llvv->GetListOfBranches()->Remove(old_branch);
        newtree_llvv->GetListOfBranches()->Compress();

        auto old_leaf = newtree_llvv->GetLeaf(old_branch_names.at(i));
        newtree_llvv->GetListOfLeaves()->Remove(old_leaf);
        newtree_llvv->GetListOfLeaves()->Compress();
    }

    newtree_llvv->Write("tree_PFLOW");
    newfile.Close();
}

void Remove_keys(std::string fname, std::set<std::string> &keys_to_save)
{
    TFile *f = new TFile(fname.c_str(), "update");

    std::map<std::string, int> count;
    TIter next1(f->GetListOfKeys());
    TIter next2(f->GetListOfKeys());
    TKey *key;
    while((key = (TKey*)next1()))
    {
        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (keys_to_save.find(name) != keys_to_save.end())
        {
            if (count.find(name) == count.end()) count[name] = cycle;
            else if (count[name] < cycle)        count[name] = cycle;
        }
    }

    while ((key = (TKey*)next2()))
    {
        std::string name = key->GetName();
        int cycle = key->GetCycle();
        if (!(count.find(name) != count.end() && count[name] == cycle))
        {
            printf("%10s  %20s  %2d   delete\n", key->GetClassName(), name.c_str(), cycle);
            gDirectory->Delete((name + ";" + std::to_string(cycle)).c_str());
        }
        else
        {
            printf("%10s  %20s  %2d   keep  \n", key->GetClassName(), name.c_str(), cycle);
        }

    }

    f->Close();
}

int main(int argc, char* argv[])
{
    TString path = argv[1];
    TString filename = argv[2];
    gSystem->cd("/lustre/collider/zhuyifan/VBSZZ/NewMinitrees/Fiducial/" + path);

    Rename(filename);
    std::set<std::string> keys = {
         "tree_PFLOW",
         "tree_emCR_PFLOW",
         "tree_3lCR_PFLOW",
         "tree_4lCR_PFLOW",
         "hInfo",
    };
    Remove_keys(filename.Data(), keys);

    return 0;
}
