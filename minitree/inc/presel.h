#ifndef PRESEL_FACTOR_H
#define PRESEL_FACTOR_H

#include "TString.h"

double get_presel_factor_ZZjj(const TString &filename, double var)
{
    if(std::isnan(var) || std::isinf(var)) return 0.;

    if(var < 2.1) return 1.;

    if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10201_p4252.root"))
        return -0.107724*var*var*var*var*var + 0.906699*var*var*var*var + -2.650737*var*var*var + 2.847576*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10724_p4252.root"))
        return -0.136599*var*var*var*var*var + 1.166410*var*var*var*var + -3.411302*var*var*var + 3.579551*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r9364_p4252.root"))
        return -0.069382*var*var*var*var*var + 0.671379*var*var*var*var + -2.260794*var*var*var + 2.759723*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10201_p4252.root"))
        return -0.024926*var*var*var*var*var + 0.304576*var*var*var*var + -1.244319*var*var*var + 1.807523*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10724_p4252.root"))
        return -0.052545*var*var*var*var*var + 0.514665*var*var*var*var + -1.771004*var*var*var + 2.242305*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r9364_p4252.root"))
        return -0.033708*var*var*var*var*var + 0.380697*var*var*var*var + -1.466501*var*var*var + 2.025357*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10201_r10210_p4252.root"))
        return -0.104147*var*var*var*var*var + 0.863411*var*var*var*var + -2.500883*var*var*var + 2.687986*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10724_r10726_p4252.root"))
        return -0.090218*var*var*var*var*var + 0.815202*var*var*var*var + -2.554964*var*var*var + 2.910187*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_s3126_r9364_r9315_p4252.root"))
        return -0.049371*var*var*var*var*var + 0.479374*var*var*var*var + -1.631001*var*var*var + 2.060973*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10201_p4252.root"))
        return -0.036175*var*var*var*var*var + 0.375643*var*var*var*var + -1.377487*var*var*var + 1.873221*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10724_p4252.root"))
        return -0.052174*var*var*var*var*var + 0.531341*var*var*var*var + -1.857093*var*var*var + 2.345306*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r9364_p4252.root"))
        return -0.053480*var*var*var*var*var + 0.527974*var*var*var*var + -1.820655*var*var*var + 2.300457*var*var + 0.000000*var + 0.000000;
/*
    if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10201_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.54292;
        else if (var >= 30 && var < 40) return 1.35098;
        else if (var >= 40 && var < 50) return 1.25174;
        else if (var >= 50 && var < 60) return 1.29242;
        else if (var >= 60 && var < 70) return 1.30600;
        else if (var >= 70 && var < 80) return 1.28142;
        else if (var >= 80 && var < 90) return 1.10570;
        else if (var >= 90 && var < 100) return 1.20657;
        else if (var >= 100 && var < 110) return 1.12321;
        else if (var >= 110 && var < 120) return 1.20394;
        else if (var >= 120 && var < 140) return 1.59364;
        else if (var >= 140 && var < 160) return 1.84493;
        else if (var >= 160 && var < 200) return 1.11553;
        else if (var >= 200 && var < 1000) return 1.87880;
    
    }
    else if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10724_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.59879;
        else if (var >= 30 && var < 40) return 1.36081;
        else if (var >= 40 && var < 50) return 1.34378;
        else if (var >= 50 && var < 60) return 1.28607;
        else if (var >= 60 && var < 70) return 1.46113;
        else if (var >= 70 && var < 80) return 1.29137;
        else if (var >= 80 && var < 90) return 1.26764;
        else if (var >= 90 && var < 100) return 1.20622;
        else if (var >= 100 && var < 110) return 1.15543;
        else if (var >= 110 && var < 120) return 1.48243;
        else if (var >= 120 && var < 140) return 1.41563;
        else if (var >= 140 && var < 160) return 1.58768;
        else if (var >= 160 && var < 200) return 1.17146;
        else if (var >= 200 && var < 1000) return 1.54235;
    
    }
    else if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r9364_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.78508;
        else if (var >= 30 && var < 40) return 1.33236;
        else if (var >= 40 && var < 50) return 1.24062;
        else if (var >= 50 && var < 60) return 1.31030;
        else if (var >= 60 && var < 70) return 1.27339;
        else if (var >= 70 && var < 80) return 1.28169;
        else if (var >= 80 && var < 90) return 1.24394;
        else if (var >= 90 && var < 100) return 1.28390;
        else if (var >= 100 && var < 110) return 1.19679;
        else if (var >= 110 && var < 120) return 1.49618;
        else if (var >= 120 && var < 140) return 1.37908;
        else if (var >= 140 && var < 160) return 1.27276;
        else if (var >= 160 && var < 200) return 1.49011;
        else if (var >= 200 && var < 1000) return 1.94371;
    
    }
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10201_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.52062;
        else if (var >= 30 && var < 40) return 1.35466;
        else if (var >= 40 && var < 50) return 1.27884;
        else if (var >= 50 && var < 60) return 1.27950;
        else if (var >= 60 && var < 70) return 1.22844;
        else if (var >= 70 && var < 80) return 1.22664;
        else if (var >= 80 && var < 90) return 1.22006;
        else if (var >= 90 && var < 100) return 1.22863;
        else if (var >= 100 && var < 110) return 1.24306;
        else if (var >= 110 && var < 120) return 1.25000;
        else if (var >= 120 && var < 140) return 1.18152;
        else if (var >= 140 && var < 160) return 1.27439;
        else if (var >= 160 && var < 200) return 1.15244;
        else if (var >= 200 && var < 1000) return 1.32903;
    
    }
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10724_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.55820;
        else if (var >= 30 && var < 40) return 1.31793;
        else if (var >= 40 && var < 50) return 1.28930;
        else if (var >= 50 && var < 60) return 1.24817;
        else if (var >= 60 && var < 70) return 1.27102;
        else if (var >= 70 && var < 80) return 1.21826;
        else if (var >= 80 && var < 90) return 1.22514;
        else if (var >= 90 && var < 100) return 1.22289;
        else if (var >= 100 && var < 110) return 1.18605;
        else if (var >= 110 && var < 120) return 1.19811;
        else if (var >= 120 && var < 140) return 1.21635;
        else if (var >= 140 && var < 160) return 1.22645;
        else if (var >= 160 && var < 200) return 1.24286;
        else if (var >= 200 && var < 1000) return 1.23894;
    
    }
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r9364_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.46763;
        else if (var >= 30 && var < 40) return 1.32223;
        else if (var >= 40 && var < 50) return 1.27245;
        else if (var >= 50 && var < 60) return 1.28398;
        else if (var >= 60 && var < 70) return 1.24987;
        else if (var >= 70 && var < 80) return 1.22626;
        else if (var >= 80 && var < 90) return 1.23965;
        else if (var >= 90 && var < 100) return 1.27821;
        else if (var >= 100 && var < 110) return 1.16357;
        else if (var >= 110 && var < 120) return 1.22414;
        else if (var >= 120 && var < 140) return 1.25084;
        else if (var >= 140 && var < 160) return 1.27778;
        else if (var >= 160 && var < 200) return 1.25581;
        else if (var >= 200 && var < 1000) return 1.25235;
    
    }
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10201_r10210_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.46810;
        else if (var >= 30 && var < 40) return 1.27601;
        else if (var >= 40 && var < 50) return 1.29183;
        else if (var >= 50 && var < 60) return 1.22322;
        else if (var >= 60 && var < 70) return 1.35737;
        else if (var >= 70 && var < 80) return 1.20005;
        else if (var >= 80 && var < 90) return 1.47426;
        else if (var >= 90 && var < 100) return 1.20472;
        else if (var >= 100 && var < 110) return 1.15375;
        else if (var >= 110 && var < 120) return 1.12633;
        else if (var >= 120 && var < 140) return 1.00000;
        else if (var >= 140 && var < 160) return 1.19213;
        else if (var >= 160 && var < 200) return 1.37493;
        else if (var >= 200 && var < 1000) return 1.59819;
    
    }
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10724_r10726_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.49400;
        else if (var >= 30 && var < 40) return 1.33531;
        else if (var >= 40 && var < 50) return 1.35677;
        else if (var >= 50 && var < 60) return 1.26592;
        else if (var >= 60 && var < 70) return 1.29072;
        else if (var >= 70 && var < 80) return 1.29789;
        else if (var >= 80 && var < 90) return 1.28922;
        else if (var >= 90 && var < 100) return 1.05538;
        else if (var >= 100 && var < 110) return 1.18157;
        else if (var >= 110 && var < 120) return 1.18023;
        else if (var >= 120 && var < 140) return 1.32560;
        else if (var >= 140 && var < 160) return 1.37716;
        else if (var >= 160 && var < 200) return 1.15172;
        else if (var >= 200 && var < 1000) return 1.58275;
    
    }
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_s3126_r9364_r9315_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.42488;
        else if (var >= 30 && var < 40) return 1.28738;
        else if (var >= 40 && var < 50) return 1.32781;
        else if (var >= 50 && var < 60) return 1.35464;
        else if (var >= 60 && var < 70) return 1.46176;
        else if (var >= 70 && var < 80) return 1.25676;
        else if (var >= 80 && var < 90) return 1.19694;
        else if (var >= 90 && var < 100) return 1.06599;
        else if (var >= 100 && var < 110) return 1.27426;
        else if (var >= 110 && var < 120) return 1.66953;
        else if (var >= 120 && var < 140) return 1.39711;
        else if (var >= 140 && var < 160) return 1.43273;
        else if (var >= 160 && var < 200) return 1.37237;
        else if (var >= 200 && var < 1000) return 1.00000;
    
    }
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10201_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.50941;
        else if (var >= 30 && var < 40) return 1.34980;
        else if (var >= 40 && var < 50) return 1.35198;
        else if (var >= 50 && var < 60) return 1.26181;
        else if (var >= 60 && var < 70) return 1.27364;
        else if (var >= 70 && var < 80) return 1.26995;
        else if (var >= 80 && var < 90) return 1.25794;
        else if (var >= 90 && var < 100) return 1.22569;
        else if (var >= 100 && var < 110) return 1.14444;
        else if (var >= 110 && var < 120) return 1.29629;
        else if (var >= 120 && var < 140) return 1.24646;
        else if (var >= 140 && var < 160) return 1.27430;
        else if (var >= 160 && var < 200) return 1.24350;
        else if (var >= 200 && var < 1000) return 1.46554;
    
    }
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10724_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.50071;
        else if (var >= 30 && var < 40) return 1.38285;
        else if (var >= 40 && var < 50) return 1.33943;
        else if (var >= 50 && var < 60) return 1.28037;
        else if (var >= 60 && var < 70) return 1.26966;
        else if (var >= 70 && var < 80) return 1.25932;
        else if (var >= 80 && var < 90) return 1.33804;
        else if (var >= 90 && var < 100) return 1.19548;
        else if (var >= 100 && var < 110) return 1.29966;
        else if (var >= 110 && var < 120) return 1.19113;
        else if (var >= 120 && var < 140) return 1.17174;
        else if (var >= 140 && var < 160) return 1.31452;
        else if (var >= 160 && var < 200) return 1.21355;
        else if (var >= 200 && var < 1000) return 1.38728;
    
    }
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r9364_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.60776;
        else if (var >= 30 && var < 40) return 1.31298;
        else if (var >= 40 && var < 50) return 1.33102;
        else if (var >= 50 && var < 60) return 1.32588;
        else if (var >= 60 && var < 70) return 1.27743;
        else if (var >= 70 && var < 80) return 1.30016;
        else if (var >= 80 && var < 90) return 1.33056;
        else if (var >= 90 && var < 100) return 1.26356;
        else if (var >= 100 && var < 110) return 1.19947;
        else if (var >= 110 && var < 120) return 1.12216;
        else if (var >= 120 && var < 140) return 1.23331;
        else if (var >= 140 && var < 160) return 1.27592;
        else if (var >= 160 && var < 200) return 1.32105;
        else if (var >= 200 && var < 1000) return 1.25338;
    }
*/
    return 1.;
}

double get_presel_factor_inclusive(const TString &filename, double var)
{
    if(std::isnan(var) || std::isinf(var)) return 0.;

    if(var < 2.1) return 1.;

    if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10201_p4252.root"))
        return -0.032354*var*var*var*var*var + 0.347745*var*var*var*var + -1.313495*var*var*var + 1.826078*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10724_p4252.root"))
        return -0.126911*var*var*var*var*var + 1.119023*var*var*var*var + -3.380679*var*var*var + 3.638475*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r9364_p4252.root"))
        return -0.028154*var*var*var*var*var + 0.333089*var*var*var*var + -1.342213*var*var*var + 1.929857*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10201_p4252.root"))
        return -0.036782*var*var*var*var*var + 0.392226*var*var*var*var + -1.459838*var*var*var + 1.983261*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10724_p4252.root"))
        return -0.050914*var*var*var*var*var + 0.500728*var*var*var*var + -1.731171*var*var*var + 2.202808*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r9364_p4252.root"))
        return -0.040050*var*var*var*var*var + 0.422193*var*var*var*var + -1.549937*var*var*var + 2.071679*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10201_r10210_p4252.root"))
        return -0.029462*var*var*var*var*var + 0.328433*var*var*var*var + -1.272980*var*var*var + 1.800568*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10724_r10726_p4252.root"))
        return -0.042702*var*var*var*var*var + 0.447625*var*var*var*var + -1.628660*var*var*var + 2.151427*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_s3126_r9364_r9315_p4252.root"))
        return -0.020445*var*var*var*var*var + 0.246199*var*var*var*var + -1.028033*var*var*var + 1.562415*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10201_p4252.root"))
        return -0.028296*var*var*var*var*var + 0.323717*var*var*var*var + -1.276634*var*var*var + 1.827495*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10724_p4252.root"))
        return -0.045119*var*var*var*var*var + 0.473558*var*var*var*var + -1.705528*var*var*var + 2.221817*var*var + 0.000000*var + 0.000000;
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r9364_p4252.root"))
        return -0.069938*var*var*var*var*var + 0.648670*var*var*var*var + -2.106254*var*var*var + 2.516543*var*var + 0.000000*var + 0.000000;
/*
    if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10201_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.38808;
        else if (var >= 30 && var < 40) return 1.27817;
        else if (var >= 40 && var < 50) return 1.28656;
        else if (var >= 50 && var < 60) return 1.27311;
        else if (var >= 60 && var < 70) return 1.22464;
        else if (var >= 70 && var < 80) return 1.19742;
        else if (var >= 80 && var < 90) return 1.22009;
        else if (var >= 90 && var < 110) return 1.32899;
        else if (var >= 110 && var < 120) return 1.21741;
        else if (var >= 120 && var < 140) return 1.16826;
        else if (var >= 140 && var < 160) return 1.20900;
        else if (var >= 160 && var < 200) return 1.20642;
        else if (var >= 200 && var < 1000) return 1.42227;
    
    }
    else if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r10724_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.44048;
        else if (var >= 30 && var < 40) return 1.27757;
        else if (var >= 40 && var < 50) return 1.24605;
        else if (var >= 50 && var < 60) return 1.24027;
        else if (var >= 60 && var < 70) return 1.23134;
        else if (var >= 70 && var < 80) return 1.20398;
        else if (var >= 80 && var < 90) return 1.16391;
        else if (var >= 90 && var < 110) return 1.17390;
        else if (var >= 110 && var < 120) return 1.19186;
        else if (var >= 120 && var < 140) return 1.22377;
        else if (var >= 140 && var < 160) return 1.14043;
        else if (var >= 160 && var < 200) return 0.97015;
        else if (var >= 200 && var < 1000) return 1.41945;
    
    }
    else if(filename.Contains("345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_STDM3.e6240_s3126_r9364_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.38962;
        else if (var >= 30 && var < 40) return 1.26082;
        else if (var >= 40 && var < 50) return 1.25077;
        else if (var >= 50 && var < 60) return 1.24537;
        else if (var >= 60 && var < 70) return 1.22120;
        else if (var >= 70 && var < 80) return 1.29119;
        else if (var >= 80 && var < 90) return 1.23691;
        else if (var >= 90 && var < 110) return 1.42874;
        else if (var >= 110 && var < 120) return 1.37575;
        else if (var >= 120 && var < 140) return 1.36988;
        else if (var >= 140 && var < 160) return 1.10146;
        else if (var >= 160 && var < 200) return 1.25695;
        else if (var >= 200 && var < 1000) return 1.20009;
    
    }
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10201_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.45646;
        else if (var >= 30 && var < 40) return 1.28833;
        else if (var >= 40 && var < 50) return 1.26606;
        else if (var >= 50 && var < 60) return 1.24302;
        else if (var >= 60 && var < 70) return 1.21739;
        else if (var >= 70 && var < 80) return 1.20649;
        else if (var >= 80 && var < 90) return 1.19893;
        else if (var >= 90 && var < 110) return 1.19644;
        else if (var >= 110 && var < 120) return 1.20175;
        else if (var >= 120 && var < 140) return 1.19621;
        else if (var >= 140 && var < 160) return 1.20283;
        else if (var >= 160 && var < 200) return 1.20056;
        else if (var >= 200 && var < 1000) return 1.24802;
    
    }
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r10724_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.46176;
        else if (var >= 30 && var < 40) return 1.29006;
        else if (var >= 40 && var < 50) return 1.26626;
        else if (var >= 50 && var < 60) return 1.24333;
        else if (var >= 60 && var < 70) return 1.22083;
        else if (var >= 70 && var < 80) return 1.20451;
        else if (var >= 80 && var < 90) return 1.20065;
        else if (var >= 90 && var < 110) return 1.19945;
        else if (var >= 110 && var < 120) return 1.20426;
        else if (var >= 120 && var < 140) return 1.20461;
        else if (var >= 140 && var < 160) return 1.20908;
        else if (var >= 160 && var < 200) return 1.21002;
        else if (var >= 200 && var < 1000) return 1.25359;
    
    }
    else if(filename.Contains("345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_STDM3.e6213_s3126_r9364_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.41710;
        else if (var >= 30 && var < 40) return 1.28144;
        else if (var >= 40 && var < 50) return 1.26153;
        else if (var >= 50 && var < 60) return 1.23951;
        else if (var >= 60 && var < 70) return 1.22690;
        else if (var >= 70 && var < 80) return 1.21328;
        else if (var >= 80 && var < 90) return 1.20702;
        else if (var >= 90 && var < 110) return 1.21416;
        else if (var >= 110 && var < 120) return 1.22070;
        else if (var >= 120 && var < 140) return 1.22493;
        else if (var >= 140 && var < 160) return 1.18052;
        else if (var >= 160 && var < 200) return 1.24052;
        else if (var >= 200 && var < 1000) return 1.27945;
    
    }
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10201_r10210_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.47936;
        else if (var >= 30 && var < 40) return 1.31780;
        else if (var >= 40 && var < 50) return 1.26255;
        else if (var >= 50 && var < 60) return 1.25787;
        else if (var >= 60 && var < 70) return 1.22585;
        else if (var >= 70 && var < 80) return 1.21314;
        else if (var >= 80 && var < 90) return 1.17739;
        else if (var >= 90 && var < 110) return 1.20004;
        else if (var >= 110 && var < 120) return 1.23714;
        else if (var >= 120 && var < 140) return 1.23127;
        else if (var >= 140 && var < 160) return 1.21529;
        else if (var >= 160 && var < 200) return 1.25558;
        else if (var >= 200 && var < 1000) return 1.20032;
    
    }
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_e5984_s3126_r10724_r10726_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.43588;
        else if (var >= 30 && var < 40) return 1.27086;
        else if (var >= 40 && var < 50) return 1.28239;
        else if (var >= 50 && var < 60) return 1.25376;
        else if (var >= 60 && var < 70) return 1.23828;
        else if (var >= 70 && var < 80) return 1.20634;
        else if (var >= 80 && var < 90) return 1.22761;
        else if (var >= 90 && var < 110) return 1.22475;
        else if (var >= 110 && var < 120) return 1.14944;
        else if (var >= 120 && var < 140) return 1.16280;
        else if (var >= 140 && var < 160) return 1.26260;
        else if (var >= 160 && var < 200) return 1.22355;
        else if (var >= 200 && var < 1000) return 1.30224;
    
    }
    else if(filename.Contains("361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_STDM3.e4475_s3126_r9364_r9315_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.41633;
        else if (var >= 30 && var < 40) return 1.27736;
        else if (var >= 40 && var < 50) return 1.27010;
        else if (var >= 50 && var < 60) return 1.26883;
        else if (var >= 60 && var < 70) return 1.24309;
        else if (var >= 70 && var < 80) return 1.25662;
        else if (var >= 80 && var < 90) return 1.22120;
        else if (var >= 90 && var < 110) return 1.24811;
        else if (var >= 110 && var < 120) return 1.22333;
        else if (var >= 120 && var < 140) return 1.19275;
        else if (var >= 140 && var < 160) return 1.27256;
        else if (var >= 160 && var < 200) return 1.19215;
        else if (var >= 200 && var < 1000) return 1.34772;
    
    }
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10201_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.53204;
        else if (var >= 30 && var < 40) return 1.36479;
        else if (var >= 40 && var < 50) return 1.36134;
        else if (var >= 50 && var < 60) return 1.30645;
        else if (var >= 60 && var < 70) return 1.29992;
        else if (var >= 70 && var < 80) return 1.27740;
        else if (var >= 80 && var < 90) return 1.26647;
        else if (var >= 90 && var < 110) return 1.21976;
        else if (var >= 110 && var < 120) return 1.30620;
        else if (var >= 120 && var < 140) return 1.28862;
        else if (var >= 140 && var < 160) return 1.23743;
        else if (var >= 160 && var < 200) return 1.22550;
        else if (var >= 200 && var < 1000) return 1.48428;
    
    }
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10724_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.52846;
        else if (var >= 30 && var < 40) return 1.40720;
        else if (var >= 40 && var < 50) return 1.35255;
        else if (var >= 50 && var < 60) return 1.30086;
        else if (var >= 60 && var < 70) return 1.28544;
        else if (var >= 70 && var < 80) return 1.25361;
        else if (var >= 80 && var < 90) return 1.34443;
        else if (var >= 90 && var < 110) return 1.26559;
        else if (var >= 110 && var < 120) return 1.21710;
        else if (var >= 120 && var < 140) return 1.23109;
        else if (var >= 140 && var < 160) return 1.30350;
        else if (var >= 160 && var < 200) return 1.24361;
        else if (var >= 200 && var < 1000) return 1.40087;
    
    }
    else if(filename.Contains("363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r9364_p4252.root"))
    {
        if (var >= 0 && var < 30) return 1.61250;
        else if (var >= 30 && var < 40) return 1.32012;
        else if (var >= 40 && var < 50) return 1.33315;
        else if (var >= 50 && var < 60) return 1.32357;
        else if (var >= 60 && var < 70) return 1.27026;
        else if (var >= 70 && var < 80) return 1.30291;
        else if (var >= 80 && var < 90) return 1.29848;
        else if (var >= 90 && var < 110) return 1.24531;
        else if (var >= 110 && var < 120) return 1.14503;
        else if (var >= 120 && var < 140) return 1.24793;
        else if (var >= 140 && var < 160) return 1.25762;
        else if (var >= 160 && var < 200) return 1.30705;
        else if (var >= 200 && var < 1000) return 1.28282;
    }
*/
    return 1.;
}

#endif // PRESEL_FACTOR_H


