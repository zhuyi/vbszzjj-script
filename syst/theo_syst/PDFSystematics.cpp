#include "inc/hist_maker.h"
#include "../inc/Setting.h"
#include "../inc/Util.h"
#include "inc/bin.h"

#include "TString.h"
#include "TStyle.h"
#include "TH1D.h"
#include "TCanvas.h"

#include <vector>
#include <iostream>
#include <string>
#include <tuple>
#include <functional>
#include <algorithm>

std::vector<std::string> pdf_variations = {
                                           "weight_var_th_MUR1_MUF1_PDF261001",
                                           "weight_var_th_MUR1_MUF1_PDF261002",
                                           "weight_var_th_MUR1_MUF1_PDF261003",
                                           "weight_var_th_MUR1_MUF1_PDF261004",
                                           "weight_var_th_MUR1_MUF1_PDF261005",
                                           "weight_var_th_MUR1_MUF1_PDF261006",
                                           "weight_var_th_MUR1_MUF1_PDF261007",
                                           "weight_var_th_MUR1_MUF1_PDF261008",
                                           "weight_var_th_MUR1_MUF1_PDF261009",
                                           "weight_var_th_MUR1_MUF1_PDF261010",
                                           "weight_var_th_MUR1_MUF1_PDF261011",
                                           "weight_var_th_MUR1_MUF1_PDF261012",
                                           "weight_var_th_MUR1_MUF1_PDF261013",
                                           "weight_var_th_MUR1_MUF1_PDF261014",
                                           "weight_var_th_MUR1_MUF1_PDF261015",
                                           "weight_var_th_MUR1_MUF1_PDF261016",
                                           "weight_var_th_MUR1_MUF1_PDF261017",
                                           "weight_var_th_MUR1_MUF1_PDF261018",
                                           "weight_var_th_MUR1_MUF1_PDF261019",
                                           "weight_var_th_MUR1_MUF1_PDF261020",
                                           "weight_var_th_MUR1_MUF1_PDF261021",
                                           "weight_var_th_MUR1_MUF1_PDF261022",
                                           "weight_var_th_MUR1_MUF1_PDF261023",
                                           "weight_var_th_MUR1_MUF1_PDF261024",
                                           "weight_var_th_MUR1_MUF1_PDF261025",
                                           "weight_var_th_MUR1_MUF1_PDF261026",
                                           "weight_var_th_MUR1_MUF1_PDF261027",
                                           "weight_var_th_MUR1_MUF1_PDF261028",
                                           "weight_var_th_MUR1_MUF1_PDF261029",
                                           "weight_var_th_MUR1_MUF1_PDF261030",
                                           "weight_var_th_MUR1_MUF1_PDF261031",
                                           "weight_var_th_MUR1_MUF1_PDF261032",
                                           "weight_var_th_MUR1_MUF1_PDF261033",
                                           "weight_var_th_MUR1_MUF1_PDF261034",
                                           "weight_var_th_MUR1_MUF1_PDF261035",
                                           "weight_var_th_MUR1_MUF1_PDF261036",
                                           "weight_var_th_MUR1_MUF1_PDF261037",
                                           "weight_var_th_MUR1_MUF1_PDF261038",
                                           "weight_var_th_MUR1_MUF1_PDF261039",
                                           "weight_var_th_MUR1_MUF1_PDF261040",
                                           "weight_var_th_MUR1_MUF1_PDF261041",
                                           "weight_var_th_MUR1_MUF1_PDF261042",
                                           "weight_var_th_MUR1_MUF1_PDF261043",
                                           "weight_var_th_MUR1_MUF1_PDF261044",
                                           "weight_var_th_MUR1_MUF1_PDF261045",
                                           "weight_var_th_MUR1_MUF1_PDF261046",
                                           "weight_var_th_MUR1_MUF1_PDF261047",
                                           "weight_var_th_MUR1_MUF1_PDF261048",
                                           "weight_var_th_MUR1_MUF1_PDF261049",
                                           "weight_var_th_MUR1_MUF1_PDF261050",
                                           "weight_var_th_MUR1_MUF1_PDF261051",
                                           "weight_var_th_MUR1_MUF1_PDF261052",
                                           "weight_var_th_MUR1_MUF1_PDF261053",
                                           "weight_var_th_MUR1_MUF1_PDF261054",
                                           "weight_var_th_MUR1_MUF1_PDF261055",
                                           "weight_var_th_MUR1_MUF1_PDF261056",
                                           "weight_var_th_MUR1_MUF1_PDF261057",
                                           "weight_var_th_MUR1_MUF1_PDF261058",
                                           "weight_var_th_MUR1_MUF1_PDF261059",
                                           "weight_var_th_MUR1_MUF1_PDF261060",
                                           "weight_var_th_MUR1_MUF1_PDF261061",
                                           "weight_var_th_MUR1_MUF1_PDF261062",
                                           "weight_var_th_MUR1_MUF1_PDF261063",
                                           "weight_var_th_MUR1_MUF1_PDF261064",
                                           "weight_var_th_MUR1_MUF1_PDF261065",
                                           "weight_var_th_MUR1_MUF1_PDF261066",
                                           "weight_var_th_MUR1_MUF1_PDF261067",
                                           "weight_var_th_MUR1_MUF1_PDF261068",
                                           "weight_var_th_MUR1_MUF1_PDF261069",
                                           "weight_var_th_MUR1_MUF1_PDF261070",
                                           "weight_var_th_MUR1_MUF1_PDF261071",
                                           "weight_var_th_MUR1_MUF1_PDF261072",
                                           "weight_var_th_MUR1_MUF1_PDF261073",
                                           "weight_var_th_MUR1_MUF1_PDF261074",
                                           "weight_var_th_MUR1_MUF1_PDF261075",
                                           "weight_var_th_MUR1_MUF1_PDF261076",
                                           "weight_var_th_MUR1_MUF1_PDF261077",
                                           "weight_var_th_MUR1_MUF1_PDF261078",
                                           "weight_var_th_MUR1_MUF1_PDF261079",
                                           "weight_var_th_MUR1_MUF1_PDF261080",
                                           "weight_var_th_MUR1_MUF1_PDF261081",
                                           "weight_var_th_MUR1_MUF1_PDF261082",
                                           "weight_var_th_MUR1_MUF1_PDF261083",
                                           "weight_var_th_MUR1_MUF1_PDF261084",
                                           "weight_var_th_MUR1_MUF1_PDF261085",
                                           "weight_var_th_MUR1_MUF1_PDF261086",
                                           "weight_var_th_MUR1_MUF1_PDF261087",
                                           "weight_var_th_MUR1_MUF1_PDF261088",
                                           "weight_var_th_MUR1_MUF1_PDF261089",
                                           "weight_var_th_MUR1_MUF1_PDF261090",
                                           "weight_var_th_MUR1_MUF1_PDF261091",
                                           "weight_var_th_MUR1_MUF1_PDF261092",
                                           "weight_var_th_MUR1_MUF1_PDF261093",
                                           "weight_var_th_MUR1_MUF1_PDF261094",
                                           "weight_var_th_MUR1_MUF1_PDF261095",
                                           "weight_var_th_MUR1_MUF1_PDF261096",
                                           "weight_var_th_MUR1_MUF1_PDF261097",
                                           "weight_var_th_MUR1_MUF1_PDF261098",
                                           "weight_var_th_MUR1_MUF1_PDF261099",
                                           "weight_var_th_MUR1_MUF1_PDF261100"
                                          };

std::tuple<double, double> get_pdf_systematics(const double &content, const std::vector<double> &variations)
{
    double variation_std = 0.;
    for(const auto &variation : variations) variation_std += (variation - content)*(variation - content);
    return { std::sqrt(variation_std/variations.size())/content,
            -std::sqrt(variation_std/variations.size())/content};
}

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    Bin::connect(get_pdf_systematics);

    std::map<TString, std::vector<std::string>> leaves = {{"inclusive",
                                                           {"n_jets",
                                                            "dphill",
                                                            "leading_pT_lepton",
                                                            "mT_ZZ",
                                                            "pt_zz",
                                                            "Z_pT",
                                                            "Z_rapidity",
                                                            "CP_ZZ_1",
                                                            "CP_ZZ_2",}
                                                           },
                                                          {"ZZjj",
                                                           {"dphill",
                                                            "leading_jet_pt",
                                                            "mT_ZZ",
                                                            "mjj",
                                                            "pt_zz",
                                                            "Z_pT",
                                                            "Z_rapidity",}
                                                          }
                                                         };

    TString measurement = "inclusive";
    TString channel = "inclusive";
    auto node_recon = GetInitNode(measurement, channel);
    auto node_truth = GetInitNode(measurement, channel, true);
    std::cout << *node_recon.Count() << std::endl;
    std::cout << *node_truth.Count() << std::endl;

    auto c = new TCanvas("c", "", 800., 600.);

    for(const auto &leaf : leaves.at(measurement))
    {
        std::cout << leaf << std::endl;

        auto [hist_nomimal_truth, hists_variation_truth] = get_systematic_hists(node_truth, measurement, channel, leaf, pdf_variations, true);
        auto bins = get_bins(hist_nomimal_truth, hists_variation_truth);
        auto [up_uncertainties, down_uncertainties] = get_uncertainties(bins);

        std::cout << "- [\"pdf__1up\",   " << get_formatted(up_uncertainties) << "]" << std::endl;
        std::cout << "- [\"pdf__1down\", " << get_formatted(down_uncertainties) << "]" << std::endl;

        std::cout << std::endl;
    }
}
