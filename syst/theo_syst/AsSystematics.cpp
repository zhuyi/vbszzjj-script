#include "inc/hist_maker.h"
#include "../inc/Setting.h"
#include "../inc/Util.h"
#include "inc/bin.h"

#include "TString.h"
#include "TStyle.h"
#include "TH1D.h"
#include "TCanvas.h"

#include <vector>
#include <iostream>
#include <string>
#include <tuple>
#include <functional>
#include <algorithm>
#include <map>

std::vector<std::string> as_variations = {
                                          "weight_var_th_MUR1_MUF1_PDF270000",
                                          "weight_var_th_MUR1_MUF1_PDF269000",
                                         };

std::tuple<double, double> get_as_systematics(const double &content, const std::vector<double> &variations)
{
    auto max = *std::max_element(variations.begin(), variations.end());
    auto min = *std::min_element(variations.begin(), variations.end());
    return {0.5*(max - min)/content, -0.5*(max - min)/content};
}

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    Bin::connect(get_as_systematics);

    std::map<TString, std::vector<std::string>> leaves = {{"inclusive",
                                                           {"n_jets",
                                                            "dphill",
                                                            "leading_pT_lepton",
                                                            "mT_ZZ",
                                                            "pt_zz",
                                                            "Z_pT",
                                                            "Z_rapidity",
                                                            "CP_ZZ_1",
                                                            "CP_ZZ_2",}
                                                           },
                                                          {"ZZjj",
                                                           {"dphill",
                                                            "leading_jet_pt",
                                                            "mT_ZZ",
                                                            "mjj",
                                                            "pt_zz",
                                                            "Z_pT",
                                                            "Z_rapidity",}
                                                          }
                                                         }; 

    TString measurement = "inclusive";
    TString channel = "QCDZZ";
    auto node_recon = GetInitNode(measurement, channel);
    auto node_truth = GetInitNode(measurement, channel, true);
    std::cout << *node_recon.Count() << std::endl;
    std::cout << *node_truth.Count() << std::endl;

    auto c = new TCanvas("c", "", 800., 600.);

    for(const auto &leaf : leaves.at(measurement))
    {
        std::cout << leaf << std::endl;

        auto [hist_nomimal_truth, hists_variation_truth] = get_systematic_hists(node_truth, measurement, channel, leaf, as_variations, true);
        auto bins = get_bins(hist_nomimal_truth, hists_variation_truth);
        auto [up_uncertainties, down_uncertainties] = get_uncertainties(bins);

        std::cout << "- [\"alphas__1up\",   " << get_formatted(up_uncertainties) << "]" << std::endl;
        std::cout << "- [\"alphas__1down\", " << get_formatted(down_uncertainties) << "]" << std::endl;

        std::cout << std::endl;
    }
}
