#ifndef RNODE_H
#define RNODE_H

#include "../../inc/MC.h"
#include "../../inc/Setting.h"

#include "ROOT/RDataFrame.hxx"
#include "TString.h"
#include "TH1D.h"

#include <vector>
#include <tuple>
#include <string>

using RNode = ROOT::RDF::RNode;

RNode GetInitNode(const TString &measurement, const TString &channel, bool if_truth = false)
{
    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path_fiducial + file).Data());

    std::string pre_selection = if_truth ? settings.at(measurement).truth_preselection.GetTitle()
                                         : settings.at(measurement).pre_selection.GetTitle();
    std::string selection     = if_truth ? settings.at(measurement).FR.GetTitle()
                                         : settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Filter(pre_selection).Filter(selection);
}

std::tuple<TH1D*, std::vector<TH1D*>> get_systematic_hists(RNode &node, const TString &measurement, const TString &channel, std::string leaf,
                                                           const std::vector<std::string> &variations, bool if_truth = false)
{
    int n_bins = settings.at(measurement).get_binning(leaf).size();
    double *xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(leaf).at(i);

    if(if_truth) leaf = "truth_" + leaf;
    TString leaf_to_draw("abs_" + leaf);

    bool if_sign = leaf.find("CP_ZZ") != std::string::npos;

    std::string corrections = "scale*ew_correction*tautau_correction";
    auto node_to_draw = node.Define(leaf_to_draw.Data(), if_sign ? leaf : "std::abs(" + leaf + ")");

    auto nominal_final_weight = corrections + (if_truth ? "*weight_gen" : "*weight");
    auto hist_nominal = new TH1D(*node_to_draw.Define("nominal_final_weight", nominal_final_weight)
                                              .Histo1D({"nominal", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "nominal_final_weight"));

    std::vector<TH1D*> hists_variation;
    hists_variation.reserve(variations.size());
    for(const auto &variation : variations)
    {
        auto variation_final_weight = corrections + (if_truth ? "*" + variation : "*weight/weight_gen*" + variation);
        auto hist_variation = new TH1D(*node_to_draw.Define("variation_final_weight", variation_final_weight)
                                                    .Histo1D({variation.data(), "", n_bins - 1, xbins}, leaf_to_draw.Data(), "variation_final_weight"));
        hists_variation.emplace_back(std::move(hist_variation));
    }

//    hist_nominal->Print("range");

    return {hist_nominal, hists_variation};
}

#endif // RNODE_H
