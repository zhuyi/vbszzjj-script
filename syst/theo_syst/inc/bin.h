#ifndef BIN_H
#define BIN_H

#include "../../inc/Util.h"

#include <vector>
#include <functional>
#include <tuple>
#include <limits>
#include <iostream>

#include "TH1D.h"
#include "TString.h"

class Bin
{
private:
    using uncertainty_function_t = std::function<std::tuple<double, double>(const double &, const std::vector<double> &)>;

public:
    Bin() = default;
    Bin(const double &content, const std::vector<double> &variations) : content_(content),
                                                                        variations_(variations)
    {}
    Bin(double &&content, std::vector<double> &&variations) : content_(std::move(content)),
                                                              variations_(std::move(variations))
    {}
    Bin(const Bin &) = default;
    Bin(Bin &&) = default;
    ~Bin() = default;

    static void connect(uncertainty_function_t uncertainty_function)
    {
        uncertainty_function_ = uncertainty_function;
    }

    double content() const {return content_;};
    std::vector<double> variations() const {return variations_;}
    std::tuple<double, double> uncertainties() const
    {
        if(!uncertainty_function_) return {-std::numeric_limits<double>::infinity(),
                                           -std::numeric_limits<double>::infinity()};

        return uncertainty_function_(content_, variations_);
    }

    friend std::ostream& operator<<(std::ostream& os, const Bin& bin)
    {
        os << "content: " << bin.content_ << "\nvariations: " << std::endl;
        for(const auto &variation : bin.variations_) os << variation << "\t";
        os << std::endl;

        return os;
    }

private:
    inline static uncertainty_function_t uncertainty_function_{nullptr};

    double content_{0.};
    std::vector<double> variations_;
};

std::tuple<TH1D*, std::vector<TH1D*>> get_C_hists(const TH1D *hist_nominal_recon, const std::vector<TH1D*> &hists_variation_recon,
                                                  const TH1D *hist_nominal_truth, const std::vector<TH1D*> &hists_variation_truth)
{
    auto hist_nominal_C = new TH1D(*hist_nominal_recon);
    hist_nominal_C->Divide(hist_nominal_truth);

    std::vector<TH1D*> hists_variation_C;
    hists_variation_C.reserve(hists_variation_recon.size());
    for(auto &&[hist_variation_recon, hist_variation_truth] : make_container_ref_tuple(hists_variation_recon, hists_variation_truth))
    {
        auto hist_variation_C = new TH1D(*hist_variation_recon);
        hist_variation_C->Divide(hist_variation_truth);
        hists_variation_C.emplace_back(std::move(hist_variation_C));
    }

    return {hist_nominal_C, hists_variation_C};
}

std::vector<Bin> get_bins(const TH1D *hist_nominal, const std::vector<TH1D*> &hists_vatiation)
{
    std::vector<Bin> bins;
    const auto n_bins = hist_nominal->GetNbinsX();
    bins.reserve(n_bins);

    for(int i = 0; i < n_bins; i++)
    {
        double content = hist_nominal->GetBinContent(i + 1);
        std::vector<double> variations;
        variations.reserve(hists_vatiation.size());

        for(const auto &hist_variation : hists_vatiation)
            variations.push_back(hist_variation->GetBinContent(i + 1));

        bins.emplace_back(std::move(content), std::move(variations));
    }

    return bins;
}

std::tuple<std::vector<double>, std::vector<double>> get_uncertainties(const std::vector<Bin> &bins)
{
    auto n_bins = bins.size();
    std::vector<double> up_uncertainties;
    std::vector<double> down_uncertainties;
    up_uncertainties.reserve(n_bins);
    down_uncertainties.reserve(n_bins);

    for(const auto &bin : bins)
    {
        auto [up, down] = bin.uncertainties();
        up_uncertainties.push_back(up);
        down_uncertainties.push_back(down);
    }

    return {up_uncertainties, down_uncertainties};
}

TString get_formatted(const std::vector<double> &numbers)
{
    TString formatted;

    for(const auto &number : numbers)
        formatted += TString::Format(number >= 0. ? "+%.3f, " : "%.3f, ", number);

    return formatted;
}

#endif // BIN_H
