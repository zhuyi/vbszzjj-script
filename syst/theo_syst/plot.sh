#!/bin/bash

#source /home/zhuyifan/DarkShine/dp.env
#source /cvmfs/sft.cern.ch/lcg/views/LCG_95/x86_64-centos7-gcc8-opt/setup.sh

if [[ $1 = "pdf" ]]; then
    g++ -o bin/pdf PDFSystematics.cpp `root-config --cflags --libs`
    bin/pdf $2
elif [[ $1 = "scale" ]]; then
    g++ -o bin/scale ScaleSystematics.cpp `root-config --cflags --libs`
    bin/scale $2
elif [[ $1 = "as" ]]; then
    g++ -o bin/as AsSystematics.cpp `root-config --cflags --libs`
    bin/as $2
fi
