#ifndef RNODE_H
#define RNODE_H

#include "../../inc/MC.h"
#include "../../inc/Setting.h"

#include "ROOT/RDataFrame.hxx"
#include "TString.h"
#include "TH1D.h"

#include <vector>
#include <tuple>
#include <string>

using RNode = ROOT::RDF::RNode;

RNode GetInitNode(const TString &measurement, const TString &channel, const TString &variation, bool if_truth = false)
{
    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path + "/" + variation + "/" + file).Data());

    std::string pre_selection = if_truth ? settings.at(measurement).truth_preselection.GetTitle()
                                         : settings.at(measurement).pre_selection.GetTitle();
    std::string selection     = if_truth ? settings.at(measurement).FR.GetTitle()
                                         : settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Filter(pre_selection).Filter(selection);
}

TH1D* get_hist(RNode &node, const TString &measurement, const TString &channel, std::string leaf, bool if_truth = false)
{
    if(leaf == "weight")
    {
        auto hist = new TH1D(*node.Histo1D({"hist", "", 100, -20., 180.}, "weight"));
        return hist;
    }

    int n_bins = settings.at(measurement).get_binning(leaf).size();
    double *xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(leaf).at(i);

    if(if_truth) leaf = "truth_" + leaf;
    TString leaf_to_draw(leaf);

    std::string corrections = "scale*ew_correction*tautau_correction";

    auto final_weight = corrections + (if_truth ? "*weight_gen" : "*weight");
    auto hist = new TH1D(*node.Define("final_weight", final_weight)
                              .Histo1D({"hist", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "final_weight"));

    hist->Print("range");

    return hist;
}

#endif // RNODE_H
