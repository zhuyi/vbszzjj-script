#include "inc/hist_maker.h"
#include "../inc/Setting.h"
#include "../inc/Util.h"

#include "TString.h"
#include "TStyle.h"
#include "TH1D.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TPaveText.h"

#include <vector>
#include <iostream>
#include <string>
#include <tuple>
#include <functional>
#include <algorithm>
#include <map>

const double upper_pad_height = 0.75;
const double lower_pad_height = 1. - upper_pad_height;

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    std::map<TString, std::vector<std::string>> leaves = {{"inclusive",
                                                           {
                                                            //"weight",
                                                            "n_jets",
                                                            //"dphill",
                                                            //"leading_pT_lepton",
                                                            //"mT_ZZ",
                                                            //"pt_zz",
                                                            //"Z_pT",
                                                            //"Z_rapidity",
                                                            //"CP_ZZ_1",
                                                            //"CP_ZZ_2",
                                                            }
                                                           },
                                                          {"ZZjj",
                                                           {"dphill",
                                                            "leading_jet_pt",
                                                            "mT_ZZ",
                                                            "mjj",
                                                            "pt_zz",
                                                            "Z_pT",
                                                            "Z_rapidity",
                                                           }
                                                          }
                                                         }; 

   
    TString variation = "JET_Flavor_Composition__1"; 
    TString variation_up   = variation + "up";
    TString variation_down = variation + "down";
    TString measurement = "inclusive";
    TString channel = "top";
    auto node_nominal = GetInitNode(measurement, channel, "PFLOW");
    auto node_up      = GetInitNode(measurement, channel, variation_up);
    auto node_down    = GetInitNode(measurement, channel, variation_down);
    std::cout << *node_nominal.Count() << std::endl;
    std::cout << *node_up     .Count() << std::endl;
    std::cout << *node_down   .Count() << std::endl;

    auto c = new TCanvas("c", "", 800., 800.);
//    c->SetLogy();

    for(const auto &leaf : leaves.at(measurement))
    {
        std::cout << leaf << std::endl;

        TPad *upper_pad = new TPad("upper_pad", "", 0., lower_pad_height, 1., 1.);
        upper_pad->Draw();
        upper_pad->SetLogy();
        upper_pad->SetBottomMargin(0.);
        upper_pad->cd();

        auto hist_nominal = get_hist(node_nominal, measurement, channel, leaf);
        auto hist_up      = get_hist(node_up,      measurement, channel, leaf);
        auto hist_down    = get_hist(node_down,    measurement, channel, leaf);

        hist_nominal->Draw("hist");
        hist_up->Draw("hsit same");
        hist_down->Draw("hsit same");

        hist_nominal->GetYaxis()->SetTitle("events/bin");
        hist_nominal->GetYaxis()->SetTitleSize(0.04);
        hist_nominal->GetYaxis()->SetTitleOffset(1.);
        hist_nominal->SetLineColor(kBlack);
        hist_up     ->SetLineColor(kRed + 2);
        hist_down   ->SetLineColor(kBlue + 2);
        hist_nominal->SetLineWidth(2);
        hist_up     ->SetLineWidth(2);
        hist_down   ->SetLineWidth(2);

        c->cd();

        TPad *lower_pad = new TPad("lower_pad", "", 0., 0., 1., lower_pad_height);
        lower_pad->Draw();
        lower_pad->SetTopMargin(0.);
        lower_pad->SetBottomMargin(0.30);
        lower_pad->cd();

        auto hist_nominal_ratio = new TH1D(*hist_nominal);
        auto hist_up_ratio = new TH1D(*hist_up);
        auto hist_down_ratio = new TH1D(*hist_down);
        hist_nominal_ratio->Divide(hist_nominal);
        hist_up_ratio->Divide(hist_nominal);
        hist_down_ratio->Divide(hist_nominal);

        hist_nominal_ratio->GetXaxis()->SetLabelSize(hist_nominal_ratio->GetXaxis()->GetLabelSize()*upper_pad_height/lower_pad_height);
        hist_nominal_ratio->GetYaxis()->SetLabelSize(hist_nominal_ratio->GetYaxis()->GetLabelSize()*upper_pad_height/lower_pad_height);
        hist_nominal_ratio->GetXaxis()->SetTitle("jet multiplicity");
        hist_nominal_ratio->GetYaxis()->SetTitle("variation/nominal");
        hist_nominal_ratio->GetXaxis()->SetTitleSize(0.12);
        hist_nominal_ratio->GetYaxis()->SetTitleSize(0.10);
        hist_nominal_ratio->GetYaxis()->SetTitleOffset(0.45);
        hist_nominal_ratio->GetYaxis()->SetRangeUser(0.5, 1.5);
        hist_nominal_ratio->GetYaxis()->SetNdivisions(5, 5, 0);
        hist_nominal_ratio->Draw("e1");
        hist_up_ratio->Draw("e1 same");
        hist_down_ratio->Draw("e1 same");

        c->cd();

        auto legend = new TLegend(0.65, 0.70, 0.90, 0.80);
        legend->AddEntry(hist_nominal, "nominal", "L");
        legend->AddEntry(hist_up,      "up",      "L");
        legend->AddEntry(hist_down,    "down",    "L");
        legend->SetTextSize(0.03);
        legend->SetBorderSize(0);
        legend->Draw();

        auto text = new TPaveText(0.65, 0.81, 0.90, 0.85, "brNDC");
        text->SetFillColor(0);
        text->SetFillStyle(0);
        text->SetBorderSize(0);
        text->SetTextAlign(12);
        text->SetTextSize(0.03);
        text->AddText(channel);
        text->Draw();

        TString name = channel + "_" + variation + "_" + measurement;
        c->SaveAs("fig/" + name + ".png");

        std::cout << std::endl;
    }
}
