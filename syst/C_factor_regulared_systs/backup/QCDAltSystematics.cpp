#include "inc/Data.h"
#include "inc/MC.h"
#include "inc/Setting.h"
#include "inc/Util.h"

#include "TString.h"
#include "TColor.h"
#include "TStyle.h"
#include "TH1D.h"
#include "ROOT/RDataFrame.hxx"
#include "TCanvas.h"

#include <vector>
#include <map>
#include <iostream>
#include <string>

using RNode = ROOT::RDF::RNode;

double* GetBinning(const TString &leaf, int &n_bins)
{
    if     (leaf == "Z_pT")     {n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);         return x_z_pt;    }
    else if(leaf == "dLepR")    {n_bins = sizeof(x_dlepr)/sizeof(x_dlepr[0]);       return x_dlepr;   }
    else if(leaf == "met_tst")  {n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);   return x_met_tst; }
    else if(leaf == "dMetZPhi") {n_bins = sizeof(x_dmetzphi)/sizeof(x_dmetzphi[0]); return x_dmetzphi;}
    else if(leaf == "n_jets")   {n_bins = sizeof(x_n_jets)/sizeof(x_n_jets[0]);     return x_n_jets;  }
    else if(leaf == "dphill")   {n_bins = sizeof(x_dphill)/sizeof(x_dphill[0]);     return x_dphill;  }
    else if(leaf == "mT_ZZ")    {n_bins = sizeof(x_mt_zz)/sizeof(x_mt_zz[0]);       return x_mt_zz;   }
    else if(leaf == "mjj")      {n_bins = sizeof(x_mjj)/sizeof(x_mjj[0]);           return x_mjj;     }
    else if(leaf == "isMC")     {n_bins = sizeof(x_ismc)/sizeof(x_ismc[0]);         return x_ismc;    }
    return 0;
}

RNode GetInitNode(const TString &region, const TString &channel, bool if_truth = false)
{
    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path_fiducial + file).Data());

    TString region_selections = (if_truth ? truth_regions.at(region).GetTitle()
                                          : regions.at(region).GetTitle());

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Filter(region_selections.Data());
}

void QCDAltSystematics(const TString &region, const TString &channel, const TString &leaf, int event_type)
{
    int n_bins = 0;
    auto xbins = GetBinning(leaf, n_bins);

    TString leaf_to_draw(leaf);
    if(leaf == "dphill") leaf_to_draw = "std::abs(dphill)";    

    std::cout << channel << std::endl;
    auto nominal_reco = GetInitNode(region, "qqZZ");
    std::cout << *nominal_reco.Count() << std::endl;
    auto nominal_truth = GetInitNode(region, "qqZZ",  true);
    std::cout << *nominal_truth.Count() << std::endl;

    auto alternt_reco = GetInitNode(region, channel);
    std::cout << *alternt_reco.Count() << std::endl;
    auto alternt_truth = GetInitNode(region, channel, true);
    std::cout << *alternt_truth.Count() << std::endl;

    TString corrections = "scale*ew_correction*tautau_correction";
    TString final_reco_weight = "weight*" + corrections;
    auto hist_nominal_reco = nominal_reco.Define("final_reco_weight", final_reco_weight.Data()).Histo1D({"nominal", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "final_reco_weight");
    auto hist_alternt_reco = alternt_reco.Define("final_reco_weight", final_reco_weight.Data()).Histo1D({"alternt", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "final_reco_weight");

    TString final_truth_weight = "weight_gen*" + corrections;
    auto hist_nominal_truth = nominal_truth.Define("final_truth_weight", final_truth_weight.Data()).Histo1D({"nominal", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "final_truth_weight");
    auto hist_alternt_truth = alternt_truth.Define("final_truth_weight", final_truth_weight.Data()).Histo1D({"alternt", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "final_truth_weight");

    double nominal_C_factor = hist_nominal_reco->GetSumOfWeights()/hist_nominal_truth->GetSumOfWeights();
    double alternt_C_factor = hist_alternt_reco->GetSumOfWeights()/hist_alternt_truth->GetSumOfWeights();

    std::cout << std::abs(alternt_C_factor - nominal_C_factor)/nominal_C_factor << std::endl;
}

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    QCDAltSystematics("A", "qqZZ_alt", "n_jets", 0);
}
