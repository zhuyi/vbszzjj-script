#include "inc/Data.h"
#include "inc/MC.h"
#include "inc/Setting.h"
#include "inc/Util.h"

#include "TString.h"
#include "TColor.h"
#include "TStyle.h"
#include "TH1D.h"
#include "ROOT/RDataFrame.hxx"
#include "TCanvas.h"

#include <vector>
#include <map>
#include <iostream>
#include <string>

std::vector<TString> as_variations = {"weight_gen",
                                      "weight_var_th_MUR1_MUF1_PDF270000",
                                      "weight_var_th_MUR1_MUF1_PDF269000",
                                     };
using RNode = ROOT::RDF::RNode;

double* GetBinning(const TString &leaf, int &n_bins)
{
    if     (leaf == "Z_pT")     {n_bins = sizeof(x_z_pt)/sizeof(x_z_pt[0]);         return x_z_pt;    }
    else if(leaf == "dLepR")    {n_bins = sizeof(x_dlepr)/sizeof(x_dlepr[0]);       return x_dlepr;   }
    else if(leaf == "met_tst")  {n_bins = sizeof(x_met_tst)/sizeof(x_met_tst[0]);   return x_met_tst; }
    else if(leaf == "dMetZPhi") {n_bins = sizeof(x_dmetzphi)/sizeof(x_dmetzphi[0]); return x_dmetzphi;}
    else if(leaf == "n_jets")   {n_bins = sizeof(x_n_jets)/sizeof(x_n_jets[0]);     return x_n_jets;  }
    else if(leaf == "dphill")   {n_bins = sizeof(x_dphill)/sizeof(x_dphill[0]);     return x_dphill;  }
    else if(leaf == "mT_ZZ")    {n_bins = sizeof(x_mt_zz)/sizeof(x_mt_zz[0]);       return x_mt_zz;   }
    else if(leaf == "mjj")      {n_bins = sizeof(x_mjj)/sizeof(x_mjj[0]);           return x_mjj;     }
    else if(leaf == "isMC")     {n_bins = sizeof(x_ismc)/sizeof(x_ismc[0]);         return x_ismc;    }
    return 0;
}

RNode GetInitNode(const TString &region, const TString &channel, bool if_truth = false)
{
    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path_fiducial + file).Data());

    TString pre_selections = (if_truth ? truth_preselection.GetTitle()
                                       : pre_selection.GetTitle());
    TString region_selections = (if_truth ? truth_regions.at(region).GetTitle()
                                          : regions.at(region).GetTitle());

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Filter(pre_selections.Data()).Filter(region_selections.Data());
}

void AsSystematics(const TString &region, const TString &channel, const TString &leaf, int event_type)
{
    int n_bins = 0;
    auto xbins = GetBinning(leaf, n_bins);

    TString leaf_to_draw(leaf);
    if(leaf == "dphill") leaf_to_draw = "std::abs(dphill)";

    std::cout << channel << std::endl;
    auto reco = GetInitNode(region, channel);
    std::cout << *reco.Count() << std::endl;
    auto truth = GetInitNode(region, channel, true);
    std::cout << *truth.Count() << std::endl;
    TString corrections = "scale*ew_correction*tautau_correction";

    //auto c = new TCanvas("c", "", 800, 600);

    std::vector<TH1D*> hists_reco;
    std::vector<TH1D*> hists_truth;
    for(const auto &pdf_variation : as_variations)
    {
        TString final_reco_weight = "weight/weight_gen*" + pdf_variation + "*" + corrections;
        auto hist_reco = reco.Define("final_reco_weight", final_reco_weight.Data()).Histo1D({"nominal", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "final_reco_weight");
        hists_reco.push_back(dynamic_cast<TH1D*>(hist_reco->Clone()));;

        TString final_truth_weight = pdf_variation + "*" + corrections;
        auto hist_truth = truth.Define("final_truth_weight", final_truth_weight.Data()).Histo1D({"nominal", "", n_bins - 1, xbins}, ("truth_" + leaf_to_draw).Data(), "final_truth_weight");
        hists_truth.push_back(dynamic_cast<TH1D*>(hist_truth->Clone()));
    }

    std::vector<double> nominals;
    std::vector<std::vector<double>> N_recos;
    std::vector<std::vector<double>> N_truths;
    std::vector<std::vector<double>> C_factors;
    std::vector<std::vector<double>> variations;
    for(size_t i = 0; i < hists_reco.at(0)->GetNbinsX(); i++)
    {
        N_recos .push_back(std::vector<double>());
        N_truths.push_back(std::vector<double>());
        C_factors.push_back(std::vector<double>());
        variations.push_back(std::vector<double>());
        for(int j = 0; j < hists_reco.size(); j++)
        {
            double N_reco_j  = hists_reco .at(j)->GetBinContent(i + 1);
            double N_truth_j = hists_truth.at(j)->GetBinContent(i + 1);
            double C_factor_j = N_reco_j/N_truth_j;

            N_recos .back().push_back(N_reco_j);
            N_truths.back().push_back(N_truth_j);
            C_factors.back().push_back(C_factor_j);

            if(j == 0) nominals.push_back(C_factor_j);
            else       variations.back().push_back(C_factor_j);
        }
    }

    for(auto&& [N_reco, N_truth, C_factor, variation, nominal] : make_container_ref_tuple(N_recos, N_truths, C_factors, variations, nominals))
    {
        std::sort(variation.begin(), variation.end());
        double variation_j = 0.5*(variation.back() - variation.front())/nominal;
        std::cout << variation.front() << ",\t" << variation.back() << ",\t" << variation_j << std::endl;
    }

    std::cout << std::endl;
    //c->SaveAs("test.png");
}

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    std::vector<TString> leaves = {"n_jets",
	                           //"Z_pT",
				   //"dphill",
				   //"mT_ZZ",
				   //"leading_pT_lepton",
				   //"pt_zz",
				   //"Z_rapidity"
                                   };

    for(const auto &leaf : leaves)
    {
        std::cout << leaf << std::endl;
//        AsSystematics("A", "qqZZ", leaf, 0);
//        AsSystematics("A", "ggZZ", leaf, 0);
//        AsSystematics("A", "EWKZZ", leaf, 0);
        AsSystematics("A", "QCDZZ", leaf, 0);
	std::cout << std::endl;
    }
}
