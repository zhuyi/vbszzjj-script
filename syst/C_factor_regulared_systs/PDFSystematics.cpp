#include "../inc/Data.h"
#include "../inc/MC.h"
#include "../inc/Setting.h"
#include "../inc/Util.h"

#include "TString.h"
#include "TColor.h"
#include "TStyle.h"
#include "TH1D.h"
#include "ROOT/RDataFrame.hxx"
#include "TCanvas.h"

#include <vector>
#include <map>
#include <iostream>
#include <string>

std::vector<TString> pdf_variations = {"weight_gen",
                                       "weight_var_th_MUR1_MUF1_PDF261001",
                                       "weight_var_th_MUR1_MUF1_PDF261002",
                                       "weight_var_th_MUR1_MUF1_PDF261003",
                                       "weight_var_th_MUR1_MUF1_PDF261004",
                                       "weight_var_th_MUR1_MUF1_PDF261005",
                                       "weight_var_th_MUR1_MUF1_PDF261006",
                                       "weight_var_th_MUR1_MUF1_PDF261007",
                                       "weight_var_th_MUR1_MUF1_PDF261008",
                                       "weight_var_th_MUR1_MUF1_PDF261009",
                                       "weight_var_th_MUR1_MUF1_PDF261010",
                                       "weight_var_th_MUR1_MUF1_PDF261011",
                                       "weight_var_th_MUR1_MUF1_PDF261012",
                                       "weight_var_th_MUR1_MUF1_PDF261013",
                                       "weight_var_th_MUR1_MUF1_PDF261014",
                                       "weight_var_th_MUR1_MUF1_PDF261015",
                                       "weight_var_th_MUR1_MUF1_PDF261016",
                                       "weight_var_th_MUR1_MUF1_PDF261017",
                                       "weight_var_th_MUR1_MUF1_PDF261018",
                                       "weight_var_th_MUR1_MUF1_PDF261019",
                                       "weight_var_th_MUR1_MUF1_PDF261020",
                                       "weight_var_th_MUR1_MUF1_PDF261021",
                                       "weight_var_th_MUR1_MUF1_PDF261022",
                                       "weight_var_th_MUR1_MUF1_PDF261023",
                                       "weight_var_th_MUR1_MUF1_PDF261024",
                                       "weight_var_th_MUR1_MUF1_PDF261025",
                                       "weight_var_th_MUR1_MUF1_PDF261026",
                                       "weight_var_th_MUR1_MUF1_PDF261027",
                                       "weight_var_th_MUR1_MUF1_PDF261028",
                                       "weight_var_th_MUR1_MUF1_PDF261029",
                                       "weight_var_th_MUR1_MUF1_PDF261030",
                                       "weight_var_th_MUR1_MUF1_PDF261031",
                                       "weight_var_th_MUR1_MUF1_PDF261032",
                                       "weight_var_th_MUR1_MUF1_PDF261033",
                                       "weight_var_th_MUR1_MUF1_PDF261034",
                                       "weight_var_th_MUR1_MUF1_PDF261035",
                                       "weight_var_th_MUR1_MUF1_PDF261036",
                                       "weight_var_th_MUR1_MUF1_PDF261037",
                                       "weight_var_th_MUR1_MUF1_PDF261038",
                                       "weight_var_th_MUR1_MUF1_PDF261039",
                                       "weight_var_th_MUR1_MUF1_PDF261040",
                                       "weight_var_th_MUR1_MUF1_PDF261041",
                                       "weight_var_th_MUR1_MUF1_PDF261042",
                                       "weight_var_th_MUR1_MUF1_PDF261043",
                                       "weight_var_th_MUR1_MUF1_PDF261044",
                                       "weight_var_th_MUR1_MUF1_PDF261045",
                                       "weight_var_th_MUR1_MUF1_PDF261046",
                                       "weight_var_th_MUR1_MUF1_PDF261047",
                                       "weight_var_th_MUR1_MUF1_PDF261048",
                                       "weight_var_th_MUR1_MUF1_PDF261049",
                                       "weight_var_th_MUR1_MUF1_PDF261050",
                                       "weight_var_th_MUR1_MUF1_PDF261051",
                                       "weight_var_th_MUR1_MUF1_PDF261052",
                                       "weight_var_th_MUR1_MUF1_PDF261053",
                                       "weight_var_th_MUR1_MUF1_PDF261054",
                                       "weight_var_th_MUR1_MUF1_PDF261055",
                                       "weight_var_th_MUR1_MUF1_PDF261056",
                                       "weight_var_th_MUR1_MUF1_PDF261057",
                                       "weight_var_th_MUR1_MUF1_PDF261058",
                                       "weight_var_th_MUR1_MUF1_PDF261059",
                                       "weight_var_th_MUR1_MUF1_PDF261060",
                                       "weight_var_th_MUR1_MUF1_PDF261061",
                                       "weight_var_th_MUR1_MUF1_PDF261062",
                                       "weight_var_th_MUR1_MUF1_PDF261063",
                                       "weight_var_th_MUR1_MUF1_PDF261064",
                                       "weight_var_th_MUR1_MUF1_PDF261065",
                                       "weight_var_th_MUR1_MUF1_PDF261066",
                                       "weight_var_th_MUR1_MUF1_PDF261067",
                                       "weight_var_th_MUR1_MUF1_PDF261068",
                                       "weight_var_th_MUR1_MUF1_PDF261069",
                                       "weight_var_th_MUR1_MUF1_PDF261070",
                                       "weight_var_th_MUR1_MUF1_PDF261071",
                                       "weight_var_th_MUR1_MUF1_PDF261072",
                                       "weight_var_th_MUR1_MUF1_PDF261073",
                                       "weight_var_th_MUR1_MUF1_PDF261074",
                                       "weight_var_th_MUR1_MUF1_PDF261075",
                                       "weight_var_th_MUR1_MUF1_PDF261076",
                                       "weight_var_th_MUR1_MUF1_PDF261077",
                                       "weight_var_th_MUR1_MUF1_PDF261078",
                                       "weight_var_th_MUR1_MUF1_PDF261079",
                                       "weight_var_th_MUR1_MUF1_PDF261080",
                                       "weight_var_th_MUR1_MUF1_PDF261081",
                                       "weight_var_th_MUR1_MUF1_PDF261082",
                                       "weight_var_th_MUR1_MUF1_PDF261083",
                                       "weight_var_th_MUR1_MUF1_PDF261084",
                                       "weight_var_th_MUR1_MUF1_PDF261085",
                                       "weight_var_th_MUR1_MUF1_PDF261086",
                                       "weight_var_th_MUR1_MUF1_PDF261087",
                                       "weight_var_th_MUR1_MUF1_PDF261088",
                                       "weight_var_th_MUR1_MUF1_PDF261089",
                                       "weight_var_th_MUR1_MUF1_PDF261090",
                                       "weight_var_th_MUR1_MUF1_PDF261091",
                                       "weight_var_th_MUR1_MUF1_PDF261092",
                                       "weight_var_th_MUR1_MUF1_PDF261093",
                                       "weight_var_th_MUR1_MUF1_PDF261094",
                                       "weight_var_th_MUR1_MUF1_PDF261095",
                                       "weight_var_th_MUR1_MUF1_PDF261096",
                                       "weight_var_th_MUR1_MUF1_PDF261097",
                                       "weight_var_th_MUR1_MUF1_PDF261098",
                                       "weight_var_th_MUR1_MUF1_PDF261099",
                                       "weight_var_th_MUR1_MUF1_PDF261100"};
using RNode = ROOT::RDF::RNode;

RNode GetInitNode(const TString &measurement, const TString &channel, bool if_truth = false)
{
    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path_fiducial + file).Data());

    std::string pre_selection = if_truth ? settings.at(measurement).truth_preselection.GetTitle()
                                         : settings.at(measurement).pre_selection.GetTitle();
    std::string selection     = if_truth ? settings.at(measurement).FR.GetTitle()
                                         : settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Filter(pre_selection).Filter(selection);
}

void PDFSystematics(const TString &measurement, const TString &channel, const std::string &leaf, int event_type)
{
    int n_bins = settings.at(measurement).get_binning(leaf).size();
    double *xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(leaf).at(i);

    TString leaf_to_draw("abs_" + leaf);
    TString truth_to_draw("abs_truth_" + leaf);

    bool if_sign = leaf.find("CP_ZZ") != std::string::npos;

    std::cout << channel << std::endl;
    auto reco = GetInitNode(measurement, channel).Define(leaf_to_draw.Data(), if_sign ? leaf : "std::abs(" + leaf + ")");
    std::cout << *reco.Count() << std::endl;
    auto truth = GetInitNode(measurement, channel, true).Define(truth_to_draw.Data(), if_sign ? "truth_" + leaf : "std::abs(truth_" + leaf + ")");
    std::cout << *truth.Count() << std::endl;
    TString corrections = "scale*ew_correction*tautau_correction";

    //auto c = new TCanvas("c", "", 800, 600);

    std::vector<TH1D*> hists_reco;
    std::vector<TH1D*> hists_truth;
    for(const auto &pdf_variation : pdf_variations)
    {
        TString final_reco_weight = "weight/weight_gen*" + pdf_variation + "*" + corrections;
        auto hist_reco = reco.Define("final_reco_weight", final_reco_weight.Data()).Histo1D({"nominal", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "final_reco_weight");
        hists_reco.push_back(dynamic_cast<TH1D*>(hist_reco->Clone()));;

        TString final_truth_weight = pdf_variation + "*" + corrections;
        auto hist_truth = truth.Define("final_truth_weight", final_truth_weight.Data()).Histo1D({"nominal", "", n_bins - 1, xbins}, truth_to_draw.Data(), "final_truth_weight");
        hists_truth.push_back(dynamic_cast<TH1D*>(hist_truth->Clone()));
    }

    std::vector<double> nominals;
    std::vector<std::vector<double>> N_recos;
    std::vector<std::vector<double>> N_truths;
    std::vector<std::vector<double>> C_factors;
    std::vector<std::vector<double>> variations;
    for(size_t i = 0; i < hists_reco.at(0)->GetNbinsX(); i++)
    {
        N_recos .push_back(std::vector<double>());
        N_truths.push_back(std::vector<double>());
        C_factors.push_back(std::vector<double>());
        variations.push_back(std::vector<double>());
        for(int j = 0; j < hists_reco.size(); j++)
        {
            double N_reco_j  = hists_reco .at(j)->GetBinContent(i + 1);
            double N_truth_j = hists_truth.at(j)->GetBinContent(i + 1);
            double C_factor_j = N_reco_j/N_truth_j;

            N_recos .back().push_back(N_reco_j);
            N_truths.back().push_back(N_truth_j);
            C_factors.back().push_back(C_factor_j);

            if(j == 0) nominals.push_back(C_factor_j);
            else       variations.back().push_back(C_factor_j);
        }
    }

    TString up_variations;
    TString down_variations;

    for(auto&& [N_reco, N_truth, C_factor, variation, nominal] :
        make_container_ref_tuple(N_recos, N_truths, C_factors, variations, nominals))
    {
        double variation_std(0.);
        for(const auto variation_j : variation) variation_std += (variation_j - nominal)*(variation_j - nominal);
        variation_std = std::sqrt(variation_std/pdf_variations.size());
        //std::cout << N_truth.at(0) << ",\t" << N_truth.at(0)*(1 + variation_std) << ",\t" << N_truth.at(0)*(1 - variation_std) << "\n"
        //          << N_reco. at(0) << ",\t" << N_reco .at(0)*(1 + variation_std) << ",\t" << N_reco .at(0)*(1 - variation_std) << "\n"
        //          << nominal << ",\t" << nominal*(1 + variation_std) << ",\t" << nominal*(1 - variation_std) << std::endl
        //          << variation_std << "\n";

        up_variations   += TString::Format("+%.3f, ", variation_std);
        down_variations += TString::Format("-%.3f, ", variation_std);
    }
    std::cout << "- [\"pdf__1up\",   " << up_variations   << "]" << std::endl
              << "- [\"pdf__1down\", " << down_variations << "]" << std::endl;

    std::cout << std::endl;
    //c->SaveAs("test.png");
}

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    std::vector<std::string> leaves = {"n_jets",
                                       "dphill",
                                       //"leading_jet_pt",
                                       "leading_pT_lepton",
                                       //"mjj",
                                       "mT_ZZ",
                                       "pt_zz",
                                       "Z_pT",
                                       "Z_rapidity",
                                       "CP_ZZ_1",
                                       "CP_ZZ_2",
                                      };

    for(const auto &leaf : leaves)
    {
        std::cout << leaf << std::endl;
        PDFSystematics("inclusive", "inclusive", leaf, 0);
//        PDFSystematics("ZZjj", "inclusive", leaf, 0);
        std::cout << std::endl;
    }
}
