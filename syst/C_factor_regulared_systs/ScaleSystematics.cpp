#include "../inc/Data.h"
#include "../inc/MC.h"
#include "../inc/Setting.h"
#include "../inc/Util.h"

#include "TString.h"
#include "TColor.h"
#include "TStyle.h"
#include "TH1D.h"
#include "ROOT/RDataFrame.hxx"
#include "TCanvas.h"

#include <vector>
#include <map>
#include <iostream>
#include <string>

std::vector<TString> scale_variations = {"weight_gen",
                                         "weight_var_th_MUR0p5_MUF0p5_PDF261000",
                                         "weight_var_th_MUR0p5_MUF1_PDF261000",
                                         "weight_var_th_MUR1_MUF0p5_PDF261000",
                                         "weight_var_th_MUR1_MUF2_PDF261000",
                                         "weight_var_th_MUR2_MUF1_PDF261000",
                                         "weight_var_th_MUR2_MUF2_PDF261000"};
std::map<TString, int> colors = {{"weight_gen",     kBlack},
                                 {"weight_var_th_MUR0p5_MUF0p5_PDF261000", kMagenta + 2},
                                 {"weight_var_th_MUR0p5_MUF1_PDF261000",   kBlue - 6},
                                 {"weight_var_th_MUR1_MUF0p5_PDF261000",   kGreen + 2},
                                 {"weight_var_th_MUR1_MUF2_PDF261000",     kOrange},
                                 {"weight_var_th_MUR2_MUF1_PDF261000",     kViolet - 4},
                                 {"weight_var_th_MUR2_MUF2_PDF261000",     kRed + 1}};

using RNode = ROOT::RDF::RNode;

RNode GetInitNode(const TString &measurement, const TString &channel, bool if_truth = false)
{
    MC mcs;
    std::vector<std::string> files;
    for(const auto &file : mcs.mcs.at(channel))
        files.push_back((mcs.mc_path_fiducial + file).Data());

    std::string pre_selection = if_truth ? settings.at(measurement).truth_preselection.GetTitle()
                                         : settings.at(measurement).pre_selection.GetTitle();
    std::string selection     = if_truth ? settings.at(measurement).FR.GetTitle()
                                         : settings.at(measurement).SR.GetTitle();

    ROOT::RDataFrame data("tree_PFLOW", files);
    return data.Filter(pre_selection).Filter(selection);
}

void ScaleSystematics(const TString &measurement, const TString &channel, const std::string &leaf, int event_type)
{
    int n_bins = settings.at(measurement).get_binning(leaf).size();
    double *xbins = new double[n_bins];
    for(int i = 0; i < n_bins; i++) xbins[i] = settings.at(measurement).get_binning(leaf).at(i);

    TString leaf_to_draw("abs_" + leaf);
    TString truth_to_draw("abs_truth_" + leaf);

    bool if_sign = leaf.find("CP_ZZ") != std::string::npos;

    std::cout << channel << std::endl;
    auto reco = GetInitNode(measurement, channel).Define(leaf_to_draw.Data(), if_sign ? leaf : "std::abs(" + leaf + ")");
    std::cout << *reco.Count() << std::endl;
    auto truth = GetInitNode(measurement, channel, true).Define(truth_to_draw.Data(), if_sign ? "truth_" + leaf : "std::abs(truth_" + leaf + ")");
    std::cout << *truth.Count() << std::endl;
    TString corrections = "scale*ew_correction*tautau_correction";

    //auto c = new TCanvas("c", "", 800, 600);

    std::vector<TH1D*> hists_reco;
    std::vector<TH1D*> hists_truth;
    for(const auto &scale_variation : scale_variations)
    {
        TString final_reco_weight = "weight/weight_gen*" + scale_variation + "*" + corrections;
        auto hist_reco = reco.Define("final_reco_weight", final_reco_weight.Data()).Histo1D({"nominal", "", n_bins - 1, xbins}, leaf_to_draw.Data(), "final_reco_weight");
        hists_reco.push_back(dynamic_cast<TH1D*>(hist_reco->Clone()));;

        TString final_truth_weight = scale_variation + "*" + corrections;
        auto hist_truth = truth.Define("final_truth_weight", final_truth_weight.Data()).Histo1D({"nominal", "", n_bins - 1, xbins}, truth_to_draw.Data(), "final_truth_weight");
        hists_truth.push_back(dynamic_cast<TH1D*>(hist_truth->Clone()));
    }

    std::vector<double> nominals;
    std::vector<std::vector<double>> N_recos;
    std::vector<std::vector<double>> N_truths;
    std::vector<std::vector<double>> C_factors;
    std::vector<std::vector<double>> variations;
    for(size_t i = 0; i < hists_reco.at(0)->GetNbinsX(); i++)
    {
        N_recos .push_back(std::vector<double>());
        N_truths.push_back(std::vector<double>());
        C_factors.push_back(std::vector<double>());
        variations.push_back(std::vector<double>());
	for(int j = 0; j < hists_reco.size(); j++)
        {
            double N_reco_j  = hists_reco .at(j)->GetBinContent(i + 1);
            double N_truth_j = hists_truth.at(j)->GetBinContent(i + 1);
            double C_factor_j = N_reco_j/N_truth_j;

            N_recos .back().push_back(N_reco_j);
            N_truths.back().push_back(N_truth_j);
            C_factors.back().push_back(C_factor_j);

            if(j == 0) nominals.push_back(C_factor_j);
            else       variations.back().push_back(C_factor_j);
        }
    }

    TString up_variations;
    TString down_variations;

    for(auto&& [N_reco, N_truth, C_factor, variation, nominal] :
	make_container_ref_tuple(N_recos, N_truths, C_factors, variations, nominals))
    {
        //for(const auto N_truth_j : N_truth) std::cout << TString::Format("%.3f", N_truth_j) << ",\t";
        //std::cout << std::endl;
        //for(const auto N_reco_j : N_reco) std::cout << TString::Format("%.3f", N_reco_j) << ",\t";
        //std::cout << std::endl;
        //for(const auto C_factor_j : C_factor) std::cout << TString::Format("%.3f", C_factor_j) << ",\t";
        //std::cout << std::endl;

        std::sort(variation.begin(), variation.end());
        //std::cout << "up,\t"   << variation.back() - nominal  << "\n"
        //          << "down,\t" << variation.front() - nominal << std::endl;

        up_variations   += TString::Format("+%.3f, ", variation.back() - nominal);
        down_variations += TString::Format("%.3f, ",  variation.front() - nominal);
    }
    std::cout << "- [\"scale__1up\",   " << up_variations   << "]" << std::endl
              << "- [\"scale__1down\", " << down_variations << "]" << std::endl;

    std::cout << std::endl;
    //c->SaveAs("test.png");
}

int main(int argc, char *argv[])
{
    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    TH1::SetDefaultSumw2(kTRUE);

    std::vector<std::string> leaves = {
                                       "n_jets",
                                       "dphill",
                                       //"leading_jet_pt",
                                       "leading_pT_lepton",
                                       //"mjj",
                                       "mT_ZZ",
                                       "pt_zz",
                                       "Z_pT",
                                       "Z_rapidity",
                                       "CP_ZZ_1",
                                       "CP_ZZ_2",
                                      };

    for(const auto &leaf : leaves)
    {
        std::cout << leaf << std::endl;
        ScaleSystematics("inclusive", "inclusive", leaf, 0);
//        ScaleSystematics("ZZjj", "inclusive", leaf, 0);
	std::cout << std::endl;
    }
}
