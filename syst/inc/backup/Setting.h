#ifndef SETTING_H
#define SETTING_H

#include "TString.h"
#include "TCut.h"

double x_ismc[]     = {-1, 0, 1, 2};
double x_n_jets[]   = {0, 1, 2, 3, 15};
//double x_mt_zz[]    = {200, 300, 350, 450, 550, 750, 3000};
double x_mt_zz[]    = {0, 350, 450, 550, 3000};
double x_z_pt[]     = {50, 110, 130, 150, 170, 200, 250, 350, 1500};
//double x_z_pt[]     = {0, 130, 250, 250, 1500};
double x_dphill[]   = {0., 0.3, 0.6, 1.0, 1.4, 1.8};
//double x_mjj[]      = {100., 300., 500., 1000., 8000.};
double x_mjj[]      = {0, 100, 200, 400, 3500};
double x_dlepr[]    = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0};
double x_met_tst[]  = {70., 75., 80., 85., 90., 95., 100., 105., 110.,115., 120., 125.};
double x_dmetzphi[] = {2.2, 2.3, 2.4, 2.5, 2.6, 2.70, 2.80, 2.9, 3.0, 3.1, 3.2};
double x_pt_zz[]    = {0, 30, 80, 150, 1500};
double x_leading_jet_pt[] = {0, 40, 60, 1500};
double x_leading_pT_lepton[] = {30, 90, 110, 130, 150, 200, 900};
double x_z_rapidity[] = {0, 0.5, 1, 1.5, 2., 2.5};

TString scale = "scale*weight*ew_correction*tautau_correction";
TCut pre_selection = "met_tst>70&&(event_type==0||event_type==1)&&event_3CR==0&&M2Lep>80&&M2Lep<100&&leading_pT_lepton>30&&subleading_pT_lepton>20";
TCut truth_preselection = "truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20&&"
                          "abs(truth_leading_eta_lepton)<2.5&&abs(truth_subleading_eta_lepton)<2.5&&"
                          "truth_M2Lep>76&&truth_M2Lep<106";
//TCut pre_selection = "met_tst>70&&event_3CR==0&&(event_type==1||event_type==0)&&M2Lep>80&&M2Lep<100&&leading_pT_lepton>30&&subleading_pT_lepton>20&&leading_jet_pt>30&&second_jet_pt>30&&abs(leading_jet_eta)<4.5&&abs(second_jet_eta)<4.5";
//inclusive
TCut var1_1 = "met_tst>110&&MetOHT>0.65";
TCut var1_2 = "met_tst<90"&&!var1_1;
TCut var2_1 = "dLepR<1.8&&dMetZPhi>2.2&&n_bjets==0";
TCut var2_2 = !var2_1;
//VBS
//TCut var1_1 = "met_tst>150&&MetOHT>0.4&&leading_jet_pt>30&&second_jet_pt>30&&mjj>100";
//TCut var1_2 = !var1_1;
//TCut var2_1 = "dLepR<1.8&&dMetZPhi>2.2&&n_bjets==0&&detajj>1";
//TCut var2_2 = !var2_1;
//TCut var1_1 = "met_tst>150&&MetOHT>0.65";
//TCut var1_2 = "met_tst<100"&&!var1_1;
//TCut var2_1 = "dLepR<1.8&&dMetZPhi>2.2&&n_bjets==0&&n_jets>=2";
//TCut var2_2 = !var2_1;

std::map<TString, TCut> regions = {{"A", var1_1&&var2_1},
                                   {"B", var1_2&&var2_1},
                                   {"C", var1_1&&var2_2},
                                   {"D", var1_2&&var2_2},
                                  };

TCut truth_var1_1 = "truth_met_tst>95&&truth_MetOHT_neu>0.65";
TCut truth_var2_1 = "truth_dLepR<1.8&&abs(truth_dMetZPhi)>2.7";
std::map<TString, TCut> truth_regions = {{"A", truth_var1_1&&truth_var2_1},
                                        };

std::map<TString, TString> title = {{"met_signif", "E^{missing}_{T} signif"},
                                    {"met_tst",    "E^{missing}_{T} [GeV]"},
                                    {"dMetZPhi",   "#Delta#Phi(E^{missing}_{T},Z)"},
                                    {"dLepR",      "#DeltaR(l,l)"},
                                    {"Z_pT",       "p_{T}^{Z} [GeV]"},
                                    {"n_jets",     "No. of jets"},
                                    {"dphill",     "#Delta#Phi(l,l)"},
                                    {"mT_ZZ",      "m_{T}^(Z,Z) [GeV]"},
                                    {"mjj",        "m_{jj} [GeV]"},
                                    {"leading_jet_pt", "p_{T}^{leading jet} [GeV]"},
                                    {"leading_pT_lepton", "p_{T}^{leading lepton} [GeV]"},
                                    {"pt_zz",      "p_{T}^{Z,Z} [GeV]"},
                                    {"Z_rapidity", "y^{Z}"}
                                   };

#endif
